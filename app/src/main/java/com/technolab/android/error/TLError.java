package com.technolab.android.error;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.technolab.android.ui.entity.BaseResponse;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class TLError {
    private static String TAG = "TLError";
    private String errorCode;
    private String errorMessage;

    public TLError()
    {
        errorCode = "";
        errorMessage = "";
    }

    public void setErrorCode(String errorCode)
    {
        this.errorCode = errorCode;
    }
    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode()
    {
        return errorCode;
    }
    public String getErrorMessage()
    {
        return errorMessage;
    }

    public static TLError checkForError(Throwable throwable)
    {
        TLError error = null;
        if (throwable instanceof HttpException) {
            if (((HttpException) throwable).response() != null) {
                if (((HttpException) throwable).response().errorBody() != null) {
                    error = new TLError();
                    ResponseBody body = ((HttpException) throwable).response().errorBody();
                    Gson gson = new Gson();
                    TypeAdapter<BaseResponse> adapter = gson.getAdapter(BaseResponse.class);
                    try {
                        BaseResponse errorParser = adapter.fromJson(body.string());
                        error.setErrorCode(String.valueOf(((HttpException) throwable).code()));
                        error.setErrorMessage(errorParser.getMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return error;
    }

}
