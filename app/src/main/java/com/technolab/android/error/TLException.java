package com.technolab.android.error;

public class TLException extends Exception
{
    private String message;
    public TLException()
    {
        super();
    }
    public TLException(String message)
    {
        super(message);
        this.message = message;
    }
    public String getMessage()
    {
        return message;
    }
}
