package com.technolab.android.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class TLStorage {

    private static final TLStorage sInstance = new TLStorage();
    private static final String PREF_LOGGED_IN = "logged_in";
    private static final String PREF_FIRST_TIME = "first_time";
    private static final String PREF_AUTH_TOKEN = "auth_token";
    //My Code for Others
    private static final String PREF_MEMBER_REFERRAL_CODE = "referral_code";
    //Others Code for Me
    private static final String PREF_MEMBER_REFERRER_CODE = "referrer_code";
    private static final String PREF_MEMBER_STATE = "member_state";
    private static final String PREF_USER_ID = "user_id";
    private static final String PREF_USER_NAME = "user_name";
    private static final String PREF_USER_EMAIL = "user_email";
    private static final String PREF_USER_PHONE = "user_phone";
    private static final String PREF_USER_IMAGE = "user_image";
    private static final String PREF_USER_EXPERTISE = "user_expertise";
    private static final String PREF_MERCHANT_FRESH = "merchant_fresh";
    private static final String PREF_TOTAL_LIVE_TIME = "merchant_total_live_timer";

    private static final String PREF_MERCHANT_ID = "merchant_id";
    private static final String PREF_MERCHANT_COMPANY_NAME = "merchant_company_name";
    private static final String PREF_MERCHANT_IMAGE = "merchant_image";

    private static final String PREF_MERCHANT_REG_STEP_1 = "merchant_registration_step_1";
    private static final String PREF_MERCHANT_REG_STEP_2 = "merchant_registration_step_2";
    private static final String PREF_MERCHANT_REG_STEP_3 = "merchant_registration_step_3";
    private static final String PREF_MERCHANT_REG_STEP_4 = "merchant_registration_step_4";

    private SharedPreferences mSharedPreferences;

    public static synchronized TLStorage getInstance() {
        return TLStorage.sInstance;
    }

    public static synchronized void resetInstance(){
        TLStorage.sInstance.setLoggedIn(false);
        TLStorage.sInstance.setCurrentUserId(null);
        TLStorage.sInstance.setMerchantId(null);
        TLStorage.sInstance.setFirstTimeUser(false);
        TLStorage.sInstance.setCurrentUserName(null);
        TLStorage.sInstance.setUserEmail(null);
        TLStorage.sInstance.setUserPhone(null);
        TLStorage.sInstance.setUserImage(null);
        TLStorage.sInstance.setMerchantCompanyName(null);
        TLStorage.sInstance.setMerchantImage(null);
        TLStorage.sInstance.setFirstStepComplete(false);
        TLStorage.sInstance.setSecondStepComplete(false);
        TLStorage.sInstance.setThirdStepComplete(false);
        TLStorage.sInstance.setFourthStepComplete(false);
        TLStorage.sInstance.setReferralCode(null);
        TLStorage.sInstance.setReferrerCode(null);
        TLStorage.sInstance.setMerchantFresh(true);
        TLStorage.sInstance.setTotalLiveTime(0);
    }

    public TLStorage() {
        super();
    }

    public void initSharedData(final Context context) {
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void setTotalLiveTime(final long time){
        setLong(PREF_TOTAL_LIVE_TIME, time);
    }

    public long getTotalLiveTime(){
        return getLong(PREF_TOTAL_LIVE_TIME);
    }

    public void setLoggedIn(final boolean flag) {
        setBoolean(PREF_LOGGED_IN, flag);
    }

    public boolean getIsLoggedIn() {
        return getBoolean(PREF_LOGGED_IN);
    }

    public void setMerchantFresh(final boolean flag) {
        setBoolean(PREF_MERCHANT_FRESH, flag);
    }

    public boolean isMerchantFresh() {
        return getBoolean(PREF_MERCHANT_FRESH);
    }

    public void setFirstTimeUser(final Boolean flag) {
        setBoolean(PREF_FIRST_TIME, flag);
    }

    public Boolean isFirstTimeUser() {
        return getBoolean(PREF_FIRST_TIME);
    }

    public void setAuthToken(final String token) {
        setString(PREF_AUTH_TOKEN, token);
    }

    public String getAuthToken() {
        return getString(PREF_AUTH_TOKEN);
    }

    public void setReferralCode(final String code) {
        setString(PREF_MEMBER_REFERRAL_CODE, code);
    }

    public String getReferralCode() {
        return getString(PREF_MEMBER_REFERRAL_CODE);
    }

    public void setReferrerCode(final String code) {
        setString(PREF_MEMBER_REFERRER_CODE, code);
    }

    public String getReferrerCode() {
        return getString(PREF_MEMBER_REFERRER_CODE);
    }

    public void setCurrentUserId(final String userId) {
        setString(PREF_USER_ID, userId);
    }

    public String getCurrentUserId() {
        return getString(PREF_USER_ID);
    }

    public void setCurrentUserName(final String userName) { setString(PREF_USER_NAME, userName); }

    public String getUserEmail() { return getString(PREF_USER_EMAIL); }

    public void setUserEmail(final String userEmail){setString(PREF_USER_EMAIL,userEmail);}

    public String getUserPhone() { return getString(PREF_USER_PHONE); }

    public void setUserPhone(final String userPhone){setString(PREF_USER_PHONE, userPhone);}

    public String getUserExpertise() { return getString(PREF_USER_EXPERTISE); }

    public void setUserExpertise(final String expertise){setString(PREF_USER_EXPERTISE, expertise);}

    public String getPrefUserImage() { return getString(PREF_USER_IMAGE); }

    public void setUserImage(final String userImage){setString(PREF_USER_IMAGE, userImage);}

    public String getCurrentUserName() {
        return getString(PREF_USER_NAME);
    }

    public String getMerchantId() { return getString(PREF_MERCHANT_ID); }

    public void setMerchantId(final String merchantId) {setString(PREF_MERCHANT_ID, merchantId);}

    public String getMerchantCompanyName() { return getString(PREF_MERCHANT_COMPANY_NAME); }

    public void setMerchantCompanyName(final String merchantCompanyName) { setString(PREF_MERCHANT_COMPANY_NAME, merchantCompanyName);}

    public String getPrefMerchantImage() { return getString(PREF_MERCHANT_IMAGE); }

    public void setMerchantImage(final String merchantImage){setString(PREF_MERCHANT_IMAGE, merchantImage);}

    public void setFirstStepComplete(final Boolean flag) { setBoolean(PREF_MERCHANT_REG_STEP_1, flag);}

    public Boolean isFirstStepComplete() {
        return getBoolean(PREF_MERCHANT_REG_STEP_1);
    }

    public void setSecondStepComplete(final Boolean flag) { setBoolean(PREF_MERCHANT_REG_STEP_2, flag);}

    public Boolean isSecondStepComplete() {
        return getBoolean(PREF_MERCHANT_REG_STEP_2);
    }

    public void setThirdStepComplete(final Boolean flag) { setBoolean(PREF_MERCHANT_REG_STEP_3, flag);}

    public Boolean isThirdStepComplete() {
        return getBoolean(PREF_MERCHANT_REG_STEP_3);
    }

    public void setFourthStepComplete(final Boolean flag) { setBoolean(PREF_MERCHANT_REG_STEP_4, flag);}

    public Boolean isFourthStepComplete() {
        return getBoolean(PREF_MERCHANT_REG_STEP_4);
    }


    /********************************************
     * *************** CONVENIENCE **************
     * ******************************************
     */

    private void setBoolean(final String key, final boolean value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putBoolean(key, value).apply();
        }
    }

    private boolean getBoolean(final String key) {
        return mSharedPreferences != null && mSharedPreferences.getBoolean(key, false);
    }

    private void setInt(final String key, final int value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putInt(key, value).apply();
        }
    }

    private int getInt(final String key) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getInt(key, 0);
        } else {
            return 0;
        }
    }

    private void setLong(final String key, final long value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putLong(key, value).apply();
        }
    }

    private long getLong(final String key) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getLong(key, 0);
        } else {
            return 0;
        }
    }

    private void setString(final String key, final String value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putString(key, value).apply();
        }
    }

    private String getString(final String key) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getString(key, null);
        } else {
            return null;
        }
    }

    private void setFloat(final String key, final float value) {
        if (mSharedPreferences != null) {
            mSharedPreferences.edit().putFloat(key, value).apply();
        }
    }

    private float getFloat(final String key) {
        if (mSharedPreferences != null) {
            return mSharedPreferences.getFloat(key, 0);
        } else {
            return 0;
        }
    }
}
