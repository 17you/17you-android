package com.technolab.android.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;

import com.technolab.android.db.DBController;
import com.technolab.android.security.KeyOne;
import com.technolab.android.security.SecOne;
import com.technolab.android.utility.TLConstant;
import com.technolab.android.utility.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TLEnvironment {
    private static final String TAG = "Environment";
    private static TLEnvironment ourInstance;

    //system_profile
    private double userDBVersion;
    private double userDBLatestVersion;
    private double cacheDBVersion;
    private double cacheDBLatestVersion;
    private String appName;
    private UserSettings userSettings;

    //Others
    private Map<String, Map<String, String>> cacheTableVersion;
    private JSONObject cacheTableLatestVersion;

    public static synchronized TLEnvironment getInstance() {
        if (ourInstance == null)
            ourInstance = new TLEnvironment();

        return ourInstance;
    }

    public static void resetInstance() {
        ourInstance = null;
    }

    private TLEnvironment() {
        init();
    }

    private void init() {
        cacheTableLatestVersion = null;
        cacheTableVersion = new HashMap<String, Map<String, String>>();

        String sql = "SELECT key, value FROM system_profile";
        Cursor cursor = DBController.getInstance().executeQuery(sql);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            Integer valueCol = cursor.getColumnIndex("value");
            do {
                String temp = cursor.getString(cursor.getColumnIndex("key"));
                if (temp.equalsIgnoreCase("app_name"))
                    appName = cursor.getString(valueCol);
                else if (temp.equalsIgnoreCase("db_version")) {
                    try {
                        userDBLatestVersion = Double.parseDouble(cursor.getString(valueCol));
                    } catch (NumberFormatException e) {
                        if (TLConstant.DEBUG_OTHERS)
                            Log.e(TAG, "Exception while getting latest DB version - " + e.getMessage());

                        userDBLatestVersion = 1.00;
                    }
                }
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();

        //Read DB Version for embedded cache DB
        cursor = DBController.getInstance().getDb_caching().rawQuery(sql, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            Integer valueCol = cursor.getColumnIndex("value");
            do {
                String temp = cursor.getString(cursor.getColumnIndex("key"));
                if (temp.equalsIgnoreCase("db_version"))
                    try {
                        cacheDBLatestVersion = Double.parseDouble(cursor.getString(valueCol));
                    } catch (NumberFormatException e) {
                        if (TLConstant.DEBUG_OTHERS)
                            Log.e(TAG, "Exception while getting latest cached DB version - " + e.getMessage());

                        cacheDBLatestVersion = 1.00;
                    }
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
    }

    /**
     * Read version of user DB, shared table cache DB, and cached tables. Make sure you call this method after point DBController.db to user DB
     */

    public double getUserDBVersion() {
        return userDBVersion;
    }

    public double getUserDBLatestVersion() {
        return userDBLatestVersion;
    }

    public double getCacheDBVersion() {
        return cacheDBVersion;
    }

    public double getCacheDBLatestVersion() {
        return cacheDBLatestVersion;
    }

    public void readDBVersion() {
        String sql = "SELECT value FROM system_profile WHERE key=?";

        Cursor cursor = DBController.getInstance().executeQuery(sql, new String[]{"db_version"});
        if (cursor != null && cursor.getCount() == 1) {
            cursor.moveToFirst();
            try {
                userDBVersion = Double.parseDouble(cursor.getString(cursor.getColumnIndex("value")));
            } catch (NumberFormatException e) {
                if (TLConstant.DEBUG_OTHERS)
                    Log.e(TAG, "Exception while getting user DB version");

                userDBVersion = 1.0;
            }
        }
        if (cursor != null)
            cursor.close();

        cursor = DBController.getInstance().getDb_caching().rawQuery(sql, new String[]{"db_version"});
        if (cursor != null && cursor.getCount() == 1) {
            cursor.moveToFirst();
            try {
                cacheDBVersion = Double.parseDouble(cursor.getString(cursor.getColumnIndex("value")));
            } catch (NumberFormatException e) {
                if (TLConstant.DEBUG_OTHERS)
                    Log.e(TAG, "Exception while getting cache DB version");

                cacheDBVersion = 1.0;
            }
        }
        if (cursor != null)
            cursor.close();

        cursor = DBController.getInstance().getDb_caching().rawQuery("SELECT * FROM table_cache_info", null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                Map<String, String> temp = new HashMap<String, String>();
                temp.put("version", cursor.getString(cursor.getColumnIndex("table_version")));
                temp.put("table_name", cursor.getString(cursor.getColumnIndex("server_table_name")));

                cacheTableVersion.put(cursor.getString(cursor.getColumnIndex("table_name")), temp);
            } while (cursor.moveToNext());
        }
        if (cursor != null)
            cursor.close();
    }

    public boolean isLoggedIn(){
        String sql = "SELECT key, value FROM system_profile";
        boolean isLoggedIn = false;
        Cursor cursor = DBController.getInstance().executeQuery(sql);
        if (cursor != null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            int valueCol = cursor.getColumnIndex("value");
            do {
                String temp = cursor.getString(cursor.getColumnIndex("key"));
                if(temp.equalsIgnoreCase("pref_logged_in")){
                    isLoggedIn = cursor.getInt(valueCol) == 1;
                    break;
                }
            } while (cursor.moveToNext());
        }

        if (cursor != null && !cursor.isClosed())
            cursor.close();

        return isLoggedIn;
    }

    public void updateLoggedIn(int result){
        String sql = "UPDATE system_profile SET value = ? WHERE key = 'pref_logged_in'";
        String value = String.valueOf(result);
        DBController.getInstance().executeUpdate(sql,new String[]{value});
    }

    private void updateSystemProfile(String value, String key) {
        String sql = "UPDATE system_profile SET value=? WHERE key=?";
        DBController.getInstance().executeUpdate(sql, new String[]{value, key});
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    public JSONObject getCacheTableLatestVersion()
    {
        return cacheTableLatestVersion;
    }

    public void setCacheTableLatestVersion(JSONObject cacheTableLatestVersion)
    {
        this.cacheTableLatestVersion = cacheTableLatestVersion;
    }

    public Map<String, Map<String, String>> getCacheTableVersion()
    {
        return cacheTableVersion;
    }

}
