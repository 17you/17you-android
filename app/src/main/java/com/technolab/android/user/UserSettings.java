package com.technolab.android.user;

import java.util.ArrayList;
import java.util.HashMap;

public class UserSettings {
    private String customerName;
    private String peopleID;
    private String userID;
    private String role;

    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public void setPeopleID(String peopleID)
    {
        this.peopleID = peopleID;
    }

    public void setUserID(String userID)
    {
        this.userID = userID;
    }

    public void setRole(String role)
    {
        this.role = role;
    }

    public String getRole() { return this.role; }

    public String getCustomerName() {
        return customerName;
    }

    public String getPeopleID() {
        return peopleID;
    }

    public String getUserID() {
        return userID;
    }

    public UserSettings()
    {
        role = "ML";
    }
}
