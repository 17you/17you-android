package com.technolab.android.security;

public class SecOne {
    private static SecOne ourInstance;

    public static synchronized SecOne getInstance() {
        if (ourInstance == null)
            ourInstance = new SecOne();
        return ourInstance;
    }

    private String secMot_Com;
    private String secMot_Usr;
    private String secMot_Nam;

    private String keyMot_Com;
    private String keyMot_Usr;
    private String keyMot_Nam;

    private String secone_token;

    private String touchIDKey_1;
    private String touchIDSec_1;

    private String touchIDStoreKey_1;
    private String touchIDStoreSec_1;

    private SecOne() {
        secMot_Com = "JcDPDL7E4EW!24ke";
        secMot_Usr = "9wkAUJpWLN1o4XDV";
        secMot_Nam = "BbfcSZiEtZR0AWbO";

        keyMot_Com = "DCCigVOQNC8J!azz";
        keyMot_Usr = "URpdNg9WGOMqOLMX";
        keyMot_Nam = "nSOeUEwZdEbj40kP";

        secone_token = "NjM3MzI3MjAz";

        touchIDKey_1 = "Nn0UcH5cfav2cIfr";
        touchIDSec_1 = "SAm1Qx6JYBGpsjz8";

        touchIDStoreKey_1 = "BZiRjrjjxOXvHl1T";
        touchIDStoreSec_1 = "TCbvRsANdd2c40WH";
    }

    public String getComSec() {
        return secMot_Com;
    }

    public String getUsrSec() {
        return secMot_Usr;
    }

    public String getNamSec() {
        return secMot_Nam;
    }

    public String getComKey() {
        return keyMot_Com;
    }

    public String getUsrKey() {
        return keyMot_Usr;
    }

    public String getNamKey() {
        return keyMot_Nam;
    }

    public String getSecone_token() {
        return secone_token;
    }

    public String getTouchIDKey() {
        return touchIDKey_1;
    }

    public String getTouchIDSec() {
        return touchIDSec_1;
    }

    public String getTouchIDStoreKey() {
        return touchIDStoreKey_1;
    }

    public String getTouchIDStoreSec() {
        return touchIDStoreSec_1;
    }
}