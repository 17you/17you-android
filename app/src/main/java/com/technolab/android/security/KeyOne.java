package com.technolab.android.security;

public class KeyOne {

    private static KeyOne ourInstance;

    public static synchronized KeyOne getInstance() {
        if (ourInstance == null)
            ourInstance = new KeyOne();
        return ourInstance;
    }

    private String secHai_Com;
    private String secHai_Usr;
    private String secHai_Nam;

    private String keyHai_Com;
    private String keyHai_Usr;
    private String keyHai_Nam;

    private String keyone_token;

    private String touchIDKey_2;
    private String touchIDSec_2;
    private String touchIDStoreKey_2;
    private String touchIDStoreSec_2;

    private KeyOne() {
        secHai_Com = "8yrYh6PovX7ecmsy";
        secHai_Usr = "1uGqP15liVgzc!QW";
        secHai_Nam = "*gwXS8*tJWWrtuOg";

        keyHai_Com = "i!p8K*HvpsFYB7pd";
        keyHai_Usr = "!8DpQe7WhkymmW*t";
        keyHai_Nam = "4WmM$y$k9l4LdQaG";

        keyone_token = "Mzg0NTYxODgz";

        touchIDKey_2 = "AA9TdFc2A2ZTZ7kd";
        touchIDSec_2 = "luvCbSwQV5DpP9Qh";

        touchIDStoreKey_2 = "yjRZhDaMGVU96Wxu";
        touchIDStoreSec_2 = "tIgvSMhZw0RT9T0o";
    }

    public String getComSec() {
        return secHai_Com;
    }

    public String getUsrSec() {
        return secHai_Usr;
    }

    public String getNamSec() {
        return secHai_Nam;
    }

    public String getComKey() {
        return keyHai_Com;
    }

    public String getUsrKey() {
        return keyHai_Usr;
    }

    public String getNamKey() {
        return keyHai_Nam;
    }

    public String getKeyone_token() {
        return keyone_token;
    }

    public String getTouchIDKey() {
        return touchIDKey_2;
    }

    public String getTouchIDSec() {
        return touchIDSec_2;
    }

    public String getTouchIDStoreKey() {
        return touchIDStoreKey_2;
    }

    public String getTouchIDStoreSec() {
        return touchIDStoreSec_2;
    }
}
