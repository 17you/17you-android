package com.technolab.android.di.module;

import androidx.annotation.NonNull;

import com.technolab.android.ui.entity.BaseResponse;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

    @POST("member/login")
    Observable<BaseResponse> login(@Body RequestBody requestBody);

    @POST("member/login/mobile")
    Observable<BaseResponse> loginWithPhone(@Body RequestBody requestBody);

    @POST("member/login/mobile/check")
    Observable<BaseResponse> checkPhoneNumber(@Body RequestBody requestBody);

    @POST("member/register/check")
    Observable<BaseResponse> checkEmailAndPhone(@Body RequestBody requestBody);

    @POST("member/update/password")
    Observable<BaseResponse> updatePassword(@Body RequestBody requestBody);

    @POST("member/register")
    Observable<BaseResponse> register(@Body RequestBody requestBody);

    @GET("industry/get")
    Observable<BaseResponse> retrieveIndustries();

    @GET("merchant/register/status/{id}")
    Observable<BaseResponse> retrieveApplicationStatus(@Path("id") String id);

    @POST("merchant/register/ic/{id}")
    @Multipart
    Observable<BaseResponse> identificationUploader(@Path("id") String id, @NonNull @Part("industry_category_id") RequestBody category_id, @Part MultipartBody.Part front, @Part MultipartBody.Part back);

    @POST("merchant/register/selfie/{id}")
    @Multipart
    Observable<BaseResponse> selfieUploader(@Path("id") String id, @Part MultipartBody.Part selfie);

    @POST("merchant/register/bank/{id}")
    Observable<BaseResponse> infoUploader(@Path("id") String id, @Body RequestBody requestBody);

    @POST("merchant/register/subcription/{id}")
    @Multipart
    Observable<BaseResponse> uploadBankSlip(@Path("id") String memberID, @Part MultipartBody.Part bankSlip);

    @POST("merchant/register/policy/{id}")
    Observable<BaseResponse> submitPrivacyPolicy(@Path("id") String id);

    @GET("live/all/{id}")
    Observable<BaseResponse> retrieveMerchantLiveStream(@Path("id") String merchantID);

    @GET("merchant/{id}")
    Observable<BaseResponse> retrieveMerchantDetail(@Path("id") String userID);

    @GET("product/all/{id}")
    Observable<BaseResponse> retrieveMerchantProduct(@Path("id") String merchantID);

    @POST("live/add/{id}")
    @Multipart
    Observable<BaseResponse> createLiveStream(@Path("id") String merchantID,
                                              @NonNull @Part("title") RequestBody title,
                                              @NonNull @Part("description") RequestBody description,
                                              @NonNull @Part("date") RequestBody date,
                                              @NonNull @Part("start_time") RequestBody start_time,
                                              @NonNull @Part("end_time") RequestBody end_time,
                                              @NonNull @Part("product") RequestBody product,
                                              @NonNull @Part("status") RequestBody status,
                                              @NonNull @Part("shipping_fee") RequestBody shippingFee,
                                              @Part MultipartBody.Part liveStreamImage);

    @GET("live/active")
    Observable<BaseResponse> retrieveActiveLiveStream();

    @POST("live/delete/{id}")
    Observable<BaseResponse> deleteLiveStream(@Path("id")String liveStreamID);

    @POST("live/update/status/{id}")
    Observable<BaseResponse> publishLiveStream(@Path("id") String liveStreamID,
                                               @Body RequestBody requestBody);

    @POST("live/update/{liveStreamID}/{merchantID}")
    @Multipart
    Observable<BaseResponse> updateLiveStream(@Path("liveStreamID") String liveStreamID,
                                              @Path("merchantID") String merchantID,
                                              @NonNull @Part("title") RequestBody title,
                                              @NonNull @Part("description") RequestBody description,
                                              @NonNull @Part("date") RequestBody date,
                                              @NonNull @Part("start_time") RequestBody start_time,
                                              @NonNull @Part("end_time") RequestBody end_time,
                                              @NonNull @Part("product") RequestBody product,
                                              @NonNull @Part("status") RequestBody status,
                                              @NonNull @Part("shipping_fee") RequestBody shippingFee,
                                              @Part MultipartBody.Part liveStreamImage);

    @GET("live/one/{id}")
    Observable<BaseResponse> retrieveSingleLiveStream(@Path("id") String liveStreamID);

    @POST("product/add/{id}")
    @Multipart
    Observable<BaseResponse> createProduct(@Path("id") String merchantID,
                                              @NonNull @Part("name") RequestBody name,
                                              @NonNull @Part("description") RequestBody description,
                                              @NonNull @Part("variation") RequestBody variation,
                                              @NonNull @Part("category_id") RequestBody categoryId,
                                              @NonNull @Part("subcategory_id") RequestBody subCategoryId,
                                              @Part MultipartBody.Part productImage);

    @POST("product/update/{productID}/{id}")
    @Multipart
    Observable<BaseResponse> updateProduct(@Path("productID") String productID,
                                           @Path("id") String merchantID,
                                           @Part("name") RequestBody name,
                                           @Part("description") RequestBody description,
                                           @Part("variation") RequestBody variation,
                                           @Part("category_id") RequestBody categoryId,
                                           @Part("subcategory_id") RequestBody subCategoryId,
                                           @Part MultipartBody.Part productImage);

    @GET("product/one/{id}")
    Observable<BaseResponse> retrieveSingleProduct(@Path("id") String productID);


    @GET("product/categories")
    Observable<BaseResponse> retrieveProductCategories();

    @POST("member/edit/{id}")
    @Multipart
    Observable<BaseResponse> updateProfile(@Path("id") String userID,
                                           @Part("username") RequestBody username,
                                           @Part("expertise") RequestBody expertise,
                                           @Part MultipartBody.Part userProfileImage);

    @POST("live/start/{id}")
    Observable<BaseResponse> startLiveStream(@Path("id") String liveStreamID, @Body RequestBody requestBody);

    @POST("live/stop/{id}")
    Observable<BaseResponse> stopLiveStream(@Path("id") String liveStreamID, @Body RequestBody requestBody);

    @GET("live/categories/all/{id}")
    Observable<BaseResponse> retrieveAllLiveStream(@Path("id") String memberID);

    @POST("live/detailed/{id}")
    Observable<BaseResponse> retrieveLiveStreamDetails(@Path("id") String liveStreamID, @Body RequestBody requestBody);

    @POST("live/products/{id}")
    Observable<BaseResponse> retrieveLiveStreamProduct(@Path("id")String liveStreamID, @Body RequestBody requestBody);

    @POST("live/products/update/{id}")
    Observable<BaseResponse> updateLiveStreamProduct(@Path("id")String liveStreamID, @Body RequestBody requestBody);

    @POST("member/address/{id}")
    Observable<BaseResponse> retrieveAddress(@Path("id")String memberID, @Body RequestBody requestBody);

    @POST("member/address/{id}")
    Observable<BaseResponse> addAddress(@Path("id")String memberID, @Body RequestBody requestBody);

    @GET("referral/get/results/total/{id}")
    Observable<BaseResponse> getTotalReferral(@Path("id")String memberID);

    @GET("referral/get/results/today/{id}")
    Observable<BaseResponse> getTodayReferral(@Path("id")String memberID);

    @GET("referral/get/results/month/{id}")
    Observable<BaseResponse> getMonthReferral(@Path("id")String memberID);

    @GET("referral/get/results/week/{id}")
    Observable<BaseResponse> getWeekReferral(@Path("id")String memberID);

    @GET("referral/get/emails/total/{id}")
    Observable<BaseResponse> getEmailTotalReferral(@Path("id")String memberID);

    @GET("referral/get/emails/today/{id}")
    Observable<BaseResponse> getEmailTodayReferral(@Path("id")String memberID);

    @GET("referral/get/emails/month/{id}")
    Observable<BaseResponse> getEmailMonthReferral(@Path("id")String memberID);

    @GET("referral/get/emails/week/{id}")
    Observable<BaseResponse> getEmailWeekReferral(@Path("id")String memberID);

    @GET("states/all")
    Observable<BaseResponse> getStates();

    @POST("merchant/profile/edit/{id}")
    @Multipart
    Observable<BaseResponse> updateMerchantProfile(@Path("id")String merchantID,
                                                   @Part("companyname") RequestBody companyName,
                                                   @Part("bankname") RequestBody bankName,
                                                   @Part("bankaccno") RequestBody bankAccountNumber,
                                                   @Part("bankaccholdername") RequestBody bankAccountHolder,
                                                   @Part("state") RequestBody state,
                                                   @Part("email") RequestBody email,
                                                   @Part("streetaddress") RequestBody streetAddress,
                                                   @Part("city") RequestBody city,
                                                   @Part("postcode") RequestBody postCode,
                                                   @Part MultipartBody.Part profileImageFilePart);

    @GET("merchant/profile/view/{id}")
    Observable<BaseResponse> retrieveMerchantProfile(@Path("id")String merchantId);

    @POST("product/delete/{id}")
    Observable<BaseResponse> deleteProduct(@Path("id") String productID);

    @GET("live/upcoming/one/{id}")
    Observable<BaseResponse> fetchUpcomingLive(@Path("id") String merchantID);

    @GET("transaction/member/get/{id}")
    Observable<BaseResponse> fetchTransaction(@Path("id") String memberID, @Query("status") int status);

    @GET("transaction/merchant/get/{id}")
    Observable<BaseResponse> fetchMerchantTransaction(@Path("id") String merchantID, @Query("status") int status);

    @POST("transaction/merchant/update/{id}")
    Observable<BaseResponse> updateTransactionStatus(@Path("id") String transactionID,
                                                     @Query("tracking_url") String trackingURL,
                                                     @Query("status") int status,
                                                     @Query("courier_company") String courier);

    @POST("transaction/reject/{id}")
    Observable<BaseResponse> rejectTransaction(@Path("id") String transactionID, @Body RequestBody body);

    @POST("transaction/approve/{id}")
    Observable<BaseResponse> approveTransaction(@Path("id") String transactionID, @Body RequestBody body);

    @GET("live/top/{id}")
    Observable<BaseResponse> fetchTopLiveStream(@Path("id") String merchantID);

    @GET("transaction/merchant/history/{id}")
    Observable<BaseResponse> fetchTransactionHistory(@Path("id") String merchantID);

    @POST("transaction/add/receipt")
    @Multipart
    Observable<BaseResponse> performTransaction(@Part("live_stream_id") RequestBody liveStreamID,
                                                @Part("merchant_id") RequestBody merchantID,
                                                @Part("address_id") RequestBody addressID,
                                                @Part("member_id") RequestBody memberID,
                                                @Part("payment_method") RequestBody paymentMethod,
                                                @Part("product") RequestBody product,
                                                @Part("total_product_price") RequestBody totalProductPrice,
                                                @Part("delivery_fees") RequestBody deliveryFees,
                                                @Part("final_price") RequestBody finalPrice,
                                                @Part MultipartBody.Part fileTransactionReceipt);

    @POST("transaction/add")
    Observable<BaseResponse> performTransactionWithoutImage(@Body RequestBody body);

    @POST("transaction/update/receipt/{id}")
    @Multipart
    Observable<BaseResponse> updateTransaction(@Path("id") String transactionID,
                                               @Part("member_id") RequestBody memberID,
                                               @Part("merchant_id") RequestBody merchantID,
                                               @Part("payment_method") RequestBody paymentMethod,
                                               @Part MultipartBody.Part fileTransactionReceipt);

    @GET("energy/summary/{id}")
    Observable<BaseResponse> retrieveEnergySummary(@Path("id") String memberID);

    @GET("energy/level/summary/{id}")
    Observable<BaseResponse> retrieveEnergyLevel(@Path("id") String memberID);

    @GET("energy/level/detailed/{id}")
    Observable<BaseResponse> retrieveEnergyHistory(@Path("id") String memberID);

    @POST("energy/redeem/{type}/{id}")
    Observable<BaseResponse> redeemEnergy(@Path("type") String type, @Path("id") String memberID);

    @GET("ranking/get")
    Observable<BaseResponse> retrieveRanking();

    @POST("referral/add/share/{id}")
    Observable<BaseResponse> addShare(@Path("id") String memberID);

    @GET("referral/get/counts/{id}")
    Observable<BaseResponse> fetchReferralEnergyCount(@Path("id") String memberID);

    @GET("energy/merchant/summary/{id}")
    Observable<BaseResponse> fetchMerchantEnergyCount(@Path("id") String merchantID);

    @POST("member/fav/{id}")
    Observable<BaseResponse> updateFavourites(@Path("id") String memberID, @Body RequestBody body);

    @GET("reward/check/{id}")
    Observable<BaseResponse> retrieveRewardInfo(@Path("id") String memberID);

    @POST("reward/claim/{id}")
    Observable<BaseResponse> claimReward(@Path("id") String memberID, @Body RequestBody body);

}
