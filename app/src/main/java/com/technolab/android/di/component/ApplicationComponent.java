package com.technolab.android.di.component;

import android.content.Context;

import com.technolab.android.db.DataRepositoryManager;
import com.technolab.android.di.module.ApplicationModule;
import com.technolab.android.di.qualifier.ApplicationContext;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword.TLForgotPassword;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword.TLForgotPassword3;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLMobileVerification;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLSignInPhone;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp.TLSignUp;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp.TLState;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp.TLTellUsIndustry;
import com.technolab.android.ui.itl.main.MainApplication;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLSignInEmail;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLLiveManagement;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLCreateLiveStream;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLLiveProductSetupActivity;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLMerchantLiveStreaming;
import com.technolab.android.ui.itl.main.MerchantMainActivity;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLCreateProduct;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLProductManagement;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLSelectProductCategory;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLMerchantChooseIndustry;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLMerchantInfo;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLMerchantInfoFillUp;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLMerchantPayment;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLScanICBack;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLScanSelfie;
import com.technolab.android.ui.itl.main.MerchantViewProfileActivity;
import com.technolab.android.ui.itl.main.TLAddAddress;
import com.technolab.android.ui.itl.main.TLAddressSetting;
import com.technolab.android.ui.itl.main.TLEnergyHistory;
import com.technolab.android.ui.itl.main.TLInviteFriendDetail;
import com.technolab.android.ui.itl.main.TLLiveMarket;
import com.technolab.android.ui.itl.main.TLLiveMarketCategory;
import com.technolab.android.ui.itl.main.TLMyPurchases;
import com.technolab.android.ui.itl.main.TLMyReferral;
import com.technolab.android.ui.itl.main.TLMyReferralDetail;
import com.technolab.android.ui.itl.main.TLRanking;
import com.technolab.android.ui.itl.main.TLRedeemEnergy;
import com.technolab.android.ui.itl.main.TLReward;
import com.technolab.android.ui.itl.main.TLSalesApprovement;
import com.technolab.android.ui.itl.main.TLSalesManagement;
import com.technolab.android.ui.itl.main.TLSalesManagementDetail;
import com.technolab.android.ui.itl.main.TLUserLiveStreaming;
import com.technolab.android.ui.itl.main.TLUserSetting;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.main.ViewProfileActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MainApplication app);

    @ApplicationContext
    Context context();

    DataRepositoryManager dataRepositoryManager();

    //Activities
    void inject(TLSignInEmail activity);
    void inject(TLSignInPhone activity);
    void inject(TLMobileVerification activity);
    void inject(TLSignUp activity);
    void inject(TLTellUsIndustry activity);

    void inject(TLForgotPassword activity);
    void inject(TLForgotPassword3 activity);

    void inject(TLMerchantChooseIndustry activity);
    void inject(TLMerchantInfo activity);
    void inject(TLScanICBack activity);
    void inject(TLScanSelfie activity);
    void inject(TLMerchantInfoFillUp activity);
    void inject(TLMerchantPayment activity);

    void inject(TLLiveManagement activity);
    void inject(UserMainActivity activity);
    void inject(TLCreateLiveStream activity);
    void inject(TLLiveProductSetupActivity activity);

    void inject(TLProductManagement activity);
    void inject(TLLiveMarket activity);
    void inject(TLSelectProductCategory activity);

    void inject(TLCreateProduct activity);
    void inject(ViewProfileActivity activity);
    void inject(TLMerchantLiveStreaming activity);

    void inject(TLLiveMarketCategory activity);
    void inject(TLUserLiveStreaming activity);
    void inject(TLAddressSetting activity);
    void inject(TLAddAddress activity);

    void inject(TLMyReferral activity);
    void inject(TLMyReferralDetail activity);
    void inject(TLState activity);

    void inject(MerchantViewProfileActivity activity);
    void inject(MerchantMainActivity activity);

    void inject(TLMyPurchases activity);
    void inject(TLSalesManagement activity);
    void inject(TLSalesManagementDetail activity);

    void inject(TLRedeemEnergy activity);
    void inject(TLEnergyHistory activity);
    void inject(TLRanking activity);
    void inject(TLReward activity);

    void inject(TLUserSetting activity);
    void inject(TLInviteFriendDetail activity);
    void inject(TLSalesApprovement activity);
}
