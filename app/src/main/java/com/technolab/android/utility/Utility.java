package com.technolab.android.utility;

import android.app.Activity;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;
import android.util.TypedValue;

import androidx.core.content.ContextCompat;

import com.technolab.android.R;
import com.technolab.android.error.TLException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

import java.io.InputStream;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class Utility {
    private static final String TAG = "Utility";

    private static Utility ourInstance = null;

    public static synchronized Utility getInstance() {
        if (ourInstance == null)
            ourInstance = new Utility();

        return ourInstance;
    }

    private Utility() {
    }

    public String encryptPassword(String password, String key) {
        return encrypt(password, key);
    }

    private String encrypt(String password, String key) {
        byte[] keyBytes = null;

        keyBytes = key.getBytes();

        SecretKeySpec sks = null;
        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(128, sr);
            sks = new SecretKeySpec(keyBytes, "AES");
        } catch (Exception e) {
            Log.e(TAG, "AES secret key spec error");
        }

        // Encode the original data with AES
        byte[] encodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, sks);
            encodedBytes = c.doFinal(password.getBytes("UTF-8"));
        } catch (Exception e) {
            Log.e(TAG, "AES encryption error");
        }

        return new String(Hex.encodeHex(encodedBytes));
    }

    public String decryptPassword(String password, String key) {
        return decrypt(password, key);
    }

    private String decrypt(String pwd, String key) {
        byte[] password;
        try {
            password = Hex.decodeHex(pwd.toCharArray());
        } catch (DecoderException e) {
            password = pwd.getBytes();
        }
        byte[] keyBytes = null;

        keyBytes = key.getBytes();

        SecretKeySpec sks = null;
        try {
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            KeyGenerator kg = KeyGenerator.getInstance("AES");
            kg.init(128, sr);
            sks = new SecretKeySpec(keyBytes, "AES");
        } catch (Exception e) {
            Log.e(TAG, "AES secret key spec error");
        }

        // Encode the original data with AES
        byte[] encodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, sks);
            encodedBytes = c.doFinal(password);
        } catch (Exception e) {
            Log.e(TAG, "AES decryption error");
        }

        return new String(encodedBytes);
    }

    private String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b & 0xff));
        }
        return sb.toString();
    }

    /**
     * Return a JSON Object which is stored in a JSON file.
     *
     * @param file JSON file to be parsed.
     * @return A mapped JSON object
     */
    public JSONObject getJSONObjectFromJSON(String file) {
        JSONObject jsonObject = null;
        InputStream stream = GlobalVariables.getInstance().getContext().getResources().openRawResource(GlobalVariables.getInstance().getContext().getResources().getIdentifier(file, "raw", GlobalVariables.getInstance().getContext().getPackageName()));

        try {
            Integer size = stream.available();
            byte[] bytes = new byte[size];
            stream.read(bytes);
            stream.close();

            jsonObject = new JSONObject(new String(bytes, "utf-8"));

        } catch (Exception e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while reading " + file + " - " + e.getMessage());

            jsonObject = null;
        }

        if (jsonObject == null)
            jsonObject = new JSONObject();

        return jsonObject;
    }

    /**
     * Return a JSON Array which is stored in a JSON file.
     *
     * @param file JSON file to be parsed.
     * @return A JSON array
     */
    public JSONArray getJSONArrayFromJSON(String file) {
        JSONArray jsonArray = null;
        InputStream stream = GlobalVariables.getInstance().getContext().getResources().openRawResource(GlobalVariables.getInstance().getContext().getResources().getIdentifier(file, "raw", GlobalVariables.getInstance().getContext().getPackageName()));

        try {
            Integer size = stream.available();
            byte[] bytes = new byte[size];
            stream.read(bytes);
            stream.close();

            jsonArray = new JSONArray(new String(bytes, "utf-8"));

        } catch (Exception e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while reading " + file + " - " + e.getMessage());

            jsonArray = null;
        }

        if (jsonArray == null)
            jsonArray = new JSONArray();

        return jsonArray;
    }

    /**
     * Return a Map which is stored in a XML file.
     *
     * @param file XML file to be parsed. The node in xml file should be in this format:
     *             <map>
     *             <entry key="key"> "message"</entry>
     *             </map>
     * @return A map that holds key and message
     */
    public Map<String, String> getMapFromXML(String file) {
        Map<String, String> map = new HashMap<String, String>();
        XmlResourceParser parser = GlobalVariables.getInstance().getContext().getResources().getXml(GlobalVariables.getInstance().getContext().getResources().getIdentifier(file, "xml", GlobalVariables.getInstance().getContext().getPackageName()));

        try {
            int event;
            String key = null;
            String value = null;
            do {
                event = parser.next();
                if (event == XmlPullParser.START_TAG) {
                    if (parser.getName().equals("entry")) {
                        key = parser.getAttributeValue(null, "key");
                        if (key == null)
                            throw new TLException("Key is NULL");
                    }
                } else if (event == XmlPullParser.TEXT) {
                    value = parser.getText();
                    if (value == null)
                        throw new TLException("Value is NULL");
                } else if (event == XmlPullParser.END_TAG) {
                    if (value != null && key != null)
                        map.put(key, value);
                    else
                        throw new TLException("Key or Value is NULL");
                }

            } while (event != XmlPullParser.END_DOCUMENT);
        } catch (Exception e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while mapping - " + e.getMessage());
        } finally {
            parser.close();
        }

        return map;
    }

    /**
     * This method is used to translate a given XML file to a DB change list in JSON format
     * The return object will be used to update user/cache DB
     *
     * @param file XML file name, either change list for user DB or change list for cache DB
     * @return JSON object to be used for DB update operation
     */
    public JSONArray getDBChangeList(String file) {
        JSONArray jsonArray = new JSONArray();

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(GlobalVariables.getInstance().getContext().getResources().openRawResource(GlobalVariables.getInstance()
                    .getContext().getResources().getIdentifier(file, "raw", GlobalVariables.getInstance().getContext().getPackageName())));

            doc.getDocumentElement().normalize();

            if (!doc.getDocumentElement().getNodeName().equalsIgnoreCase("dbchangeList"))
                return jsonArray;

            NodeList nList = doc.getElementsByTagName("changeList");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("version", Double.parseDouble(eElement.getElementsByTagName("version").item(0).getTextContent()));

                    Node nodeList = eElement.getElementsByTagName("list").item(0);

                    if (nodeList.getNodeType() == Node.ELEMENT_NODE) {
                        NodeList nodeListItem = ((Element) nodeList).getElementsByTagName("item");
                        JSONArray list = new JSONArray();
                        for (int i = 0; i < nodeListItem.getLength(); i++) {
                            Node myNode = nodeListItem.item(i);

                            if (myNode.getNodeType() == Node.ELEMENT_NODE)
                                list.put(myNode.getTextContent());
                        }

                        jsonObject.put("list", list);
                        jsonArray.put(jsonObject);
                    }
                }
            }
        } catch (Exception e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while reading DB change list - " + file + " - " + e.getMessage());
            return new JSONArray();
        }
        return jsonArray;
    }

    public static String getHexColorCode(Activity activity, int color){
        return "#" + Integer.toHexString(ContextCompat.getColor(activity, color) & 0x00ffffff);
    }

    public static int pxToDp(float px) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px, Resources.getSystem().getDisplayMetrics());
    }

    public static int dpToPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics());
    }
}
