package com.technolab.android.utility;

import androidx.multidex.BuildConfig;

public class TLConstant {
    //////////////////////////////////////////////////////////////////////
    //DB INFO
    public static final String DB_NAME = "tl_v1";
    public static final String DB_CACHE_NAME = "tl_tbc_v1";
    public static final String DB_DIR = "Org";
    public static final String DYNAMIC_LINK_REFERRAL = "";
    public static final String DYNAMIC_LINK_PRODUCT = "";

    //////////////////////////////////////////////////////////////////////
    //URLs
    public enum TLScheme {
        PRODUCTION, DEV
    }

    public static final TLScheme scheme = TLScheme.valueOf(BuildConfig.FLAVOR);

    public static final String URL_BASE = scheme == TLScheme.PRODUCTION ? "http://202.9.99.236:5000" :
            scheme == TLScheme.DEV ? "http://202.9.99.236:5000" : "";

    //////////////////////////////////////////////////////////////////////
    //Debug
    public static final Boolean DEBUG_OTHERS = BuildConfig.DEBUG;

    /**
     * Package name of all fragments
     */
    public static final String PACKAGE_MAIN = "com.technolab.android.ui.itl.main.";
}
