package com.technolab.android.utility;

public enum EnumCompany {

    company1("Individual"),
    company2("Partnership"),
    company3("Corporation"),
    company4("Non-Profit Organization"),
    company5("Cooperative");

    private String value;

    EnumCompany(String value){
        this.value=value;
    }
    public String getValue(){
        return this.value;
    }
}
