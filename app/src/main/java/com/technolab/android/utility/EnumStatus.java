package com.technolab.android.utility;

public enum EnumStatus {
    status1("Active"),
    status2("Inactive");

    private String value;

    EnumStatus(String value){
        this.value=value;
    }

    public String getValue(){
        return this.value;
    }
}
