package com.technolab.android.utility;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class GlobalVariables {
    private static String TAG = "Global";
    private static GlobalVariables ourInstance;

    private static Handler handlerWorker;
    private static Handler handlerUI;

    public static synchronized Handler getHandlerWorker() {
        if (handlerWorker == null) {
            HandlerThread thread = new HandlerThread("Handler Worker");
            thread.start();
            handlerWorker = new Handler(thread.getLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    Runnable runnable = (Runnable) msg.obj;
                    runnable.run();
                }
            };
        }

        return handlerWorker;
    }

    public static synchronized Handler getHandlerUI() {
        if (handlerUI == null)
            handlerUI = new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    Runnable runnable = (Runnable) msg.obj;
                    runnable.run();
                }
            };

        return handlerUI;
    }

    public static synchronized GlobalVariables getInstance() {
        if (ourInstance == null)
            ourInstance = new GlobalVariables();
        return ourInstance;
    }

    public static synchronized void resetInstance() {
        ourInstance = null;
    }

    private String userDBName;
    private Activity mainActivity;
    private Activity currentActivity;
    private ArrayList popupList;
    private Lock popupLock;

    private GlobalVariables() {
        userDBName = "";
        popupList = new ArrayList();
        popupLock = new Lock() {
            @Override
            public void lock() {

            }

            @Override
            public void lockInterruptibly() throws InterruptedException {

            }

            @Override
            public boolean tryLock() {
                return false;
            }

            @Override
            public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
                return false;
            }

            @Override
            public void unlock() {

            }

            @NonNull
            @Override
            public Condition newCondition() {
                return null;
            }
        };
    }

    public Context getContext() {
        return mainActivity != null ? mainActivity.getApplicationContext(): null;
    }

    public Activity getMainActivity() {
        return mainActivity;
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public void setMainActivity(Activity activity) {
        mainActivity = activity;
    }

    public String getUserDBName() {
        return userDBName;
    }

    public void setUserDBName(String userDBName) {
        this.userDBName = userDBName;
    }

    //Popup registration
    public boolean registerForPopup(Object fragment) {
        synchronized (popupLock) {
            if (popupList.contains(fragment))
                return false;
            popupList.add(fragment.getClass().getName());
            return true;
        }
    }

    public void unregisterForPopup(Object fragment) {
        synchronized (popupLock) {
            popupList.remove(fragment.getClass().getName());
        }
    }
}
