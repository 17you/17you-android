package com.technolab.android.utility;

public enum MessageType {
    CHAT_MINE(0),
    USER_JOIN(1),
    USER_LEAVE(2);

    private final int value;

    MessageType(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}
