package com.technolab.android.utility;

public enum EnumStates {

    state1("SELANGOR"),
    state2("PAHANG"),
    state3("SABAH"),
    state4("SARAWAK"),
    state5("PERLIS"),
    state6("PENANG"),
    state7("MALACCA"),
    state8("JOHOR"),
    state9("TERENGGANU"),
    state10("PERAK"),
    state11("KELANTAN"),
    state12("KUALA LUMPUR"),
    state13("PUTRAJAYA"),
    state14("LABUAN"),
    state15("KEDAH"),
    state16("NEGERI SEMBILAN"),;


    private String value;

    EnumStates(String value){
        this.value=value;
    }

    public String getValue(){
        return this.value;
    }
}
