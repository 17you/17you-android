package com.technolab.android.utility;

public enum EnumLiveStreamStatus {

    PUBLISHED("PUBLISHED"),
    SAVED("SAVED"),
    LIVE_NOW("LIVE NOW"),
    FINISHED("FINISHED"),
    EXPIRED("EXPIRED");

    private String value;

    EnumLiveStreamStatus(String value){
        this.value=value;
    }
    public String getValue(){
        return this.value;
    }
}
