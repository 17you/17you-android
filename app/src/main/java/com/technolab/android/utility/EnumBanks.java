package com.technolab.android.utility;

public enum EnumBanks {
    bank1("AFFINBANK"),
    bank2("AGROBANK / BANK PERTANIAN MALAYSIA BERHAD"),
    bank3("ALLIANCE BANK MALAYSIA BERHAD"),
    bank4("AL RAJHI BANKING INVESTMENT CORPORTATION (MALAYSIA) BERHAD"),
    bank5("AMBANK (M) BERHAD"),
    bank6("BANK ISLAM MALAYSIA BERHAD"),
    bank7("BANK KERJASAMA RAKYAT MALAYSIA BERHAD"),
    bank8("BANK MUALAMAT (MALAYSIA) BERHADD"),
    bank9("BANK SIMPANAN NASIONAL BERHAD"),
    bank10("CIMB BANK BERHAD"),
    bank11("CITYBANK BERHAD"),
    bank12("HONG LEONG BANK BERHAD"),
    bank13("HSBC BANK MALAYSIA BERHAD"),
    bank14("MAYBANK BANKING BERHAD"),
    bank15("OCBC BANK (MALAYSIA) BERHAD"),
    bank16("PUBLIC BANK BERHAD"),
    bank17("RHB BANK BERHAD"),
    bank18("STANDARD CHARTERED BANK (MALAYSIA) BERHAD"),
    bank19("UNITED OVERSEAS BANK (MALAYSIA) BERHAD"),;


    private String value;

    EnumBanks(String value){
        this.value=value;
    }

    public String getValue(){
        return this.value;
    }
}
