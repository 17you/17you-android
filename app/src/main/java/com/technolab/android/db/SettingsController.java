package com.technolab.android.db;

import android.database.Cursor;
import android.util.Log;

import com.technolab.android.ui.entity.Menu;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.utility.TLConstant;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

public class SettingsController {
    private static String TAG = "SettingsController";
    private static SettingsController settingsController = null;

    private SettingsController() {
        // private constructor prevents any instantiation
    }

    public static synchronized SettingsController getInstance() {
        if (settingsController == null) {
            settingsController = new SettingsController();
        }
        return settingsController;
    }

    public static synchronized void resetInstance() {
        settingsController = null;
    }

    //////////////////////////////////////////
    //MENU handling
    /////////////////////////////////////////

    //Get a list containing all main menus
    public ArrayList<Menu> getAllMainMenu(Boolean isMerchant) {
        ArrayList<Menu> list = new ArrayList<Menu>();
        String sql = null;
        sql = isMerchant ? "SELECT menu_id, description, image, is_merchant, image_selected, menu_seq FROM main_menu WHERE status = 1 AND is_merchant = 1 order by menu_seq ASC" : "SELECT menu_id, description, image, is_merchant, image_selected, menu_seq FROM main_menu WHERE status = 1 AND is_merchant = 0 order by menu_seq ASC";
        Cursor cursor = (Cursor) DBController.getInstance().executeQuery(sql, new String[]{});

        if (cursor != null) {

            while (cursor.moveToNext()) {
                Menu menu = new Menu(cursor.getString(cursor.getColumnIndex("menu_id")));
                menu.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                menu.setMenuIcon(cursor.getString(cursor.getColumnIndex("image")));
                menu.setMerchant(cursor.getInt(cursor.getColumnIndex("is_merchant")));
                menu.setMenuOrder(cursor.getInt(cursor.getColumnIndex("menu_seq")));
                list.add(menu);
            }

            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return list;
    }

    public ArrayList<AbstractActivity> getActivityCtrlListForViewID(String mainViewId) {
        ArrayList<AbstractActivity> list = new ArrayList<AbstractActivity>();
        String sql = "SELECT c.class_name FROM views v, menu_view_list m, view_class c WHERE m.view_id=v.view_id AND v.class_id=c.class_id AND menu_id=?";
        Cursor cursor = (Cursor) DBController.getInstance().executeQuery(sql, new String[]{mainViewId});

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String className = cursor.getString(cursor.getColumnIndex("class_name"));
                try {
                    Class<AbstractActivity> aClass = (Class<AbstractActivity>) Class.forName(TLConstant.PACKAGE_MAIN + className);
                    Constructor<AbstractActivity> constructor = aClass.getConstructor();
                    AbstractActivity activity = constructor.getDeclaringClass().newInstance();
                    list.add(activity);
                } catch (Exception e) {
                    continue;
                }

            } while (cursor.moveToNext());
        }

        if (cursor != null)
            cursor.close();

//        if(list.size() == 0)
//            list = null;

        return list;
    }
}