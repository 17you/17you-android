package com.technolab.android.db;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.technolab.android.security.KeyOne;
import com.technolab.android.security.SecOne;
import com.technolab.android.user.TLEnvironment;
import com.technolab.android.utility.GlobalVariables;
import com.technolab.android.utility.TLConstant;
import com.technolab.android.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DBController {

    private static DBController dbCtrl = null;
    private static final String TAG = "DBController";

    private SQLiteDatabase db = null;
    private SQLiteDatabase db_caching = null;

    public static synchronized DBController getInstance() {
        if (dbCtrl == null)
            dbCtrl = new DBController();

        return dbCtrl;
    }

    public static synchronized void resetInstance() {
        if (dbCtrl != null && dbCtrl.db.isOpen())
            dbCtrl.db.close();

        if (dbCtrl != null && dbCtrl.db_caching.isOpen())
            dbCtrl.db_caching.close();

        dbCtrl = null;
    }

    /**
     * Create new DB from encrypted/plain DB. The new DB will come with/without a password.
     * If source DB and target DB are plain (no password) then it is simply a copy-paste execution
     * If source DB is plain (no password) and target DB is encrypted (with password) then it is an encrypting execution
     * If source DB is encrypted (with password) and target DB is plain (no password) then it is a decrypting execution
     * If source DB and target DB are encrypted (with different passwords) then it is then it is an encrypting/key changing execution
     * This is a static method so it shouldn't be confused with the singleton object
     *
     * @param sourceDB  Source DB path
     * @param sourcePwd Source DB password
     * @param targetDB  Target DB path
     * @param targetPwd Target DB password
     * @return Creation status
     */
    public static boolean createNewDB(String sourceDB, String sourcePwd, String targetDB, String targetPwd) {
        boolean status = true;
        copyIfNotExist(sourceDB, targetDB);
        return status;
    }

    /**
     * Copy and replace file from Raw in app APK to other location on the device
     *
     * @param resourceFile name of the file in raw folder
     * @param targetFile   Target file. This object must be a File class
     */
    public static File copyFromRaw(String resourceFile, File targetFile) {
        //Remove if found any existing file
        if (targetFile.exists())
            targetFile.delete();

        try {
            targetFile.createNewFile();

            copyFile(GlobalVariables.getInstance().getContext().getResources().openRawResource(GlobalVariables.getInstance()
                            .getContext().getResources().getIdentifier(resourceFile, "raw", GlobalVariables.getInstance().getContext()
                                    .getPackageName())),
                    new FileOutputStream(targetFile));
        } catch (Exception e) {
            if (targetFile.exists()) ;
            targetFile.delete();

            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while copying cache DB - " + e.getMessage());

            return null;
        }
        return targetFile;
    }

    /**
     * Copy over a file to another location if that file doesn't exist in that location.
     *
     * @param sourceFilePath Source file path (Full path)
     * @param targetFilePath Target file path (Full path)
     */
    public static void copyIfNotExist(String sourceFilePath, String targetFilePath) {
        File targetFile = new File(targetFilePath);

        //Remove if found any existing file
        if (targetFile.exists())
            return;

        try {
            targetFile.createNewFile();

            copyFile(new FileInputStream(sourceFilePath),
                    new FileOutputStream(targetFile));
        } catch (Exception e) {
            if (targetFile.exists()) ;
            targetFile.delete();

            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while copying cache DB - " + e.getMessage());
        }
    }

    /**
     * Export a file to another location.
     *
     * @param fromFile source file input stream
     * @param toFile   target file output stream
     * @throws IOException
     */
    private static void copyFile(InputStream fromFile, OutputStream toFile) throws IOException {
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fromFile.read(buffer)) > 0) {
            toFile.write(buffer, 0, length);
        }

        //Close the streams
        toFile.flush();
        toFile.close();
        fromFile.close();
    }

    /**
     * Try to create a DB for user using the DB template located in DB_DIR
     *
     * @return Result of the operation. 0 means unable to create DB, -1 means DB exists, 1 means DB is created successfully
     */
    public static Integer createUserDBIfNeeded() {
        String userDBName = Utility.getInstance().encryptPassword("user", SecOne.getInstance().getNamKey() + KeyOne.getInstance().getNamKey())
                + "_" + TLConstant.DB_NAME;

        File userDBFile = GlobalVariables.getInstance().getContext().getFileStreamPath(userDBName);

        if (userDBFile.exists())
            return -1;

        try {
            boolean status = createNewDB(GlobalVariables.getInstance().getContext().getDir(TLConstant.DB_DIR,
                    0).getAbsolutePath() + File.separator + TLConstant.DB_NAME,
                    Utility.getInstance().encryptPassword(SecOne.getInstance().getComSec() + KeyOne.getInstance().getComSec(),
                            SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey()),
                    GlobalVariables.getInstance().getContext().getFileStreamPath(userDBName).getAbsolutePath(),
                    Utility.getInstance().encryptPassword("user" + SecOne.getInstance()
                            .getUsrSec() + KeyOne.getInstance().getUsrSec(), SecOne.getInstance().getUsrKey() + KeyOne.getInstance().getUsrKey()));

            if (status)
                return 1;
            else {
                if (userDBFile.exists())
                    userDBFile.delete();

                return 0;
            }
        } catch (Exception e) {
            if (userDBFile.exists())
                userDBFile.delete();

            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Unable to create user DB - " + e.getMessage());

            return 0;
        }
    }

    /**
     * Compare version of two given DB
     *
     * @param file1 DB-1
     * @param pwd1  DB-1's password
     * @param file2 DB-2
     * @param pwd2  DB-2's password
     * @return true if the version is same
     */
    public static boolean compareDBVersion(File file1, String pwd1, File file2, String pwd2) {
        if (file1.exists() && file2.exists()) {
            SQLiteDatabase db1 = SQLiteDatabase.openDatabase(file1.getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            SQLiteDatabase db2 = SQLiteDatabase.openDatabase(file2.getAbsolutePath(), null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);

            String versionCache = "";
            String versionOrg = "";
            String sql = "SELECT value FROM system_profile WHERE key=?";
            if (db1 != null && db1.isOpen()) {
                Cursor cursor = db1.rawQuery(sql, new String[]{"db_version"});
                if (cursor != null) {
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        versionCache = cursor.getString(cursor.getColumnIndex("value"));
                    }

                    cursor.close();
                }
                db1.close();
            }

            if (db2 != null && db2.isOpen()) {
                Cursor cursor = db2.rawQuery(sql, new String[]{"db_version"});
                if (cursor != null) {
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        versionOrg = cursor.getString(cursor.getColumnIndex("value"));
                    }

                    cursor.close();
                }
                db2.close();
            }

            if (versionCache != null && versionCache.length() != 0 && versionOrg != null && versionOrg.length() != 0 && versionCache.equals(versionOrg))
                return true;
        }
        return false;
    }

    /**
     * Private Constructor for Singleton
     */
    private DBController() {
            db = SQLiteDatabase.openDatabase(GlobalVariables.getInstance().getContext().getFileStreamPath(GlobalVariables.getInstance()
                    .getUserDBName()).getAbsolutePath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            db_caching = SQLiteDatabase.openDatabase(GlobalVariables.getInstance().getContext().getFileStreamPath(TLConstant.DB_CACHE_NAME).getAbsolutePath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public SQLiteDatabase getDb_caching() {
        return db_caching;
    }

    /**
     * A method to execute a query statement with binding parameters (By using ?)
     *
     * @param sql    SQL statement
     * @param params binding parameter for the sql
     * @return Cursor containing requested column information
     */
    public Cursor executeQuery(String sql, String[] params) {
        Cursor cursor = db.rawQuery(sql, params);
        return cursor;
    }

    /**
     * A method to execute a query statement. No binding parameter is needed for the execution
     *
     * @param sql SQL statement (No binding param needed)
     * @return Cursor containing requested column information
     */
    public Cursor executeQuery(String sql) {
        Cursor cursor = db.rawQuery(sql, null);
        return cursor;
    }

    /**
     * A method to execute an update/delete/insert statement. Binding parameter is available (By using ?)
     *
     * @param sql    SQL statement (No Select statement is allowed)
     * @param params Binding param for the sql statement
     * @return Execution status (true/false)
     */
    public boolean executeUpdate(String sql, Object[] params) {
        boolean status = true;
        try {
            db.execSQL(sql, params);
        } catch (SQLException e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while executing SQL update - " + e.getMessage());
            status = false;
        }
        return status;
    }

    /**
     * Perform SQL update/delete/insert without arguments
     *
     * @param sql SQL update statement
     * @return Execution status
     */
    public boolean executeUpdate(String sql) {
        boolean status = true;
        try {
            db.execSQL(sql);
        } catch (SQLException e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while executing SQL update - " + e.getMessage());
            status = false;
        }
        return status;
    }

    /**
     * This method is to test if user DB can be accessed and ready for reading/writing
     *
     * @return test result
     */
    public boolean testUserDBAccess() {
        boolean status = false;
        Cursor cursor = executeQuery("SELECT * FROM views");
        if (cursor != null && cursor.getCount() > 0)
            status = true;

        if (cursor != null)
            cursor.close();

        return status;
    }

    /**
     * This method is to test if cache DB (counter cache and other cache table can be accessed and ready for reading/writing
     *
     * @return test result
     */
    public boolean testCacheDBAccess() {
        boolean status = false;
        Cursor cursor = db_caching.rawQuery("SELECT * from system_profile", null);
        if (cursor != null && cursor.getCount() > 0)
            status = true;

        if (cursor != null)
            cursor.close();

        return status;
    }

    /**
     * Run this method to read contents in dbchangelist.xml located in RAW folder and use the returning SQL statements to update user DB
     */
    public void executeUpdateUserDB() {
        JSONArray changeList = Utility.getInstance().getDBChangeList("dbchangelist");
        db.beginTransaction();
        try {
            for (int i = 0; i < changeList.length(); i++) {
                JSONObject jsonObject = changeList.getJSONObject(i);
                double version = jsonObject.getDouble("version");
                if (version > TLEnvironment.getInstance().getUserDBVersion()) {
                    JSONArray jsonArray = jsonObject.getJSONArray("list");
                    for (int j = 0; j < jsonArray.length(); j++) {
                        String sql = jsonArray.getString(j);
                        db.execSQL(sql);
                    }
                }
            }
            db.setTransactionSuccessful();
        } catch (JSONException e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception with JSON while updating user DB - " + e.getMessage());
        } catch (Exception e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while updating user DB - " + e.getMessage());
        } finally {
            db.endTransaction();
        }
    }

    /**
     * Run this method to read contents in dbcachechangelist.xml located in RAW folder and use the returning SQL statements to update shared cache
     * DB located in app's document folder
     */
    public void executeUpdateCacheDB() {
        JSONArray changeList = Utility.getInstance().getDBChangeList("dbcachechangelist");
        db_caching.beginTransaction();
        try {
            for (int i = 0; i < changeList.length(); i++) {
                JSONObject jsonObject = changeList.getJSONObject(i);
                double version = jsonObject.getDouble("version");
                if (version > TLEnvironment.getInstance().getCacheDBVersion()) {
                    JSONArray jsonArray = jsonObject.getJSONArray("list");
                    for (int j = 0; j < jsonArray.length(); j++) {
                        String sql = jsonArray.getString(j);
                        db_caching.execSQL(sql);
                    }
                }
            }
            db_caching.setTransactionSuccessful();
        } catch (JSONException e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception with JSON while updating cache DB - " + e.getMessage());
        } catch (Exception e) {
            if (TLConstant.DEBUG_OTHERS)
                Log.e(TAG, "Exception while updating cache DB - " + e.getMessage());
        } finally {
            db_caching.endTransaction();
        }
    }

    public static void deleteAllFilesInFolder(File folder) {
        if (!folder.exists())
            return;
        if (folder.isDirectory()) {
            for (File f : folder.listFiles())
                deleteAllFilesInFolder(f);
        } else
            folder.delete();
    }

}
