package com.technolab.android.db;

import android.content.Context;

import com.technolab.android.di.module.APIInterface;
import com.technolab.android.ui.entity.BaseResponse;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Path;

@Singleton
public class DataRepositoryManager {

    private Context context;
    private APIInterface mApiInterface;

    @Inject
    public DataRepositoryManager(Context context, APIInterface apiInterface) {
        this.context = context;
        this.mApiInterface = apiInterface;
    }

    public Observable<BaseResponse> login(RequestBody loginRequestBody) {
        return mApiInterface.login(loginRequestBody);
    }

    public Observable<BaseResponse> loginWithPhone(RequestBody loginRequestBody) {
        return mApiInterface.loginWithPhone(loginRequestBody);
    }

    public Observable<BaseResponse> checkPhoneNumber(RequestBody requestBody) {
        return mApiInterface.checkPhoneNumber(requestBody);
    }

    public Observable<BaseResponse> checkEmailAndPhone(RequestBody requestBody) {
        return mApiInterface.checkEmailAndPhone(requestBody);
    }

    public Observable<BaseResponse> updatePassword(RequestBody requestBody) {
        return mApiInterface.updatePassword(requestBody);
    }

    public Observable<BaseResponse> register(RequestBody registerRequestBody) {
        return mApiInterface.register(registerRequestBody);
    }

    //Merchant Registration
    public Observable<BaseResponse> retrieveIndustries(){
        return mApiInterface.retrieveIndustries();
    }
    public Observable<BaseResponse> retrieveMerchantApplicationStatus(String id){
        return mApiInterface.retrieveApplicationStatus(id);
    }
    public Observable<BaseResponse> uploadIdentification(String id, RequestBody categoryID, MultipartBody.Part front, MultipartBody.Part back){
        return mApiInterface.identificationUploader(id, categoryID, front, back);
    }
    public Observable<BaseResponse> uploadSelfie(String id, MultipartBody.Part selfie){
        return mApiInterface.selfieUploader(id, selfie);
    }
    public Observable<BaseResponse> uploadInfoDetail(String id, RequestBody infoRequestBody){
        return mApiInterface.infoUploader(id, infoRequestBody);
    }
    public Observable<BaseResponse> uploadBankSlip(String id, MultipartBody.Part bankSlip){
        return mApiInterface.uploadBankSlip(id, bankSlip);
    }
    public Observable<BaseResponse> submitPrivacyPolicy(String id){
        return mApiInterface.submitPrivacyPolicy(id);
    }

    //Live Management
    public Observable<BaseResponse> retrieveMerchantLiveStreams(String merchantID){
        return mApiInterface.retrieveMerchantLiveStream(merchantID);
    }
    public Observable<BaseResponse> retrieveMerchantDetails(String userID){
        return mApiInterface.retrieveMerchantDetail(userID);
    }
    public Observable<BaseResponse> retrieveMerchantProduct(String merchantID){
        return mApiInterface.retrieveMerchantProduct(merchantID);
    }
    public Observable<BaseResponse> uploadLiveStream(String merchantID, RequestBody title,
                                                     RequestBody desc, RequestBody date,
                                                     RequestBody startTime, RequestBody endTime,
                                                     RequestBody product, RequestBody status, RequestBody shippingFee, MultipartBody.Part liveStreamImage){
        return mApiInterface.createLiveStream(merchantID,title,desc,date,startTime,endTime,product,status, shippingFee, liveStreamImage);
    }
    public Observable<BaseResponse> retrieveActiveLiveStream(){
        return mApiInterface.retrieveActiveLiveStream();
    }
    public Observable<BaseResponse> deleteLiveStream(String liveStreamID){
        return mApiInterface.deleteLiveStream(liveStreamID);
    }

    public Observable<BaseResponse> publishLiveStream(String liveStreamID, RequestBody requestBody){
        return mApiInterface.publishLiveStream(liveStreamID,requestBody);
    }

    public Observable<BaseResponse> updateLiveStream(String liveStreamID,String merchantID, RequestBody title,
                                                     RequestBody desc, RequestBody date,
                                                     RequestBody startTime, RequestBody endTime,
                                                     RequestBody product, RequestBody status, RequestBody shippingFee, MultipartBody.Part liveStreamImage){
        return mApiInterface.updateLiveStream(liveStreamID,merchantID,title,desc,date,startTime,endTime,product,status,shippingFee, liveStreamImage);
    }
    public Observable<BaseResponse> retrieveSingleLiveStream(String liveStreamID){
        return mApiInterface.retrieveSingleLiveStream(liveStreamID);
    }

    //Product Management
    public Observable<BaseResponse> retrieveProductCategories(){
        return mApiInterface.retrieveProductCategories();
    }

    public Observable<BaseResponse> createProduct(String merchantID,
                                                  RequestBody name,
                                                  RequestBody description,
                                                  RequestBody variation,
                                                   RequestBody categoryId,
                                                  RequestBody subCategoryId,
                                                  MultipartBody.Part productImage){

        return mApiInterface.createProduct(merchantID,name,description,variation,categoryId,subCategoryId,productImage);
    }
    public Observable<BaseResponse> updateProduct(String productID, String merchantID,
                                                  RequestBody name,
                                                  RequestBody description,
                                                  RequestBody variation,
                                                  RequestBody categoryId,
                                                  RequestBody subCategoryId,
                                                  MultipartBody.Part productImage){

        return mApiInterface.updateProduct(productID,merchantID,name,description,variation,categoryId,subCategoryId,productImage);
    }
    public Observable<BaseResponse> retrieveSingleProduct(String productID){
        return mApiInterface.retrieveSingleProduct(productID);
    }

    public Observable<BaseResponse> updateProfile(String userID, RequestBody name, RequestBody expertise, MultipartBody.Part profileImage){
        return mApiInterface.updateProfile(userID,name,expertise,profileImage);
    }

    public Observable<BaseResponse> startLiveStream(String liveStreamID, RequestBody body){
        return mApiInterface.startLiveStream(liveStreamID, body);
    }

    public Observable<BaseResponse> stopLiveStream(String liveStreamID, RequestBody body){
        return mApiInterface.stopLiveStream(liveStreamID, body);
    }

    public Observable<BaseResponse> retrieveAllLiveStream(String memberID){
        return mApiInterface.retrieveAllLiveStream(memberID);
    }

    public Observable<BaseResponse> retrieveLiveStreamDetails(String liveStreamID, RequestBody requestBody){
        return mApiInterface.retrieveLiveStreamDetails(liveStreamID, requestBody);
    }

    public Observable<BaseResponse> retrieveLiveStreamProduct(String liveStreamID, RequestBody body){
        return mApiInterface.retrieveLiveStreamProduct(liveStreamID, body);
    }

    public Observable<BaseResponse> updateLiveStreamProduct(String liveStreamID, RequestBody body){
        return mApiInterface.updateLiveStreamProduct(liveStreamID, body);
    }

    public Observable<BaseResponse> retrieveAddress(String memberID, RequestBody body){
        return mApiInterface.retrieveAddress(memberID, body);
    }

    public Observable<BaseResponse> addAddress(String memberID, RequestBody body){
        return mApiInterface.addAddress(memberID, body);
    }

    public Observable<BaseResponse> getTotalReferral(String memberID){
        return mApiInterface.getTotalReferral(memberID);
    }

    public Observable<BaseResponse> getTodayReferral(String memberID){
        return mApiInterface.getTodayReferral(memberID);
    }

    public Observable<BaseResponse> getMonthReferral(String memberID){
        return mApiInterface.getMonthReferral(memberID);
    }

    public Observable<BaseResponse> getWeekReferral(String memberID){
        return mApiInterface.getEmailWeekReferral(memberID);
    }

    public Observable<BaseResponse> getEmailTotalReferral(String memberID){
        return mApiInterface.getEmailTotalReferral(memberID);
    }

    public Observable<BaseResponse> getEmailTodayReferral(String memberID){
        return mApiInterface.getEmailTodayReferral(memberID);
    }

    public Observable<BaseResponse> getEmailMonthReferral(String memberID){
        return mApiInterface.getEmailMonthReferral(memberID);
    }

    public Observable<BaseResponse> getEmailWeekReferral(String memberID){
        return mApiInterface.getEmailWeekReferral(memberID);
    }

    public Observable<BaseResponse> getStates(){
        return mApiInterface.getStates();
    }

    public Observable<BaseResponse> updateMerchantProfile(String merchantID, RequestBody companyName,
                                                          RequestBody bankName, RequestBody bankAccountNumber,
                                                          RequestBody bankAccountHolder, RequestBody state, RequestBody email,
                                                          RequestBody streetAddress, RequestBody city,
                                                          RequestBody postCode,
                                                          MultipartBody.Part profileImageFilePart) {
        return mApiInterface.updateMerchantProfile(merchantID,companyName,bankName,bankAccountNumber, bankAccountHolder, state, email, streetAddress, city, postCode, profileImageFilePart);

    }

    public Observable<BaseResponse> retrieveMerchantProfile(String merchantId) {
        return mApiInterface.retrieveMerchantProfile(merchantId);
    }

    public Observable<BaseResponse> deleteProduct(String productID) {
        return mApiInterface.deleteProduct(productID);
    }

    public Observable<BaseResponse> fetchUpcomingLive(String merchantID){
        return mApiInterface.fetchUpcomingLive(merchantID);
    }

    public Observable<BaseResponse> fetchTransaction(String memberID, int status){
        return mApiInterface.fetchTransaction(memberID, status);
    }

    public Observable<BaseResponse> fetchMerchantTransaction(String merchantID, int status){
        return mApiInterface.fetchMerchantTransaction(merchantID, status);
    }

    public Observable<BaseResponse> updateTransactionStatus(String transactionID, String trackingURL, int status, String courier){
        return mApiInterface.updateTransactionStatus(transactionID, trackingURL, status, courier);
    }

    public Observable<BaseResponse> rejectTransaction(String transactionID, RequestBody body){
        return mApiInterface.rejectTransaction(transactionID, body);
    }

    public Observable<BaseResponse> approveTransaction(String transactionID, RequestBody body){
        return mApiInterface.approveTransaction(transactionID, body);
    }

    public Observable<BaseResponse> fetchTopLiveStream(String merchantID){
        return mApiInterface.fetchTopLiveStream(merchantID);
    }

    public Observable<BaseResponse> fetchTransactionHistory(String merchantID){
        return mApiInterface.fetchTransactionHistory(merchantID);
    }

    public Observable<BaseResponse> performTransaction(RequestBody liveStreamID,
                                                          RequestBody merchantID, RequestBody addressID,
                                                          RequestBody memberID, RequestBody paymentMethod, RequestBody product,
                                                          RequestBody totalProductPrice, RequestBody deliveryFees,
                                                          RequestBody finalPrice,
                                                          MultipartBody.Part fileTransactionReceipt) {
        return mApiInterface.performTransaction(liveStreamID,merchantID,addressID,memberID,paymentMethod,product,totalProductPrice,deliveryFees,finalPrice,fileTransactionReceipt);
    }

    public Observable<BaseResponse> performTransactionWithoutImage(RequestBody body){
        return mApiInterface.performTransactionWithoutImage(body);
    }

    public Observable<BaseResponse> updateTransaction(String transactionID, RequestBody memberID,
                                                      RequestBody merchantID, RequestBody paymentMethod,
                                                      MultipartBody.Part fileTransactionReceipt) {
        return mApiInterface.updateTransaction(transactionID,memberID,merchantID,paymentMethod,fileTransactionReceipt);
    }

    public Observable<BaseResponse> retrieveEnergySummary(String memberID){
        return mApiInterface.retrieveEnergySummary(memberID);
    }

    public Observable<BaseResponse> retrieveEnergyLevel(String memberID){
        return mApiInterface.retrieveEnergyLevel(memberID);
    }

    public Observable<BaseResponse> retrieveEnergyHistory(String memberID){
        return mApiInterface.retrieveEnergyHistory(memberID);
    }

    public Observable<BaseResponse> redeemEnergy(String type, String memberID){
        return mApiInterface.redeemEnergy(type, memberID);
    }

    public Observable<BaseResponse> retrieveRanking(){
        return mApiInterface.retrieveRanking();
    }

    public Observable<BaseResponse> addShare(String memberID){
        return mApiInterface.addShare(memberID);
    }

    public Observable<BaseResponse> fetchReferralEnergyCount(String memberID){
        return mApiInterface.fetchReferralEnergyCount(memberID);
    }

    public Observable<BaseResponse> fetchMerchantEnergyCount( String merchantID){
        return mApiInterface.fetchMerchantEnergyCount(merchantID);
    }

    public Observable<BaseResponse> updateFavourites(String memberID, RequestBody body){
        return mApiInterface.updateFavourites(memberID, body);
    }

    public Observable<BaseResponse> retrieveRewardInfo(String memberID){
        return mApiInterface.retrieveRewardInfo(memberID);
    }

    public Observable<BaseResponse> claimReward(String memberID, RequestBody body){
        return mApiInterface.claimReward(memberID, body);
    }


}
