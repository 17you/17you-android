package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.ncorti.slidetoact.SlideToActView;
import com.technolab.android.R;
import com.technolab.android.ui.entity.LevelResult;
import com.technolab.android.ui.entity.RankingOthers;
import com.technolab.android.ui.entity.ReferralCount;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.adapter.MerchantIndustryAdapter;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.circularprogressbar.CircularProgressBar;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLRedeemEnergy extends AbstractActivity implements TLMainMvpView.RedeemEnergy {

    private TextView tv_total_share_value;
    private TextView tv_total_sign_up_value;
    private TextView tv_today_sign_up_value;

    private Button btn_redeem_1;
    private Button btn_redeem_2;
    private Button btn_redeem_3;

    private SlideToActView swipe_redeem_view;
    CircularProgressBar energy_progress;

    private TextView txt_level;
    private TextView txt_level_value;
    private RecyclerView recyclerView;

    @Inject
    TLMainPresenter.TLRedeemEnergy redeemEnergyPresenter;

    public TLRedeemEnergy() {
        TAG = "TLRedeemEnergy";
        layoutID = R.layout.activity_redeem_energy;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        setupRefresh(this);
        tv_total_share_value = findViewById(R.id.tv_total_share_value);
        tv_total_sign_up_value = findViewById(R.id.tv_total_sign_up_value);
        tv_today_sign_up_value = findViewById(R.id.tv_today_sign_up_value);
        btn_redeem_1 = findViewById(R.id.btn_redeem_1);
        btn_redeem_2 = findViewById(R.id.btn_redeem_2);
        btn_redeem_3 = findViewById(R.id.btn_redeem_3);
        swipe_redeem_view = findViewById(R.id.swipe_redeem_view);
        txt_level = findViewById(R.id.txt_level);
        txt_level_value = findViewById(R.id.txt_level_value);
        energy_progress = findViewById(R.id.energy_progress);
        recyclerView = findViewById(R.id.recycler_view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLRedeemEnergy.this);
        redeemEnergyPresenter.onAttach(this);
        redeemEnergyPresenter.retrieveEnergySummary();
        redeemEnergyPresenter.retrieveEnergyLevel();
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Total Energy");
        txtSubHeader.setVisibility(View.GONE);
        txtSubHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);

        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> {
            redeemEnergyPresenter.retrieveEnergySummary();
            redeemEnergyPresenter.retrieveEnergyLevel();
        });
    }

    @Override
    public void onReceiveEnergyData(ReferralCount referralCount) {
        refreshLayout.setRefreshing(false);
        txtSubHeader.setText(String.valueOf(referralCount.getTotal_energy()));
        txtSubHeader.setVisibility(View.VISIBLE);
        tv_total_share_value.setText(String.valueOf(referralCount.getTs_redeemable_count()));
        tv_total_sign_up_value.setText(String.valueOf(referralCount.getOsu_redeemable_count()));
        tv_today_sign_up_value.setText(String.valueOf(referralCount.getTsu_redeemable_count()));

        String[] energyType = {"1", "0", "1000", "2", "1001", "2000", "3", "2001", "3000", "4", "3001",
                "4000", "5", "4001", "5000", "6", "5001", "6000", "7", "6001", "7000", "8", "7001", "8000"};
        String energyLevel = "";
        int energyLimit = 0;
        int energy = referralCount.getTotal_energy();
        for (int j = 0; j < energyType.length; j++) {
            energyLevel = energyType[j];
            j = j + 1;
            int min = Integer.parseInt(energyType[j]);
            j = j + 1;
            int max = Integer.parseInt(energyType[j]);
            if (energy >= min && energy <= max) {
                energyLimit = max;
                break;
            }
        }

        float totalEnergy = (float) referralCount.getTotal_energy();
        float energyPercentage = (float) (totalEnergy / energyLimit) * 100;
        energy_progress.setValue(energyPercentage);
        txt_level.setText(String.format("Level %s", energyLevel));

        Spannable word = new SpannableString(String.format("%d/", referralCount.getTotal_energy()));
        word.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorO)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_level_value.setText(word);

        Spannable wordTwo = new SpannableString(String.valueOf(energyLimit));
        wordTwo.setSpan(new ForegroundColorSpan(Color.BLACK), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_level_value.append(wordTwo);

        swipe_redeem_view.setLocked(!referralCount.is_deb_redeemable);
        swipe_redeem_view.setText(!referralCount.is_deb_redeemable ? "Redeemed for today" : "Swipe to redeem");


        if (referralCount.ts_redeemable_energy == 0) {
            btn_redeem_1.setEnabled(false);
            btn_redeem_1.setAlpha(0.3f);
        }

        if (referralCount.tsu_redeemable_energy == 0) {
            btn_redeem_2.setEnabled(false);
            btn_redeem_2.setAlpha(0.3f);
        }

        if (referralCount.osu_redeemable_energy == 0) {
            btn_redeem_3.setEnabled(false);
            btn_redeem_3.setAlpha(0.3f);
        }

        btn_redeem_1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                redeemEnergyPresenter.redeemEnergy("totalshare");
            }
        });

        btn_redeem_2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                redeemEnergyPresenter.redeemEnergy("todaysignup");
            }
        });

        btn_redeem_3.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                redeemEnergyPresenter.redeemEnergy("overallsignup");
            }
        });

        swipe_redeem_view.setOnSlideCompleteListener(new SlideToActView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(@NotNull SlideToActView slideToActView) {
                redeemEnergyPresenter.redeemEnergy("daily");
            }
        });
    }

    @Override
    public void onReceiveEnergyLevel(ArrayList<LevelResult> levelResults) {
        refreshLayout.setRefreshing(false);
        if(levelResults.size() == 0)
            return;

        EnergyLevelAdapter energyLevelAdapter = new EnergyLevelAdapter(this, levelResults);
        recyclerView.setAdapter(energyLevelAdapter);
    }

    @Override
    public void onRedeemEnergy(Boolean status, String message, int dailyEnergyRedeemed) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        swipe_redeem_view.resetSlider();
        redeemEnergyPresenter.retrieveEnergySummary();
    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public class EnergyLevelAdapter extends RecyclerView.Adapter<EnergyLevelAdapter.ViewHolder> {

        private ArrayList<LevelResult> levelResults;
        private Context context;

        public EnergyLevelAdapter(Context context, ArrayList<LevelResult> levelResults) {
            this.levelResults = levelResults;
            this.context = context;
        }

        @Override
        public EnergyLevelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_redeem_level, parent, false);
            return new EnergyLevelAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(EnergyLevelAdapter.ViewHolder holder, int position) {
            LevelResult levelResult = levelResults.get(position);

            holder.txtCompletedLevel.setText("Completed Level " + levelResult.getLevel());
            holder.txtDate.setText(levelResult.getStart_date() + " - " + levelResult.getEnd_date());
            if(levelResult.getReferral_count() != 0){
                holder.txtReferred.setVisibility(View.VISIBLE);
                holder.txtReferred.setText(String.format("Referral %d friends", levelResult.getReferral_count()));
            }else{
                holder.txtReferred.setVisibility(View.GONE);
            }

            Spannable word = new SpannableString(String.format("%d/", levelResult.getLevel_points()));
            word.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorO)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.txtEnergyCount.setText(word);

            Spannable wordTwo = new SpannableString(String.valueOf(levelResult.getLevel_points()));
            wordTwo.setSpan(new ForegroundColorSpan(Color.BLACK), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.txtEnergyCount.append(wordTwo);

            holder.itemView.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    startActivity(new Intent(TLRedeemEnergy.this, TLEnergyHistory.class));
                }
            });
        }

        @Override
        public int getItemCount() {
            return levelResults.size() < 2 ? levelResults.size() : 2;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView txtCompletedLevel;
            private TextView txtDate;
            private TextView txtReferred;
            private TextView txtEnergyCount;

            public ViewHolder(View itemView) {
                super(itemView);
                txtCompletedLevel = itemView.findViewById(R.id.txt_completed_level);
                txtDate = itemView.findViewById(R.id.txt_date);
                txtReferred = itemView.findViewById(R.id.txt_referred);
                txtEnergyCount = itemView.findViewById(R.id.txt_energy);
            }
        }
    }
}
