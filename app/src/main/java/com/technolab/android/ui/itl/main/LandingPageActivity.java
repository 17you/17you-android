package com.technolab.android.ui.itl.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.core.app.ActivityCompat;

import com.technolab.android.BuildConfig;
import com.technolab.android.R;
import com.technolab.android.db.DBController;
import com.technolab.android.security.KeyOne;
import com.technolab.android.security.SecOne;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLSignInEmail;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp.TLSignUp;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.utility.GlobalVariables;
import com.technolab.android.utility.TLConstant;
import com.technolab.android.utility.Utility;

import java.io.File;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class LandingPageActivity extends AbstractActivity {


    private Button btnSignIn;
    private Button btnSignUp;


    public LandingPageActivity() {
        TAG = "Landing Page";
        layoutID = R.layout.activity_landing_page;
        statusBarColorID = R.color.colorO;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        this.btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        this.btnSignUp = (Button) findViewById(R.id.btn_sign_up);

        if (BuildConfig.DEBUG)
            Log.d(this.getLocalClassName(), "Init");

        btnSignIn.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(LandingPageActivity.this, TLSignInEmail.class));
            }
        });

        btnSignUp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(LandingPageActivity.this, TLSignUp.class));
            }
        });
    }
}
