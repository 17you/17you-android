package com.technolab.android.ui.itl.spine;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.technolab.android.R;
import com.technolab.android.di.component.ApplicationComponent;
import com.technolab.android.ui.itl.main.MainApplication;
import com.technolab.android.ui.itl.utils.ProgressDialogUtils;
import com.technolab.android.utility.GlobalVariables;
import com.technolab.android.utility.Utility;

import java.util.ArrayList;
import java.util.Map;


public class AbstractActivity extends AppCompatActivity implements AbstractMvpView {

    protected String TAG = "";
    protected Integer layoutID;
    protected int statusBarColorID = 0;

    protected ImageView imgLeft1;
    protected ImageView imgRight1;
    protected ImageView imgRight2;
    protected TextView txtHeader;
    protected TextView txtSubHeader;
    protected TextView txtTemp;

    protected SwipeRefreshLayout refreshLayout;

    private ProgressDialog mProgressDialog;
    private ProgressBar progressBar;

    protected ArrayList<AbstractActivity> activeActivityArrayList;
    protected Map<String, ArrayList<AbstractActivity>> cacheActivityMap;


    public ApplicationComponent getAppComponent() {
        return MainApplication.getApplicationComponent(this);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        updateStatusBarColor(Utility.getHexColorCode(this, statusBarColorID == 0 ? R.color.colorD : statusBarColorID));
        super.onCreate(savedInstanceState);
        setContentView(layoutID);
        Log.d(TAG, "Init");


    }

    protected void updateStatusBarColor(String color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

    protected void setupToolbar(AbstractActivity activity){
        imgLeft1 = activity.findViewById(R.id.btn_left_1);
        imgRight1 = activity.findViewById(R.id.btn_right_1);
        imgRight2 = activity.findViewById(R.id.btn_right_2);
        txtHeader = activity.findViewById(R.id.txtHeader);
        txtSubHeader = activity.findViewById(R.id.txtSubHeader);
        txtTemp = activity.findViewById(R.id.temp_text);
    }

    protected void setupRefresh(AbstractActivity activity){
        refreshLayout = activity.findViewById(R.id.swipe_refresh_layout);
    }

    /**
     *  This method to show the loading spinner for the fragment
     */
    @Override
    public void showLoadingSpinner() {
        hideLoadingSpinner();
        mProgressDialog = ProgressDialogUtils.showLoadingDialog(this);
    }

    /**
     * This method to hide the loading spinner
     */
    @Override
    public void hideLoadingSpinner() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }


}
