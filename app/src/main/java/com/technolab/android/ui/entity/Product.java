package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable {
    @SerializedName("product_id")
    private String product_id;
    @SerializedName("product_name")
    private String product_name;
    @SerializedName("product_description")
    private String product_description;
    @SerializedName("product_image")
    private String product_image;
    @SerializedName("product_main_cat_id")
    private int product_main_cat_id;
    @SerializedName("product_sub_cat_id")
    private int product_sub_cat_id;
    @SerializedName("product_main_cat_name")
    private String product_main_cat_name;
    @SerializedName("product_sub_cat_name")
    private String product_sub_cat_name;
    @SerializedName("merchant_id")
    private int merchant_id;
    @SerializedName("product_status")
    private boolean product_status;
    @SerializedName("variation")
    private ArrayList<Variation> variationArrayList;
    @SerializedName("product_variation")
    private ArrayList<Variation> variations;
    @SerializedName("is_selected")
    private boolean isSelected;

    private boolean status;
    private int sequence;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getSequence() {
        return sequence;
    }

    public void setStatus(int sequence) {
        this.sequence = sequence;
    }


    public String getProduct_main_cat_name() {
        return product_main_cat_name;
    }

    public void setProduct_main_cat_name(String product_main_cat_name) {
        this.product_main_cat_name = product_main_cat_name;
    }

    public String getProduct_sub_cat_name() {
        return product_sub_cat_name;
    }

    public void setProduct_sub_cat_name(String product_sub_cat_name) {
        this.product_sub_cat_name = product_sub_cat_name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ArrayList<Variation> getVariations() {
        return variations;
    }

    public void setVariations(ArrayList<Variation> variations) {
        this.variations = variations;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public int getProduct_main_cat_id() {
        return product_main_cat_id;
    }

    public void setProduct_main_cat_id(int product_main_cat_id) {
        this.product_main_cat_id = product_main_cat_id;
    }

    public int getProduct_sub_cat_id() {
        return product_sub_cat_id;
    }

    public void setProduct_sub_cat_id(int product_sub_cat_id) {
        this.product_sub_cat_id = product_sub_cat_id;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public boolean isProduct_status() {
        return product_status;
    }

    public void setProduct_status(boolean product_status) {
        this.product_status = product_status;
    }

    public ArrayList<Variation> getVariationArrayList() {
        return variationArrayList;
    }

    public void setVariationArrayList(ArrayList<Variation> variationArrayList) {
        this.variationArrayList = variationArrayList;
    }
}
