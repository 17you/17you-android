package com.technolab.android.ui.itl.main.LoginManagement.presenter;

import com.technolab.android.error.TLError;
import com.technolab.android.ui.itl.spine.AbstractPresenter;
import com.technolab.android.user.TLStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class TLSignUpPresenter extends AbstractPresenter<TLSignUpMvpView> {

    @Inject
    public TLSignUpPresenter() {

    }

    public void signUp(String username, String password, String phoneNumber, String email, ArrayList<Integer> industryList, int stateID) {
        getMvpView().showLoadingSpinner();
        JSONObject object = new JSONObject();
        JSONArray industryArray = new JSONArray();
        for (Integer value: industryList) {
            industryArray.put(value);
        }

        try {
            object.put("email", email);
            object.put("password", password);
            object.put("username", username);
            object.put("phone", phoneNumber);
            object.put("industry", industryArray);
            if(TLStorage.getInstance().getReferrerCode() != null)
                object.put("referrer", TLStorage.getInstance().getReferrerCode());
            object.put("state", stateID);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));

        mCompositeDisposable.add(
                mDataRepositoryManager
                        .register(requestBody)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(baseResponse -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            getMvpView().hideLoadingSpinner();
                            if (baseResponse != null && baseResponse.getStatus())
                                getMvpView().onRegistrationSuccess(baseResponse.getMessage());

                        }, throwable -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            TLError error = TLError.checkForError(throwable);
                            if (error != null)
                            {
                                System.out.println("Testing :"+error.getErrorCode());
                                System.out.println("Testing :"+error.getErrorMessage());
                            }

                            getMvpView().hideLoadingSpinner();
                            getMvpView().onRegistrationFailed(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                        }));
    }

    public void checkMobileAndEmail(String mobile, String email){

        getMvpView().showLoadingSpinner();
        JSONObject object = new JSONObject();

        try {
            object.put("email", email);
            object.put("phone", mobile);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));

        mCompositeDisposable.add(
                mDataRepositoryManager
                        .checkEmailAndPhone(requestBody)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(baseResponse -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            getMvpView().hideLoadingSpinner();
                            if (baseResponse != null && baseResponse.getStatus())
                                getMvpView().onRegistrationSuccess(baseResponse.getMessage());

                        }, throwable -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            TLError error = TLError.checkForError(throwable);
                            if (error != null)
                            {
                                System.out.println("Testing :"+error.getErrorCode());
                                System.out.println("Testing :"+error.getErrorMessage());
                            }

                            getMvpView().hideLoadingSpinner();
                            getMvpView().onRegistrationFailed(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                        }));
    }


    public void fetchIndustries(){
        getMvpView().showLoadingSpinner();
        mCompositeDisposable.add(
                mDataRepositoryManager
                        .retrieveIndustries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(baseResponse -> {
                    if (!isViewAttached()) {
                        return;
                    }
                    getMvpView().hideLoadingSpinner();
                    if (baseResponse != null && baseResponse.getStatus())
                        getMvpView().onReceiveIndustryInfo(baseResponse.getIndustry());
                },throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }
                    TLError error = TLError.checkForError(throwable);
                    if (error != null)
                    {
                        System.out.println("Testing :"+error.getErrorCode());
                        System.out.println("Testing :"+error.getErrorMessage());
                    }

                    getMvpView().hideLoadingSpinner();
                    getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                }));
    }

    public void fetchStates(){
        getMvpView().showLoadingSpinner();
        mCompositeDisposable.add(
                mDataRepositoryManager
                        .getStates()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(baseResponse -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            getMvpView().hideLoadingSpinner();
                            if (baseResponse != null && baseResponse.getStatus())
                                getMvpView().onReceiveStates(baseResponse.getStateArrayList());
                        },throwable -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            TLError error = TLError.checkForError(throwable);
                            if (error != null)
                            {
                                System.out.println("Testing :"+error.getErrorCode());
                                System.out.println("Testing :"+error.getErrorMessage());
                            }

                            getMvpView().hideLoadingSpinner();
                            getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                        }));
    }
}
