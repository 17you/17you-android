package com.technolab.android.ui.entity;

public class ReferralEmailResult {
    public String mask_email;
    public String member_signed_up_date_and_time;
    public int total_referred;
    public int total_booster;


    public int getTotal_booster() {
        return total_booster;
    }

    public void setTotal_booster(int total_booster) {
        this.total_booster = total_booster;
    }

    public String getMask_email() {
        return mask_email;
    }

    public void setMask_email(String mask_email) {
        this.mask_email = mask_email;
    }

    public String getMember_signed_up_date_and_time() {
        return member_signed_up_date_and_time;
    }

    public void setMember_signed_up_date_and_time(String member_signed_up_date_and_time) {
        this.member_signed_up_date_and_time = member_signed_up_date_and_time;
    }

    public int getTotal_referred() {
        return total_referred;
    }

    public void setTotal_referred(int total_referred) {
        this.total_referred = total_referred;
    }


}
