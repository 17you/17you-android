package com.technolab.android.ui.itl.utils.drawer.utils;

public abstract class TLDrawerUtil {
    public static float evaluate(float fraction, float startValue, float endValue) {
        return startValue + fraction * (endValue - startValue);
    }
}
