package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.LevelResult;
import com.technolab.android.ui.entity.ReferralContent;
import com.technolab.android.ui.entity.ReferralCount;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

public class TLEnergyHistory extends AbstractActivity implements TLMainMvpView.RedeemEnergy {

    private RecyclerView recyclerView;

    @Inject
    TLMainPresenter.TLRedeemEnergy redeemEnergyPresenter;

    public TLEnergyHistory (){
        TAG = "Energy History Activity";
        layoutID = R.layout.activity_energy_history;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        recyclerView = findViewById(R.id.recycler_view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLEnergyHistory.this);
        redeemEnergyPresenter.onAttach(this);
        redeemEnergyPresenter.retrieveEnergyHistory();
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);

        txtHeader.setText("Energy History");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }

    @Override
    public void onReceiveEnergyData(ReferralCount referralCount) {

    }

    @Override
    public void onReceiveEnergyLevel(ArrayList<LevelResult> levelResults) {
        if(levelResults.size() == 0)
            return;

        EnergyHistoryAdapter energyLevelAdapter = new EnergyHistoryAdapter(this, levelResults);
        recyclerView.setAdapter(energyLevelAdapter);
    }

    @Override
    public void onRedeemEnergy(Boolean status, String message, int energy) {

    }

    @Override
    public void onError(String message) {

    }


    public class EnergyHistoryAdapter extends RecyclerView.Adapter<EnergyHistoryAdapter.ViewHolder> {

        private ArrayList<LevelResult> levelResults;
        private Context context;

        public EnergyHistoryAdapter(Context context, ArrayList<LevelResult> levelResults) {
            this.levelResults = levelResults;
            this.context = context;
        }

        @Override
        public EnergyHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_energy_history, parent, false);
            return new EnergyHistoryAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(EnergyHistoryAdapter.ViewHolder holder, int position) {
            LevelResult levelResult = levelResults.get(position);

            holder.txtLevel.setText(String.format("Level %d", levelResult.getLevel()));
            if(levelResult.isIs_completed())
                holder.txtLevelValue.setText(levelResult.getLevel_points()+"/"+levelResult.getLevel_points());

            holder.txtTopDailyRewardValue.setText(levelResult.getTotal_daily_reward() != null ? levelResult.getTotal_daily_reward() : "0");

            Map<String, List<ReferralContent>> map = new HashMap<>();
            for (ReferralContent referralContent: levelResult.getReferral_content()) {
                String key = referralContent.getMember_signed_up_date_and_time();
                if (map.get(key) == null) {
                    map.put(key, new ArrayList<>());
                }
                map.get(key).add(referralContent);
            }

            for (String key : map.keySet()) {
                LayoutInflater layoutInflater = LayoutInflater.from(context);
                ConstraintLayout mainItem = (ConstraintLayout) layoutInflater.inflate(R.layout.view_energy_history_item, null);

                ((TextView)mainItem.findViewById(R.id.txt_date)).setText(key);
                LinearLayout containerEmailList = mainItem.findViewById(R.id.container_email_list);
                TextView txtTotalEnergy = mainItem.findViewById(R.id.txt_total_energy);

                int totalEnergy = 0;
                for(ReferralContent referralContent : map.get(key)){
                    LayoutInflater layoutInflater2 = LayoutInflater.from(context);
                    LinearLayout emailItem = (LinearLayout) layoutInflater2.inflate(R.layout.view_text_email, null);
                    ((TextView)emailItem.findViewById(R.id.tv_masked_email)).setText(referralContent.getMask_email());
                    totalEnergy += referralContent.getEnergy();
                    containerEmailList.addView(emailItem);
                }
                txtTotalEnergy.setText(String.valueOf(totalEnergy));

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 15, 0, 15);
                mainItem.setLayoutParams(params);
                holder.containerContent.addView(mainItem);
            }
        }

        @Override
        public int getItemCount() {
            return levelResults.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView txtLevel;
            private TextView txtLevelValue;
            private LinearLayout containerContent;
            private TextView txtTopDailyRewardValue;
            private TextView txtTopRankingRewardValue;


            public ViewHolder(View itemView) {
                super(itemView);
                txtLevel = itemView.findViewById(R.id.txt_level);
                txtLevelValue = itemView.findViewById(R.id.txt_level_value);
                containerContent = itemView.findViewById(R.id.container_content);
                txtTopDailyRewardValue = itemView.findViewById(R.id.txt_top_daily_reward_value);
                txtTopRankingRewardValue = itemView.findViewById(R.id.txt_top_ranking_reward_value);
            }
        }
    }
}
