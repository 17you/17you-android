package com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.db.SettingsController;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.State;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignUpMvpView;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignUpPresenter;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLSignInEmail;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLEnvironment;
import com.technolab.android.user.TLStorage;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLTellUsIndustry extends AbstractActivity implements TLSignUpMvpView,IndustryAdapter.ItemClickListener {

    @Inject
    TLSignUpPresenter mTLSignUpPresenter;

    ArrayList<Integer> industryList;
    RecyclerView industriesRecyclerView;
    IndustryAdapter industryAdapter;
    Button btnSubmit;

    public TLTellUsIndustry() {
        TAG = "Tell Us";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_tell_us;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnSubmit = findViewById(R.id.btnSubmit);
        industryList = new ArrayList<>();
        industriesRecyclerView  = findViewById(R.id.recycler_view);
        btnSubmit.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTLSignUpPresenter.signUp(getIntent().getStringExtra("username"), getIntent().getStringExtra("password"), getIntent().getStringExtra("phone"), getIntent().getStringExtra("email"), industryList, getIntent().getIntExtra("state", 0));
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLTellUsIndustry.this);
        mTLSignUpPresenter.onAttach(this);
        mTLSignUpPresenter.fetchIndustries();
    }

    @Override
    public void onRegistrationSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(TLTellUsIndustry.this, TLSignInEmail.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    public void onRegistrationFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReceiveIndustryInfo(ArrayList<Industry> industry) {
        btnSubmit.setVisibility(View.VISIBLE);
        industryAdapter = new IndustryAdapter(this, industry);
        industriesRecyclerView.setAdapter(industryAdapter);
        industryAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onReceiveStates(ArrayList<State> stateArrayList) {

    }


    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTLSignUpPresenter.onDetach();
    }

    @Override
    public void onItemClicked(Integer id, String action) {
        if(action.equalsIgnoreCase("Add"))
            industryList.add(id);
        else if(action.equalsIgnoreCase("Remove"))
            industryList.remove(id);

        if(industryList.isEmpty()){
            btnSubmit.setEnabled(false);
            btnSubmit.setAlpha(0.3f);
        }else
        {
            btnSubmit.setEnabled(true);
            btnSubmit.setAlpha(1);
        }
    }
}
