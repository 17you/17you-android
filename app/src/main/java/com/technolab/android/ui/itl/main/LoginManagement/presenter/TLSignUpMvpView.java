package com.technolab.android.ui.itl.main.LoginManagement.presenter;

import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.State;
import com.technolab.android.ui.itl.spine.AbstractMvpView;

import java.util.ArrayList;

public interface TLSignUpMvpView extends AbstractMvpView {
    void onRegistrationSuccess(String message);
    void onRegistrationFailed(String message);
    void onReceiveIndustryInfo(ArrayList<Industry> industry);
    void onReceiveStates(ArrayList<State> stateArrayList);
    void onError(String message);
}