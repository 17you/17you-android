package com.technolab.android.ui.entity;

public class EnergyLevel {
    public int energy_level;
    public int energy_level_limit;

    public int getEnergy_level() {
        return energy_level;
    }

    public void setEnergy_level(int energy_level) {
        this.energy_level = energy_level;
    }

    public int getEnergy_level_limit() {
        return energy_level_limit;
    }

    public void setEnergy_level_limit(int energy_level_limit) {
        this.energy_level_limit = energy_level_limit;
    }
}
