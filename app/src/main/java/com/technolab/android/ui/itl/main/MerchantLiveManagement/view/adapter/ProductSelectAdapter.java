package com.technolab.android.ui.itl.main.MerchantLiveManagement.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.Variation;

import java.util.ArrayList;

public class ProductSelectAdapter extends RecyclerView.Adapter<ProductSelectAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Product> productArrayList;
    private ProductSelectAdapter.ItemClickListener mCallback;

    public ProductSelectAdapter(Context context, ArrayList<Product> productArrayList){
        this.context = context;
        this.productArrayList = productArrayList;
    }

    @NonNull
    @Override
    public ProductSelectAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_merchant_live_product_setup, parent, false);
        return new ProductSelectAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductSelectAdapter.ViewHolder holder, int position) {
        Product product = productArrayList.get(position);

        Glide.with(context)
                .load(product.getProduct_image())
                .thumbnail(0.1f)
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.ivProduct);

        holder.productTitle.setText(product.getProduct_name());
        holder.productDesc.setText(product.getProduct_description());

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(product.getVariationArrayList() != null) {
            for (Variation variation : product.getVariationArrayList()) {
                View child = inflater.inflate(R.layout.view_text_variation, null);
                TextView txtVariation = (TextView) child.findViewById(R.id.tv_variation);
                txtVariation.setText(String.format("%s   RM%.2f", variation.getProduct_variation_size(), variation.getProduct_variation_price()));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(0, 10, 0, 0);
                child.setLayoutParams(params);
                holder.variationContainer.addView(child);
            }
        }

        holder.checkBox.setChecked(product.getStatus());

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCallback != null) {
                    mCallback.onItemClicked(product, isChecked ? "Add" : "Remove");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox checkBox;
        ImageView ivProduct;
        TextView productTitle;
        TextView productDesc;
        LinearLayout variationContainer;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBox = itemView.findViewById(R.id.iv_three_dots);
            ivProduct = itemView.findViewById(R.id.iv_product);
            productTitle = itemView.findViewById(R.id.tv_product_title);
            productDesc = itemView.findViewById(R.id.tv_product_desc);
            variationContainer = itemView.findViewById(R.id.container_variation);
        }
    }

    public void setOnItemClickListener(final ProductSelectAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(Product product, String action);
    }
}
