package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.TopLiveStream;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

public class TopLiveStreamAdapter extends RecyclerView.Adapter<TopLiveStreamAdapter.ViewHolder> {

    private ArrayList<TopLiveStream> topLiveStreams;
    private Context context;
    private TopLiveStreamAdapter.ItemClickListener mCallback;

    public TopLiveStreamAdapter(Context context, ArrayList<TopLiveStream> topLiveStreams) {
        this.topLiveStreams = topLiveStreams;
        this.context = context;
    }

    @Override
    public TopLiveStreamAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_top_live_stream, parent, false);
        return new TopLiveStreamAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TopLiveStream topLiveStream = topLiveStreams.get(position);

        Glide.with(context)
                .load(topLiveStream.getLive_stream_image())
                .thumbnail(0.1f)
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.imageView);

        holder.txtTitle.setText(topLiveStream.getLive_stream_title());
        holder.txtTotalView.setText(String.valueOf(topLiveStream.getLive_stream_count()));
        holder.txtTotalBuyers.setText(String.valueOf(topLiveStream.getTotal_buyers()));
        holder.txtTotalSales.setText(String.format("RM%s", topLiveStream.getTotal_sale()));

        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(mCallback != null)
                    mCallback.onItemClicked();
            }
        });
    }

    @Override
    public int getItemCount() {
        return topLiveStreams.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private TextView txtTotalView;
        private TextView txtTotalBuyers;
        private TextView txtTotalSales;
        private ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_live_stream);
            txtTitle = itemView.findViewById(R.id.txt_title);
            txtTotalView = itemView.findViewById(R.id.txt_total_view);
            txtTotalBuyers = itemView.findViewById(R.id.txt_total_buyers);
            txtTotalSales = itemView.findViewById(R.id.txt_total_sales);
        }
    }
    public void setOnItemClickListener(final TopLiveStreamAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked();
    }
}