package com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter;

import com.technolab.android.error.TLError;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.spine.AbstractPresenter;
import com.technolab.android.user.TLStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TLMerchantLiveManagementPresenter {
    public static class TLMerchantLiveStreamPresenter extends AbstractPresenter<TLMerchantLiveManagementMvpView.LiveStreamMVP> {

        @Inject
        public TLMerchantLiveStreamPresenter() {
        }

        public void fetchLiveStreams() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveMerchantLiveStreams(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onRetrieveLiveStreams(baseResponse.getLiveStreamArrayList());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void fetchProductList() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveMerchantProduct(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onRetrieveProductList(baseResponse.getProductArrayList());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void uploadLiveStream(String title,
                                     String desc, String date,
                                     String startTime, String endTime,
                                     ArrayList<Product> product, String status, String shippingFee, String liveStreamImage) {

            getMvpView().showLoadingSpinner();
            MultipartBody.Part liveStreamFilePart = null;
            if(liveStreamImage != null) {
                File liveStreamFile = new File(liveStreamImage);
                RequestBody liveStreamRB = RequestBody.create(MediaType.parse("image/png"), liveStreamFile);
                liveStreamFilePart = MultipartBody.Part.createFormData("file_livestream_image", liveStreamFile.getName(), liveStreamRB);
            }

            JSONArray productArray = new JSONArray();
            for (int i = 0; i < product.size(); i++) {
                try {
                    JSONObject objects = new JSONObject();
                    objects.put("product_id", product.get(i).getProduct_id());
                    objects.put("sequence", i + 1);
                    productArray.put(objects);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .uploadLiveStream(TLStorage.getInstance().getMerchantId(),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, title),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, desc),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, date),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, startTime),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, endTime),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, productArray.toString()),
                                    RequestBody.create(MultipartBody.FORM, status),
                                    RequestBody.create(MultipartBody.FORM, shippingFee),
                                    liveStreamFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onResponse(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void updateLiveStream(String liveStreamID, String title,
                                     String desc, String date,
                                     String startTime, String endTime,
                                     ArrayList<Product> product, String status, String shippingFee, String liveStreamImage) {

            getMvpView().showLoadingSpinner();
            MultipartBody.Part liveStreamFilePart = null;
            if (liveStreamImage != null) {
                File liveStreamFile = new File(liveStreamImage);
                RequestBody liveStreamRB = RequestBody.create(MediaType.parse("image/png"), liveStreamFile);
                liveStreamFilePart = MultipartBody.Part.createFormData("file_livestream_image", liveStreamFile.getName(), liveStreamRB);
            }


            JSONArray productArray = new JSONArray();
            for (int i = 0; i < product.size(); i++) {
                try {
                    JSONObject objects = new JSONObject();
                    objects.put("product_id", product.get(i).getProduct_id());
                    objects.put("sequence", i + 1);
                    productArray.put(objects);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .updateLiveStream(liveStreamID, TLStorage.getInstance().getMerchantId(),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, title),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, desc),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, date),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, startTime),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, endTime),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, productArray.toString()),
                                    RequestBody.create(MultipartBody.FORM, status),
                                    RequestBody.create(MultipartBody.FORM, shippingFee),
                                    liveStreamFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onResponse(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));

        }

        public void fetchSingleLiveStream(String liveStreamID) {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveSingleLiveStream(liveStreamID)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onFetchData(baseResponse.getLiveStream());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void deleteLiveStream(String liveStreamID) {
            getMvpView().showLoadingSpinner();

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .deleteLiveStream(liveStreamID)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onDelete(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void publishLiveStream(String liveStreamID) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("status", "PUBLISHED");
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .publishLiveStream(liveStreamID, requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onPublish(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }
    }

    public static class TLLiveStreamingPresenter extends AbstractPresenter<TLMerchantLiveManagementMvpView.LiveStreamingMVP> {
        @Inject
        public TLLiveStreamingPresenter() {
        }

        public void startLiveStream(String liveStreamID, String broadcastID) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();

            try {
                object.put("bambuserid", broadcastID);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(String.valueOf(object), MediaType.parse("raw"));

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .startLiveStream(liveStreamID, requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onResponse(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void stopLiveStream(String liveStreamID, String broadcastID, long totalCount, long timeRemaining) {
            JSONObject object = new JSONObject();
            try {
                object.put("bambuserid", broadcastID);
                object.put("livestreamcount", totalCount);
                object.put("remaininglivetime", timeRemaining);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(String.valueOf(object), MediaType.parse("raw"));

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .stopLiveStream(liveStreamID, requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onStop(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void retrieveProduct(String liveStreamID) {
            JSONObject object = new JSONObject();

            try {
                object.put("merchantid", TLStorage.getInstance().getMerchantId());
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(String.valueOf(object), MediaType.parse("raw"));

            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveLiveStreamProduct(liveStreamID, requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onProductResponse(baseResponse.getLiveStreamProduct());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void updateLiveStreamProduct(String liveStreamID, ArrayList<Product> product) {
            getMvpView().showLoadingSpinner();
            JSONArray productArray = new JSONArray();
            for (int i = 0; i < product.size(); i++) {
                try {
                    JSONObject objects = new JSONObject();
                    objects.put("product_id", product.get(i).getProduct_id());
                    objects.put("status", product.get(i).getStatus());
                    objects.put("sequence", i + 1);
                    productArray.put(objects);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            JSONObject finalObject = new JSONObject();
            try {
                finalObject.put("product", productArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(finalObject), MediaType.parse("raw"));

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .updateLiveStreamProduct(liveStreamID, requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onProductUpdate(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }
}
