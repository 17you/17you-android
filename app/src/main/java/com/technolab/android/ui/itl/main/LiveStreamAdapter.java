package com.technolab.android.ui.itl.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ShareCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import org.w3c.dom.Text;

import java.util.List;

public class LiveStreamAdapter extends RecyclerView.Adapter<LiveStreamAdapter.ViewHolder> {

    private LiveStreamAdapter.ItemClickListener mCallback;
    private List<LiveStream> liveStreamList;
    private Activity activity;
    private boolean isClickEnabled;

    public LiveStreamAdapter(Activity activity, List<LiveStream> liveStreamList, boolean isClickEnabled) {
        this.liveStreamList = liveStreamList;
        this.activity = activity;
        this.isClickEnabled = isClickEnabled;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_live_stream_item, parent, false);
        return new LiveStreamAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LiveStream liveStream = liveStreamList.get(position);

        Glide.with(activity)
                .load(liveStream.getLive_stream_image())
                .thumbnail(0.1f)
                .placeholder(activity.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.img_live_placeholder);

        holder.txt_merchant_title.setText(liveStream.getMerchant_name());

        holder.txt_live_stream_timing.setText(String.format("%s, %s", liveStream.getLive_stream_date(), liveStream.getLive_stream_start_time()));
        holder.txt_live_stream_title.setText(liveStream.getLive_stream_title());
        holder.txt_live_stream_desc.setText(liveStream.getLive_stream_description());
        holder.txt_merchant_state.setText(liveStream.getMerchant_state());

        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onItemClicked(String.valueOf(liveStream.getLive_stream_id()));
                }
            }
        });

        holder.btn_share.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String title = liveStream.getLive_stream_title();
                String desc = liveStream.getLive_stream_description();
                Uri longURL = DeepLinkUtils.createDeepLink(String.valueOf(liveStream.getLive_stream_id()), "live");
                DeepLinkUtils.shortTheLink(longURL, activity, "live");
                Uri shortURL = DeepLinkUtils.getShortLink();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, String.format("%s\n%s", String.format("Watch 17You's Live : %s - %s.", title, desc), shortURL.toString()));

                activity.startActivity(Intent.createChooser(i, "Share URL"));
            }
        });

        holder.container_live_now.setVisibility(isClickEnabled ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return liveStreamList != null ? liveStreamList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img_live_placeholder;
        TextView txt_merchant_title;
        TextView txt_live_stream_timing;
        TextView txt_live_stream_title;
        TextView txt_live_stream_desc;
        TextView txt_merchant_state;
        Button btn_share;
        LinearLayout container_live_now;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_live_placeholder = itemView.findViewById(R.id.img_live_placeholder);
            txt_merchant_title = itemView.findViewById(R.id.txt_merchant_title);
            txt_live_stream_timing = itemView.findViewById(R.id.txt_live_stream_timing);
            txt_live_stream_title = itemView.findViewById(R.id.txt_live_stream_title);
            txt_live_stream_desc = itemView.findViewById(R.id.txt_live_stream_desc);
            txt_merchant_state = itemView.findViewById(R.id.txt_merchant_state);
            btn_share = itemView.findViewById(R.id.btn_share);
            container_live_now = itemView.findViewById(R.id.container_live_now);
        }
    }

    public void setOnItemClickListener(final LiveStreamAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(String id);
    }
}
