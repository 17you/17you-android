package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.mackhartley.roundedprogressbar.RoundedProgressBar;
import com.technolab.android.R;
import com.technolab.android.db.SettingsController;
import com.technolab.android.ui.entity.DetailedTransaction;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Menu;
import com.technolab.android.ui.entity.MerchantEnergyInfo;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.TopLiveStream;
import com.technolab.android.ui.entity.TransactionHistory;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLCreateLiveStream;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLLiveManagement;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.RoundedCornerLayout;
import com.technolab.android.ui.itl.utils.TLActionSheet;
import com.technolab.android.ui.itl.utils.drawer.TLDrawerLayout;
import com.technolab.android.ui.itl.utils.drawer.TLDrawerLayoutBuilder;
import com.technolab.android.user.TLStorage;
import com.technolab.android.utility.GlobalVariables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

public class MerchantMainActivity extends AbstractActivity implements MenuListAdapter.MenuClickListener, TLMainMvpView.MerchantMVP, TopLiveStreamAdapter.ItemClickListener {

    private ArrayList<AbstractActivity> activeActivityArrayList;
    private Map<String, ArrayList<AbstractActivity>> cacheActivityMap;
    private RoundedCornerLayout rounded_corner_layout;
    private ImageView imgProfile;
    private TextView profileName;

    private ImageView imgLiveStream;
    private TextView tvProductTitle;
    private TextView tvProductDesc;
    private TextView tvDateTime;

    private RecyclerView recyclerViewTopLive;
    private LottieAnimationView pbLoadingTopLiveStream;
    private FrameLayout containerTopLiveStream;

    private LinearLayout noDataContainer;
    private LottieAnimationView pbLoadingUpcoming;
    private TopLiveStreamAdapter topLiveStreamAdapter;

    private TextView txtFilterTransactionHistory;
    private RecyclerView recyclerViewTransHistory;
    private TransactionHistoryAdapter transactionHistoryAdapter;
    private LinearLayout transactionHistoryContainer;
    private LottieAnimationView pbLoadingTransHistory;
    private FrameLayout mainContainerTransHistory;
    private Button btnCreateLiveStream;

    private RoundedProgressBar roundedProgressBar;

    @Inject
    TLMainPresenter.TLMerchantPresenter merchantPresenter;

    public MerchantMainActivity() {
        TAG = "Merchant Main Activity";
        layoutID = R.layout.activity_merchant_main;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        setupRefresh(this);
        cacheActivityMap = new HashMap<String, ArrayList<AbstractActivity>>();
        roundedProgressBar = findViewById(R.id.rounded_progress_bar);
        rounded_corner_layout = findViewById(R.id.rounded_corner_layout);
        rounded_corner_layout.setRadius(25f);
        rounded_corner_layout.setCornerEnabled(true, true, true, true);

        recyclerViewTopLive = findViewById(R.id.recycler_view_top_live);
        pbLoadingTopLiveStream = findViewById(R.id.pb_loading_top_live);
        containerTopLiveStream = findViewById(R.id.container_top_live_stream);

        txtFilterTransactionHistory = findViewById(R.id.txt_filter);
        recyclerViewTransHistory = findViewById(R.id.recycler_view_trans_history);
        transactionHistoryContainer = findViewById(R.id.transaction_history_container);
        pbLoadingTransHistory = findViewById(R.id.pb_loading_trans_history);
        mainContainerTransHistory = findViewById(R.id.main_container_trans_history);

        imgLiveStream = findViewById(R.id.img_live_stream);
        tvProductTitle = findViewById(R.id.tv_product_title);
        tvProductDesc = findViewById(R.id.tv_product_desc);
        tvDateTime = findViewById(R.id.tv_date_time);

        noDataContainer = findViewById(R.id.container_no_data);
        pbLoadingUpcoming = findViewById(R.id.pb_loading_upcoming);
        btnCreateLiveStream = findViewById(R.id.btn_create_live_stream);

        TLDrawerLayout drawerLayout = new TLDrawerLayoutBuilder(this)
                .withViewMenuToggle(imgLeft1)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.fragment_merchant_drawer)
                .inject();

        RecyclerView menuRecyclerView = drawerLayout.findViewById(R.id.menu_recycler_view);

        MenuListAdapter menuListAdapter = new MenuListAdapter(this, true);
        menuListAdapter.setOnMenuClickListener(this);
        menuRecyclerView.setAdapter(menuListAdapter);

        drawerLayout.findViewById(R.id.container_view_profile).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(MerchantMainActivity.this, MerchantViewProfileActivity.class));
            }
        });

        profileName = drawerLayout.findViewById(R.id.txt_merchant_title);
        profileName.setText(TLStorage.getInstance().getMerchantCompanyName());
        imgProfile = drawerLayout.findViewById(R.id.img_profile);

    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> {
            GlobalVariables.getHandlerUI().postDelayed(new Runnable() {
                @Override
                public void run() {
                    merchantPresenter.fetchMerchantEnergyCount();
                }
            }, 0);

            setupUpcomingLive();
            setupTopLiveStream();
            setupTransactionHistory();
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(MerchantMainActivity.this);
        merchantPresenter.onAttach(this);
        GlobalVariables.getHandlerUI().postDelayed(new Runnable() {
            @Override
            public void run() {
                merchantPresenter.fetchMerchantEnergyCount();
            }
        }, 0);

        btnCreateLiveStream.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                merchantPresenter.isProductsAvailable();
            }
        });

        setupUpcomingLive();
        setupTopLiveStream();
        setupTransactionHistory();
    }

    private void setupUpcomingLive() {
        GlobalVariables.getHandlerUI().postDelayed(new Runnable() {
            @Override
            public void run() {
                pbLoadingUpcoming.setVisibility(View.VISIBLE);
                merchantPresenter.fetchUpcomingLive();
            }
        }, 1000);
    }

    private void setupTopLiveStream() {
        GlobalVariables.getHandlerUI().postDelayed(new Runnable() {
            @Override
            public void run() {
                pbLoadingTopLiveStream.setVisibility(View.VISIBLE);
                merchantPresenter.fetchTopLiveStream();
            }
        }, 3000);

    }

    private void setupTransactionHistory() {
        GlobalVariables.getHandlerUI().postDelayed(new Runnable() {
            @Override
            public void run() {
                pbLoadingTransHistory.setVisibility(View.VISIBLE);
                merchantPresenter.fetchTransactionHistory();
            }
        }, 4000);

    }

    @Override
    protected void onResume() {
        super.onResume();
        profileName.setText(TLStorage.getInstance().getMerchantCompanyName());
        Glide.with(this)
                .load(TLStorage.getInstance().getPrefMerchantImage())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into(imgProfile);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Welcome");
        txtSubHeader.setText(TLStorage.getInstance().getMerchantCompanyName());
        txtHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        txtSubHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

        imgRight1.setImageDrawable(getResources().getDrawable(R.drawable.ic_synchronize));
        imgRight1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
                startActivity(new Intent(MerchantMainActivity.this, UserMainActivity.class));
            }
        });
        imgRight2.setImageDrawable(getResources().getDrawable(R.drawable.ic_notification));
        imgRight2.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        imgRight2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(MerchantMainActivity.this, TLMerchantNotification.class));
            }
        });
    }

    @Override
    public void onMenuClick(Menu menu) {
        onClickMenu(menu);
    }

    private void onClickMenu(final Menu menu) {

        if (menu.getDescription() != null) {
            if (menu.getDescription().equalsIgnoreCase("Switch to User")) {
                finish();
                startActivity(new Intent(MerchantMainActivity.this, UserMainActivity.class));
                return;
            }

            if (menu.getDescription().equalsIgnoreCase("Live Management")) {
                merchantPresenter.isProductsAvailable();
                return;
            }
        }

        cacheActivityMap.clear();
        ArrayList<AbstractActivity> newList = getActivityForMenu(menu.getMenuID());
        activeActivityArrayList = newList;

        GlobalVariables.getHandlerUI().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (AbstractActivity activity : activeActivityArrayList) {
                    startActivity(new Intent(MerchantMainActivity.this, activity.getClass()));
                }
            }
        }, 100);
    }

    private ArrayList<AbstractActivity> getActivityForMenu(String mainViewID) {
        ArrayList<AbstractActivity> list = new ArrayList<AbstractActivity>();
        ArrayList<AbstractActivity> temp = cacheActivityMap.get(mainViewID);
        if (temp == null && !cacheActivityMap.containsKey(mainViewID)) {
            temp = SettingsController.getInstance().getActivityCtrlListForViewID(mainViewID);
            cacheActivityMap.put(mainViewID, temp);
        }
        if (temp != null)
            list.addAll(temp);

        return list;
    }

    @Override
    public void onFetchUpcomingLive(Boolean status, LiveStream liveStream) {
        refreshLayout.setRefreshing(false);
        pbLoadingUpcoming.setVisibility(View.GONE);

        if(!status)
            findViewById(R.id.container_main_upcoming_live).setVisibility(TLStorage.getInstance().isMerchantFresh() ? View.VISIBLE : View.GONE);

        noDataContainer.setVisibility(!status ? View.VISIBLE : View.GONE);
        rounded_corner_layout.setVisibility(status ? View.VISIBLE : View.GONE);

        rounded_corner_layout.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent clsIntent = new Intent(MerchantMainActivity.this, TLCreateLiveStream.class);
                clsIntent.putExtra("isEditMode", true);
                clsIntent.putExtra("liveStreamID", String.valueOf(liveStream.getLive_stream_id()));
                startActivity(clsIntent);
            }
        });

        if (status) {
            Glide.with(this)
                    .load(liveStream.getLive_stream_image())
                    .thumbnail(0.1f)
                    .placeholder(getResources().getDrawable(R.drawable.placeholder))
                    .into(imgLiveStream);
            tvProductTitle.setText(liveStream.getLive_stream_title());
            tvProductDesc.setText(liveStream.getLive_stream_description());
            String finalDate = "";
            String finalTime = "";
            try {
                String date = liveStream.getLive_stream_date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
                Date newDate = format.parse(date);
                format = new SimpleDateFormat("dd/MM/yyyy");
                finalDate = format.format(newDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                String startTime = liveStream.getLive_stream_start_time();
                SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
                Date newDate = format.parse(startTime);
                format = new SimpleDateFormat("hh:mm aa");
                finalTime = format.format(newDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tvDateTime.setText(String.format("%s , %s", finalDate, finalTime));
        }
    }

    @Override
    public void onFetchEnergy(MerchantEnergyInfo merchantEnergyInfo) {
        refreshLayout.setRefreshing(false);
        String[] energyType = {"1", "0", "1000", "2", "1001", "2000", "3", "2001", "3000", "4", "3001",
                "4000", "5", "4001", "5000", "6", "5001", "6000", "7", "6001", "7000", "8", "7001", "8000"};
        String energyLevel = "";
        int energyLimit = 0;
        int energy = merchantEnergyInfo.getTotal_energy();
        for (int j = 0; j < energyType.length; j++) {
            energyLevel = energyType[j];
            j = j + 1;
            int min = Integer.parseInt(energyType[j]);
            j = j + 1;
            int max = Integer.parseInt(energyType[j]);
            if (energy >= min && energy <= max) {
                energyLimit = max;
                break;
            }
        }
        ((TextView) findViewById(R.id.txt_level_progress)).setText(String.format("Level %s progress", energyLevel));
        ((TextView) findViewById(R.id.txt_level_value)).setText(String.valueOf(merchantEnergyInfo.getTotal_energy()));
        ((TextView) findViewById(R.id.txt_total_live_stream)).setText(String.format("Total %d Live Streaming", merchantEnergyInfo.getTotal_live_stream()));
        ((TextView) findViewById(R.id.txt_energy_remaining)).setText(String.format("%d/%d", merchantEnergyInfo.getTotal_energy(), energyLimit));
        ((TextView) findViewById(R.id.txt_redeem_energy_count)).setText(String.format("%skwp", merchantEnergyInfo.getTotal_energy()));

        float totalEnergy = (float) merchantEnergyInfo.getTotal_energy();
        float energyPercentage = (float) (totalEnergy / energyLimit) * 100;
        roundedProgressBar.setAnimationLength(1500);
        roundedProgressBar.setProgressPercentage(energyPercentage, true);
    }

    @Override
    public void onFetchTopLiveStream(Boolean status, ArrayList<TopLiveStream> liveStreams) {
        refreshLayout.setRefreshing(false);
        pbLoadingTopLiveStream.setVisibility(View.GONE);
        if (liveStreams == null || liveStreams.size() == 0) {
            containerTopLiveStream.setVisibility(View.GONE);
            return;
        }

        if (status && liveStreams.size() > 0) {
            recyclerViewTopLive.setVisibility(View.VISIBLE);
            topLiveStreamAdapter = new TopLiveStreamAdapter(this, liveStreams);
            recyclerViewTopLive.setAdapter(topLiveStreamAdapter);
            topLiveStreamAdapter.setOnItemClickListener(this);
        }
    }

    @Override
    public void onRetrieveProductDetails(ArrayList<Product> products) {
        if (products.size() <= 0) {
            Toast.makeText(this, "Please add products before proceeding with livestream.", Toast.LENGTH_SHORT).show();
        } else {
            startActivity(new Intent(MerchantMainActivity.this, TLLiveManagement.class));
        }
    }

    @Override
    public void onFetchTransactionHistory(Boolean status, ArrayList<TransactionHistory> transactionHistories) {
        refreshLayout.setRefreshing(false);
        pbLoadingTransHistory.setVisibility(View.GONE);
        transactionHistoryContainer.setVisibility(View.VISIBLE);

        if (transactionHistories == null || transactionHistories.size() == 0) {
            mainContainerTransHistory.setVisibility(View.GONE);
            return;
        }

        String finalDate = "";
        try {
            String date = transactionHistories.get(0).getLive_stream_date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("dd.MM.yyyy");
            finalDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtFilterTransactionHistory.setText(transactionHistories.get(0).getLive_stream_title() + "(" + finalDate + ", " + transactionHistories.get(0).getLive_stream_start_time() + ")");
        parseTransactionHistoryData(transactionHistories.get(0).getDetailed_transaction());


        TLActionSheet actionSheet = new TLActionSheet(MerchantMainActivity.this);
        for (TransactionHistory history : transactionHistories) {
            String finalDate2 = "";
            try {
                String date = history.getLive_stream_date();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
                Date newDate = format.parse(date);
                format = new SimpleDateFormat("dd.MM.yyyy");
                finalDate2 = format.format(newDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            actionSheet.addAction(history.getLive_stream_title() + "(" + finalDate2 + ", " + history.getLive_stream_start_time() + ")");
        }

        actionSheet.setEventListener(new TLActionSheet.OnEventListener() {
            @Override
            public void onActionItemClick(TLActionSheet dialog, TLActionSheet.ActionItem item, int position) {
                txtFilterTransactionHistory.setText(item.title);
                parseTransactionHistoryData(transactionHistories.get(position).getDetailed_transaction());
            }

            @Override
            public void onCancelItemClick(TLActionSheet dialog) {

            }
        });

        txtFilterTransactionHistory.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                actionSheet.setCancelVisible(false);
                actionSheet.setCanceledOnTouchOutside(true);
                actionSheet.show();
            }
        });
    }

    private void parseTransactionHistoryData(ArrayList<DetailedTransaction> detailedTransaction) {
        transactionHistoryAdapter = new TransactionHistoryAdapter(this, detailedTransaction);
        recyclerViewTransHistory.setAdapter(transactionHistoryAdapter);
        transactionHistoryAdapter.notifyDataSetChanged();
    }


    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
        pbLoadingUpcoming.setVisibility(View.GONE);
        pbLoadingTopLiveStream.setVisibility(View.GONE);
        pbLoadingTransHistory.setVisibility(View.GONE);
    }

    @Override
    public void onItemClicked() {
        onClickMenu(new Menu("14"));
    }
}
