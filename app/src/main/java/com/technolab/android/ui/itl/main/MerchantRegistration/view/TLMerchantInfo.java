package com.technolab.android.ui.itl.main.MerchantRegistration.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.ui.entity.MerchantApplication;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLSignInEmail;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationMvpView;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationPresenter;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.spine.AbstractFragment;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import javax.inject.Inject;

import static com.technolab.android.ui.itl.utils.FocusView.INIT_TYPE_PARCELABLE;
import static com.technolab.android.ui.itl.utils.FocusView.TYPE_RECTANGLE;
import static com.technolab.android.ui.itl.utils.FocusView.TYPE_SQUARE;

public class TLMerchantInfo extends AbstractActivity implements TLMerchantRegistrationMvpView.MerchantInfo {

    private Button firstButton;
    private Button secondButton;
    private Button thirdButton;
    private Button fourthButton;
    private CheckBox cbTerms;
    private Button btnSubmitDocument;

    @Inject
    TLMerchantRegistrationPresenter.TLMerchantInfoPresenter merchantRegistrationPresenter;
    public TLMerchantInfo() {
        TAG = "Merchant Info Fill Up";
        layoutID = R.layout.fragment_merchant_info;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstButton = findViewById(R.id.firstButton);
        secondButton = findViewById(R.id.secondButton);
        thirdButton = findViewById(R.id.thirdButton);
        fourthButton = findViewById(R.id.fourthButton);
        cbTerms = findViewById(R.id.cb_terms);
        btnSubmitDocument = findViewById(R.id.btn_submit_document);

        setupToolbar(this);

        firstButton.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMerchantInfo.this, TLScanICFront.class);
                intent.putExtra(INIT_TYPE_PARCELABLE, TYPE_SQUARE);
                intent.putExtra("industry_id",getIntent().getIntExtra("industry_id",0));
                startActivity(intent);
            }
        });

        secondButton.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMerchantInfo.this, TLScanSelfie.class);
                intent.putExtra(INIT_TYPE_PARCELABLE, TYPE_RECTANGLE);
                startActivity(intent);
            }
        });

        thirdButton.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLMerchantInfo.this, TLMerchantInfoFillUp.class));
            }
        });

        fourthButton.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLMerchantInfo.this, TLMerchantPayment.class));
            }
        });

        btnSubmitDocument.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(!cbTerms.isChecked()){
                    Toast.makeText(TLMerchantInfo.this, "Please check the terms and conditions before proceeding further.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(cbTerms.isChecked() && TLStorage.getInstance().isFirstStepComplete() && TLStorage.getInstance().isSecondStepComplete() && TLStorage.getInstance().isThirdStepComplete() && TLStorage.getInstance().isFourthStepComplete()){
                    merchantRegistrationPresenter.submitPrivacyPolicy();
                }
                else{
                    Toast.makeText(TLMerchantInfo.this, "Please complete the merchant application before submitting.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Business Application");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setImageDrawable(getResources().getDrawable(R.drawable.ic_round_close));
        imgRight2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLMerchantInfo.this, TLMerchantOnboardingActivity.class));
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLMerchantInfo.this);
        merchantRegistrationPresenter.onAttach(this);
        merchantRegistrationPresenter.fetchApplicationStatus(TLStorage.getInstance().getCurrentUserId());
    }

    @Override
    public void onRetrieveApplicationStatus(MerchantApplication merchantApplication) {
        TLStorage.getInstance().setFirstStepComplete(merchantApplication.isIdentificationSubmitted());
        TLStorage.getInstance().setSecondStepComplete(merchantApplication.isSelfieSubmitted());
        TLStorage.getInstance().setThirdStepComplete(merchantApplication.isBusinessDetailSubmitted());
        TLStorage.getInstance().setFourthStepComplete(merchantApplication.isLiveSubReceiptSubmitted());
        if(merchantApplication.getApplicationStatus().equalsIgnoreCase("INCOMPLETE")) {
            if (!merchantApplication.isIdentificationSubmitted()) {
                firstButton.setVisibility(View.VISIBLE);
                secondButton.setVisibility(View.GONE);
                thirdButton.setVisibility(View.GONE);
                fourthButton.setVisibility(View.GONE);
            } else if (!merchantApplication.isSelfieSubmitted()) {
                firstButton.setVisibility(View.GONE);
                secondButton.setVisibility(View.VISIBLE);
                thirdButton.setVisibility(View.GONE);
                fourthButton.setVisibility(View.GONE);
            } else if (!merchantApplication.isBusinessDetailSubmitted()) {
                firstButton.setVisibility(View.GONE);
                secondButton.setVisibility(View.GONE);
                thirdButton.setVisibility(View.VISIBLE);
                fourthButton.setVisibility(View.GONE);
            }else if(!merchantApplication.isLiveSubReceiptSubmitted()){
                firstButton.setVisibility(View.GONE);
                secondButton.setVisibility(View.GONE);
                thirdButton.setVisibility(View.GONE);
                fourthButton.setVisibility(View.VISIBLE);
            }else{
                firstButton.setVisibility(View.GONE);
                secondButton.setVisibility(View.GONE);
                thirdButton.setVisibility(View.GONE);
                fourthButton.setVisibility(View.GONE);
            }
        }else{
            firstButton.setVisibility(View.GONE);
            secondButton.setVisibility(View.GONE);
            thirdButton.setVisibility(View.GONE);
            fourthButton.setVisibility(View.GONE);
        }

        if(cbTerms.isChecked() && TLStorage.getInstance().isFirstStepComplete() && TLStorage.getInstance().isSecondStepComplete() && TLStorage.getInstance().isThirdStepComplete() && TLStorage.getInstance().isFourthStepComplete()){
            btnSubmitDocument.setEnabled(true);
        }
    }

    @Override
    public void onFinish(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(TLMerchantInfo.this, UserMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onError(String message) {

    }
}
