package com.technolab.android.ui.entity;

import java.util.List;

public class LevelResult{
    public int level_points;
    public int level;
    public String start_date;
    public String end_date;
    private int referral_count = 0;

    public boolean is_completed;
    public String total_daily_reward;
    public List<ReferralContent> referral_content;

    public boolean isIs_completed() {
        return is_completed;
    }

    public void setIs_completed(boolean is_completed) {
        this.is_completed = is_completed;
    }

    public String getTotal_daily_reward() {
        return total_daily_reward;
    }

    public void setTotal_daily_reward(String total_daily_reward) {
        this.total_daily_reward = total_daily_reward;
    }

    public List<ReferralContent> getReferral_content() {
        return referral_content;
    }

    public void setReferral_content(List<ReferralContent> referral_content) {
        this.referral_content = referral_content;
    }

    public int getReferral_count() {
        return referral_count;
    }

    public void setReferral_count(int referral_count) {
        this.referral_count = referral_count;
    }

    public int getLevel_points() {
        return level_points;
    }

    public void setLevel_points(int level_points) {
        this.level_points = level_points;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}