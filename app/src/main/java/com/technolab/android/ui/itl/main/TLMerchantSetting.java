package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.technolab.android.BuildConfig;
import com.technolab.android.R;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLSignInEmail;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

public class TLMerchantSetting extends AbstractActivity {

    LinearLayout editContainer;
    private LinearLayout processingContainer;
    private LinearLayout shippedContainer;
    private LinearLayout deliveredContainer;

    public TLMerchantSetting(){
        TAG = "TLMerchantSetting";
        layoutID = R.layout.activity_merchant_setting;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);

        processingContainer = findViewById(R.id.container_processing);
        shippedContainer = findViewById(R.id.container_shipped);
        deliveredContainer = findViewById(R.id.container_delivered);

        TextView txtVersion = findViewById(R.id.txt_version);
        txtVersion.setText(String.format("Version %s.%d", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));

        findViewById(R.id.switch_user_dashboard).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
                startActivity(new Intent(TLMerchantSetting.this, UserMainActivity.class));
            }
        });

        processingContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMerchantSetting.this, TLSalesManagement.class);
                intent.putExtra("viewType", 1);
                startActivity(intent);
            }
        });

        shippedContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMerchantSetting.this, TLSalesManagement.class);
                intent.putExtra("viewType", 2);
                startActivity(intent);
            }
        });

        deliveredContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMerchantSetting.this, TLSalesManagement.class);
                intent.putExtra("viewType", 3);
                startActivity(intent);
            }
        });


        TextView profileName = findViewById(R.id.txt_profile_name);
        profileName.setText(TLStorage.getInstance().getMerchantCompanyName());
        editContainer = findViewById(R.id.edit_container);
        editContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLMerchantSetting.this, MerchantViewProfileActivity.class));
            }
        });

        findViewById(R.id.container_logout).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMerchantSetting.this, TLSignInEmail.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                TLStorage.resetInstance();
            }
        });

        findViewById(R.id.container_notification).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLMerchantSetting.this, TLMerchantNotification.class));
            }
        });

        Glide.with(this)
                .load(TLStorage.getInstance().getPrefMerchantImage())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into((ImageView)findViewById(R.id.img_profile));
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Setting");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }
}
