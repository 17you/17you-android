package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.technolab.android.R;
import com.technolab.android.ui.itl.spine.AbstractActivity;

public class DeepLinkActivity extends AbstractActivity {

    public DeepLinkActivity() {
        TAG = "DeepLink Activity";
        layoutID = R.layout.activity_splash_screen;
        statusBarColorID = R.color.colorO;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        String referral = null;
                        String live = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            Log.d(" Full Dynamic Link", String.valueOf(pendingDynamicLinkData.toString()));
                            if (deepLink.toString().contains("referral")) {
                                referral = deepLink.getQueryParameter("referral");
                                Log.d("Dynamic Link:", "it's a product!");
                            } else if (deepLink.toString().contains("live")) {
                                live = deepLink.getQueryParameter("live");
                                Log.d("Dynamic Link:", "it's a live!");
                            }
                        }

                        if(live != null){
                            Intent newIntent = new Intent(DeepLinkActivity.this, SplashScreenActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.putExtra("liveStreamID", live);
                            newIntent.putExtra("isFromDeeplink", true);
                            startActivityForResult(newIntent, 1);
                            finish();
                        }else if(referral != null){
                            Intent newIntent = new Intent(DeepLinkActivity.this, SplashScreenActivity.class);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            newIntent.putExtra("referral", referral);
                            newIntent.putExtra("isFromDeeplink", true);
                            startActivityForResult(newIntent, 1);
                            finish();
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("DynamicLinksActivity", "getDynamicLink:onFailure", e);
                    }
                });
    }
}
