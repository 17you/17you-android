package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RankingType {
    @SerializedName("top")
    public ArrayList<RankingTop> top;
    @SerializedName("others")
    public ArrayList<RankingOthers> others;

    public ArrayList<RankingTop> getTop() {
        return top;
    }

    public void setTop(ArrayList<RankingTop> top) {
        this.top = top;
    }

    public ArrayList<RankingOthers> getOthers() {
        return others;
    }

    public void setOthers(ArrayList<RankingOthers> others) {
        this.others = others;
    }
}
