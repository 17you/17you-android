package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Address;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder>{

    private Context context;
    private ArrayList<Address> addressList;
    private AddressAdapter.ItemClickListener mCallback;

    public AddressAdapter(Context context, ArrayList<Address> addressList){
        this.context = context;
        this.addressList = addressList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_delivery_address, parent, false);
        return new AddressAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Address address = addressList.get(position);
        holder.txtDefault.setVisibility(address.isIsdefault() ? View.VISIBLE : View.GONE);
        holder.addressLine1.setText(address.getAddress_line_1());
        holder.addressLine2.setText(address.getAddress_line_2());
        holder.addressLine3.setText(address.getPostcode() + " " + address.getCity());
        holder.addressLine4.setText(address.getState());

        holder.imgEdit.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onItemClicked(address);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgEdit;
        private TextView txtDefault;
        private TextView addressLine1;
        private TextView addressLine2;
        private TextView addressLine3;
        private TextView addressLine4;

        public ViewHolder(View itemView) {
            super(itemView);
            imgEdit = itemView.findViewById(R.id.img_edit);
            txtDefault = itemView.findViewById(R.id.txt_default);
            addressLine1 =  itemView.findViewById(R.id.txt_address_line_1);
            addressLine2 =  itemView.findViewById(R.id.txt_address_line_2);
            addressLine3 =  itemView.findViewById(R.id.txt_address_line_3);
            addressLine4 =  itemView.findViewById(R.id.txt_address_line_4);
        }
    }

    public void setOnItemClickListener(final AddressAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(Address address);
    }
}
