package com.technolab.android.ui.itl.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;

import com.technolab.android.R;

import io.github.inflationx.calligraphy3.TypefaceUtils;

public class TypefaceButton extends Button
{
    public TypefaceButton(Context context)
    {
        super(context);
    }

    public TypefaceButton(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        if(attrs!=null){
            TypedArray ta = context.obtainStyledAttributes(attrs, FONT_PATH_ATTRS);

            String fontPath = ta.getString(0);
            if (fontPath != null)
                setTypeface(TypefaceUtils.load(getResources().getAssets(), fontPath));

            ta.recycle();
        }
    }

    public TypefaceButton(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);

        if(attrs!=null){
            TypedArray ta = context.obtainStyledAttributes(attrs, FONT_PATH_ATTRS);

            String fontPath = ta.getString(0);
            if (fontPath != null)
                setTypeface(TypefaceUtils.load(getResources().getAssets(), fontPath));

            ta.recycle();
        }
    }
    private static int[] FONT_PATH_ATTRS = {R.attr.fontPath};

    @Override
    public void setTextAppearance(Context context, int resId)
    {
        super.setTextAppearance(context, resId);

        TypedArray ta = context.obtainStyledAttributes(resId, FONT_PATH_ATTRS);

        String fontPath = ta.getString(0);
        if (fontPath != null)
            setTypeface(TypefaceUtils.load(getResources().getAssets(), fontPath));

        ta.recycle();
    }
}
