package com.technolab.android.ui.itl.main.MerchantLiveManagement.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter.TLMerchantLiveManagementMvpView;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter.TLMerchantLiveManagementPresenter;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.adapter.ProductSelectAdapter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLLiveProductSetupActivity extends AbstractActivity implements TLMerchantLiveManagementMvpView.LiveStreamMVP, ProductSelectAdapter.ItemClickListener{

    @Inject
    TLMerchantLiveManagementPresenter.TLMerchantLiveStreamPresenter merchantLiveStreamPresenter;

    private RecyclerView rv_live_stream_product;
    private ProductSelectAdapter productSelectAdapter;
    private ArrayList<Product> productArrayList;

    public TLLiveProductSetupActivity(){
        TAG = "TL Product Setup";
        layoutID = R.layout.activity_product_live_setup;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rv_live_stream_product = findViewById(R.id.rv_live_stream_product);
        productArrayList = new ArrayList<>();
        setupToolbar(this);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Product Setup");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(productArrayList.size() == 0) {
                    finish();
                    return;
                }

                Intent intent = new Intent();
                intent.putExtra("productList", productArrayList);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLLiveProductSetupActivity.this);
        merchantLiveStreamPresenter.onAttach(this);
        merchantLiveStreamPresenter.fetchProductList();
    }

    @Override
    public void onRetrieveLiveStreams(ArrayList<LiveStream> liveStreamArrayList) {
        //Do nothing
    }

    @Override
    public void onRetrieveProductList(ArrayList<Product> productArrayList) {
        productSelectAdapter = new ProductSelectAdapter(this,productArrayList);
        rv_live_stream_product.setAdapter(productSelectAdapter);
        productSelectAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onFetchData(LiveStream liveStream) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onResponse(String message) {

    }

    @Override
    public void onDelete(String message) {

    }

    @Override
    public void onPublish(String message) {

    }

    @Override
    public void onItemClicked(Product product, String action) {
        if(action.equalsIgnoreCase("Add"))
            productArrayList.add(product);
        else if(action.equalsIgnoreCase("Remove"))
            productArrayList.remove(product);

    }
}
