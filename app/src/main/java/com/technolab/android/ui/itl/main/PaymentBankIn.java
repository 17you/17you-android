package com.technolab.android.ui.itl.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.technolab.android.R;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

public class PaymentBankIn extends AbstractActivity {

    private Button btnChooseFile;
    private ConstraintLayout nonUploadedContainer;
    private ConstraintLayout uploadedContainer;
    private String imagePath;
    private ImageView imgUploadedFile;
    private TextView txtAmountValue;
    private TextView txtRecipientValue;
    private TextView txtBankNameValue;
    private TextView txtAccountValue;

    public PaymentBankIn() {
        TAG = "Payment Bank In";
        layoutID = R.layout.activity_payment_bank_in;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        btnChooseFile = findViewById(R.id.btn_choose_file);
        nonUploadedContainer = findViewById(R.id.non_uploaded_container);
        uploadedContainer = findViewById(R.id.uploaded_container);
        imgUploadedFile = findViewById(R.id.img_uploaded_file);
        txtAmountValue = findViewById(R.id.txt_amount_value);
        txtRecipientValue = findViewById(R.id.txt_recipient_value);
        txtBankNameValue = findViewById(R.id.txt_bank_name_value);
        txtAccountValue = findViewById(R.id.txt_account_value);

        txtRecipientValue.setText(getIntent().getStringExtra("name"));
        txtBankNameValue.setText(getIntent().getStringExtra("bank"));
        txtAccountValue.setText(getIntent().getStringExtra("acc"));

        switch (getIntent().getIntExtra("paymentType", 0)) {
            case 1:
                txtAmountValue.setText(String.format("RM%s/ month", getIntent().getStringExtra("amount")));
                break;
            case 2:
                txtAmountValue.setText(String.format("RM%s/ annum", getIntent().getStringExtra("amount")));
                break;
            case 3:
                txtAmountValue.setText(String.format("RM%s", getIntent().getStringExtra("amount")));
                break;
        }

        btnChooseFile.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(PaymentBankIn.this, ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, true);
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, false);
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);
                startActivityForResult(intent, 1213);
            }
        });

        findViewById(R.id.btn_pay).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(imagePath != null) {
                    Intent intent = new Intent();
                    intent.putExtra("bankSlip", imagePath);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Payment");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(v -> onBackPressed());
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            imagePath = filePath;
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            imgUploadedFile.setImageBitmap(selectedImage);
            nonUploadedContainer.setVisibility(View.GONE);
            uploadedContainer.setVisibility(View.VISIBLE);
        }
    }
}
