package com.technolab.android.ui.entity;

import java.io.Serializable;

public class PurchasedProduct implements Serializable {
    public int quantity_purchased;
    public int product_variation_id;
    public int product_id;
    public Double unit_price;
    public Double total_price;
    public String product_name;
    public String product_description;
    public String product_image;
    public String product_variation_size;
    public String product_variation_color;

    public int getQuantity_purchased() {
        return quantity_purchased;
    }

    public void setQuantity_purchased(int quantity_purchased) {
        this.quantity_purchased = quantity_purchased;
    }

    public int getProduct_variation_id() {
        return product_variation_id;
    }

    public void setProduct_variation_id(int product_variation_id) {
        this.product_variation_id = product_variation_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public Double getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(Double unit_price) {
        this.unit_price = unit_price;
    }

    public Double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(Double total_price) {
        this.total_price = total_price;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_variation_size() {
        return product_variation_size;
    }

    public void setProduct_variation_size(String product_variation_size) {
        this.product_variation_size = product_variation_size;
    }

    public String getProduct_variation_color() {
        return product_variation_color;
    }

    public void setProduct_variation_color(String product_variation_color) {
        this.product_variation_color = product_variation_color;
    }
}
