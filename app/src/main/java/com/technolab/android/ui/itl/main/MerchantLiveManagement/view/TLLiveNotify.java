package com.technolab.android.ui.itl.main.MerchantLiveManagement.view;

import com.technolab.android.R;
import com.technolab.android.ui.itl.spine.AbstractActivity;

public class TLLiveNotify extends AbstractActivity {

    public TLLiveNotify(){
        TAG = "Notify Friends";
        layoutID = R.layout.activity_live_notify;
        statusBarColorID = R.color.colorBlue;
    }
}
