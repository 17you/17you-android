package com.technolab.android.ui.itl.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.DetailedTransaction;
import com.technolab.android.ui.entity.PurchasedProduct;
import com.technolab.android.ui.entity.Transaction;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLMerchantLiveStreaming;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TLActionSheet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

public class TLSalesManagementDetail extends AbstractActivity implements TLMainMvpView.MerchantTransaction{

    private int viewType;
    private TextView txtReferenceNumber;
    private TextView txtStatus;
    private TextView txtCustomerName;
    private TextView txtMobile;
    private TextView txtDeliveryAddress;
    private TextView txtPurchaseDate;
    private TextView txtDeliveryFees;
    private TextView txtGrandTotal;
    private TextView txtTracking;
    private TextView txtCourier;
    private LinearLayout productContainer;
    private Group trackingContainer;

    private int position = -1;
    private int transactionID;

    @Inject
    TLMainPresenter.TLMerchantTransaction merchantTransactionPresenter;

    public TLSalesManagementDetail(){
        TAG = "TLSalesManagementDetail";
        layoutID = R.layout.activity_sales_management_detail;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewType = getIntent().getIntExtra("viewType", 0);
        setupToolbar(this);

        DetailedTransaction transaction = (DetailedTransaction) getIntent().getSerializableExtra("transaction");
        transactionID = transaction.getTransaction_id();
        trackingContainer = findViewById(R.id.tracking_container);
        txtReferenceNumber = findViewById(R.id.txt_reference_no_value);
        txtStatus = findViewById(R.id.txt_status_value);
        txtCustomerName = findViewById(R.id.txt_customer_name_value);
        txtMobile = findViewById(R.id.txt_mobile_value);
        txtDeliveryAddress = findViewById(R.id.txt_delivery_address_value);
        txtPurchaseDate = findViewById(R.id.txt_purchase_date);
        txtDeliveryFees = findViewById(R.id.txt_delivery_value);
        txtGrandTotal = findViewById(R.id.txt_grand_total_value);
        productContainer = findViewById(R.id.product_container);
        txtTracking = findViewById(R.id.txt_tracking_value);
        txtCourier = findViewById(R.id.txt_tracking_courier_value);

        txtReferenceNumber.setText(transaction.getReference_number());
        switch (viewType) {
            case 1:
                txtStatus.setText("Processing");
                break;
            case 2:
                txtStatus.setText("Shipped");
                break;
            case 3:
                txtStatus.setText("Delivered");
                break;
        }
        txtCustomerName.setText(transaction.member_username);
        txtMobile.setText(transaction.getMember_phone());
        txtDeliveryAddress.setText(String.format("%s\n%s\n%s %s\n%s", transaction.getAddress_line_1(), transaction.getAddress_line_2(), transaction.getPostcode(), transaction.getCity(), transaction.getState()));

        String finalDate = "";
        try {
            String date = transaction.getTransaction_date_and_time();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            finalDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        txtPurchaseDate.setText(finalDate);

        productContainer.removeAllViews();
        ArrayList<PurchasedProduct> purchasedProducts = transaction.getPurchased_products();
        for (int i = 0; i < purchasedProducts.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            ConstraintLayout productView = (ConstraintLayout) layoutInflater.inflate(R.layout.layout_transaction_product, null);
            ImageView imgProduct = productView.findViewById(R.id.img_product);
            TextView txtProductTitle = productView.findViewById(R.id.txt_product_title);
            TextView txtProductVariantQuantity = productView.findViewById(R.id.txt_product_variant_quantity);
            TextView txtTotal = productView.findViewById(R.id.txt_total_value);

            Glide.with(this)
                    .load(purchasedProducts.get(i).product_image)
                    .thumbnail(0.1f)
                    .placeholder(getResources().getDrawable(R.drawable.placeholder))
                    .into(imgProduct);

            txtProductTitle.setText(purchasedProducts.get(i).getProduct_name());
            txtProductVariantQuantity.setText(String.format("%s x %d", purchasedProducts.get(i).getProduct_variation_size(), purchasedProducts.get(i).getQuantity_purchased()));
            txtTotal.setText(String.format("RM%.2f", Double.parseDouble(String.valueOf(purchasedProducts.get(i).getTotal_price()))));
            productContainer.addView(productView);
        }
        txtDeliveryFees.setText(String.format("RM%s", transaction.getDelivery_fees()));
        txtGrandTotal.setText(String.format("RM%s",transaction.getFinal_price()));

        txtStatus.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                TLActionSheet actionSheet = new TLActionSheet(TLSalesManagementDetail.this);
                actionSheet.addAction("Processing");
                actionSheet.addAction("Shipped");
                actionSheet.addAction("Delivered");

                actionSheet.setEventListener(new TLActionSheet.OnEventListener() {
                    @Override
                    public void onActionItemClick(TLActionSheet dialog, TLActionSheet.ActionItem item, int position) {
                        TLSalesManagementDetail.this.position = position;
                        trackingContainer.setVisibility(position != 0 ? View.VISIBLE : View.GONE);
                        if(position == 0)
                            txtStatus.setText("Processing");
                        else if(position == 1)
                            txtStatus.setText("Shipped");
                        else if(position == 2)
                            txtStatus.setText("Delivered");
                    }

                    @Override
                    public void onCancelItemClick(TLActionSheet dialog) {

                    }
                });
                actionSheet.setCancelVisible(true);
                actionSheet.setCanceledOnTouchOutside(true);
                actionSheet.show();
            }
        });
        if (!transaction.getLogistic_status().equals("PROCESSING")) {
            trackingContainer.setVisibility(View.VISIBLE);
            txtTracking.setText(transaction.getTracking_url());
        }
        else {
            trackingContainer.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLSalesManagementDetail.this);
        merchantTransactionPresenter.onAttach(this);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);

        txtHeader.setText("Sales Management");
        txtTemp.setText("Save");
        txtTemp.setVisibility(View.VISIBLE);
        txtTemp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(position == -1){
                    Toast.makeText(TLSalesManagementDetail.this, "No changes has been made.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(position != 0 && !txtTracking.getText().toString().isEmpty() && !txtCourier.getText().toString().isEmpty())
                    merchantTransactionPresenter.updateTransactionStatus(String.valueOf(transactionID), txtTracking.getText().toString(), position, txtCourier.getText().toString());
            }
        });

        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
    }


    @Override
    public void onReceiveTransaction(ArrayList<Transaction> transactions) {

    }

    @Override
    public void onResponse(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
