package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Address;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLAddressSetting extends AbstractActivity implements TLMainMvpView.UserProfile, AddressAdapter.ItemClickListener{

    private ImageView imgProfile;
    private LinearLayout noDataContainer;
    private RecyclerView recyclerView;
    private Button btnAddMore;
    private AddressAdapter addressAdapter;
    private int addressCount = 0;

    @Inject
    TLMainPresenter.TLUserProfilePresenter userProfilePresenter;

    public TLAddressSetting() {
        TAG = "TLAddressSetting";
        layoutID = R.layout.activity_delivery_address;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        imgProfile = findViewById(R.id.img_profile);
        noDataContainer = findViewById(R.id.container_no_data);
        recyclerView = findViewById(R.id.recycler_view);
        btnAddMore = findViewById(R.id.btn_add_more);
        btnAddMore.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLAddressSetting.this, TLAddAddress.class);
                intent.putExtra("status",2);
                intent.putExtra("isFirstAddress", addressCount == 0);
                startActivity(intent);
            }
        });
        Glide.with(this)
                .load(TLStorage.getInstance().getPrefUserImage())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into(imgProfile);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Profile");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(this);
        userProfilePresenter.onAttach(this);
        userProfilePresenter.retrieveAddress();
    }


    @Override
    public void onReceiveAddress(ArrayList<Address> addressList) {
        if(addressList == null){
            noDataContainer.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }

        addressCount = addressList.size();
        if(addressList.size() == 0){
            noDataContainer.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else{
            noDataContainer.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            addressAdapter = new AddressAdapter(this,addressList);
            recyclerView.setAdapter(addressAdapter);
            addressAdapter.setOnItemClickListener(this);
        }
    }

    @Override
    public void onFinish(String message) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onItemClicked(Address address) {
        Intent intent = new Intent(TLAddressSetting.this, TLAddAddress.class);
        intent.putExtra("status",3);
        intent.putExtra("address", address);
        startActivity(intent);
    }
}
