package com.technolab.android.ui.entity;

import com.technolab.android.utility.MessageType;

public class Message {
    public String userName;
    public String messageContent;
    public String roomName;
    public MessageType viewType;

    public Message(String userName, String messageContent, String roomName, MessageType viewType) {
        this.userName = userName;
        this.messageContent = messageContent;
        this.roomName = roomName;
        this.viewType = viewType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public MessageType getViewType() {
        return viewType;
    }

    public void setViewType(MessageType viewType) {
        this.viewType = viewType;
    }
}
