package com.technolab.android.ui.itl.utils;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.technolab.android.R;

import java.util.ArrayList;

public class TLActionSheet extends Dialog implements View.OnClickListener, AdapterView.OnItemClickListener {

    private TextView mTextTitle = null;
    private TextView mTextMessage = null;
    private TextView mTextCancel = null;
    private LinearLayout mLayoutHeader = null;
    private RelativeLayout mLayoutCancel = null;
    private ListView mListViewContent = null;
    private ListItemAdapter mListItemAdapter = null;
    private OnEventListener mEventListener = null;
    private ArrayList<ActionItem> mListActionItems = new ArrayList<>();

    // CLASSES
    public static class ActionItem {
        public boolean destructive = false;
        public String title = null;
        public Object key = 0;
    }

    private class ListItemAdapter extends ArrayAdapter<ActionItem> {
        private LayoutInflater mLayoutInflater = null;

        public ListItemAdapter(Context context, ArrayList<ActionItem> items) {
            super(context, 0, items);

            mLayoutInflater = LayoutInflater.from(getContext());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.list_item_action, parent, false);
            }

            final ActionItem item = getItem(position);
            final TextView textTitle = (TextView)convertView.findViewById(R.id.action_item_text_title);
            textTitle.setText(item.title);

            if (item.destructive) {
                textTitle.setTextColor(getColor(R.color.action_sheet_red));
            } else {
                textTitle.setTextColor(getColor(R.color.action_sheet_blue));
            }

            return convertView;
        }

        private int getColor(int colorId) {
            return getContext().getResources().getColor(colorId);
        }
    }

    public interface OnEventListener {
        void onActionItemClick(TLActionSheet dialog, ActionItem item, int position);
        void onCancelItemClick(TLActionSheet dialog);
    }

    public TLActionSheet(Context context) {
        super(context, R.style.ActionSheet);

        setupViews();
        setupWindow();
    }

    @Override
    public void onClick(View v) {
        if (v == mLayoutCancel) {
            dismiss();

            if (mEventListener != null) {
                mEventListener.onCancelItemClick(this);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mEventListener != null) {
            mEventListener.onActionItemClick(this, mListActionItems.get(position), position);
        }

        dismiss();
    }

    @Override
    public void setTitle(CharSequence text) {
        super.setTitle(text);

        setTitle(text.toString());
    }

    @Override
    public void setTitle(int textId) {
        super.setTitle(textId);

        setTitle(getContext().getString(textId));
    }

    public void setTitle(String text) {
        if (!TextUtils.isEmpty(text)) {
            mTextTitle.setText(text);
            mTextTitle.setVisibility(View.VISIBLE);
        } else {
            mTextTitle.setVisibility(View.GONE);
        }

        updateViews();
    }

    public void setMessage(int textId) {
        setMessage(getContext().getString(textId));
    }

    public void setMessage(CharSequence text) {
        setMessage(text.toString());
    }

    public void setMessage(String text) {
        if (!TextUtils.isEmpty(text)) {
            mTextMessage.setText(text);
            mTextMessage.setVisibility(View.VISIBLE);
        } else {
            mTextMessage.setVisibility(View.GONE);
        }

        updateViews();
    }

    public void setCancelText(int textId) {
        setCancelText(getContext().getString(textId));
    }

    public void setCancelText(CharSequence text) {
        setCancelText(text.toString());
    }

    public void setCancelText(String text) {
        mTextCancel.setText(text);
    }

    public void setCancelVisible(boolean visible) {
        ((View)mLayoutCancel.getParent()).setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public void setEventListener(OnEventListener listener) {
        mEventListener = listener;
    }

    public void addAction(ActionItem item) {
        mListActionItems.add(item);
    }

    public void addAction(int textId) {
        addAction(getContext().getString(textId));
    }

    public void addAction(String title) {
        addAction(title, null);
    }

    public void addAction(String title, Object key) {
        ActionItem item = new ActionItem();
        item.title = title;
        item.key = key;
        mListActionItems.add(item);
        mListItemAdapter.notifyDataSetChanged();
    }

    public void addAction(String title, boolean destructive) {
        ActionItem item = new ActionItem();
        item.title = title;
        item.destructive = destructive;
        mListActionItems.add(item);
        mListItemAdapter.notifyDataSetChanged();
    }

    public void setActions(int arrayId) {
        setActions(getContext().getResources().getStringArray(arrayId));
    }

    public void setActions(String[] titles) {
        mListActionItems.clear();

        for (String title : titles) {
            addAction(title);
        }
    }

    public void clearActions() {
        mListActionItems.clear();
        mListItemAdapter.notifyDataSetChanged();
    }

    private void setupViews() {
        final LayoutInflater inflater = LayoutInflater.from(getContext());
        final View viewContent = inflater.inflate(R.layout.dialog_action, null);
        setContentView(viewContent);

        // Header
        mLayoutHeader = (LinearLayout)viewContent.findViewById(R.id.action_dialog_layout_header);
        mLayoutHeader.setVisibility(View.GONE);

        // Title
        mTextTitle = (TextView)viewContent.findViewById(R.id.action_dialog_text_title);
        mTextTitle.setVisibility(View.GONE);

        // Message
        mTextMessage = (TextView)viewContent.findViewById(R.id.action_dialog_text_message);
        mTextMessage.setVisibility(View.GONE);

        // Adapter
        mListItemAdapter = new ListItemAdapter(getContext(), mListActionItems);

        // List
        mListViewContent = (ListView)viewContent.findViewById(R.id.action_dialog_list_content);
        mListViewContent.setAdapter(mListItemAdapter);
        mListViewContent.setOnItemClickListener(this);

        // Cancel
        mLayoutCancel = (RelativeLayout)viewContent.findViewById(R.id.action_dialog_layout_cancel);
        mLayoutCancel.setOnClickListener(this);

        mTextCancel = (TextView)viewContent.findViewById(R.id.action_dialog_text_cancel);
    }

    private void updateViews() {
        if ((mTextTitle.getVisibility() == View.VISIBLE) ||
                (mTextMessage.getVisibility() == View.VISIBLE)) {
            mLayoutHeader.setVisibility(View.VISIBLE);
        } else {
            mLayoutHeader.setVisibility(View.GONE);
        }
    }

    private void setupWindow() {
        final Window window = getWindow();
        final WindowManager.LayoutParams layoutParams = window.getAttributes();
        layoutParams.width = getContext().getResources().getDisplayMetrics().widthPixels;
        window.setAttributes(layoutParams);
        window.setGravity(Gravity.BOTTOM);
    }
}