package com.technolab.android.ui.itl.main;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.technolab.android.utility.TLConstant;

public class DeepLinkUtils {
    public static Uri dynamicShortLink;

    public static Uri createDeepLink(CharSequence id, String type) {
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(type.equalsIgnoreCase("live")
                        ? Uri.parse("https://technolab.my/?live=" + id) : type.equalsIgnoreCase("referral")
                        ? Uri.parse("https://technolab.my/?referral=" + id) : Uri.parse("https://technolab.my/?referral=" + id))
                .setDomainUriPrefix("https://technolab.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .buildDynamicLink();

        Uri dynamicLinkUri = dynamicLink.getUri();
        Log.e("Dynamic Link", "Deep Long link : " + dynamicLinkUri.toString());

        return dynamicLinkUri;
    }


    public static void shortTheLink(Uri dynamicLinkUri, Activity activity, String type) {
        dynamicShortLink = dynamicLinkUri;
        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(dynamicLinkUri)
                .buildShortDynamicLink()
                .addOnCompleteListener(activity, new OnCompleteListener<ShortDynamicLink>() {

                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink();
                            Log.e("Dynamic Link", "Dynamic Short link : " + shortLink);
                            dynamicShortLink = shortLink;
                            if (type.equals(TLConstant.DYNAMIC_LINK_REFERRAL)) {
                                //AppController.getInstance().setReferralLink(dynamicShortLink);
                            }
                        } else {
                            dynamicShortLink = dynamicLinkUri;
                        }
                    }
                });
    }

    public static Uri getShortLink(){
        return dynamicShortLink;
    }

}
