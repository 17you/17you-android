package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.DetailedTransaction;


import java.util.ArrayList;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder> {

    private ArrayList<DetailedTransaction> detailedTransactions;
    private Context context;

    public TransactionHistoryAdapter(Context context, ArrayList<DetailedTransaction> detailedTransactions) {
        this.detailedTransactions = detailedTransactions;
        this.context = context;
    }

    @Override
    public TransactionHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_transaction_history, parent, false);
        return new TransactionHistoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TransactionHistoryAdapter.ViewHolder holder, int position) {
        DetailedTransaction detailedTransaction = detailedTransactions.get(position);
        holder.txtEmail.setText(detailedTransaction.getMember_email());
        holder.txtPrice.setText(String.format("RM%.2f", Double.parseDouble(String.valueOf(detailedTransaction.getFinal_price()))));
        holder.txtMobile.setText(detailedTransaction.getMember_phone());
        holder.txtQuantity.setText(String.format("Quantity: %d", detailedTransaction.getQuantity_purchased()));
    }

    @Override
    public int getItemCount() {
        return detailedTransactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtEmail;
        private TextView txtPrice;
        private TextView txtMobile;
        private TextView txtQuantity;
        private TextView txtDate;

        public ViewHolder(View itemView) {
            super(itemView);
            txtEmail = itemView.findViewById(R.id.txt_email);
            txtPrice = itemView.findViewById(R.id.txt_price);
            txtMobile = itemView.findViewById(R.id.txt_mobile);
            txtQuantity = itemView.findViewById(R.id.txt_quantity);
            txtDate = itemView.findViewById(R.id.txt_date);
        }
    }
}