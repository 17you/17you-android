package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductMainCategory {

    @SerializedName("product_main_cat_id")
    private Integer productMainCategoryId;

    @SerializedName("product_main_cat_name")
    private String productMainCategoryName;

    public ArrayList<ProductSubCategory> getProductSubCategoryList() {
        return productSubCategoryList;
    }

    public void setProductSubCategoryList(ArrayList<ProductSubCategory> productSubCategoryList) {
        this.productSubCategoryList = productSubCategoryList;
    }

    @SerializedName("sub_category")
    private ArrayList<ProductSubCategory> productSubCategoryList;

    public Integer getProductMainCategoryId() {
        return productMainCategoryId;
    }

    public void setProductMainCategoryId(Integer productMainCategoryId) {
        this.productMainCategoryId = productMainCategoryId;
    }

    public String getProductMainCategoryName() {
        return productMainCategoryName;
    }

    public void setProductMainCategoryName(String productMainCategoryName) {
        this.productMainCategoryName = productMainCategoryName;
    }


}
