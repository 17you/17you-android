package com.technolab.android.ui.itl.main.MerchantLiveManagement.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.main.DeepLinkUtils;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter.TLMerchantLiveManagementMvpView;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter.TLMerchantLiveManagementPresenter;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.adapter.MerchantLiveStreamAdapter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TLActionSheet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import ir.androidexception.andexalertdialog.AndExAlertDialog;
import ir.androidexception.andexalertdialog.AndExAlertDialogListener;

public class TLLiveManagement extends AbstractActivity implements TLMerchantLiveManagementMvpView.LiveStreamMVP, MerchantLiveStreamAdapter.ItemClickListener {

    private Group zeroStateLiveStream;
    private RecyclerView rvLiveStream;
    private Button btnCreateLiveStream;

    MerchantLiveStreamAdapter merchantLiveStreamAdapter;

    @Inject
    TLMerchantLiveManagementPresenter.TLMerchantLiveStreamPresenter merchantLiveStreamPresenter;

    public TLLiveManagement(){
        TAG = "TLLiveManagement";
        layoutID = R.layout.activity_live_management;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        setupRefresh(this);
        zeroStateLiveStream = findViewById(R.id.zero_state_live_stream);
        rvLiveStream = findViewById(R.id.rv_live_stream);
        btnCreateLiveStream = findViewById(R.id.btn_create_live_stream);
        btnCreateLiveStream.setVisibility(View.GONE);
        btnCreateLiveStream.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLLiveManagement.this, TLCreateLiveStream.class);
                intent.putExtra("isEditMode", false);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLLiveManagement.this);
        merchantLiveStreamPresenter.onAttach(this);
        merchantLiveStreamPresenter.fetchLiveStreams();
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> merchantLiveStreamPresenter.fetchLiveStreams());
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Live Stream Setup");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onRetrieveLiveStreams(ArrayList<LiveStream> liveStreamArrayList) {
        refreshLayout.setRefreshing(false);
        if(liveStreamArrayList.size() == 0) {
            zeroStateLiveStream.setVisibility(View.VISIBLE);
            rvLiveStream.setVisibility(View.GONE);
        }else{
            zeroStateLiveStream.setVisibility(View.GONE);
            rvLiveStream.setVisibility(View.VISIBLE);
            merchantLiveStreamAdapter = new MerchantLiveStreamAdapter(this,liveStreamArrayList);
            rvLiveStream.setAdapter(merchantLiveStreamAdapter);
            merchantLiveStreamAdapter.setOnItemClickListener(this);
        }
        btnCreateLiveStream.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRetrieveProductList(ArrayList<Product> productArrayList) {
        //Do nothing
    }

    @Override
    public void onFetchData(LiveStream liveStream) {

    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(String message) {

    }

    @Override
    public void onDelete(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        merchantLiveStreamPresenter.fetchLiveStreams();
    }

    @Override
    public void onPublish(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        merchantLiveStreamPresenter.fetchLiveStreams();
    }

    @Override
    public void onItemClicked(LiveStream liveStream) {

        TLActionSheet actionSheet = new TLActionSheet(this);

        switch (liveStream.getLive_stream_status())
        {
            case "SAVED":
                actionSheet.addAction("Edit");
                actionSheet.addAction("Delete");
                actionSheet.addAction("Publish");
                break;
            case "PUBLISHED":
                actionSheet.addAction("Edit");
                actionSheet.addAction("Open Stream");
                actionSheet.addAction("Share Friend");
                actionSheet.addAction("Delete");
                break;
            case "LIVE NOW":
                actionSheet.addAction("Open Stream");
                actionSheet.addAction("Delete");
                break;
            case "EXPIRED":
                actionSheet.addAction("Edit");
                actionSheet.addAction("Delete");
                break;
            case "FINISHED":
                actionSheet.addAction("Delete");
                break;
        }

        actionSheet.setEventListener(new TLActionSheet.OnEventListener() {
            @Override
            public void onActionItemClick(TLActionSheet dialog, TLActionSheet.ActionItem item, int position) {
                switch (item.title){
                    case "Edit":
                        Intent clsIntent = new Intent(TLLiveManagement.this, TLCreateLiveStream.class);
                        clsIntent.putExtra("isEditMode", true);
                        clsIntent.putExtra("liveStreamID", String.valueOf(liveStream.getLive_stream_id()));
                        startActivity(clsIntent);
                        break;
                    case "Open Stream":
                        Intent intent = new Intent(new Intent(TLLiveManagement.this, TLMerchantLiveStreaming.class));
                        intent.putExtra("id", String.valueOf(liveStream.getLive_stream_id()));
                        intent.putExtra("image", liveStream.getLive_stream_image());
                        intent.putExtra("title", String.valueOf(liveStream.getLive_stream_title()));
                        intent.putExtra("description", String.valueOf(liveStream.getLive_stream_description()));
                        intent.putExtra("sTime", liveStream.getLive_stream_start_time());
                        intent.putExtra("eTime", liveStream.getLive_stream_end_time());
                        startActivity(intent);
                        break;
                    case "Share Friend":
                        Uri longURL = DeepLinkUtils.createDeepLink(String.valueOf(liveStream.getLive_stream_id()), "live");
                        DeepLinkUtils.shortTheLink(longURL, TLLiveManagement.this, "live");
                        Uri shortURL = DeepLinkUtils.getShortLink();
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_TEXT, String.format("%s\n%s", String.format("Watch 17You's Live : %s - %s.", liveStream.getLive_stream_title(), liveStream.getLive_stream_description()), shortURL.toString()));
                        startActivity(Intent.createChooser(i, "Share URL"));
                        break;
                    case "Delete":
                        String message ="Are you sure you want to delete the "+ liveStream.getLive_stream_title()+"\n\n"+ liveStream.getLive_stream_description();
                        new AndExAlertDialog.Builder(TLLiveManagement.this)
                                .setTitle("Delete Live Stream Setup")
                                .setMessage(message)
                                .setPositiveBtnText("Yes, Delete")
                                .setNegativeBtnText("No, Thank You!")
                                .setCancelableOnTouchOutside(true)
                                .OnNegativeClicked(new AndExAlertDialogListener() {
                                    @Override
                                    public void OnClick(String input) {
                                    }
                                })
                                .OnPositiveClicked(new AndExAlertDialogListener() {
                                    @Override
                                    public void OnClick(String input) {
                                        merchantLiveStreamPresenter.deleteLiveStream(String.valueOf(liveStream.getLive_stream_id()));
                                    }
                                })
                                .setMessageTextColor(R.color.black)
                                .setButtonTextColor(R.color.black)
                                .build();
                        break;

                    case "Publish":
                        String message1 = "Are you sure you want to publish the "+ liveStream.getLive_stream_title()+"\n\n"+ liveStream.getLive_stream_description();
                        new AndExAlertDialog.Builder(TLLiveManagement.this)
                                .setTitle("Publish Live Stream")
                                .setMessage(message1)
                                .setPositiveBtnText("Yes, Publish")
                                .setNegativeBtnText("No, Thank You!")
                                .setCancelableOnTouchOutside(true)
                                .OnNegativeClicked(new AndExAlertDialogListener() {
                                    @Override
                                    public void OnClick(String input) {
                                    }
                                })
                                .OnPositiveClicked(new AndExAlertDialogListener() {
                                    @Override
                                    public void OnClick(String input) {
                                        merchantLiveStreamPresenter.publishLiveStream(String.valueOf(liveStream.getLive_stream_id()));
                                    }
                                })
                                .setMessageTextColor(R.color.black)
                                .setButtonTextColor(R.color.black)
                                .build();
                        break;
                }
            }

            @Override
            public void onCancelItemClick(TLActionSheet dialog) {

            }
        });
        actionSheet.setCancelVisible(true);
        actionSheet.setCanceledOnTouchOutside(true);
        actionSheet.show();

        ArrayList<String> data = new ArrayList<>();
        data.add("Edit");
        data.add("Open Stream");
        data.add("Share Friend");
        if (!liveStream.getLive_stream_status().equalsIgnoreCase("PUBLISHED"))
            data.add("Delete");

    }
}
