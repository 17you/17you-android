package com.technolab.android.ui.itl.main.MerchantRegistration.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Variation;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.MerchantShowVariationAdapter;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLCreateProduct;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLSelectProductCategory;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationMvpView;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationPresenter;
import com.technolab.android.ui.itl.main.PaymentBankIn;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import java.util.ArrayList;

import javax.inject.Inject;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

public class TLMerchantPayment extends AbstractActivity implements TLMerchantRegistrationMvpView.MerchantPayment{

    @Inject
    TLMerchantRegistrationPresenter.TLMerchantPaymentPresenter merchantRegistrationPresenter;

    public TLMerchantPayment() {
        TAG = "Merchant Payment";
        layoutID = R.layout.fragment_merchant_payment;
    }

    /**
     * amount -> Total amount
     * paymentType -> 1-month, 2-annual
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        findViewById(R.id.btn_pay).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent paymentIntent = new Intent(TLMerchantPayment.this, PaymentBankIn.class);
                paymentIntent.putExtra("name","17APP");
                paymentIntent.putExtra("bank", "MAYBANK");
                paymentIntent.putExtra("acc", "8888888881111");
                paymentIntent.putExtra("amount", "600");
                paymentIntent.putExtra("paymentType", 2);
                startActivityForResult(paymentIntent, 1234);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(this);
        merchantRegistrationPresenter.onAttach(this);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Subscription Pricing");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234 && resultCode == Activity.RESULT_OK) {
            String bankSlip = data.getExtras().getString("bankSlip");
            merchantRegistrationPresenter.uploadBankSlip(TLStorage.getInstance().getCurrentUserId(),bankSlip);
        }
    }

    @Override
    public void onBankSlipUpload(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError(String message) {

    }
}
