package com.technolab.android.ui.entity;

public class LiveMarket {
    public String title;
    public String backgroundImage;
    public int totalCount;

    public LiveMarket(String title, String backgroundImage, int totalCount){
        this.title =  title;
        this.backgroundImage = backgroundImage;
        this.totalCount = totalCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
