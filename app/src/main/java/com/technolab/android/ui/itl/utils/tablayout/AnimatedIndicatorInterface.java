package com.technolab.android.ui.itl.utils.tablayout;

import android.graphics.Canvas;

import androidx.annotation.ColorInt;

public interface AnimatedIndicatorInterface {

    long DEFAULT_DURATION = 500;
    void setSelectedTabIndicatorColor(@ColorInt int color);
    void setSelectedTabIndicatorHeight(int height);
    void setIntValues(int startXLeft, int endXLeft, int startXCenter, int endXCenter, int startXRight, int endXRight);
    void setCurrentPlayTime(long currentPlayTime);
    void draw(Canvas canvas);
    long getDuration();
}