package com.technolab.android.ui.itl.main.presenter;

import com.technolab.android.error.TLError;
import com.technolab.android.ui.entity.Cart;
import com.technolab.android.ui.entity.ReferralEmailResult;
import com.technolab.android.ui.itl.spine.AbstractPresenter;
import com.technolab.android.user.TLStorage;
import com.technolab.android.utility.GlobalVariables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TLMainPresenter {

    public static class TLSwitchMerchantPresenter extends AbstractPresenter<TLMainMvpView.SwitchMerchantMVP> {

        @Inject
        public TLSwitchMerchantPresenter() {
        }

        public void fetchMerchantDetail() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveMerchantDetails(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onFetchMerchantDetails(baseResponse.getStatus(), baseResponse.getMessage(), baseResponse.getMerchant());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void fetchActiveLiveStream() {
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveActiveLiveStream()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                if (baseResponse != null)
                                    getMvpView().onFetchLiveMarket(baseResponse.getActiveLiveStream());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void addShare() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .addShare(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onShareSuccessful(baseResponse.getMessage());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void fetchReferralEnergyCount() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .fetchReferralEnergyCount(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onFetchReferralEnergyCount(baseResponse.getReferralCountResults(), baseResponse.getEnergyLevels());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

    public static class TLLiveMarketPresenter extends AbstractPresenter<TLMainMvpView.LiveMarket> {
        @Inject
        public TLLiveMarketPresenter() {
        }

        public void fetchApplicationStatus(String userID) {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveMerchantApplicationStatus(userID)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onCheckMerchantRegistrationStatus(baseResponse.getStatus(), baseResponse.getMerchantApplication());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void fetchActiveLiveStream() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveActiveLiveStream()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onFetchLiveMarket(baseResponse.getActiveLiveStream());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

    public static class TLLiveMarketCategoryPresenter extends AbstractPresenter<TLMainMvpView.LiveMarketCategory> {
        @Inject
        public TLLiveMarketCategoryPresenter() {
        }

        public void fetchAllLiveStream() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveAllLiveStream(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onFetchLiveMarket(baseResponse.getLiveStreamsCategory());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }


    }

    public static class TLLiveStreamDetailPresenter extends AbstractPresenter<TLMainMvpView.LiveStreamingCategory> {
        @Inject
        public TLLiveStreamDetailPresenter() {
        }

        public void fetchLiveStreamDetail(String liveStreamID, boolean isInitialFetch) {
            if (isInitialFetch) {
                GlobalVariables.getHandlerUI().post(new Runnable() {
                    @Override
                    public void run() {
                        getMvpView().showLoadingSpinner();
                    }
                });
            }

            JSONObject object = new JSONObject();
            try {
                object.put("memberid", TLStorage.getInstance().getCurrentUserId());
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveLiveStreamDetails(liveStreamID, requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                if (isInitialFetch)
                                    getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onFetchLiveStreamDetails(baseResponse.getLiveStreamDetail(), isInitialFetch);
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                if (isInitialFetch) {
                                    GlobalVariables.getHandlerUI().post(new Runnable() {
                                        @Override
                                        public void run() {
                                            getMvpView().hideLoadingSpinner();
                                        }
                                    });
                                }
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void updateFavourites(int merchantID, int action) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("action", action);
                object.put("merchantid", merchantID);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .updateFavourites(TLStorage.getInstance().getCurrentUserId(), requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onUpdateFavourite(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveAddress(ArrayList<Cart> cartArrayList) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("action", 5);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveAddress(TLStorage.getInstance().getCurrentUserId(), requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onFetchAddress(baseResponse.getStatus(), baseResponse.getAddress(), cartArrayList);
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void performTransaction(String liveStreamID,
                                       String merchantID, String addressID,
                                       String memberID, String paymentMethod, ArrayList<Cart> cart,
                                       String totalProductPrice, String deliveryFees,
                                       String finalPrice,
                                       String fileTransactionReceipt) {


            MultipartBody.Part fileTransactionReceiptFilePart = null;
            getMvpView().showLoadingSpinner();
            if (fileTransactionReceipt != null) {
                File fileTransactionReceiptFile = new File(fileTransactionReceipt);
                RequestBody fileTransactionReceiptRB = RequestBody.create(MediaType.parse("image/png"), fileTransactionReceiptFile);
                fileTransactionReceiptFilePart = MultipartBody.Part.createFormData("file_transaction_receipt", fileTransactionReceiptFile.getName(), fileTransactionReceiptRB);
            }

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < cart.size(); i++) {
                try {
                    JSONObject objects = new JSONObject();
                    objects.put("product_id", cart.get(i).getProduct().getProduct_id());
                    objects.put("product_variation_id", cart.get(i).getProduct().getVariations().get(cart.get(i).getSelectedVariantIndex()).getProduct_variation_id());
                    objects.put("quantity", cart.get(i).getQuantity());
                    objects.put("unit_price", cart.get(i).getPrice());
                    objects.put("total_price", cart.get(i).getPrice() * cart.get(i).getQuantity());
                    jsonArray.put(objects);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .performTransaction(liveStreamID != null ? RequestBody.create(okhttp3.MultipartBody.FORM, liveStreamID) : null,
                                    merchantID != null ? RequestBody.create(okhttp3.MultipartBody.FORM, merchantID) : null,
                                    addressID != null ? RequestBody.create(okhttp3.MultipartBody.FORM, addressID) : null,
                                    memberID != null ? RequestBody.create(okhttp3.MultipartBody.FORM, memberID) : null,
                                    paymentMethod != null ? RequestBody.create(okhttp3.MultipartBody.FORM, paymentMethod) : null,
                                    jsonArray != null ? RequestBody.create(okhttp3.MultipartBody.FORM, jsonArray.toString()) : null,
                                    totalProductPrice != null ? RequestBody.create(okhttp3.MultipartBody.FORM, totalProductPrice) : null,
                                    deliveryFees != null ? RequestBody.create(okhttp3.MultipartBody.FORM, deliveryFees) : null,
                                    finalPrice != null ? RequestBody.create(okhttp3.MultipartBody.FORM, finalPrice) : null,
                                    fileTransactionReceiptFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus()) {
                                    getMvpView().onFinish(baseResponse.getMessage());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void performTransactionWithoutImage(String liveStreamID, String merchantID, String addressID,
                                                   String memberID, String totalProductPrice, String deliveryFees,
                                                   String finalPrice, ArrayList<Cart> cart) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();

            JSONArray productArray = new JSONArray();
            for (int i = 0; i < cart.size(); i++) {
                try {
                    JSONObject objects = new JSONObject();
                    objects.put("product_id", cart.get(i).getProduct().getProduct_id());
                    objects.put("product_variation_id", cart.get(i).getProduct().getVariations().get(cart.get(i).getSelectedVariantIndex()).getProduct_variation_id());
                    objects.put("quantity", cart.get(i).getQuantity());
                    objects.put("unit_price", cart.get(i).getPrice());
                    objects.put("total_price", cart.get(i).getPrice() * cart.get(i).getQuantity());
                    productArray.put(objects);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            try {
                object.put("live_stream_id", liveStreamID);
                object.put("merchant_id", merchantID);
                object.put("address_id", addressID);
                object.put("member_id", memberID);
                object.put("total_product_price", totalProductPrice);
                object.put("delivery_fees", deliveryFees);
                object.put("final_price", finalPrice);
                object.put("product", productArray);


            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .performTransactionWithoutImage(requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onPartialFinish(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

    }

    public static class TLUserProfilePresenter extends AbstractPresenter<TLMainMvpView.UserProfile> {

        private MultipartBody.Part profileImageFilePart = null;

        @Inject
        public TLUserProfilePresenter() {
        }

        public void uploadUserDetail(String name, String expertise, String profileImage) {

            getMvpView().showLoadingSpinner();
            if (profileImage != null) {
                File profileImageFile = new File(profileImage);
                RequestBody profileImageRB = RequestBody.create(MediaType.parse("image/png"), profileImageFile);
                profileImageFilePart = MultipartBody.Part.createFormData("file_member_profile", profileImageFile.getName(), profileImageRB);
            }

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .updateProfile(TLStorage.getInstance().getCurrentUserId(),
                                    name != null ? RequestBody.create(okhttp3.MultipartBody.FORM, name) : null,
                                    expertise != null ? RequestBody.create(okhttp3.MultipartBody.FORM, expertise) : null,
                                    profileImageFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus()) {
                                    TLStorage.getInstance().setUserImage(profileImage != null ? profileImage : TLStorage.getInstance().getPrefUserImage());
                                    TLStorage.getInstance().setCurrentUserName(name != null ? name : TLStorage.getInstance().getCurrentUserName());
                                    TLStorage.getInstance().setUserExpertise(expertise != null ? expertise : TLStorage.getInstance().getUserExpertise());
                                    getMvpView().onFinish(baseResponse.getMessage());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveAddress() {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("action", 1);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveAddress(TLStorage.getInstance().getCurrentUserId(), requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onReceiveAddress(baseResponse.getAddressList());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void addAddress(String address1, String address2, String city, String postcode, String state, boolean isDefault) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("action", 2);
                object.put("address_line_1", address1);
                object.put("address_line_2", address2);
                object.put("city", city);
                object.put("postcode", postcode);
                object.put("state", state);
                object.put("isdefault", isDefault);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .addAddress(TLStorage.getInstance().getCurrentUserId(), requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onFinish(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void deleteAddress(int addressID, boolean isDefault) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("action", 3);
                object.put("addressid", addressID);
                object.put("isdefault", isDefault);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .addAddress(TLStorage.getInstance().getCurrentUserId(), requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onFinish(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void updateAddress(int addressID, String address1, String address2, String city, int postcode, String state, boolean isDefaultBefore, boolean isDefaultAfter) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("addressid", addressID);
                object.put("action", 4);
                object.put("address_line_1", address1);
                object.put("address_line_2", address2);
                object.put("city", city);
                object.put("postcode", postcode);
                object.put("state", state);
                object.put("isdefaultbefore", isDefaultBefore);
                object.put("isdefaultafter", isDefaultAfter);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .addAddress(TLStorage.getInstance().getCurrentUserId(), requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onFinish(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

    public static class TLMerchantProfilePresenter extends AbstractPresenter<TLMainMvpView.MerchantProfile> {

        private MultipartBody.Part profileImageFilePart = null;

        @Inject
        public TLMerchantProfilePresenter() {
        }

        public void uploadMerchantDetail(String companyName, String bankName, String bankAccountNumber, String bankAccountHolder, String state,
                                         String email, String streetAddress, String city, String postCode, String profileImage) {

            getMvpView().showLoadingSpinner();
            if (profileImage != null) {
                File profileImageFile = new File(profileImage);
                RequestBody profileImageRB = RequestBody.create(MediaType.parse("image/png"), profileImageFile);
                profileImageFilePart = MultipartBody.Part.createFormData("file_merchant_profile", profileImageFile.getName(), profileImageRB);
            }

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .updateMerchantProfile(TLStorage.getInstance().getMerchantId(),
                                    companyName != null ? RequestBody.create(okhttp3.MultipartBody.FORM, companyName) : null,
                                    bankName != null ? RequestBody.create(okhttp3.MultipartBody.FORM, bankName) : null,
                                    bankAccountNumber != null ? RequestBody.create(okhttp3.MultipartBody.FORM, bankAccountNumber) : null,
                                    bankAccountHolder != null ? RequestBody.create(okhttp3.MultipartBody.FORM, bankAccountHolder) : null,
                                    state != null ? RequestBody.create(okhttp3.MultipartBody.FORM, state) : null,
                                    email != null ? RequestBody.create(okhttp3.MultipartBody.FORM, email) : null,
                                    streetAddress != null ? RequestBody.create(okhttp3.MultipartBody.FORM, streetAddress) : null,
                                    city != null ? RequestBody.create(okhttp3.MultipartBody.FORM, city) : null,
                                    postCode != null ? RequestBody.create(okhttp3.MultipartBody.FORM, postCode) : null,
                                    profileImageFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus()) {
                                    getMvpView().onFinish(baseResponse.getMessage());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveMerchantDetails() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveMerchantProfile(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onReceiveProfile(baseResponse.getStatus(), baseResponse.getMessage(), baseResponse.getMerchant());
                                    TLStorage.getInstance().setMerchantCompanyName(baseResponse.getMerchant().getMerchantCompanyName());
                                    TLStorage.getInstance().setMerchantImage(baseResponse.getMerchant().getMerchantImage());
                                    TLStorage.getInstance().setMerchantFresh(baseResponse.getMerchant().getFreshMerchant());
                                    TLStorage.getInstance().setTotalLiveTime(Long.parseLong(baseResponse.getMerchant().getRemainingLiveTime()));
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

    }

    public static class TLUserReferralPresenter extends AbstractPresenter<TLMainMvpView.Referral> {

        @Inject
        public TLUserReferralPresenter() {
        }

        public void retrieveTotalReferral() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .getTotalReferral(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onResponseReferral(baseResponse.getOverallReferralResults());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveTodayReferral() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .getTodayReferral(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onResponseReferral(baseResponse.getOverallReferralResults());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveWeekReferral() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .getWeekReferral(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onResponseReferral(baseResponse.getOverallReferralResults());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveMonthReferral() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .getMonthReferral(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onResponseReferral(baseResponse.getOverallReferralResults());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveEmailTotalReferral() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .getEmailTotalReferral(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    if (baseResponse.getReferralEmailResults() != null && baseResponse.getReferralEmailResults().size() > 0)
                                        getMvpView().onResponseEmail(baseResponse.getReferralEmailResults());
                                    else
                                        getMvpView().onResponseEmail(new ArrayList<>());

                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveEmailTodayReferral() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .getEmailTodayReferral(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    if (baseResponse.getReferralEmailResults() != null && baseResponse.getReferralEmailResults().size() > 0)
                                        getMvpView().onResponseEmail(baseResponse.getReferralEmailResults());
                                    else
                                        getMvpView().onResponseEmail(new ArrayList<>());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveEmailWeekReferral() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .getEmailWeekReferral(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    if (baseResponse.getReferralEmailResults() != null && baseResponse.getReferralEmailResults().size() > 0)
                                        getMvpView().onResponseEmail(baseResponse.getReferralEmailResults());
                                    else
                                        getMvpView().onResponseEmail(new ArrayList<>());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void retrieveEmailMonthReferral() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .getEmailMonthReferral(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    if (baseResponse.getReferralEmailResults() != null && baseResponse.getReferralEmailResults().size() > 0)
                                        getMvpView().onResponseEmail(baseResponse.getReferralEmailResults());
                                    else
                                        getMvpView().onResponseEmail(new ArrayList<>());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

    }

    public static class TLMerchantPresenter extends AbstractPresenter<TLMainMvpView.MerchantMVP> {
        @Inject
        public TLMerchantPresenter() {
        }

        public void fetchUpcomingLive() {
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .fetchUpcomingLive(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                if (baseResponse != null) {
                                    getMvpView().onFetchUpcomingLive(baseResponse.getStatus(), baseResponse.getLiveStream());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void isProductsAvailable() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveMerchantProduct(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();

                                if (baseResponse != null && baseResponse.getStatus()) {
                                    getMvpView().onRetrieveProductDetails(baseResponse.getProductArrayList());
                                } else {
                                    getMvpView().onError("Something went wrong. Please try again. ");
                                }

                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void fetchTopLiveStream() {
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .fetchTopLiveStream(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                if (baseResponse != null) {
                                    getMvpView().onFetchTopLiveStream(baseResponse.getStatus(), baseResponse.getTopLiveStreamResults());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void fetchTransactionHistory() {
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .fetchTransactionHistory(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                if (baseResponse != null) {
                                    getMvpView().onFetchTransactionHistory(baseResponse.getStatus(), baseResponse.getTransactionHistoryResults());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void fetchMerchantEnergyCount() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .fetchMerchantEnergyCount(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onFetchEnergy(baseResponse.getMerchantEnergyInfo());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }
    }

    public static class TLUserTransaction extends AbstractPresenter<TLMainMvpView.UserTransaction> {

        @Inject
        public TLUserTransaction() {
        }

        public void fetchTransactions(int status) {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .fetchTransaction(TLStorage.getInstance().getCurrentUserId(), status)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onReceiveTransaction(baseResponse.getTransactions());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void updateTransaction(String transactionID, String memberID,
                                      String merchantID, String paymentMethod,
                                      String fileTransactionReceipt) {

            MultipartBody.Part fileTransactionReceiptFilePart = null;
            getMvpView().showLoadingSpinner();
            if (fileTransactionReceipt != null) {
                File fileTransactionReceiptFile = new File(fileTransactionReceipt);
                RequestBody fileTransactionReceiptRB = RequestBody.create(MediaType.parse("image/png"), fileTransactionReceiptFile);
                fileTransactionReceiptFilePart = MultipartBody.Part.createFormData("file_transaction_receipt", fileTransactionReceiptFile.getName(), fileTransactionReceiptRB);
            }

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .updateTransaction(transactionID,
                                    memberID != null ? RequestBody.create(okhttp3.MultipartBody.FORM, memberID) : null,
                                    merchantID != null ? RequestBody.create(okhttp3.MultipartBody.FORM, merchantID) : null,
                                    paymentMethod != null ? RequestBody.create(okhttp3.MultipartBody.FORM, paymentMethod) : null,
                                    fileTransactionReceiptFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus()) {
                                    getMvpView().onPayment(baseResponse.getMessage());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }


    }

    public static class TLMerchantTransaction extends AbstractPresenter<TLMainMvpView.MerchantTransaction> {

        @Inject
        public TLMerchantTransaction() {
        }

        public void fetchTransactions(int status) {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .fetchMerchantTransaction(TLStorage.getInstance().getMerchantId(), status)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onReceiveTransaction(baseResponse.getTransactions());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void updateTransactionStatus(String transactionID, String trackingURL, int status, String courier) {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .updateTransactionStatus(transactionID, trackingURL, status, courier)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onResponse(baseResponse.getMessage());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void approveTransaction(String transactionID, String remark) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("remarks", remark);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .approveTransaction(transactionID, requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onResponse(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void rejectTransaction(String transactionID, String remark) {
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("remarks", remark);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .rejectTransaction(transactionID, requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onResponse(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

    }

    public static class TLRedeemEnergy extends AbstractPresenter<TLMainMvpView.RedeemEnergy> {
        @Inject
        public TLRedeemEnergy() {
        }

        public void retrieveEnergySummary() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveEnergySummary(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onReceiveEnergyData(baseResponse.getReferralCount());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void retrieveEnergyLevel() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveEnergyLevel(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onReceiveEnergyLevel(baseResponse.getLevel_results());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void retrieveEnergyHistory() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveEnergyHistory(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onReceiveEnergyLevel(baseResponse.getLevel_results());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void redeemEnergy(String type) {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .redeemEnergy(type, TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onRedeemEnergy(baseResponse.getStatus(), baseResponse.getMessage(), baseResponse.dailyEnergyRedeemed);
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

    }

    public static class TLRanking extends AbstractPresenter<TLMainMvpView.Ranking> {
        @Inject
        public TLRanking() {
        }

        public void retrieveRanking(int type) {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveRanking()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onReceiveData(baseResponse.getRankingResults(), type);
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }
    }

    public static class TLRewards extends AbstractPresenter<TLMainMvpView.Reward> {
        @Inject
        public TLRewards() {
        }

        public void retrieveRewards() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveRewardInfo(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null) {
                                    getMvpView().onReceiveData(baseResponse.getRedeemable(), baseResponse.getMessage(), baseResponse.getRank(), baseResponse.getRewardPoint());
                                }
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }

        public void claimReward(String rank, String points){
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();
            try {
                object.put("points", points);
                object.put("rank", rank);
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .claimReward(TLStorage.getInstance().getCurrentUserId(), requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onClaimReward(baseResponse.getMessage());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }
                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

}
