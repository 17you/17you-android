package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("member_id")
    private Integer memberId;
    @SerializedName("member_username")
    private String memberUsername;
    @SerializedName("member_is_merchant")
    private Boolean memberIsMerchant;
    @SerializedName("member_account_status")
    private Boolean memberAccountStatus;
    @SerializedName("member_expertise")
    private String memberExpertise;
    @SerializedName("member_referrer_code")
    private String memberReferrerCode;
    @SerializedName("member_state")
    private String memberState;
    @SerializedName("member_privacy_protection")
    private Boolean memberPrivacyProtection;
    @SerializedName("member_signed_up_date")
    private String memberSignedUpDate;
    @SerializedName("member_password")
    private String memberPassword;
    @SerializedName("member_referral_code")
    private String memberReferralCode;
    @SerializedName("member_phone")
    private String memberPhone;
    @SerializedName("member_email")
    private String memberEmail;
    @SerializedName("member_image")
    private String memberImage;

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberUsername() {
        return memberUsername;
    }

    public void setMemberUsername(String memberUsername) {
        this.memberUsername = memberUsername;
    }

    public Boolean getMemberIsMerchant() {
        return memberIsMerchant;
    }

    public void setMemberIsMerchant(Boolean memberIsMerchant) {
        this.memberIsMerchant = memberIsMerchant;
    }

    public Boolean getMemberAccountStatus() {
        return memberAccountStatus;
    }

    public void setMemberAccountStatus(Boolean memberAccountStatus) {
        this.memberAccountStatus = memberAccountStatus;
    }

    public String getMemberExpertise() {
        return memberExpertise;
    }

    public void setMemberExpertise(String memberExpertise) {
        this.memberExpertise = memberExpertise;
    }

    public String getMemberReferrerCode() {
        return memberReferrerCode;
    }

    public void setMemberReferrerCode(String memberReferrerCode) {
        this.memberReferrerCode = memberReferrerCode;
    }

    public String getMemberState() {
        return memberState;
    }

    public void setMemberState(String memberState) {
        this.memberState = memberState;
    }

    public Boolean getMemberPrivacyProtection() {
        return memberPrivacyProtection;
    }

    public void setMemberPrivacyProtection(Boolean memberPrivacyProtection) {
        this.memberPrivacyProtection = memberPrivacyProtection;
    }

    public String getMemberSignedUpDate() {
        return memberSignedUpDate;
    }

    public void setMemberSignedUpDate(String memberSignedUpDate) {
        this.memberSignedUpDate = memberSignedUpDate;
    }

    public String getMemberPassword() {
        return memberPassword;
    }

    public void setMemberPassword(String memberPassword) {
        this.memberPassword = memberPassword;
    }

    public String getMemberReferralCode() {
        return memberReferralCode;
    }

    public void setMemberReferralCode(String memberReferralCode) {
        this.memberReferralCode = memberReferralCode;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone;
    }

    public String getMemberEmail() {
        return memberEmail;
    }

    public void setMemberEmail(String memberEmail) {
        this.memberEmail = memberEmail;
    }

    public String getMemberImage() {
        return memberImage;
    }

    public void setMemberImage(String memberImage) {
        this.memberImage = memberImage;
    }
}
