package com.technolab.android.ui.itl.utils.drawer.callback;

public interface DragListener {
    void onDrag(float progress);
}
