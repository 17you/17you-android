package com.technolab.android.ui.itl.main.MerchantProductManagement.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.main.MerchantProductManagement.presenter.TLMerchantProductManagementMvpView;
import com.technolab.android.ui.itl.main.MerchantProductManagement.presenter.TLMerchantProductManagementPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

import ir.androidexception.andexalertdialog.AndExAlertDialog;
import ir.androidexception.andexalertdialog.AndExAlertDialogListener;

public class TLProductManagement extends AbstractActivity implements TLMerchantProductManagementMvpView.ProductMVP, MerchantProductAdapter.ItemClickListener{

    private Group zeroStateProduct;
    private RecyclerView rvProduct;
    private Button btnCreateProduct;

    @Inject
    TLMerchantProductManagementPresenter.TLProductPresenter productPresenter;
    MerchantProductAdapter merchantProductAdapter;

    public TLProductManagement(){
        TAG = "TLProductManagement";
        layoutID = R.layout.activity_product_management;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        setupRefresh(this);
        zeroStateProduct = findViewById(R.id.zero_state_product);
        rvProduct = findViewById(R.id.rv_product);
        btnCreateProduct = findViewById(R.id.btn_create_product);
        btnCreateProduct.setVisibility(View.GONE);
        btnCreateProduct.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLProductManagement.this, TLCreateProduct.class);
                intent.putExtra("isEditMode", false);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLProductManagement.this);
        productPresenter.onAttach(this);
        productPresenter.fetchProductList();
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> productPresenter.fetchProductList());
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Product Setup");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onRetrieveProductList(ArrayList<Product> productArrayList) {
        refreshLayout.setRefreshing(false);
        if(productArrayList.size() == 0) {
            zeroStateProduct.setVisibility(View.VISIBLE);
            rvProduct.setVisibility(View.GONE);
        }else{
            zeroStateProduct.setVisibility(View.GONE);
            rvProduct.setVisibility(View.VISIBLE);

            merchantProductAdapter = new MerchantProductAdapter(this,productArrayList);
            rvProduct.setAdapter(merchantProductAdapter);
            merchantProductAdapter.setOnItemClickListener(this);
        }
        btnCreateProduct.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(String message) {

    }

    @Override
    public void onDelete(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        productPresenter.fetchProductList();
    }

    @Override
    public void onItemClicked(int type, Product product) {
        if(type == 2){
            String message = "Are you sure you want to delete the "+ product.getProduct_name()+"\n"+ product.getProduct_description();
            new AndExAlertDialog.Builder(TLProductManagement.this)
                    .setTitle("Delete Product Setup")
                    .setMessage(message)
                    .setPositiveBtnText("Yes, Delete")
                    .setNegativeBtnText("No, Thank You!")
                    .setCancelableOnTouchOutside(true)
                    .OnNegativeClicked(input -> {})
                    .OnPositiveClicked(input -> productPresenter.deleteProduct(String.valueOf(product.getProduct_id())))
                    .setMessageTextColor(R.color.black)
                    .setButtonTextColor(R.color.black)
                    .build();
        }else if(type == 1){
            Intent intent = new Intent(TLProductManagement.this, TLCreateProduct.class);
            intent.putExtra("isEditMode", true);
            intent.putExtra("productID", product.getProduct_id());
            startActivity(intent);
        }
    }
}
