package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Industry implements Serializable {
    @SerializedName("industry_category_id")
    private int industry_category_id;
    @SerializedName("industry_category_name")
    private String industry_category_name;
    @SerializedName("count")
    private String count;
    @SerializedName("industry_category_image")
    private String industry_category_image;

    public String getIndustry_category_image() {
        return industry_category_image;
    }

    public void setIndustry_category_image(String industry_category_image) {
        this.industry_category_image = industry_category_image;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public int getIndustry_category_id() {
        return industry_category_id;
    }

    public void setIndustry_category_id(int industry_category_id) {
        this.industry_category_id = industry_category_id;
    }

    public String getIndustry_category_name() {
        return industry_category_name;
    }

    public void setIndustry_category_name(String industry_category_name) {
        this.industry_category_name = industry_category_name;
    }
}
