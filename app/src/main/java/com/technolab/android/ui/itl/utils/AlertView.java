package com.technolab.android.ui.itl.utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.technolab.android.R;
import com.technolab.android.utility.GlobalVariables;

public class AlertView extends Dialog
{
    private static AlertView ourInstance;

    public static synchronized AlertView getInstance(Activity activity) {
        if (ourInstance == null)
            ourInstance = new AlertView(activity);

        return ourInstance;
    }

    public static synchronized void resetInstance() {
        ourInstance = null;
    }

    private AlertView(Activity context) {
        super(context);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            dismissAlertView();
        }
    };

    private Button buttonLeft;
    private Button buttonRight;
    private Button buttonCenter;
    private TextView textViewMessage;
    private TextView textViewTitle;

    private Handler onObject_center;
    private Handler onObject_left;
    private Handler onObject_right;
    private Runnable selector_center;
    private Runnable selector_left;
    private Runnable selector_right;
    private String message;
    private String messageTitle;

    private int alertType;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_alert_dialog_with_two_buttons);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        textViewTitle = (TextView) findViewById(R.id.tv_title);
        textViewMessage = (TextView) findViewById(R.id.tv_description);
        textViewMessage.setMovementMethod(new ScrollingMovementMethod());

        buttonCenter = (Button) findViewById(R.id.tv_center_option);
        buttonLeft = (Button) findViewById(R.id.tv_left_option);
        buttonRight = (Button) findViewById(R.id.tv_right_option);

        buttonCenter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                buttonCenter.setEnabled(false);
                onObject_center.post(selector_center);
            }
        });
        buttonRight.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                buttonRight.setEnabled(false);
                onObject_right.post(selector_right);
            }
        });
        buttonLeft.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                buttonLeft.setEnabled(false);
                onObject_left.post(selector_left);
            }
        });

        this.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onBackPressed()
    {
        //Do nothing
    }

    private void resetAlertView()
    {
        onObject_center = null;
        onObject_left = null;
        onObject_right = null;
        selector_center = null;
        selector_left = null;
        selector_right = null;
        message = "";
        textViewMessage.setText("");
        textViewTitle.setVisibility(View.GONE);
        buttonCenter.setVisibility(View.INVISIBLE);
        buttonLeft.setVisibility(View.INVISIBLE);
        buttonRight.setVisibility(View.INVISIBLE);
    }

    /**
     * Dismiss Alertview. This method can be called in any thread
     */
    public void dismissAlertView()
    {
       GlobalVariables.getHandlerUI().post(runnableDismiss);
    }

    private Runnable runnableDismiss = new Runnable()
    {
        @Override
        public void run()
        {
            resetAlertView();
            dismiss();
        }
    };

    /**
     * Show Alert view with only 1 button at the center. No need to worry about threading anymore, just call this method in any thread
     * @param msg           Message of the alert
     * @param centerMsg     Title for the center button
     * @param sel_center   Runnable object to be triggered on button click
     * @param obj_center   Handler object to post selector_center runnable. If value is null, default handler and runnable will be used (to
     *                          close the alert view)
     */
    public void showAlertWithCenterButton(Activity activity, final String msg, final String centerMsg, final Runnable sel_center, final Handler obj_center)
    {
        if (!isShowing())
        {
            show();
            resetAlertView();
            alertType               = 1;
            message            = msg;
            onObject_center    = obj_center == null ? GlobalVariables.getHandlerUI() : obj_center;
            selector_center    = sel_center == null ? runnable : sel_center;
            buttonCenter.setText(centerMsg);
            showAlert();
        }
    }
    /**
     * Show Alert view with only 1 button at the center and with title message. No need to worry about threading anymore, just call this method in any thread
     * @param msg           Message of the alert
     * @param title         Title of the alert
     * @param centerMsg     Title for the center button
     * @param sel_center   Runnable object to be triggered on button click
     * @param obj_center   Handler object to post selector_center runnable. If value is null, default handler and runnable will be used (to
     *                          close the alert view)
     */
    public void showAlertWithCenterButtonAndTitle(final String msg,final String title, final String centerMsg, final Runnable sel_center, final Handler obj_center)
    {
        GlobalVariables.getHandlerUI().post(new Runnable()
        {
            @Override
            public void run()
            {
                if (!isShowing())
                {
                    show();
                    resetAlertView();
                    alertType               = 1;
                    message            = msg;
                    messageTitle       = title;
                    onObject_center    = obj_center == null ? GlobalVariables.getHandlerUI() : obj_center;
                    selector_center    = sel_center == null ? runnable : sel_center;
                    buttonCenter.setText(centerMsg);
                    showAlertWithTitle();
                }
            }
        });
    }

    /**
     * Show Alert view with 2 buttons. No need to worry about threading anymore, just call this method in any thread
     * @param msg           Message of the alert
     * @param msg_left       Title of the left button
     * @param sel_left     Runnable object to be triggered on left button click
     * @param obj_left     Handler object to post the selector_left runnable. If value is null, default handler and runnable will be used (to
     *                          close the alert view)
     * @param msg_right      Title of the right button
     * @param sel_right    Runnable object to be triggered on right button click
     * @param obj_right    Handler object to post the selector_right runnable. If value is null, default handler and runnable will be used (to
     *                          close the alert view)
     */
    public void showAlertWithLeftRightButtons(final String msg, final String msg_left, final Runnable sel_left,
                                              final Handler obj_left, final String msg_right, final Runnable sel_right, final Handler obj_right)
    {
        GlobalVariables.getHandlerUI().post(new Runnable()
        {
            @Override
            public void run()
            {
                if (!isShowing())
                {
                    show();
                    resetAlertView();
                    alertType           = 2;
                    message             = msg;
                    onObject_right     = obj_right == null ? GlobalVariables.getHandlerUI() : obj_right;
                    selector_right     = sel_right == null ? runnable : sel_right;
                    buttonRight.setText(msg_right);
                    onObject_left      = obj_left == null ? GlobalVariables.getHandlerUI() : obj_left;
                    selector_left      = sel_left == null ? runnable : sel_left;
                    buttonLeft.setText(msg_left);
                    showAlert();
                }
            }
        });

    }

    private void showAlert ()
    {
        this.textViewMessage.setText(message);
        buttonLeft.setEnabled(true);
        buttonCenter.setEnabled(true);
        buttonRight.setEnabled(true);
        switch (alertType)
        {
            case 1:
                buttonCenter.setVisibility(View.VISIBLE);
                break;

            case 2:
                buttonLeft.setVisibility(View.VISIBLE);
                buttonRight.setVisibility(View.VISIBLE);
                break;
        }
    }
    private void showAlertWithTitle()
    {
        this.textViewTitle.setVisibility(View.VISIBLE);
        this.textViewMessage.setText(message);
        buttonLeft.setEnabled(true);
        buttonCenter.setEnabled(true);
        buttonRight.setEnabled(true);
        switch (alertType)
        {
            case 1:
                buttonCenter.setVisibility(View.VISIBLE);
                break;

            case 2:
                buttonLeft.setVisibility(View.VISIBLE);
                buttonRight.setVisibility(View.VISIBLE);
                break;
        }
    }

}

