package com.technolab.android.ui.itl.spine;

import com.technolab.android.db.DataRepositoryManager;
import com.technolab.android.di.module.APIInterface;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class AbstractPresenter<V extends AbstractMvpView> implements AbstractMvpPresenter<V> {

    private V mMvpView;
    protected CompositeDisposable mCompositeDisposable;

    @Inject
    protected DataRepositoryManager mDataRepositoryManager;

    @Inject
    public AbstractPresenter() {
       mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mCompositeDisposable.dispose();
        mMvpView = null;
    }

    @Override
    public boolean isViewAttached() {
        return mMvpView != null;
    }

    @Override
    public V getMvpView() {
        return mMvpView;
    }
}
