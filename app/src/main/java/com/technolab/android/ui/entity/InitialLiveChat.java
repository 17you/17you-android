package com.technolab.android.ui.entity;

public class InitialLiveChat {
    private String userName;
    private String roomName;

    public InitialLiveChat(String userName, String roomName) {
        this.userName = userName;
        this.roomName = roomName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
