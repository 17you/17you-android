package com.technolab.android.ui.itl.main.LoginManagement.presenter;

import com.technolab.android.ui.itl.spine.AbstractMvpView;

public interface TLSignInMvpView extends AbstractMvpView {
    void onLoginSuccess(String message);
    void onLoginFailed(String message);
    void onCheck(String message);
}
