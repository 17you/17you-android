package com.technolab.android.ui.itl.main.MerchantProductManagement.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.Variation;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLCreateLiveStream;
import com.technolab.android.ui.itl.main.MerchantProductManagement.presenter.TLMerchantProductManagementMvpView;
import com.technolab.android.ui.itl.main.MerchantProductManagement.presenter.TLMerchantProductManagementPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

public class TLCreateProduct extends AbstractActivity implements TLMerchantProductManagementMvpView.CreateProduct {

    private LinearLayout ivAddProductPlaceholder;
    private ImageView ivProduct;

    private EditText etProductName;
    private EditText etProductDescription;
    private EditText etCategory;
    private EditText etVariation;

    private Button btnAddProduct;
    private Button btnSave;

    private String imagePath;
    private int mainCatId;
    private int subCatId;

    private String mainCatName;
    private String subCatName;

    private ArrayList<Variation> mProductVariation;
    private RecyclerView rvProductVariation;
    private MerchantShowVariationAdapter merchantShowVariationAdapter;
    private boolean isEditMode;

    @Inject
    TLMerchantProductManagementPresenter.TLCreateProductPresenter merchantCreateProductPresenter;

    public TLCreateProduct() {
        TAG = "TLCreateProduct";
        layoutID = R.layout.activity_merchant_create_product;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Add Product");
        txtSubHeader.setVisibility(View.GONE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        getAppComponent().inject(TLCreateProduct.this);
        merchantCreateProductPresenter.onAttach(this);
        ivAddProductPlaceholder = findViewById(R.id.iv_product);
        etProductName = findViewById(R.id.et_title);
        etProductDescription = findViewById(R.id.et_desc);
        etCategory = findViewById(R.id.et_category);
        etVariation = findViewById(R.id.et_variation);
        btnAddProduct = findViewById(R.id.btn_add);
        btnSave = findViewById(R.id.btn_save);
        ivProduct = findViewById(R.id.iv_imageView);
        rvProductVariation = findViewById(R.id.rv_variation);
        mProductVariation = new ArrayList<>();

        etCategory.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent chooseProductCategoryIntent = new Intent(TLCreateProduct.this, TLSelectProductCategory.class);
                startActivityForResult(chooseProductCategoryIntent, 1234);
            }
        });

        etVariation.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent createVariantIntent = new Intent(TLCreateProduct.this, TLAddProductVariant.class);
                if(mProductVariation!=null && mProductVariation.size()>0){
                    createVariantIntent.putExtra("productVariationData", mProductVariation);
                }

                startActivityForResult(createVariantIntent, 4321);
            }
        });

        ivAddProductPlaceholder.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLCreateProduct.this, ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, true);
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, false);
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);
                intent.putExtra(ImageSelectActivity.FLAG_CROP, true);
                startActivityForResult(intent, 1213);
            }
        });

        isEditMode = getIntent().getBooleanExtra("isEditMode", false);
        editMode(isEditMode);
    }

    private void editMode(boolean isEditMode){
        if (isEditMode) {
            btnAddProduct.setVisibility(View.GONE);
            btnSave.setVisibility(View.VISIBLE);
            txtHeader.setText("Edit Product");
            merchantCreateProductPresenter.fetchSingleProduct(getIntent().getStringExtra("productID"));
        }
        else {
            btnAddProduct.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.GONE);
            txtHeader.setText("Add Product");
            initSetup();
        }
    }

    private void setupData(Product product){
        ivProduct.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(product.getProduct_image())
                .thumbnail(0.1f)
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into(ivProduct);
        etProductName.setText(product.getProduct_name());
        etProductDescription.setText(product.getProduct_description());
        mainCatId = product.getProduct_main_cat_id();
        subCatId = product.getProduct_sub_cat_id();
        mainCatName = product.getProduct_main_cat_name();
        subCatName = product.getProduct_sub_cat_name();

        String completeCategoryForDisplay = mainCatName + " > " + subCatName;
        etCategory.setText(completeCategoryForDisplay);
        if (product.getVariationArrayList().size() > 0) {
            mProductVariation = product.getVariationArrayList();
            merchantShowVariationAdapter = new MerchantShowVariationAdapter(this, product.getVariationArrayList());
            rvProductVariation.setAdapter(merchantShowVariationAdapter);
            rvProductVariation.setVisibility(View.VISIBLE);
        }

        btnSave.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (etProductName.getText().toString().isEmpty() || etProductDescription.getText().toString().isEmpty()
                        || mainCatId == 0 || subCatId == 0) {
                    Toast.makeText(TLCreateProduct.this, "Please complete all the requested information", Toast.LENGTH_SHORT).show();
                    return;
                }

                String productID = String.valueOf(product.getProduct_id());
                String productName = !etProductName.getText().toString().equals(product.getProduct_name()) ? etProductName.getText().toString() : product.getProduct_name();
                String productDesc = !etProductDescription.getText().toString().equals(product.getProduct_description()) ? etProductDescription.getText().toString() : product.getProduct_description();
                int mainCategoryID = !mainCatName.equals(product.getProduct_main_cat_name()) ? mainCatId : product.getProduct_main_cat_id();
                int subCategoryID = !subCatName.equals(product.getProduct_sub_cat_name()) ? subCatId : product.getProduct_sub_cat_id();
                ArrayList<Variation> variations = mProductVariation != null && mProductVariation.size()>0 ? mProductVariation : product.getVariationArrayList();
                String imageFile = imagePath;

                merchantCreateProductPresenter.updateProductDetails(productID,productName,productDesc,mainCategoryID,subCategoryID,variations,imageFile);
            }
        });
    }

    private void initSetup(){
        btnAddProduct.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (etProductName.getText().toString().isEmpty() || etProductDescription.getText().toString().isEmpty()
                        || mainCatId == 0 || subCatId == 0 || mProductVariation.size()<=0 || imagePath.isEmpty()) {
                    Toast.makeText(TLCreateProduct.this, "Please complete all the requested information", Toast.LENGTH_SHORT).show();
                    return;
                }

                merchantCreateProductPresenter.addProductDetails(etProductName.getText().toString(),etProductDescription.getText().toString(),mainCatId,subCatId,mProductVariation,imagePath);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        merchantCreateProductPresenter.onDetach();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            imagePath = filePath;
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            ivProduct.setImageBitmap(selectedImage);
            ivProduct.setVisibility(View.VISIBLE);
        } else if (requestCode == 1234 && resultCode == Activity.RESULT_OK) {
            if (data.getExtras().getInt("mainCatId") != 0 &&
                    data.getExtras().getString("mainCatName") != null &&
                    data.getExtras().getInt("subCatId") != 0 &&
                    data.getExtras().getString("subCatName") != null) {
                mainCatId = data.getExtras().getInt("mainCatId");
                subCatId = data.getExtras().getInt("subCatId");
                mainCatName = data.getExtras().getString("mainCatName");
                subCatName = data.getExtras().getString("subCatName");
                String completeCategoryForDisplay = mainCatName + " > " + subCatName;
                etCategory.setText(completeCategoryForDisplay);
            }
        } else if (requestCode == 4321 && resultCode == Activity.RESULT_OK) {
            mProductVariation = (ArrayList<Variation>) data.getSerializableExtra("productVariationData");

            if (mProductVariation.size() > 0) {
                merchantShowVariationAdapter = new MerchantShowVariationAdapter(this, mProductVariation);
                rvProductVariation.setAdapter(merchantShowVariationAdapter);
                rvProductVariation.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onFetchData(Product product) {
        setupData(product);
    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(String message) {

    }
}
