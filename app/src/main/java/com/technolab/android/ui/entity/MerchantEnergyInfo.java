package com.technolab.android.ui.entity;

public class MerchantEnergyInfo {
    public int total_live_stream;
    public int total_energy;

    public int getTotal_live_stream() {
        return total_live_stream;
    }

    public void setTotal_live_stream(int total_live_stream) {
        this.total_live_stream = total_live_stream;
    }

    public int getTotal_energy() {
        return total_energy;
    }

    public void setTotal_energy(int total_energy) {
        this.total_energy = total_energy;
    }
}
