package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

public class Ranking {
    @SerializedName("daily")
    public RankingType daily;
    @SerializedName("weekly")
    public RankingType weekly;
    @SerializedName("monthly")
    public RankingType monthly;

    public RankingType getDaily() {
        return daily;
    }

    public void setDaily(RankingType daily) {
        this.daily = daily;
    }

    public RankingType getWeekly() {
        return weekly;
    }

    public void setWeekly(RankingType weekly) {
        this.weekly = weekly;
    }

    public RankingType getMonthly() {
        return monthly;
    }

    public void setMonthly(RankingType monthly) {
        this.monthly = monthly;
    }
}
