package com.technolab.android.ui.itl.main;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.technolab.android.BuildConfig;
import com.technolab.android.R;
import com.technolab.android.db.DBController;
import com.technolab.android.security.KeyOne;
import com.technolab.android.security.SecOne;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp.TLSignUp;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.user.TLEnvironment;
import com.technolab.android.user.TLStorage;
import com.technolab.android.utility.GlobalVariables;
import com.technolab.android.utility.TLConstant;
import com.technolab.android.utility.Utility;

import java.io.File;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public class SplashScreenActivity extends AbstractActivity {

    private RelativeLayout splashLayout;
    private ProgressBar progressLauncher;
    private String referral = null;
    private String live = null;
    private boolean isFromDeeplink = false;

    public SplashScreenActivity() {
        TAG = "Splash Screen";
        layoutID = R.layout.activity_splash_screen;
        statusBarColorID = R.color.colorO;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.splashLayout = (RelativeLayout) findViewById(R.id.login_splash_layout);
        this.progressLauncher = (ProgressBar) findViewById(R.id.login_launcher_progress);
        isFromDeeplink = getIntent().getBooleanExtra("isFromDeeplink", false);

        if(isFromDeeplink){
            live = getIntent().getStringExtra("liveStreamID") != null ? getIntent().getStringExtra("liveStreamID") : null;
            referral = getIntent().getStringExtra("referral") != null ? getIntent().getStringExtra("referral") : null;
        }
        if (BuildConfig.DEBUG)
            Log.d(this.getLocalClassName(), "Init");

        //Reset instances
        GlobalVariables.resetInstance();
        DBController.resetInstance();

        GlobalVariables.getInstance().setMainActivity(this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                File tmp = new File(GlobalVariables.getInstance().getContext().getDir(TLConstant.DB_DIR,
                        0).getAbsolutePath() + File.separator + "itechnolab");
                if (tmp.exists()) {
                    DBController.deleteAllFilesInFolder(GlobalVariables.getInstance().getContext().getDir(TLConstant.DB_DIR,
                            0));
                    DBController.deleteAllFilesInFolder(GlobalVariables.getInstance().getContext().getFilesDir());
                }
                File cacheFile = DBController.copyFromRaw(TLConstant.DB_NAME, new File(GlobalVariables.getInstance().getContext().getCacheDir(),
                        TLConstant.DB_NAME));
                File orgFile = new File(GlobalVariables.getInstance().getContext().getDir(TLConstant.DB_DIR,
                        0).getAbsolutePath() + File.separator + TLConstant.DB_NAME);

                boolean isNeedReplace = !DBController.compareDBVersion(cacheFile, SecOne.getInstance().getComSec() + KeyOne.getInstance().getComSec() + SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey(), orgFile, Utility.getInstance().encryptPassword(SecOne.getInstance().getComSec() + KeyOne.getInstance().getComSec(), SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey()));
                progressLauncher.setProgress(20);
                if (orgFile.exists() && isNeedReplace)
                    orgFile.delete();

                if (cacheFile != null) {
                    if (isNeedReplace) {
                        DBController.createNewDB(cacheFile.getAbsolutePath(), SecOne
                                        .getInstance().getComSec() + KeyOne.getInstance().getComSec() +
                                        SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey(),
                                orgFile.getAbsolutePath(),
                                Utility.getInstance().encryptPassword(SecOne
                                                .getInstance().getComSec() + KeyOne.getInstance().getComSec(),
                                        SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey()
                                )
                        );
                    }
                    cacheFile.delete();
                }
                progressLauncher.setProgress(40);
                cacheFile = DBController.copyFromRaw(TLConstant.DB_CACHE_NAME, new File(GlobalVariables.getInstance().getContext().getCacheDir(),
                        TLConstant.DB_CACHE_NAME));
                orgFile = new File(GlobalVariables.getInstance().getContext().getDir(TLConstant.DB_DIR,
                        0).getAbsolutePath() + File.separator + TLConstant.DB_CACHE_NAME);

                isNeedReplace = !DBController.compareDBVersion(cacheFile, SecOne.getInstance().getComSec() + KeyOne.getInstance().getComSec() + SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey(), orgFile, Utility.getInstance().encryptPassword(SecOne.getInstance().getComSec() + KeyOne.getInstance().getComSec(), SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey()));
                progressLauncher.setProgress(60);
                if (orgFile.exists() && isNeedReplace)
                    orgFile.delete();

                if (cacheFile != null) {
                    if (isNeedReplace) {
                        DBController.createNewDB(cacheFile.getAbsolutePath(), SecOne
                                        .getInstance().getComSec() + KeyOne.getInstance().getComSec() +
                                        SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey(),
                                GlobalVariables.getInstance().getContext().getDir(TLConstant.DB_DIR, 0).getAbsolutePath() + File.separator + TLConstant.DB_CACHE_NAME,
                                Utility.getInstance().encryptPassword(SecOne
                                                .getInstance().getComSec() + KeyOne.getInstance().getComSec(),
                                        SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey()
                                )
                        );
                    }
                    cacheFile.delete();
                }
                progressLauncher.setProgress(80);
                DBController.copyIfNotExist(GlobalVariables.getInstance().getContext().getDir(TLConstant.DB_DIR,
                        0).getAbsolutePath() + File.separator + TLConstant.DB_CACHE_NAME, GlobalVariables.getInstance().getContext().getFileStreamPath(TLConstant.DB_CACHE_NAME).getAbsolutePath());

                progressLauncher.setProgress(100);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }

                beginLogin();
            }
        }, "Load SQL Cipher").start();
    }


    private void beginLogin(){
        new Thread (new Runnable()
        {
            @Override
            public void run()
            {
                Integer status = DBController.createUserDBIfNeeded();
                if (status == 1 || status == -1)
                {
                    GlobalVariables.getInstance().setUserDBName(Utility.getInstance().encryptPassword("user", SecOne.getInstance().getNamKey() + KeyOne.getInstance().getNamKey())
                            + "_" + TLConstant.DB_NAME);
                    DBController.resetInstance();
                    if (!DBController.getInstance().testUserDBAccess())
                    {
                        File userDBFile = GlobalVariables.getInstance().getContext().getFileStreamPath(GlobalVariables.getInstance().getUserDBName());
                        if (userDBFile.exists())
                            userDBFile.delete();

                        status = 0;
                    }
                }

                //Test if cache DB is ok
                if (!DBController.getInstance().testCacheDBAccess())
                    status = 0;

                //Check for data migration
                boolean isDBUpdated = false;

                if (isDBUpdated)
                    TLEnvironment.getInstance().readDBVersion();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (TLStorage.getInstance().getIsLoggedIn()) {
                            if (isFromDeeplink && live != null)
                                startLiveStream(live);
                            else
                                startActivity(new Intent(SplashScreenActivity.this, UserMainActivity.class));
                        }
                        else {
                            if(isFromDeeplink && referral != null)
                                startReferralSignUp(referral);
                            else
                                startActivity(new Intent(SplashScreenActivity.this, LandingPageActivity.class));
                        }
                        finish();
                        //splashLayout.setVisibility(View.GONE);
                    }
                });
            }
        }, "Check DB").start();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    private void startLiveStream(String liveStreamID) {
        Intent newIntent = new Intent(this, TLUserLiveStreaming.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.putExtra("liveStreamID", liveStreamID);
        startActivityForResult(newIntent, 1);
        finish();
    }

    private void startReferralSignUp(String referral){
        if(TLStorage.getInstance().getReferrerCode() == null || TLStorage.getInstance().getReferrerCode().isEmpty()) {
            TLStorage.getInstance().setReferrerCode(referral);
            Intent newIntent = new Intent(this, TLSignUp.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(newIntent, 1);
            finish();
        }
    }

}
