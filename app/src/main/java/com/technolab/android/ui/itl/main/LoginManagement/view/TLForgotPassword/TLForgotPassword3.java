package com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInMvpView;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInPresenter;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLMobileVerification;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.KeyboardUtils;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.ValidateUtils;
import com.technolab.android.user.TLStorage;

import javax.inject.Inject;

public class TLForgotPassword3 extends AbstractActivity implements TLSignInMvpView {

    private Button btnConfirm;
    private String txtPhoneNumber;
    private EditText etPassword;
    private ValidateUtils mValidator;

    @Inject
    TLSignInPresenter mTLSignInPresenter;

    public TLForgotPassword3(){
        TAG = "Forgot Password";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_forgot_password_verification;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        txtPhoneNumber = getIntent().getStringExtra("phone");
        btnConfirm = findViewById(R.id.btnConfirm);
        etPassword = findViewById(R.id.et_password);
        ValidateUtils.showHidePassword(etPassword);
        mValidator = new ValidateUtils(this);
        mValidator.addField(etPassword);


        btnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mValidator.isValid()) {
                    mTLSignInPresenter.updatePassword(txtPhoneNumber, etPassword.getText().toString());
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLForgotPassword3.this);
        mTLSignInPresenter.onAttach(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        KeyboardUtils.hideSoftKeyboard(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mValidator.onDestroy();
    }

    @Override
    public void onLoginSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
        startActivity(new Intent(TLForgotPassword3.this, TLForgotPassword4.class));
    }

    @Override
    public void onLoginFailed(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheck(String message) {
        //Do nothing
    }
}
