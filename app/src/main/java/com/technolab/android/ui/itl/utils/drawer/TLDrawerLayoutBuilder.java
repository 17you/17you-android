package com.technolab.android.ui.itl.utils.drawer;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.FloatRange;
import androidx.annotation.IntRange;
import androidx.annotation.LayoutRes;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import com.technolab.android.R;
import com.technolab.android.ui.itl.utils.drawer.callback.DragListener;
import com.technolab.android.ui.itl.utils.drawer.callback.DragStateListener;
import com.technolab.android.ui.itl.utils.drawer.transformation.CompositeTransformation;
import com.technolab.android.ui.itl.utils.drawer.transformation.ElevationTransformation;
import com.technolab.android.ui.itl.utils.drawer.transformation.RootTransformation;
import com.technolab.android.ui.itl.utils.drawer.transformation.ScaleTransformation;
import com.technolab.android.ui.itl.utils.drawer.transformation.YTranslationTransformation;
import com.technolab.android.ui.itl.utils.drawer.utils.ActionBarToggleAdapter;
import com.technolab.android.ui.itl.utils.drawer.utils.DrawerListenerAdapter;
import com.technolab.android.ui.itl.utils.drawer.utils.HiddenMenuClickConsumer;
import com.technolab.android.ui.itl.utils.drawer.utils.SlideGravity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TLDrawerLayoutBuilder {
    private static final float DEFAULT_END_SCALE = 0.80f;
    private static final int DEFAULT_END_ELEVATION_DP = 0;
    private static final int DEFAULT_DRAG_DIST_DP = 250;

    private Activity activity;

    private ViewGroup contentView;

    private View menuView;
    private int menuLayoutRes;

    private List<RootTransformation> transformations;

    private List<DragListener> dragListeners;

    private List<DragStateListener> dragStateListeners;

    private int dragDistance;

    private View triggerView;

    private SlideGravity gravity;

    private boolean isMenuOpened;

    private boolean isMenuLocked;

    private boolean isContentClickableWhenMenuOpened;

    private Bundle savedState;

    public TLDrawerLayoutBuilder(Activity activity) {
        this.activity = activity;
        this.transformations = new ArrayList<>();
        this.dragListeners = new ArrayList<>();
        this.dragStateListeners = new ArrayList<>();
        this.gravity = SlideGravity.LEFT;
        this.dragDistance = dpToPx(DEFAULT_DRAG_DIST_DP);
        this.isContentClickableWhenMenuOpened = true;
    }

    public TLDrawerLayoutBuilder withMenuView(View view) {
        menuView = view;
        return this;
    }

    public TLDrawerLayoutBuilder withMenuLayout(@LayoutRes int layout) {
        menuLayoutRes = layout;
        return this;
    }

    public TLDrawerLayoutBuilder withViewMenuToggle(View tb) {
        triggerView = tb;
        return this;
    }

    public TLDrawerLayoutBuilder withGravity(SlideGravity g) {
        gravity = g;
        return this;
    }

    public TLDrawerLayoutBuilder withContentView(ViewGroup cv) {
        contentView = cv;
        return this;
    }

    public TLDrawerLayoutBuilder withMenuLocked(boolean locked) {
        isMenuLocked = locked;
        return this;
    }

    public TLDrawerLayoutBuilder withSavedState(Bundle state) {
        savedState = state;
        return this;
    }

    public TLDrawerLayoutBuilder withMenuOpened(boolean opened) {
        isMenuOpened = opened;
        return this;
    }

    public TLDrawerLayoutBuilder withContentClickableWhenMenuOpened(boolean clickable) {
        isContentClickableWhenMenuOpened = clickable;
        return this;
    }

    public TLDrawerLayoutBuilder withDragDistance(int dp) {
        return withDragDistancePx(dpToPx(dp));
    }

    public TLDrawerLayoutBuilder withDragDistancePx(int px) {
        dragDistance = px;
        return this;
    }

    public TLDrawerLayoutBuilder withRootViewScale(@FloatRange(from = 0.01f) float scale) {
        transformations.add(new ScaleTransformation(scale));
        return this;
    }

    public TLDrawerLayoutBuilder withRootViewElevation(@IntRange(from = 0) int elevation) {
        return withRootViewElevationPx(dpToPx(elevation));
    }

    public TLDrawerLayoutBuilder withRootViewElevationPx(@IntRange(from = 0) int elevation) {
        transformations.add(new ElevationTransformation(elevation));
        return this;
    }

    public TLDrawerLayoutBuilder withRootViewYTranslation(int translation) {
        return withRootViewYTranslationPx(dpToPx(translation));
    }

    public TLDrawerLayoutBuilder withRootViewYTranslationPx(int translation) {
        transformations.add(new YTranslationTransformation(translation));
        return this;
    }

    public TLDrawerLayoutBuilder addRootTransformation(RootTransformation transformation) {
        transformations.add(transformation);
        return this;
    }

    public TLDrawerLayoutBuilder addDragListener(DragListener dragListener) {
        dragListeners.add(dragListener);
        return this;
    }

    public TLDrawerLayoutBuilder addDragStateListener(DragStateListener dragStateListener) {
        dragStateListeners.add(dragStateListener);
        return this;
    }

    public TLDrawerLayout inject() {
        ViewGroup contentView = getContentView();

        View oldRoot = contentView.getChildAt(0);
        contentView.removeAllViews();

        TLDrawerLayout newRoot = createAndInitNewRoot(oldRoot);

        View menu = getMenuViewFor(newRoot);

        initToolbarMenuVisibilityToggle(newRoot, menu);

        HiddenMenuClickConsumer clickConsumer = new HiddenMenuClickConsumer(activity);
        clickConsumer.setMenuHost(newRoot);

        newRoot.addView(menu);
        newRoot.addView(clickConsumer);
        newRoot.addView(oldRoot);

        contentView.addView(newRoot);

        if (savedState == null) {
            if (isMenuOpened) {
                newRoot.openMenu(false);
            }
        }

        newRoot.setMenuLocked(isMenuLocked);

        return newRoot;
    }

    private TLDrawerLayout createAndInitNewRoot(View oldRoot) {
        TLDrawerLayout newRoot = new TLDrawerLayout(activity);
        newRoot.setId(R.id.tld_root_layout);
        newRoot.setRootTransformation(createCompositeTransformation());
        newRoot.setMaxDragDistance(dragDistance);
        newRoot.setGravity(gravity);
        newRoot.setRootView(oldRoot);
        newRoot.setContentClickableWhenMenuOpened(isContentClickableWhenMenuOpened);
        for (DragListener l : dragListeners) {
            newRoot.addDragListener(l);
        }
        for (DragStateListener l : dragStateListeners) {
            newRoot.addDragStateListener(l);
        }
        return newRoot;
    }

    private ViewGroup getContentView() {
        if (contentView == null) {
            contentView = (ViewGroup) activity.findViewById(android.R.id.content);
        }
        if (contentView.getChildCount() != 1) {
            throw new IllegalStateException("Your content view must contain exactly one child. Make sure you call inject() after setContentView().");
        }
        return contentView;
    }

    private View getMenuViewFor(TLDrawerLayout parent) {
        if (menuView == null) {
            if (menuLayoutRes == 0) {
                throw new IllegalStateException("No view for menu provided. You must either call .withMenuView(View) or pass a valid resource id to .withMenuLayout(int)");
            }
            menuView = LayoutInflater.from(activity).inflate(menuLayoutRes, parent, false);
        }
        return menuView;
    }

    private RootTransformation createCompositeTransformation() {
        if (transformations.isEmpty()) {
            return new CompositeTransformation(Arrays.asList(
                    new ScaleTransformation(DEFAULT_END_SCALE),
                    new ElevationTransformation(dpToPx(DEFAULT_END_ELEVATION_DP))));
        } else {
            return new CompositeTransformation(transformations);
        }
    }

    protected void initToolbarMenuVisibilityToggle(final TLDrawerLayout sideNav, View drawer) {
        if (triggerView != null) {
            ActionBarToggleAdapter dlAdapter = new ActionBarToggleAdapter(activity);
            dlAdapter.setAdapter(sideNav);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, dlAdapter,
                    R.string.tld_open,
                    R.string.tld_close);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.setHomeAsUpIndicator(R.drawable.ic_ico_hamburger);
            toggle.syncState();

            triggerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlAdapter.openDrawer(Gravity.NO_GRAVITY);
                }
            });

            DrawerListenerAdapter listenerAdapter = new DrawerListenerAdapter(toggle, drawer);
            sideNav.addDragListener(listenerAdapter);
            sideNav.addDragStateListener(listenerAdapter);
        }
    }

    private int dpToPx(int dp) {
        return Math.round(activity.getResources().getDisplayMetrics().density * dp);
    }

}

