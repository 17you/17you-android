package com.technolab.android.ui.itl.main.MerchantRegistration.presenter;

import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.MerchantApplication;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.spine.AbstractMvpView;

import java.util.ArrayList;

public interface TLMerchantRegistrationMvpView{

    interface IndustryMVP extends AbstractMvpView{
        void onRetrieveIndustries(ArrayList<Industry> industryArrayList);
        void onError(String message);
    }

    interface MerchantInfo extends AbstractMvpView{
        void onRetrieveApplicationStatus(MerchantApplication merchantApplication);
        void onFinish(String message);
        void onError(String message);
    }

    interface VerificationImage extends AbstractMvpView{
        void onUpload(String message);
        void onError(String message);
    }

    interface MerchantInfoDetail extends AbstractMvpView{
        void onDetailsUpload(String message);
        void onError(String message);
    }

    interface MerchantPayment extends AbstractMvpView{
        void onBankSlipUpload(String message);
        void onError(String message);
    }

}
