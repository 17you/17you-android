package com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;
import com.technolab.android.R;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInMvpView;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInPresenter;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword.TLForgotPassword;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp.TLSignUp;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class TLSignInPhone extends AbstractActivity implements TLSignInMvpView {

    private EditText etPhone;
    private Button btnVerificationCode;
    private TextView txtSignUp;
    private TextView txtForgotPassword;
    private TextView txtSignInEmail;
    private CountryCodePicker ccp;

    @Inject
    TLSignInPresenter mTLSignInPresenter;

    public TLSignInPhone(){
        TAG = "Sign In Phone";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_sign_in;
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        etPhone = findViewById(R.id.et_phone);
        btnVerificationCode = findViewById(R.id.btn_verification_code);
        txtSignUp = findViewById(R.id.txt_sign_up);
        txtForgotPassword = findViewById(R.id.txt_forgot_password);
        txtSignInEmail = findViewById(R.id.txt_sign_in_email);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.registerCarrierNumberEditText(etPhone);

        txtForgotPassword.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLSignInPhone.this, TLForgotPassword.class));
            }
        });

        txtSignInEmail.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
                startActivity(new Intent(TLSignInPhone.this, TLSignInEmail.class));
            }
        });

        txtSignUp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLSignInPhone.this, TLSignUp.class));
            }
        });

        btnVerificationCode.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(ccp.isValidFullNumber()){
                    mTLSignInPresenter.phoneNumberCheck(ccp.getFullNumberWithPlus());
                }else{
                    Toast.makeText(TLSignInPhone.this, "Invalid phone number. Please enter a valid phone number.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLSignInPhone.this);
        mTLSignInPresenter.onAttach(this);
    }

    @Override
    public void onLoginSuccess(String message) {

    }

    @Override
    public void onLoginFailed(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheck(String message) {
        Intent intent = new Intent(TLSignInPhone.this, TLMobileVerification.class);
        intent.putExtra("phone", ccp.getFullNumberWithPlus());
        startActivity(intent);
    }
}
