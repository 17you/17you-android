package com.technolab.android.ui.itl.main.MerchantRegistration.presenter;

import com.technolab.android.error.TLError;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLMerchantPayment;
import com.technolab.android.ui.itl.spine.AbstractPresenter;
import com.technolab.android.user.TLStorage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TLMerchantRegistrationPresenter {

    public static class TLMerchantIndustryPresenter extends AbstractPresenter<TLMerchantRegistrationMvpView.IndustryMVP>{

        @Inject
        public TLMerchantIndustryPresenter(){
        }

        public void fetchIndustries(){
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveIndustries()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onRetrieveIndustries(baseResponse.getIndustry());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

    public static class TLMerchantInfoPresenter extends AbstractPresenter<TLMerchantRegistrationMvpView.MerchantInfo>{
        @Inject
        public TLMerchantInfoPresenter(){
        }

        public void fetchApplicationStatus(String userID){
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveMerchantApplicationStatus(userID)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onRetrieveApplicationStatus(baseResponse.getMerchantApplication());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void submitPrivacyPolicy(){
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .submitPrivacyPolicy(TLStorage.getInstance().getCurrentUserId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null)
                                    getMvpView().onFinish(baseResponse.getMessage());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

    public static class TLVerificationPresenter extends AbstractPresenter<TLMerchantRegistrationMvpView.VerificationImage>{

        @Inject
        public TLVerificationPresenter(){}

        public void uploadIdentification(String userID, int industryID, String front, String back){
            getMvpView().showLoadingSpinner();

            File frontFile = new File(front);
            RequestBody frontRB = RequestBody.create(MediaType.parse("image/png"), frontFile);
            MultipartBody.Part frontFilePart = MultipartBody.Part.createFormData("file_merchant_ic_front_image", frontFile.getName(), frontRB);

            File backFile = new File(back);
            RequestBody backRB = RequestBody.create(MediaType.parse("image/png"),backFile);
            MultipartBody.Part backFilePart = MultipartBody.Part.createFormData("file_merchant_ic_back_image", backFile.getName(), backRB);

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .uploadIdentification(userID, RequestBody.create(okhttp3.MultipartBody.FORM, String.valueOf(industryID)), frontFilePart, backFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onUpload(baseResponse.getMessage());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void uploadSelfie(String userID, String selfie){
            getMvpView().showLoadingSpinner();

            File selfieFile = new File(selfie);
            RequestBody selfieRB = RequestBody.create(MediaType.parse("image/png"), selfieFile);
            MultipartBody.Part selfieFilePart = MultipartBody.Part.createFormData("file_merchant_selfie", selfieFile.getName(), selfieRB);

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .uploadSelfie(userID, selfieFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onUpload(baseResponse.getMessage());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

    public static class TLMerchantInfoDetailPresenter extends AbstractPresenter<TLMerchantRegistrationMvpView.MerchantInfoDetail>{
        @Inject
        public TLMerchantInfoDetailPresenter(){}

        public void uploadInfoDetail(String companyName, String companyRegistrationNumber, String companyType,
                                     String bankName, String accNumber, String holderName, String email, String businessAddress,
                                     String city, String state, String postcode){
            getMvpView().showLoadingSpinner();
            JSONObject object = new JSONObject();

            try {
                object.put("companyname", companyName);
                object.put("companytype", companyType);
                object.put("bankname", bankName);
                object.put("bankaccno", accNumber);
                object.put("bankaccholdername", holderName);
                object.put("state", state);
                object.put("email", email);
                object.put("streetaddress", businessAddress);
                object.put("companyregnum", companyRegistrationNumber);
                object.put("city", city);
                object.put("postcode", postcode);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            RequestBody requestBody = RequestBody.create(String.valueOf(object), MediaType.parse("raw"));

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .uploadInfoDetail(TLStorage.getInstance().getCurrentUserId(), requestBody)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onDetailsUpload(baseResponse.getMessage());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

    public static class TLMerchantPaymentPresenter extends AbstractPresenter<TLMerchantRegistrationMvpView.MerchantPayment>{
        @Inject
        public TLMerchantPaymentPresenter(){}

        public void uploadBankSlip(String userID, String bankSlip){
            getMvpView().showLoadingSpinner();

            File bankSlipFile = new File(bankSlip);
            RequestBody bankSlipRB = RequestBody.create(MediaType.parse("image/png"),bankSlipFile);
            MultipartBody.Part bankSlipFilePart = MultipartBody.Part.createFormData("file_merchant_receipt", bankSlipFile.getName(), bankSlipRB);

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .uploadBankSlip(userID, bankSlipFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onBankSlipUpload(baseResponse.getMessage());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }

}
