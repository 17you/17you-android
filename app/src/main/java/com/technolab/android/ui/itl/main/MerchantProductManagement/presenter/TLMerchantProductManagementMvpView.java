package com.technolab.android.ui.itl.main.MerchantProductManagement.presenter;

import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.ProductMainCategory;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLSelectProductCategory;
import com.technolab.android.ui.itl.spine.AbstractMvpView;

import java.util.ArrayList;

public interface TLMerchantProductManagementMvpView {
    interface ProductMVP extends AbstractMvpView {
        void onRetrieveProductList(ArrayList<Product> productArrayList);
        void onError(String message);
        void onResponse(String message);
        void onDelete(String message);
    }

    interface ProductCategory extends AbstractMvpView {
        void onRetrieveProductCategory(ArrayList<ProductMainCategory> productCategories);
        void onError(String message);
        void onResponse(String message);
    }

    interface CreateProduct extends AbstractMvpView {
        void onFetchData(Product product);
        void onSuccess(String message);
        void onError(String message);
        void onResponse(String message);
    }
}
