package com.technolab.android.ui.itl.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Merchant;
import com.technolab.android.ui.entity.TLSwipeUpModel;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLMerchantInfoFillUp;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.adapter.TLSwipeUpAdapter;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.KeyboardUtils;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TypefaceEditText;
import com.technolab.android.utility.EnumBanks;
import com.technolab.android.utility.EnumCompany;
import com.technolab.android.utility.EnumStates;

import java.util.ArrayList;

import javax.inject.Inject;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

public class MerchantViewProfileActivity extends AbstractActivity implements TLMainMvpView.MerchantProfile, TLSwipeUpAdapter.ItemClickListener{

    private String imagePath;
    private ImageView imgProfile;

    private TextView txt_profile_name;
    private TextView txt_bank_name;
    private TextView txt_bank_acc_number;
    private TextView txt_bank_acc_holder;
    private TextView txt_email;
    private TextView txt_street_address;
    private TextView txt_city;
    private TextView txt_post_code;

    private EditText et_profile_name;
    private EditText et_bank_name;
    private EditText et_bank_acc_number;
    private EditText et_bank_acc_holder;
    private EditText et_email;
    private EditText et_street_address;
    private EditText et_city;
    private EditText et_post_code;

    private Merchant merchantDetails;
    private TLSwipeUpAdapter mBankAdapter;
    private RecyclerView mSliderRecyclerView;
    private EditText selectedEditText;
    private View dim;
    private View mSliderView;
    private SlideUp mSlideUp;

    public MerchantViewProfileActivity(){
        TAG = "Merchant View Profile Activity";
        statusBarColorID = R.color.colorBlue;
        layoutID = R.layout.activity_merchant_view_profile;
    }

    @Inject
    TLMainPresenter.TLMerchantProfilePresenter merchantProfilePresenter;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        imgProfile = findViewById(R.id.img_profile);
        txt_profile_name = findViewById(R.id.txt_profile_name);
        txt_bank_name = findViewById(R.id.txt_bank_name);
        txt_bank_acc_number = findViewById(R.id.txt_bank_acc_number);
        txt_bank_acc_holder = findViewById(R.id.txt_bank_acc_holder);
        txt_email = findViewById(R.id.txt_email);
        txt_street_address = findViewById(R.id.txt_street_address);
        txt_city = findViewById(R.id.txt_city);
        txt_post_code = findViewById(R.id.txt_post_code);

        et_profile_name = findViewById(R.id.et_profile_name);
        et_bank_name = findViewById(R.id.et_bank_name);
        et_bank_acc_number = findViewById(R.id.et_bank_acc_number);
        et_bank_acc_holder = findViewById(R.id.et_bank_acc_holder);
        et_email = findViewById(R.id.et_email);
        et_street_address = findViewById(R.id.et_street_address);
        et_city = findViewById(R.id.et_city);
        et_post_code = findViewById(R.id.et_post_code);

        mSliderView = findViewById(R.id.cl_slide_view);
        mSliderRecyclerView = findViewById(R.id.rv_slider);
        dim = findViewById(R.id.dim);

        getAppComponent().inject(MerchantViewProfileActivity.this);
        merchantProfilePresenter.onAttach(this);
        merchantProfilePresenter.retrieveMerchantDetails();

        editMode(false);
        initSlider();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(MerchantViewProfileActivity.this);
        merchantProfilePresenter.onAttach(this);
    }

    private void initSlider() {
        ArrayList<TLSwipeUpModel> bankNameList = new ArrayList<>();
        for (EnumBanks banks : EnumBanks.values()) {
            bankNameList.add(new TLSwipeUpModel(banks.getValue()));
        }

        mSliderRecyclerView.setHasFixedSize(false);
        mBankAdapter = new TLSwipeUpAdapter(this, bankNameList);

        mSlideUp = new SlideUpBuilder(mSliderView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / 100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        dim.setVisibility(visibility == 0 ? View.VISIBLE : View.GONE);
                        dim.setClickable(visibility == 0);
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .build();

        dim.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(mSlideUp.isVisible())
                    mSlideUp.hide();
            }
        });

        et_bank_name.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mSliderRecyclerView.setAdapter(mBankAdapter);
                mBankAdapter.setOnItemClickListener(MerchantViewProfileActivity.this);
                KeyboardUtils.hideSoftKeyboard(MerchantViewProfileActivity.this);
                mSlideUp.show();
                selectedEditText = et_bank_name;
            }
        });
    }

    private void editMode(boolean isEditMode){
        txt_profile_name.setVisibility(isEditMode ? View.GONE : View.VISIBLE);
        txt_bank_name.setVisibility(isEditMode ? View.GONE : View.VISIBLE);
        txt_bank_acc_number.setVisibility(isEditMode ? View.GONE : View.VISIBLE);
        txt_bank_acc_holder.setVisibility(isEditMode ? View.GONE : View.VISIBLE);
        txt_email.setVisibility(isEditMode ? View.GONE : View.VISIBLE);
        txt_street_address.setVisibility(isEditMode ? View.GONE : View.VISIBLE);
        txt_city.setVisibility(isEditMode ? View.GONE : View.VISIBLE);
        txt_post_code.setVisibility(isEditMode ? View.GONE : View.VISIBLE);

        et_profile_name.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        et_bank_name.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        et_bank_acc_number.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        et_bank_acc_holder.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        et_email.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        et_street_address.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        et_city.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        et_post_code.setVisibility(isEditMode ? View.VISIBLE : View.GONE);

        imgProfile.setEnabled(isEditMode);
        imgProfile.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(MerchantViewProfileActivity.this, ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, true);
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, false);
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);
                intent.putExtra(ImageSelectActivity.FLAG_CROP, true);
                startActivityForResult(intent, 1213);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            imagePath = filePath;
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            Glide.with(this)
                    .load(selectedImage)
                    .thumbnail(0.1f)
                    .centerCrop()
                    .placeholder(getResources().getDrawable(R.drawable.placeholder))
                    .into(imgProfile);
        }
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Profile");
        txtTemp.setVisibility(View.VISIBLE);
        txtTemp.setText("Edit");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

        txtTemp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(txtTemp.getText().toString().equalsIgnoreCase("Edit")){
                    editMode(true);
                    txtTemp.setText("Save");
                }else if(txtTemp.getText().toString().equalsIgnoreCase("Save")){
                    String companyName = !et_profile_name.getText().toString().equalsIgnoreCase(txt_profile_name.getText().toString()) ? et_profile_name.getText().toString() : null;
                    String bankName = !et_bank_name.getText().toString().equalsIgnoreCase(txt_bank_name.getText().toString()) ? et_bank_name.getText().toString() : null;
                    String bankAccountNumber = !et_bank_acc_number.getText().toString().equalsIgnoreCase(txt_bank_acc_number.getText().toString()) ? et_bank_acc_number.getText().toString() : null;
                    String bankAccountHolder = !et_bank_acc_holder.getText().toString().equalsIgnoreCase(txt_bank_acc_holder.getText().toString()) ? et_bank_acc_holder.getText().toString() : null;
                    String state = merchantDetails.getMerchantState() != null ? merchantDetails.getMerchantState() : null;
                    String email = !et_email.getText().toString().equalsIgnoreCase(txt_email.getText().toString()) ? et_email.getText().toString() : null;
                    String streetAddress = !et_street_address.getText().toString().equalsIgnoreCase(txt_street_address.getText().toString()) ? et_street_address.getText().toString() : null;
                    String city = !et_city.getText().toString().equalsIgnoreCase(txt_city.getText().toString()) ? et_city.getText().toString() : null;
                    String postCode = !et_post_code.getText().toString().equalsIgnoreCase(txt_post_code.getText().toString()) ? et_post_code.getText().toString() : null;
                    merchantProfilePresenter.uploadMerchantDetail(companyName, bankName, bankAccountNumber, bankAccountHolder,state, email,streetAddress,city,postCode, imagePath);
                }
            }
        });
    }

    @Override
    public void onReceiveProfile(Boolean isMerchant, String msg, Merchant merchantDetail) {
        this.merchantDetails = merchantDetail;

        txt_profile_name.setText(merchantDetail.getMerchantCompanyName());
        txt_bank_name.setText(merchantDetail.getMerchantBankName());
        txt_bank_acc_number.setText(String.valueOf(merchantDetail.getMerchantBankNumber()));
        txt_bank_acc_holder.setText(merchantDetail.getMerchantBankAccHolder());
        txt_email.setText(merchantDetail.getMerchantEmail());
        txt_street_address.setText(merchantDetail.getMerchantStreetAddress());
        txt_city.setText(merchantDetail.getMerchantCity());
        txt_post_code.setText(merchantDetail.getMerchantPostcode());

        et_profile_name.setText(merchantDetail.getMerchantCompanyName());
        et_bank_name.setText(merchantDetail.getMerchantBankName());
        et_bank_acc_number.setText(String.valueOf(merchantDetail.getMerchantBankNumber()));
        et_bank_acc_holder.setText(merchantDetail.getMerchantBankAccHolder());
        et_email.setText(merchantDetail.getMerchantEmail());
        et_street_address.setText(merchantDetail.getMerchantStreetAddress());
        et_city.setText(merchantDetail.getMerchantCity());
        et_post_code.setText(merchantDetail.getMerchantPostcode());

        Glide.with(this)
                .load(merchantDetail.getMerchantImage())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into(imgProfile);
    }

    @Override
    public void onFinish(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT);
        merchantProfilePresenter.retrieveMerchantDetails();
        editMode(false);
        txtTemp.setText("Edit");
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT);
    }

    @Override
    public void onItemClicked(TLSwipeUpModel model) {
        if(selectedEditText == et_bank_name)
            et_bank_name.setText(model.getName());

        mSlideUp.hide();
    }
}
