package com.technolab.android.ui.itl.main.MerchantProductManagement.presenter;


import com.technolab.android.error.TLError;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.Variation;
import com.technolab.android.ui.itl.spine.AbstractPresenter;
import com.technolab.android.user.TLStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TLMerchantProductManagementPresenter {

    public static class TLProductPresenter extends AbstractPresenter<TLMerchantProductManagementMvpView.ProductMVP> {
        @Inject
        public TLProductPresenter() {
        }

        public void fetchProductList() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveMerchantProduct(TLStorage.getInstance().getMerchantId())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onRetrieveProductList(baseResponse.getProductArrayList());
                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void deleteProduct(String productID){
            getMvpView().showLoadingSpinner();

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .deleteProduct(productID)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onDelete(baseResponse.getMessage());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                            }));
        }


    }

    public static class TLProductCategoryPresenter extends AbstractPresenter<TLMerchantProductManagementMvpView.ProductCategory> {

        @Inject
        public TLProductCategoryPresenter() {

        }

        public void fetchProductCategory() {
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveProductCategories()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    if (baseResponse.getProductMainCategoryList() != null && baseResponse.getProductMainCategoryList().size() > 0)
                                        getMvpView().onRetrieveProductCategory(baseResponse.getProductMainCategoryList());
                                    else
                                        getMvpView().onError(baseResponse.getMessage());
                                    else
                                        getMvpView().onError(baseResponse.getMessage());


                            }, throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                TLError error = TLError.checkForError(throwable);
                                if (error != null) {
                                    System.out.println("Testing :" + error.getErrorCode());
                                    System.out.println("Testing :" + error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            })
            );
        }

    }

    public static class TLCreateProductPresenter extends AbstractPresenter<TLMerchantProductManagementMvpView.CreateProduct> {

        @Inject
        public TLCreateProductPresenter() {

        }

        public void addProductDetails(String productName, String productDescription, int mainCatId, int subCatId,
                                      ArrayList<Variation> variations, String productImagePath){

            getMvpView().showLoadingSpinner();
            File productFile = new File(productImagePath);
            RequestBody liveStreamRB = RequestBody.create(MediaType.parse("image/png"),productFile);
            MultipartBody.Part productImageFilePart = MultipartBody.Part.createFormData("file_product_image", productFile.getName(), liveStreamRB);

            JSONArray variationArray = new JSONArray();
            for (int i = 0; i < variations.size(); i++) {
                try {
                    JSONObject objects = new JSONObject();
                    objects.put("size", variations.get(i).getProduct_variation_size());
                    objects.put("color", variations.get(i).getProduct_variation_color());
                    objects.put("price", variations.get(i).getProduct_variation_price());
                    objects.put("quantity", variations.get(i).getProduct_variation_quantity());
                    objects.put("status", variations.get(i).getProduct_variation_status());

                    variationArray.put(objects);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .createProduct(TLStorage.getInstance().getMerchantId(),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, productName),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, productDescription),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, variationArray.toString()),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, String.valueOf(mainCatId)),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, String.valueOf(subCatId)),
                                    productImageFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onSuccess(baseResponse.getMessage());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));

        }

        public void updateProductDetails(String productID, String productName, String productDescription, int mainCatId, int subCatId,
                                         ArrayList<Variation> variations, String productImagePath){

            getMvpView().showLoadingSpinner();
            MultipartBody.Part productImageFilePart = null;
            if(productImagePath != null) {
                File productFile = new File(productImagePath);
                RequestBody liveStreamRB = RequestBody.create(MediaType.parse("image/png"), productFile);
                productImageFilePart = MultipartBody.Part.createFormData("file_product_image", productFile.getName(), liveStreamRB);
            }

            JSONArray variationArray = new JSONArray();
            for (int i = 0; i < variations.size(); i++) {
                try {
                    JSONObject objects = new JSONObject();
                    objects.put("size", variations.get(i).getProduct_variation_size());
                    objects.put("color", variations.get(i).getProduct_variation_color());
                    objects.put("price", variations.get(i).getProduct_variation_price());
                    objects.put("quantity", variations.get(i).getProduct_variation_quantity());
                    objects.put("status", variations.get(i).getProduct_variation_status());

                    variationArray.put(objects);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .updateProduct(productID,TLStorage.getInstance().getMerchantId(),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, productName),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, productDescription),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, variationArray.toString()),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, String.valueOf(mainCatId)),
                                    RequestBody.create(okhttp3.MultipartBody.FORM, String.valueOf(subCatId)),
                                    productImageFilePart)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onSuccess(baseResponse.getMessage());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }

        public void fetchSingleProduct(String productID){
            getMvpView().showLoadingSpinner();
            mCompositeDisposable.add(
                    mDataRepositoryManager
                            .retrieveSingleProduct(productID)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(baseResponse -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                getMvpView().hideLoadingSpinner();
                                if (baseResponse != null && baseResponse.getStatus())
                                    getMvpView().onFetchData(baseResponse.getProduct());
                            },throwable -> {
                                if (!isViewAttached()) {
                                    return;
                                }
                                TLError error = TLError.checkForError(throwable);
                                if (error != null)
                                {
                                    System.out.println("Testing :"+error.getErrorCode());
                                    System.out.println("Testing :"+error.getErrorMessage());
                                }

                                getMvpView().hideLoadingSpinner();
                                getMvpView().onError(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                            }));
        }
    }
}
