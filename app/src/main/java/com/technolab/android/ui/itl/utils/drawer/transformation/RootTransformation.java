package com.technolab.android.ui.itl.utils.drawer.transformation;

import android.view.View;

public interface RootTransformation {
    void transform(float dragProgress, View rootView);
}
