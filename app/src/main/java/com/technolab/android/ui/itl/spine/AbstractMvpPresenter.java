package com.technolab.android.ui.itl.spine;

public interface AbstractMvpPresenter<V extends AbstractMvpView> {
    void onAttach(V mvpView);
    void onDetach();
    boolean isViewAttached();
    V getMvpView();
}
