package com.technolab.android.ui.entity;

public class RankingOthers {
    public int member_id;
    public String member_username;
    public int total_sign_up;
    public String default_ranking_image;

    public int getMember_id() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }

    public String getMember_username() {
        return member_username;
    }

    public void setMember_username(String member_username) {
        this.member_username = member_username;
    }

    public int getTotal_sign_up() {
        return total_sign_up;
    }

    public void setTotal_sign_up(int total_sign_up) {
        this.total_sign_up = total_sign_up;
    }

    public String getDefault_ranking_image() {
        return default_ranking_image;
    }

    public void setDefault_ranking_image(String default_ranking_image) {
        this.default_ranking_image = default_ranking_image;
    }
}
