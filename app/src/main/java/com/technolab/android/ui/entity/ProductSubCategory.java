package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

public class ProductSubCategory {

    @SerializedName("product_sub_cat_id")
    private Integer productSubCategoryId;

    @SerializedName("product_sub_cat_name")
    private String productSubCategoryName;

    public Integer getProductSubCategoryId() {
        return productSubCategoryId;
    }

    public void setProductSubCategoryId(Integer productSubCategoryId) {
        this.productSubCategoryId = productSubCategoryId;
    }

    public String getProductSubCategoryName() {
        return productSubCategoryName;
    }

    public void setProductSubCategoryName(String productSubCategoryName) {
        this.productSubCategoryName = productSubCategoryName;
    }
}
