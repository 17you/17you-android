package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

public class MerchantApplication {
    @SerializedName("merchant_is_ic_submitted")
    private boolean isIdentificationSubmitted;
    @SerializedName("merchant_is_selfie_submitted")
    private boolean isSelfieSubmitted;
    @SerializedName("merchant_is_business_details_submitted")
    private boolean isBusinessDetailSubmitted;
    @SerializedName("merchant_is_live_sub_receipt_submitted")
    private boolean isLiveSubReceiptSubmitted;
    @SerializedName("merchant_application_status")
    private String applicationStatus;

    public MerchantApplication() {
        isSelfieSubmitted = false;
        isBusinessDetailSubmitted = false;
        isIdentificationSubmitted  = false;
        applicationStatus = "INCOMPLETE";
    }

    public boolean isLiveSubReceiptSubmitted() {
        return isLiveSubReceiptSubmitted;
    }

    public void setLiveSubReceiptSubmitted(boolean liveSubReceiptSubmitted) {
        isLiveSubReceiptSubmitted = liveSubReceiptSubmitted;
    }

    public boolean isIdentificationSubmitted() {
        return isIdentificationSubmitted;
    }

    public void setIdentificationSubmitted(boolean identificationSubmitted) {
        isIdentificationSubmitted = identificationSubmitted;
    }

    public boolean isSelfieSubmitted() {
        return isSelfieSubmitted;
    }

    public void setSelfieSubmitted(boolean selfieSubmitted) {
        isSelfieSubmitted = selfieSubmitted;
    }

    public boolean isBusinessDetailSubmitted() {
        return isBusinessDetailSubmitted;
    }

    public void setBusinessDetailSubmitted(boolean businessDetailSubmitted) {
        isBusinessDetailSubmitted = businessDetailSubmitted;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }
}
