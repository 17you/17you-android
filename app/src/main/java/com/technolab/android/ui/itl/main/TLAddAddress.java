package com.technolab.android.ui.itl.main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Address;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

import vn.luongvo.widget.iosswitchview.SwitchView;

public class TLAddAddress extends AbstractActivity implements TLMainMvpView.UserProfile{

    private int status = 0;
    private Button btnSave;
    private Button btnDelete;
    private EditText txt_address_line_1;
    private EditText txt_address_line_2;
    private EditText txt_address_locality;
    private EditText txt_address_postal;
    private EditText txt_address_state;
    private SwitchView switchView;
    private Address address;

    public TLAddAddress() {
        TAG = "TLAddAddress";
        layoutID = R.layout.activity_add_address;
        statusBarColorID = R.color.colorD;
    }

    @Inject
    TLMainPresenter.TLUserProfilePresenter userProfilePresenter;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        btnSave = findViewById(R.id.btn_save);
        btnDelete = findViewById(R.id.btn_delete);
        txt_address_line_1 = findViewById(R.id.txt_address_line_1);
        txt_address_line_2 = findViewById(R.id.txt_address_line_2);
        txt_address_locality = findViewById(R.id.txt_address_locality);
        txt_address_postal = findViewById(R.id.txt_address_postal);
        txt_address_state = findViewById(R.id.txt_address_state);
        switchView = findViewById(R.id.switchview);
        status = getIntent().getIntExtra("status", 0);

        switch (status) {
            case 2:
                //Add new address
                switchView.setChecked(getIntent().getBooleanExtra("isFirstAddress", false));

                btnDelete.setVisibility(View.GONE);
                btnSave.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {

                        String address1 = txt_address_line_1.getText().toString();
                        String address2 = txt_address_line_2.getText().toString();
                        String city = txt_address_locality.getText().toString();
                        String postcode = txt_address_postal.getText().toString();
                        String state = txt_address_state.getText().toString();
                        boolean isDefault = switchView.isChecked();

                        if(address1.isEmpty() || address2.isEmpty() || city.isEmpty() || postcode.isEmpty() || state.isEmpty()){
                            Toast.makeText(TLAddAddress.this, "Please fill in all details", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        userProfilePresenter.addAddress(address1, address2, city, postcode, state, getIntent().getBooleanExtra("isFirstAddress", false) || isDefault);
                    }
                });
                break;
            case 3:
            case 4:
                //Delete/Update address
                address = (Address) getIntent().getSerializableExtra("address");
                btnDelete.setVisibility(View.VISIBLE);
                setupData(address);

                btnDelete.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                        boolean isDefault = address.isIsdefault();
                        userProfilePresenter.deleteAddress(address.getAddress_id(), isDefault);
                    }
                });

                btnSave.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                        String address1 = txt_address_line_1.getText().toString();
                        String address2 = txt_address_line_2.getText().toString();
                        String city = txt_address_locality.getText().toString();
                        String postcode = txt_address_postal.getText().toString();
                        String state = txt_address_state.getText().toString();
                        boolean isDefault = address.isIsdefault();
                        userProfilePresenter.updateAddress(address.getAddress_id(), address1, address2, city, Integer.parseInt(postcode), state, address.isIsdefault(), switchView.isChecked());
                    }
                });
                break;
        }

    }

    private void setupData(Address address){
        txt_address_line_1.setText(address.getAddress_line_1());
        txt_address_line_2.setText(address.getAddress_line_2());
        txt_address_locality.setText(address.getCity());
        txt_address_postal.setText(address.getPostcode());
        txt_address_state.setText(address.getState());
        switchView.setChecked(address.isIsdefault());
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Profile");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(this);
        userProfilePresenter.onAttach(this);
    }


    @Override
    public void onReceiveAddress(ArrayList<Address> addressList) {

    }

    @Override
    public void onFinish(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
