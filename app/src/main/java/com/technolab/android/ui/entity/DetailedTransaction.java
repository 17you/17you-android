package com.technolab.android.ui.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DetailedTransaction implements Serializable {
    public int transaction_id;
    public String member_username;
    public String member_email;
    public String member_phone;
    public Double final_price;
    public int quantity_purchased;

    public String reference_number;
    public Double delivery_fees;
    public Double total_product_price;

    public String tracking_url;
    public String logistic_status;
    public String transaction_date_and_time;
    public String transaction_receipt;
    public String courier_company;

    public int address_id;
    public String address_line_1;
    public String address_line_2;
    public String city;
    public int postcode;
    public String state;
    public ArrayList<PurchasedProduct> purchased_products;

    public String getReference_number() {
        return reference_number;
    }

    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }

    public Double getDelivery_fees() {
        return delivery_fees;
    }

    public void setDelivery_fees(Double delivery_fees) {
        this.delivery_fees = delivery_fees;
    }

    public Double getTotal_product_price() {
        return total_product_price;
    }

    public void setTotal_product_price(Double total_product_price) {
        this.total_product_price = total_product_price;
    }

    public String getTracking_url() {
        return tracking_url;
    }

    public void setTracking_url(String tracking_url) {
        this.tracking_url = tracking_url;
    }

    public String getLogistic_status() {
        return logistic_status;
    }

    public void setLogistic_status(String logistic_status) {
        this.logistic_status = logistic_status;
    }

    public String getTransaction_date_and_time() {
        return transaction_date_and_time;
    }

    public void setTransaction_date_and_time(String transaction_date_and_time) {
        this.transaction_date_and_time = transaction_date_and_time;
    }

    public String getTransaction_receipt() {
        return transaction_receipt;
    }

    public void setTransaction_receipt(String transaction_receipt) {
        this.transaction_receipt = transaction_receipt;
    }

    public String getCourier_company() {
        return courier_company;
    }

    public void setCourier_company(String courier_company) {
        this.courier_company = courier_company;
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public void setAddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public void setAddress_line_2(String address_line_2) {
        this.address_line_2 = address_line_2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPostcode() {
        return postcode;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public ArrayList<PurchasedProduct> getPurchased_products() {
        return purchased_products;
    }

    public void setPurchased_products(ArrayList<PurchasedProduct> purchased_products) {
        this.purchased_products = purchased_products;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getMember_username() {
        return member_username;
    }

    public void setMember_username(String member_username) {
        this.member_username = member_username;
    }

    public String getMember_email() {
        return member_email;
    }

    public void setMember_email(String member_email) {
        this.member_email = member_email;
    }

    public String getMember_phone() {
        return member_phone;
    }

    public void setMember_phone(String member_phone) {
        this.member_phone = member_phone;
    }

    public Double getFinal_price() {
        return final_price;
    }

    public void setFinal_price(Double final_price) {
        this.final_price = final_price;
    }

    public int getQuantity_purchased() {
        return quantity_purchased;
    }

    public void setQuantity_purchased(int quantity_purchased) {
        this.quantity_purchased = quantity_purchased;
    }
}
