package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.State;
import com.technolab.android.ui.entity.TotalSignUpState;

import java.util.ArrayList;
import java.util.List;

public class StateAdapter extends RecyclerView.Adapter<StateAdapter.ViewHolder>{

    private Context context;
    private List<TotalSignUpState> stateArrayList;

    public StateAdapter(Context context, List<TotalSignUpState>stateArrayList){
        this.context = context;
        this.stateArrayList = stateArrayList;
    }

    @NonNull
    @Override
    public StateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_referral_state, parent, false);
       return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StateAdapter.ViewHolder holder, int position) {
        TotalSignUpState state = stateArrayList.get(position);
        holder.stateName.setText(state.getState_name());
        holder.stateCount.setText(String.valueOf(state.getCount()));
        holder.itemView.setAlpha(state.getCount() == 0 ? 0.3f : 1.0f);
    }

    @Override
    public int getItemCount() {
        return stateArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView stateName;
        TextView stateCount;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            stateName = itemView.findViewById(R.id.txt_state_name);
            stateCount = itemView.findViewById(R.id.txt_state_count);
        }
    }
}
