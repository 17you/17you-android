package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BaseResponse {

    private Boolean status;
    private String message;
    private String rank;
    @SerializedName("reward_points")
    private String rewardPoint;
    @SerializedName("isredeemable")
    private Boolean isRedeemable;
    @SerializedName("login_results")
    private LoginResponse loginResponse;
    @SerializedName("industry_list")
    private ArrayList<Industry> industry;
    @SerializedName("results")
    private MerchantApplication merchantApplication;
    @SerializedName("live_streams_result")
    private ArrayList<LiveStream> liveStreamArrayList;
    @SerializedName("merchant_results")
    private Merchant merchant;
    @SerializedName("product_results")
    private ArrayList<Product> productArrayList;
    @SerializedName("active_live_streams_count_result")
    private ArrayList<Industry> activeLiveStream;
    @SerializedName("live_streams_category_result")
    public ArrayList<LiveStreamCategory> liveStreamsCategory;
    @SerializedName("live_stream_detail_result")
    public LiveStream liveStreamDetail;
    @SerializedName("live_stream_product_results")
    public ArrayList<Product> liveStreamProduct;
    @SerializedName("address_results")
    public ArrayList<Address> addressList;
    @SerializedName("overall_referral_results")
    public OverallReferralResults overallReferralResults;
    @SerializedName("referral_email_results")
    public ArrayList<ReferralEmailResult> referralEmailResults;
    @SerializedName("states_results")
    public ArrayList<State> stateArrayList;
    @SerializedName("live_stream_result")
    public LiveStream liveStream;
    @SerializedName("product_result")
    public Product product;
    @SerializedName("transactions")
    public ArrayList<Transaction> transactions;
    @SerializedName("top_live_stream_results")
    public ArrayList<TopLiveStream> topLiveStreamResults;
    @SerializedName("transaction_history_results")
    public ArrayList<TransactionHistory> transactionHistoryResults;
    @SerializedName("default_address_results")
    public Address address;
    @SerializedName("referral_count")
    public ReferralCount referralCount;
    @SerializedName("deb_redeemed_energy")
    public int dailyEnergyRedeemed = 0;
    @SerializedName("ranking_results")
    public Ranking rankingResults;
    @SerializedName("referral_count_results")
    public ReferralCount referralCountResults;
    @SerializedName("energy_level_results")
    public ArrayList<EnergyLevel> energyLevels;
    @SerializedName("merchant_energy_info")
    public MerchantEnergyInfo merchantEnergyInfo;
    @SerializedName("level_results")
    public ArrayList<LevelResult> level_results;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getRewardPoint() {
        return rewardPoint;
    }

    public void setRewardPoint(String rewardPoint) {
        this.rewardPoint = rewardPoint;
    }

    public Boolean getRedeemable() {
        return isRedeemable;
    }

    public void setRedeemable(Boolean redeemable) {
        isRedeemable = redeemable;
    }

    public ArrayList<LevelResult> getLevel_results() {
        return level_results;
    }

    public void setLevel_results(ArrayList<LevelResult> level_results) {
        this.level_results = level_results;
    }

    public MerchantEnergyInfo getMerchantEnergyInfo() {
        return merchantEnergyInfo;
    }

    public void setMerchantEnergyInfo(MerchantEnergyInfo merchantEnergyInfo) {
        this.merchantEnergyInfo = merchantEnergyInfo;
    }

    public ArrayList<EnergyLevel> getEnergyLevels() {
        return energyLevels;
    }

    public void setEnergyLevels(ArrayList<EnergyLevel> energyLevels) {
        this.energyLevels = energyLevels;
    }

    public ReferralCount getReferralCountResults() {
        return referralCountResults;
    }

    public void setReferralCountResults(ReferralCount referralCountResults) {
        this.referralCountResults = referralCountResults;
    }

    public Ranking getRankingResults() {
        return rankingResults;
    }

    public void setRankingResults(Ranking rankingResults) {
        this.rankingResults = rankingResults;
    }

    public int getDailyEnergyRedeemed() {
        return dailyEnergyRedeemed;
    }

    public void setDailyEnergyRedeemed(int dailyEnergyRedeemed) {
        this.dailyEnergyRedeemed = dailyEnergyRedeemed;
    }

    public ReferralCount getReferralCount() {
        return referralCount;
    }

    public void setReferralCount(ReferralCount referralCount) {
        this.referralCount = referralCount;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ArrayList<TransactionHistory> getTransactionHistoryResults() {
        return transactionHistoryResults;
    }

    public void setTransactionHistoryResults(ArrayList<TransactionHistory> transactionHistoryResults) {
        this.transactionHistoryResults = transactionHistoryResults;
    }

    public ArrayList<TopLiveStream> getTopLiveStreamResults() {
        return topLiveStreamResults;
    }

    public void setTopLiveStreamResults(ArrayList<TopLiveStream> topLiveStreamResults) {
        this.topLiveStreamResults = topLiveStreamResults;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public LiveStream getLiveStream() {
        return liveStream;
    }

    public void setLiveStream(LiveStream liveStream) {
        this.liveStream = liveStream;
    }

    public ArrayList<State> getStateArrayList() {
        return stateArrayList;
    }

    public void setStateArrayList(ArrayList<State> stateArrayList) {
        this.stateArrayList = stateArrayList;
    }

    public ArrayList<ReferralEmailResult> getReferralEmailResults() {
        return referralEmailResults;
    }

    public void setReferralEmailResults(ArrayList<ReferralEmailResult> referralEmailResults) {
        this.referralEmailResults = referralEmailResults;
    }

    public OverallReferralResults getOverallReferralResults() {
        return overallReferralResults;
    }

    public void setOverallReferralResults(OverallReferralResults overallReferralResults) {
        this.overallReferralResults = overallReferralResults;
    }

    public ArrayList<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(ArrayList<Address> addressList) {
        this.addressList = addressList;
    }

    public ArrayList<Product> getLiveStreamProduct() {
        return liveStreamProduct;
    }

    public void setLiveStreamProduct(ArrayList<Product> liveStreamProduct) {
        this.liveStreamProduct = liveStreamProduct;
    }

    public LiveStream getLiveStreamDetail() {
        return liveStreamDetail;
    }

    public void setLiveStreamDetail(LiveStream liveStreamDetail) {
        this.liveStreamDetail = liveStreamDetail;
    }

    public ArrayList<LiveStreamCategory> getLiveStreamsCategory() {
        return liveStreamsCategory;
    }

    public void setLiveStreamsCategory(ArrayList<LiveStreamCategory> liveStreamsCategory) {
        this.liveStreamsCategory = liveStreamsCategory;
    }

    public ArrayList<Industry> getActiveLiveStream() {
        return activeLiveStream;
    }

    public void setActiveLiveStream(ArrayList<Industry> activeLiveStream) {
        this.activeLiveStream = activeLiveStream;
    }

    public ArrayList<ProductMainCategory> getProductMainCategoryList() {
        return productMainCategoryList;
    }

    public void setProductMainCategoryList(ArrayList<ProductMainCategory> productMainCategoryList) {
        this.productMainCategoryList = productMainCategoryList;
    }

    @SerializedName("product_category_results")
    private ArrayList<ProductMainCategory> productMainCategoryList;


    public ArrayList<Product> getProductArrayList() {
        return productArrayList;
    }

    public void setProductArrayList(ArrayList<Product> productArrayList) {
        this.productArrayList = productArrayList;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public MerchantApplication getMerchantApplication() {
        return merchantApplication;
    }

    public void setMerchantApplication(MerchantApplication merchantApplication) {
        this.merchantApplication = merchantApplication;
    }

    public ArrayList<LiveStream> getLiveStreamArrayList() {
        return liveStreamArrayList;
    }

    public void setLiveStreamArrayList(ArrayList<LiveStream> liveStreamArrayList) {
        this.liveStreamArrayList = liveStreamArrayList;
    }

    public ArrayList<Industry> getIndustry() {
        return industry;
    }

    public void setIndustry(ArrayList<Industry> industry) {
        this.industry = industry;
    }

    public LoginResponse getLoginResponse() {
        return loginResponse;
    }

    public void setLoginResponse(LoginResponse loginResponse) {
        this.loginResponse = loginResponse;
    }


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
