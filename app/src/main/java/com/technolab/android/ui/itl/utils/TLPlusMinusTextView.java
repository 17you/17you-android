package com.technolab.android.ui.itl.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.technolab.android.R;
import com.technolab.android.utility.GlobalVariables;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class TLPlusMinusTextView extends LinearLayout {
    private PlusMinusType numberType;
    public EditText editText;
    public ImageButton buttonMinus;
    public ImageButton buttonPlus;
    private int incrementUnit = 1;
    private TextWatcher textListener;
    private OnClickListener buttonListener;


    public enum PlusMinusType {
        Price, Quantity;
    }

    public TLPlusMinusTextView() {
        this(GlobalVariables.getInstance().getContext());
    }

    public TLPlusMinusTextView(Context context) {
        this(context, null);
    }

    public TLPlusMinusTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.view_plus_minus, this);
        editText = (EditText) findViewById(R.id.plusminus_text);
        buttonMinus = (ImageButton) findViewById(R.id.plusminus_minus);
        buttonPlus = (ImageButton) findViewById(R.id.plusminus_plus);
        editText.setText("1");

        OnClickListener clickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                int newValue = Integer.parseInt(editText.getText().toString()) + incrementUnit * (view.getId() == buttonMinus.getId() ? -1 : 1);
                if(newValue < 1)
                    newValue = 1;
                editText.setText(String.valueOf(newValue));

                if (buttonListener != null)
                    buttonListener.onClick(view);
            }
        };

        buttonMinus.setOnClickListener(clickListener);
        buttonPlus.setOnClickListener(clickListener);


        boolean isPriceStyle = true;
        boolean isHideButton = false;
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TLPlusMinusTextView);
            isPriceStyle = a.getBoolean(R.styleable.TLPlusMinusTextView_isPriceStyle, true);
            isHideButton = a.getBoolean(R.styleable.TLPlusMinusTextView_hideButtons, false);
        }

        if (isPriceStyle) {
            numberType = PlusMinusType.Price;
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        } else {
            numberType = PlusMinusType.Quantity;
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        }

        if (isHideButton) {
            buttonMinus.setVisibility(GONE);
            buttonPlus.setVisibility(GONE);
        } else {
            buttonMinus.setVisibility(VISIBLE);
            buttonPlus.setVisibility(VISIBLE);
        }

        editText.addTextChangedListener(new TextWatcher() {
            private DecimalFormat df = new DecimalFormat("#,###", new DecimalFormatSymbols(Locale.US));

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (textListener != null)
                    textListener.beforeTextChanged(charSequence, i, i2, i3);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (textListener != null)
                    textListener.onTextChanged(charSequence, i, i2, i3);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (numberType == PlusMinusType.Quantity) {
                    editText.removeTextChangedListener(this);
                    try {
                        int beforeLength = editText.getText().length();
                        String s = editable.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "");
                        Number number = df.parse(s);
                        int start = editText.getSelectionStart();
                        editText.setText(df.format(number));
                        int afterLength = editText.getText().length();
                        int len = start + afterLength - beforeLength;
                        if (len > 0 && len < editText.getText().length())
                            editText.setSelection(len);
                        else
                            editText.setSelection(editText.getText().length());
                    } catch (Exception e) {

                    }
                    editText.addTextChangedListener(this);
                }
                if (textListener != null)
                    textListener.afterTextChanged(editable);
            }
        });
    }

    public void setIncrementUnit(int incrementUnit) {
        this.incrementUnit = incrementUnit;
    }

    public void removeButtons() {
        buttonMinus.setVisibility(GONE);
        buttonPlus.setVisibility(GONE);
    }

    public void hideButton() {
        buttonMinus.setVisibility(INVISIBLE);
        buttonPlus.setVisibility(INVISIBLE);
    }

    public void setTextListener(TextWatcher textListener) {
        this.textListener = textListener;
    }

    public void setButtonListener(OnClickListener buttonListener) {
        this.buttonListener = buttonListener;
    }

    public double getIncrementUnit() {
        return incrementUnit;
    }
}
