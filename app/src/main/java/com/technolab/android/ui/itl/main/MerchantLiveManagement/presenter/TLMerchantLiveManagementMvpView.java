package com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter;

import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.spine.AbstractMvpView;

import java.util.ArrayList;

public interface TLMerchantLiveManagementMvpView {

    interface LiveStreamMVP extends AbstractMvpView {
        void onRetrieveLiveStreams(ArrayList<LiveStream> liveStreamArrayList);
        void onRetrieveProductList(ArrayList<Product> productArrayList);
        void onFetchData(LiveStream liveStream);
        void onError(String message);
        void onResponse(String message);
        void onDelete(String message);
        void onPublish(String message);
    }

    interface LiveStreamingMVP extends AbstractMvpView{
        void onError(String message);
        void onResponse(String message);
        void onProductResponse(ArrayList<Product> productArrayList);
        void onProductUpdate(String message);
        void onStop(String message);
    }
}
