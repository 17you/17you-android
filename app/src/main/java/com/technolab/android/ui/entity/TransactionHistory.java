package com.technolab.android.ui.entity;

import java.util.ArrayList;

public class TransactionHistory {
    public int live_stream_id;
    public String live_stream_title;
    public String live_stream_date;
    public String live_stream_start_time;
    public ArrayList<DetailedTransaction> detailed_transaction;

    public int getLive_stream_id() {
        return live_stream_id;
    }

    public void setLive_stream_id(int live_stream_id) {
        this.live_stream_id = live_stream_id;
    }

    public String getLive_stream_title() {
        return live_stream_title;
    }

    public void setLive_stream_title(String live_stream_title) {
        this.live_stream_title = live_stream_title;
    }

    public String getLive_stream_date() {
        return live_stream_date;
    }

    public void setLive_stream_date(String live_stream_date) {
        this.live_stream_date = live_stream_date;
    }

    public String getLive_stream_start_time() {
        return live_stream_start_time;
    }

    public void setLive_stream_start_time(String live_stream_start_time) {
        this.live_stream_start_time = live_stream_start_time;
    }

    public ArrayList<DetailedTransaction> getDetailed_transaction() {
        return detailed_transaction;
    }

    public void setDetailed_transaction(ArrayList<DetailedTransaction> detailed_transaction) {
        this.detailed_transaction = detailed_transaction;
    }
}
