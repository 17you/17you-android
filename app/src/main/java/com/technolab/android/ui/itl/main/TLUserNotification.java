package com.technolab.android.ui.itl.main;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

public class TLUserNotification extends AbstractActivity {

    public TLUserNotification (){
        TAG = "Notification Activity";
        layoutID = R.layout.activity_user_notification;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);

        txtHeader.setText("Notification");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }
}
