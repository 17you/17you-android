package com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;

import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.State;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignUpMvpView;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignUpPresenter;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLAddProductVariant;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLState extends AbstractActivity implements TLSignUpMvpView {

    @Inject
    TLSignUpPresenter mTLSignUpPresenter;

    private Button btnNext;
    private int stateID;
    private SingleSelectToggleGroup toggleContainer;


    public TLState() {
        TAG = "TLState";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_state;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toggleContainer = findViewById(R.id.toggle_container);
        btnNext = findViewById(R.id.btnSubmit);
        btnNext.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLState.this,TLTellUsIndustry.class);
                intent.putExtra("username", getIntent().getStringExtra("username"));
                intent.putExtra("password", getIntent().getStringExtra("password"));
                intent.putExtra("phone", getIntent().getStringExtra("phone"));
                intent.putExtra("email", getIntent().getStringExtra("email"));
                intent.putExtra("state", stateID);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLState.this);
        mTLSignUpPresenter.onAttach(this);
        mTLSignUpPresenter.fetchStates();
    }

    @Override
    public void onRegistrationSuccess(String message) {

    }

    @Override
    public void onRegistrationFailed(String message) {

    }

    @Override
    public void onReceiveIndustryInfo(ArrayList<Industry> industry) {

    }

    @Override
    public void onReceiveStates(ArrayList<State> stateArrayList) {
        btnNext.setVisibility(View.VISIBLE);
        for(int i = 0; i < stateArrayList.size(); i++){
            ToggleButton toggleButton = new ToggleButton(this);
            toggleButton.setText(stateArrayList.get(i).getState_name());
            toggleButton.setTextOff(stateArrayList.get(i).getState_name());
            toggleButton.setTextOn(stateArrayList.get(i).getState_name());
            toggleButton.setId(stateArrayList.get(i).getState_id());
            toggleButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.toggle_state_selector));
            toggleButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked)
                        stateID = Integer.parseInt(buttonView.getTag().toString());
                }
            });
            toggleContainer.addView(toggleButton);
            toggleContainer.setOnCheckedChangeListener(new SingleSelectToggleGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(SingleSelectToggleGroup group, int checkedId) {
                    stateID = checkedId;
                }
            });
        }
    }

    @Override
    public void onError(String message) {

    }
}
