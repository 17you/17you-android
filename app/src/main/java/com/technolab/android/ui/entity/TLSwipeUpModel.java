package com.technolab.android.ui.entity;

public class TLSwipeUpModel {
    private String name;

    public TLSwipeUpModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
