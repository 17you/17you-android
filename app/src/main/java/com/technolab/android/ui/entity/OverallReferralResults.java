package com.technolab.android.ui.entity;

import java.util.List;

public class OverallReferralResults{
    public int total_sign_up;
    public int total_share;
    public List<TotalSignUpState> total_sign_up_states;

    public int getTotal_sign_up() {
        return total_sign_up;
    }

    public void setTotal_sign_up(int total_sign_up) {
        this.total_sign_up = total_sign_up;
    }

    public int getTotal_share() {
        return total_share;
    }

    public void setTotal_share(int total_share) {
        this.total_share = total_share;
    }

    public List<TotalSignUpState> getTotal_sign_up_states() {
        return total_sign_up_states;
    }

    public void setTotal_sign_up_states(List<TotalSignUpState> total_sign_up_states) {
        this.total_sign_up_states = total_sign_up_states;
    }
}

