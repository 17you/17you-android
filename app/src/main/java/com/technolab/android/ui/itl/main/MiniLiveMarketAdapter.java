package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

public class MiniLiveMarketAdapter extends RecyclerView.Adapter<MiniLiveMarketAdapter.LiveMarketViewHolder>{

    ArrayList<Industry> liveMarketArrayList;
    Context context;
    private MiniLiveMarketAdapter.ItemClickListener mCallback;

    public MiniLiveMarketAdapter(Context context, ArrayList<Industry> liveMarkets){
        this.context = context;
        this.liveMarketArrayList = liveMarkets;
    }

    @NonNull
    @Override
    public MiniLiveMarketAdapter.LiveMarketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_live_market, parent, false);
        return new MiniLiveMarketAdapter.LiveMarketViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MiniLiveMarketAdapter.LiveMarketViewHolder holder, int position) {
        Industry liveMarket = liveMarketArrayList.get(position);
        holder.txtTitle.setText(liveMarket.getIndustry_category_name());


        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onItemClicked(liveMarket.getIndustry_category_id());
                }
            }
        });

        Glide.with(context)
                .load(liveMarket.getIndustry_category_image())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.imgCategory);

    }

    @Override
    public int getItemCount() {
        return liveMarketArrayList.size();
    }

    public class LiveMarketViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtLiveCount;
        ImageView imgLiveStatus;
        ImageView imgCategory;
        public LiveMarketViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txt_title);
            txtLiveCount = itemView.findViewById(R.id.txt_live_count);
            imgLiveStatus = itemView.findViewById(R.id.img_live_status);
            imgCategory = itemView.findViewById(R.id.img_category);
            itemView.findViewById(R.id.live_count_view).setVisibility(View.GONE);
            imgCategory.setColorFilter(ContextCompat.getColor(context, R.color.tint), android.graphics.PorterDuff.Mode.MULTIPLY);
            imgCategory.setImageResource(R.drawable.placeholder);
        }
    }

    public void setOnItemClickListener(final MiniLiveMarketAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(int id);
    }
}
