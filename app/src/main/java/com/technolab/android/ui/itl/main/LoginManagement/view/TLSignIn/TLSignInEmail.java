package com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.db.SettingsController;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInMvpView;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInPresenter;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword.TLForgotPassword;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp.TLSignUp;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.KeyboardUtils;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.ValidateUtils;
import com.technolab.android.user.TLEnvironment;
import com.technolab.android.user.TLStorage;

import javax.inject.Inject;

public class TLSignInEmail extends AbstractActivity implements TLSignInMvpView {

    TextView txtForgotPassword;
    TextView txtSignInMobile;
    EditText etEmail;
    EditText etPassword;
    TextView txtSignUp;
    Button btnSignIn;
    ValidateUtils mValidator;

    @Inject
    TLSignInPresenter mTLSignInPresenter;

    public TLSignInEmail() {
        TAG = "Sign In Email";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_sign_in_email;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(TLSignInEmail.this);
        txtForgotPassword = findViewById(R.id.txtForgotPassword);
        txtSignInMobile = findViewById(R.id.txtSignInMobile);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        txtSignUp = findViewById(R.id.txtSignUp);
        btnSignIn = findViewById(R.id.btnSignIn);
        mValidator = new ValidateUtils(this);
        mValidator.addField(etEmail)
                .addField(etPassword);
        mTLSignInPresenter.onAttach(this);

       ValidateUtils.showHidePassword(etPassword);

        txtForgotPassword.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLSignInEmail.this, TLForgotPassword.class));
            }
        });

        txtSignInMobile.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
                startActivity(new Intent(TLSignInEmail.this, TLSignInPhone.class));
            }
        });

        txtSignUp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLSignInEmail.this, TLSignUp.class));
            }
        });

        btnSignIn.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                KeyboardUtils.hideSoftKeyboard(TLSignInEmail.this);
                //if (mValidator.isValid()) {
                    mTLSignInPresenter.signInViaEmail(etEmail.getText().toString(), etPassword.getText().toString());
                //}
            }
        });
    }

    @Override
    public void onLoginSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        TLStorage.getInstance().setLoggedIn(true);
        Intent intent = new Intent(TLSignInEmail.this, UserMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onLoginFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheck(String message) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTLSignInPresenter.onDetach();
        mValidator.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        KeyboardUtils.hideSoftKeyboard(this);
    }
}
