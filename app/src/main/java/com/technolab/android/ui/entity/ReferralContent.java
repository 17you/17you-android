package com.technolab.android.ui.entity;

public class ReferralContent {
    public int energy;
    public int member_id;
    public String mask_email;
    public String member_signed_up_date_and_time;
    public int level;

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getMember_id() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }

    public String getMask_email() {
        return mask_email;
    }

    public void setMask_email(String mask_email) {
        this.mask_email = mask_email;
    }

    public String getMember_signed_up_date_and_time() {
        return member_signed_up_date_and_time;
    }

    public void setMember_signed_up_date_and_time(String member_signed_up_date_and_time) {
        this.member_signed_up_date_and_time = member_signed_up_date_and_time;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
