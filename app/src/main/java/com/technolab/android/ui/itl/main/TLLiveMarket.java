package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.LiveMarket;
import com.technolab.android.ui.entity.MerchantApplication;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLMerchantOnboardingActivity;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLLiveMarket extends AbstractActivity implements LiveMarketAdapter.ItemClickListener, TLMainMvpView.LiveMarket {

    private LiveMarketAdapter liveMarketAdapter;
    private RecyclerView recyclerView;
    private ArrayList<Industry> industryArrayList;

    @Inject
    TLMainPresenter.TLLiveMarketPresenter liveMarketPresenter;

    public TLLiveMarket(){
        TAG = "17Live&Shop";
        layoutID = R.layout.activity_live_market;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        setupRefresh(this);
        recyclerView = findViewById(R.id.recycler_view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLLiveMarket.this);
        liveMarketPresenter.onAttach(this);
        liveMarketPresenter.fetchActiveLiveStream();
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText(TAG);
        txtSubHeader.setText("Live Streaming and Check");

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setImageDrawable(getResources().getDrawable(R.drawable.ic_store_front));
        imgRight2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                liveMarketPresenter.fetchApplicationStatus(TLStorage.getInstance().getCurrentUserId());
            }
        });
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> liveMarketPresenter.fetchActiveLiveStream());
    }

    @Override
    public void onItemClicked(int id) {
       // startActivity(new Intent(TLLiveMarket.this,TLUserLiveStreaming.class));
        Intent intent = new Intent(TLLiveMarket.this,TLLiveMarketCategory.class);
        intent.putExtra("marketID", String.valueOf(id));
        intent.putExtra("industry", this.industryArrayList);
        startActivity(intent);

    }

    @Override
    public void onFetchLiveMarket(ArrayList<Industry> industryArrayList) {
        refreshLayout.setRefreshing(false);
        this.industryArrayList = industryArrayList;
        liveMarketAdapter = new LiveMarketAdapter(this,industryArrayList);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setJustifyContent(JustifyContent.SPACE_EVENLY);
        layoutManager.setAlignItems(AlignItems.CENTER);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(liveMarketAdapter);
        liveMarketAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onCheckMerchantRegistrationStatus(boolean status, MerchantApplication application) {
        TLStorage.getInstance().setFirstStepComplete(application.isIdentificationSubmitted());
        TLStorage.getInstance().setSecondStepComplete(application.isSelfieSubmitted());
        TLStorage.getInstance().setThirdStepComplete(application.isBusinessDetailSubmitted());
        TLStorage.getInstance().setFourthStepComplete(application.isLiveSubReceiptSubmitted());
        if(!status)
            startActivity(new Intent(TLLiveMarket.this, TLMerchantOnboardingActivity.class));
        else
            Toast.makeText(this, "Merchant Registration Complete. You will be notified once access to Merchant Dashboard is available !", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
    }
}
