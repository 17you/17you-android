package com.technolab.android.ui.entity;

import java.util.ArrayList;

public class LiveStream {
    private int live_stream_id;
    private String live_stream_title;
    private String live_stream_description;
    private String live_stream_date;
    private String live_stream_start_time;
    private String live_stream_end_time;
    private String live_stream_status;
    private int merchant_id;
    private String live_stream_image;
    public String bambuser_id;
    public String merchant_state;
    public String merchant_name;
    public boolean isfavourite;
    public String shipping_fee;
    public ArrayList<Product> products;

    public String merchant_bank_number;
    public String merchant_bank_holder_name;
    public String merchant_bank_name;

    public String getMerchant_bank_number() {
        return merchant_bank_number;
    }

    public void setMerchant_bank_number(String merchant_bank_number) {
        this.merchant_bank_number = merchant_bank_number;
    }

    public String getMerchant_bank_holder_name() {
        return merchant_bank_holder_name;
    }

    public void setMerchant_bank_holder_name(String merchant_bank_holder_name) {
        this.merchant_bank_holder_name = merchant_bank_holder_name;
    }

    public String getMerchant_bank_name() {
        return merchant_bank_name;
    }

    public void setMerchant_bank_name(String merchant_bank_name) {
        this.merchant_bank_name = merchant_bank_name;
    }

    public String getShipping_fee() {
        return shipping_fee;
    }

    public void setShipping_fee(String shipping_fee) {
        this.shipping_fee = shipping_fee;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public boolean isFavourite() {
        return isfavourite;
    }

    public void setIsfavourite(boolean isfavourite) {
        this.isfavourite = isfavourite;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public String getMerchant_state() {
        return merchant_state;
    }

    public void setMerchant_state(String merchant_state) {
        this.merchant_state = merchant_state;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getBambuser_id() {
        return bambuser_id;
    }

    public void setBambuser_id(String bambuser_id) {
        this.bambuser_id = bambuser_id;
    }

    public int getLive_stream_id() {
        return live_stream_id;
    }

    public void setLive_stream_id(int live_stream_id) {
        this.live_stream_id = live_stream_id;
    }

    public String getLive_stream_title() {
        return live_stream_title;
    }

    public void setLive_stream_title(String live_stream_title) {
        this.live_stream_title = live_stream_title;
    }

    public String getLive_stream_description() {
        return live_stream_description;
    }

    public void setLive_stream_description(String live_stream_description) {
        this.live_stream_description = live_stream_description;
    }

    public String getLive_stream_date() {
        return live_stream_date;
    }

    public void setLive_stream_date(String live_stream_date) {
        this.live_stream_date = live_stream_date;
    }

    public String getLive_stream_start_time() {
        return live_stream_start_time;
    }

    public void setLive_stream_start_time(String live_stream_start_time) {
        this.live_stream_start_time = live_stream_start_time;
    }

    public String getLive_stream_end_time() {
        return live_stream_end_time;
    }

    public void setLive_stream_end_time(String live_stream_end_time) {
        this.live_stream_end_time = live_stream_end_time;
    }

    public String getLive_stream_status() {
        return live_stream_status;
    }

    public void setLive_stream_status(String live_stream_status) {
        this.live_stream_status = live_stream_status;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getLive_stream_image() {
        return live_stream_image;
    }

    public void setLive_stream_image(String live_stream_image) {
        this.live_stream_image = live_stream_image;
    }
}
