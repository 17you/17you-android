package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.LiveMarket;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

public class LiveMarketAdapter extends RecyclerView.Adapter<LiveMarketAdapter.LiveMarketViewHolder>{

    ArrayList<Industry> liveMarketArrayList;
    Context context;
    private LiveMarketAdapter.ItemClickListener mCallback;

    public LiveMarketAdapter(Context context, ArrayList<Industry> liveMarkets){
        this.context = context;
        this.liveMarketArrayList = liveMarkets;
    }

    @NonNull
    @Override
    public LiveMarketAdapter.LiveMarketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_live_market, parent, false);
        return new LiveMarketViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LiveMarketAdapter.LiveMarketViewHolder holder, int position) {
        Industry liveMarket = liveMarketArrayList.get(position);
        holder.txtTitle.setText(liveMarket.getIndustry_category_name());
        if (Integer.parseInt(liveMarket.getCount()) != 0) {
            holder.txtLiveCount.setText(String.format("%s Live", liveMarket.getCount()));
            holder.imgLiveStatus.setBackground(context.getResources().getDrawable(R.drawable.round_green));
        }
        else {
            holder.txtLiveCount.setText("0");
            holder.imgLiveStatus.setBackground(context.getResources().getDrawable(R.drawable.round_gray));
        }

        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onItemClicked(liveMarket.getIndustry_category_id());
                }
            }
        });

        Glide.with(context)
                .load(liveMarket.getIndustry_category_image())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.imgCategory);
    }

    @Override
    public int getItemCount() {
        return liveMarketArrayList.size();
    }

    public class LiveMarketViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        TextView txtLiveCount;
        ImageView imgLiveStatus;
        ImageView imgCategory;
        public LiveMarketViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txt_title);
            txtLiveCount = itemView.findViewById(R.id.txt_live_count);
            imgLiveStatus = itemView.findViewById(R.id.img_live_status);
            imgCategory = itemView.findViewById(R.id.img_category);
            imgCategory.setColorFilter(ContextCompat.getColor(context, R.color.tint), android.graphics.PorterDuff.Mode.MULTIPLY);
            imgCategory.setImageResource(R.drawable.placeholder);
        }
    }

    public void setOnItemClickListener(final LiveMarketAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(int id);
    }
}
