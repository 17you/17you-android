package com.technolab.android.ui.itl.main.MerchantRegistration.view;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.camerakit.CameraKit;
import com.camerakit.CameraKitView;
import com.technolab.android.R;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationMvpView;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.spine.AbstractFragment;
import com.technolab.android.ui.itl.utils.FocusView;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;

import javax.inject.Inject;

import id.zelory.compressor.Compressor;

import static com.technolab.android.ui.itl.utils.FocusView.INIT_TYPE_PARCELABLE;
import static com.technolab.android.ui.itl.utils.FocusView.TYPE_SQUARE;

public class TLScanICBack extends AbstractActivity implements CameraKitView.ImageCallback, TLMerchantRegistrationMvpView.VerificationImage {

    @Inject
    TLMerchantRegistrationPresenter.TLVerificationPresenter merchantRegistrationPresenter;

    public static final int CAMERAKIT_RESULT = 1220;
    private CameraKitView cameraKitView;
    private ImageView cameraButton;
    private FocusView focusView;
    boolean cropIt = false;
    private String frontImageURI;
    private int industryID;

    public TLScanICBack(){
        TAG = "Scan IC Back";
        layoutID = R.layout.fragment_scan_ic_back;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        cameraKitView = findViewById(R.id.camera);
        cameraButton = findViewById(R.id.btn_camera);
        focusView = findViewById(R.id.focus_view);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            isPermissionsToAccessStorageAreGranted(this);
        }

        int type = getIntent().getIntExtra(INIT_TYPE_PARCELABLE,TYPE_SQUARE);
        frontImageURI = getIntent().getStringExtra("front_data");
        industryID = getIntent().getIntExtra("industry_id",0);
        focusView.setFocusType(type);
        if(type==TYPE_SQUARE){
            cameraKitView.setFacing(CameraKit.FACING_BACK);
            cropIt = true;
        }

        cameraButton.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                cameraKitView.captureImage(TLScanICBack.this::onImage);
            }
        });

        cameraKitView.setErrorListener(new CameraKitView.ErrorListener() {
            @Override
            public void onError(CameraKitView cameraKitView, CameraKitView.CameraException e) {
                e.printStackTrace();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void isPermissionsToAccessStorageAreGranted(Activity activity) {
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        cameraKitView.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStart() {
        super.onStart();
        cameraKitView.onStart();
        getAppComponent().inject(TLScanICBack.this);
        merchantRegistrationPresenter.onAttach(this);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Business Application");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setImageDrawable(getResources().getDrawable(R.drawable.ic_round_close));
        imgRight2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLScanICBack.this, TLMerchantOnboardingActivity.class));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        cameraKitView.onResume();
    }

    @Override
    public void onPause() {
        cameraKitView.onPause();
        super.onPause();
    }

    @Override
    public void onStop() {
        cameraKitView.onStop();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onImage(CameraKitView cameraKitView, byte[] bytes) {
        new BitmapCompressor(bytes,cropIt).execute();
    }

    @Override
    public void onUpload(String message) {
        finish();
    }

    @Override
    public void onError(String message) {

    }

    class BitmapCompressor extends AsyncTask<Void,String,String> {

        byte[] initBytes;
        boolean mCrop;

        public BitmapCompressor(byte[] bytes,boolean cropIt){
            initBytes = bytes;
            mCrop = cropIt;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String result = null;

            Bitmap bitmap = BitmapFactory.decodeByteArray(initBytes, 0, initBytes.length);
            Bitmap scaled = Bitmap.createScaledBitmap(bitmap,bitmap.getWidth()/2,bitmap.getHeight()/2,true);
            // crop it
            if(mCrop){
                if (scaled.getWidth() >= scaled.getHeight()){

                    scaled = Bitmap.createBitmap(
                            scaled,
                            scaled.getWidth()/2 - scaled.getHeight()/2,
                            0,
                            scaled.getHeight(),
                            scaled.getHeight()
                    );

                }else{

                    scaled = Bitmap.createBitmap(
                            scaled,
                            0,
                            scaled.getHeight()/2 - scaled.getWidth()/2,
                            scaled.getWidth(),
                            scaled.getWidth()
                    );
                }
            }

            try {
                File savedPhoto = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES) + File.separator + "ic_back.png");
                //File savedPhoto = new File(Environment.getExternalStorageDirectory(), "ic_back.png");
                FileOutputStream out = new FileOutputStream(savedPhoto);
                scaled.compress(Bitmap.CompressFormat.PNG, 100, out);
                File compressedImageFile = new Compressor(TLScanICBack.this).setCompressFormat(Bitmap.CompressFormat.PNG).setQuality(100).compressToFile(savedPhoto);
                result = compressedImageFile.getAbsolutePath();
            }catch (Exception ex){
                ex.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String backImageURI) {
            if(backImageURI.isEmpty()){
                Toast.makeText(getApplicationContext(),"Something goes wrong",Toast.LENGTH_SHORT).show();
                return;
            }

            merchantRegistrationPresenter.uploadIdentification(TLStorage.getInstance().getCurrentUserId(), industryID, frontImageURI, backImageURI);
            super.onPostExecute(backImageURI);
        }
    }
}
