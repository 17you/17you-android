package com.technolab.android.ui.itl.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bambuser.broadcaster.BroadcastPlayer;
import com.bambuser.broadcaster.PlayerState;
import com.bambuser.broadcaster.SurfaceViewWithAutoAR;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;
import com.technolab.android.BuildConfig;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Address;
import com.technolab.android.ui.entity.Cart;
import com.technolab.android.ui.entity.InitialLiveChat;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Message;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.SendMessage;
import com.technolab.android.ui.entity.Variation;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.CustomToggleButton;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.OnSwipeTouchListener;
import com.technolab.android.ui.itl.utils.TLPlusMinusTextView;
import com.technolab.android.ui.itl.utils.badge.BadgeFactory;
import com.technolab.android.ui.itl.utils.badge.BadgeView;
import com.technolab.android.ui.itl.utils.heartview.HeartLayout;
import com.technolab.android.user.TLStorage;
import com.technolab.android.utility.GlobalVariables;
import com.technolab.android.utility.MessageType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import ir.androidexception.andexalertdialog.AndExAlertDialog;
import ir.androidexception.andexalertdialog.AndExAlertDialogListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TLUserLiveStreaming extends AbstractActivity implements TLMainMvpView.LiveStreamingCategory, LiveStreamProductAdapter.ItemClickListener {

    final OkHttpClient mOkHttpClient = new OkHttpClient();
    private SurfaceViewWithAutoAR mVideoSurface;
    private View mPlayerContentView;
    private BroadcastPlayer mBroadcastPlayer;
    private TextView txtLiveCount;
    private HeartLayout heartLayout;
    private ImageView heartView;
    private Random mRandom = new Random();

    private EditText et_message_box;
    private ImageView btnSend;
    private RecyclerView liveChatRV;
    private Socket mSocket;
    private String mUsername = TLStorage.getInstance().getCurrentUserName();
    private String mRoomName;
    private ImageView btnCloseLiveStream;

    private ArrayList<Message> messageArrayList;
    private LiveChatAdapter liveChatAdapter;
    private Gson gson = new Gson();

    private View addCartSliderView;
    private SlideUp addCartSlideUp;

    private View checkoutSliderView;
    private SlideUp checkoutSlideUp;

    private LinearLayout content_container;
    private Button btn_buy;

    Display mDefaultDisplay;
    private String liveStreamID;
    private RecyclerView product_recycler_view;
    private LiveStreamProductAdapter productAdapter;
    private CheckBox imgFavourite;
    private Button btnShare;
    private ImageView btnCart;

    private BadgeView badgeView;
    private int count = 0;
    private ArrayList<Cart> cartArrayList;
    private int variationSelectedIndex;
    private double deliveryFees;

    private Double finalPrice = 0.00;
    private Double finalPriceWithDelivery = 0.00;
    private int merchantID;
    private Address defaultAddress;
    private ImageView btnCloseLive;

    private LiveStream mLiveStream;

    @Inject
    TLMainPresenter.TLLiveStreamDetailPresenter presenter;

    public TLUserLiveStreaming() {
        TAG = "TLUserLiveStreaming";
        layoutID = R.layout.activity_user_live_streaming;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        liveStreamID = getIntent().getStringExtra("liveStreamID");
        mRoomName = liveStreamID;
        //Utility.getInstance().encryptPassword(liveStreamID, SecOne.getInstance().getComKey() + KeyOne.getInstance().getComKey());

        cartArrayList = new ArrayList<>();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mVideoSurface = findViewById(R.id.VideoSurfaceView);
        heartLayout = findViewById(R.id.heart_layout);
        heartView = findViewById(R.id.btn_heart);
        mPlayerContentView = findViewById(R.id.PlayerContentView);
        txtLiveCount = findViewById(R.id.txtLiveCount);
        mDefaultDisplay = getWindowManager().getDefaultDisplay();
        et_message_box = findViewById(R.id.et_message_box);
        liveChatRV = findViewById(R.id.recycler_view);
        content_container = findViewById(R.id.content_container);
        btnCloseLiveStream = findViewById(R.id.btn_close_live);
        messageArrayList = new ArrayList<>();
        product_recycler_view = findViewById(R.id.product_recycler_view);
        imgFavourite = findViewById(R.id.img_bookmark);
        btnShare = findViewById(R.id.btn_share);
        btnCart = findViewById(R.id.btn_cart);
        btnSend = findViewById(R.id.btn_send);
        btnCloseLive = findViewById(R.id.btn_close_live);

        btnCloseLive.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        btnCart.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (cartArrayList.size() == 0) {
                    Toast.makeText(TLUserLiveStreaming.this, "No item in cart.", Toast.LENGTH_SHORT).show();
                    return;
                }

                presenter.retrieveAddress(cartArrayList);
            }
        });

        badgeView = BadgeFactory.create(this);

        addCartSliderView = findViewById(R.id.cl_slide_view_container);
        addCartSlideUp = new SlideUpBuilder(addCartSliderView)
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .build();

        checkoutSliderView = findViewById(R.id.cl_slide_view_checkout);
        checkoutSlideUp = new SlideUpBuilder(checkoutSliderView)
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .build();

        btnSend.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (!et_message_box.getText().toString().isEmpty() || !et_message_box.getText().toString().equals(""))
                    sendMessage();
            }
        });

        liveChatAdapter = new LiveChatAdapter(this, messageArrayList);
        liveChatRV.setAdapter(liveChatAdapter);

        MainApplication app = (MainApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.once(Socket.EVENT_CONNECT, onConnect);
        mSocket.on("newUserToLiveRoom", onNewUser);
        mSocket.on("updateLiveChat", onUpdateLiveChat);
        mSocket.on("userLeftLiveRoom", onUserLeft);
        mSocket.on("like", onLikeClicked);
        mSocket.on("update", onProductUpdate);
        mSocket.connect();

        heartView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InitialLiveChat data = new InitialLiveChat(mUsername, mRoomName);
                String jsonData = gson.toJson(data);
                mSocket.emit("like", jsonData);
                addHeart();
            }
        });

        mPlayerContentView.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                slideRight(content_container);
            }

            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                System.out.println("Swipe Left");
                slideLeft(content_container);
            }
        });

        btnCloseLiveStream.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLUserLiveStreaming.this);

        presenter.onAttach(this);
        presenter.fetchLiveStreamDetail(liveStreamID, true);
    }

    @Override
    public void onFetchLiveStreamDetails(LiveStream liveStream, boolean isInitialFetch) {
        mLiveStream = liveStream;
        productAdapter = new LiveStreamProductAdapter(this, liveStream.getProducts() != null ? liveStream.getProducts() : new ArrayList<>());
        product_recycler_view.setAdapter(productAdapter);
        productAdapter.setOnItemClickListener(this);
        productAdapter.notifyDataSetChanged();

        if (isInitialFetch) {
            Log.d("Broadcast Log", "Loading specific broadcast");
            getLatestResourceUri(liveStream.getBambuser_id());
        }

        merchantID = liveStream.getMerchant_id();
        deliveryFees = Double.parseDouble(liveStream.getShipping_fee());

        btnShare.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String title = liveStream.getLive_stream_title();
                String desc = liveStream.getLive_stream_description();
                Uri longURL = DeepLinkUtils.createDeepLink(String.valueOf(liveStream.getLive_stream_id()), "live");
                DeepLinkUtils.shortTheLink(longURL, TLUserLiveStreaming.this, "live");
                Uri shortURL = DeepLinkUtils.getShortLink();
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");

                i.putExtra(Intent.EXTRA_TEXT, String.format("%s\n%s", String.format("Watch 17You's Live : %s - %s.", title, desc), shortURL.toString()));

                startActivity(Intent.createChooser(i, "Share URL"));
            }
        });

        imgFavourite.setChecked(liveStream.isFavourite());
        imgFavourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                presenter.updateFavourites(liveStream.getMerchant_id(), isChecked ? 1 : 0);
            }
        });
    }

    @Override
    public void onItemClicked(Product product) {
        Glide.with(this)
                .load(product.getProduct_image())
                .thumbnail(0.1f)
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into((ImageView) addCartSliderView.findViewById(R.id.img_product));

        ((TextView) addCartSliderView.findViewById(R.id.txt_product_title)).setText(product.getProduct_name());
        ((TextView) addCartSliderView.findViewById(R.id.txt_product_description)).setText(product.getProduct_description());

        SingleSelectToggleGroup single = addCartSliderView.findViewById(R.id.group_choices);
        single.removeAllViews();
        if (product.getVariations().size() > 0 && single.getChildCount() == 0) {
            ArrayList<Variation> variations = product.getVariations();
            for (int i = 0; i < variations.size(); i++) {
                Variation variation = variations.get(i);
                if(variation.getProduct_variation_status()){
                    CustomToggleButton toggleButton = new CustomToggleButton(this);
                    toggleButton.setMarkerColor(Color.parseColor("#FCD9D3"));
                    toggleButton.setText(variation.getProduct_variation_size());
                    single.addView(toggleButton);
                }
            }

            for (int i = 0; i < single.getChildCount(); i++) {
                CustomToggleButton toggle = (CustomToggleButton) single.getChildAt(i);
                if (i == 0) {
                    toggle.setChecked(true);
                    variationSelectedIndex = i;
                }
            }
        }

        single.setOnCheckedChangeListener(new SingleSelectToggleGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SingleSelectToggleGroup group, int checkedId) {
                for (int i = 0; i < group.getChildCount(); i++) {
                    CustomToggleButton toggle = (CustomToggleButton) group.getChildAt(i);
                    if (toggle.getId() == checkedId) {
                        variationSelectedIndex = i;
                        ((TextView) addCartSliderView.findViewById(R.id.txt_product_price)).setText(String.format("RM%s", product.getVariations().get(i).getProduct_variation_price()));
                    }
                }
            }
        });

        addCartSliderView.findViewById(R.id.btn_close).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                GlobalVariables.getHandlerUI().post(new Runnable() {
                    @Override
                    public void run() {
                        addCartSlideUp.hide();
                    }
                });
            }
        });

        addCartSliderView.findViewById(R.id.button1).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                int quantity = Integer.parseInt(((TLPlusMinusTextView) addCartSliderView.findViewById(R.id.txt_plus_minus)).editText.getText().toString());
                performAddToCart(product, quantity);

                addCartSlideUp.hide();
            }
        });

        addCartSliderView.findViewById(R.id.btnCheckout).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                int quantity = Integer.parseInt(((TLPlusMinusTextView) addCartSliderView.findViewById(R.id.txt_plus_minus)).editText.getText().toString());
                performAddToCart(product, quantity);

                addCartSlideUp.hide();
                presenter.retrieveAddress(cartArrayList);
            }
        });


        GlobalVariables.getHandlerUI().post(new Runnable() {
            @Override
            public void run() {
                addCartSlideUp.show();
            }
        });
    }

    private void performAddToCart(Product product, int quantity){
        for(Cart cart : cartArrayList){
            if(cart.getProduct().getProduct_id().equals(product.getProduct_id())){
                if(cart.getSelectedVariantIndex() == variationSelectedIndex){
                    cart.setQuantity(cart.getQuantity() + quantity);
                    return;
                }
            }
        }

        cartArrayList.add(new Cart(product, quantity, variationSelectedIndex, product.getVariations().get(variationSelectedIndex).getProduct_variation_price()));
        addToCart(cartArrayList.size());
    }

    @Override
    public void onFetchAddress(Boolean status, Address address, ArrayList<Cart> cartList) {
        if (!status) {
            startActivity(new Intent(TLUserLiveStreaming.this, TLAddressSetting.class));
            return;
        }

        defaultAddress = address;

        GlobalVariables.getHandlerUI().post(new Runnable() {
            @Override
            public void run() {
                if (cartList.size() == 0)
                    return;

                finalPrice = 0.00;
                finalPriceWithDelivery = 0.00;
                LinearLayout cartProductContainer = checkoutSliderView.findViewById(R.id.cart_product_container);
                TextView txtFinalPrice = checkoutSliderView.findViewById(R.id.txt_total_price);
                TextView txtDeliveryFee = checkoutSliderView.findViewById(R.id.txt_delivery_fee);
                TextView txtAddress = checkoutSliderView.findViewById(R.id.txt_address);
                cartProductContainer.removeAllViews();

                for (Cart cart : cartList) {
                    LayoutInflater layoutInflater = LayoutInflater.from(TLUserLiveStreaming.this);
                    ConstraintLayout productView = (ConstraintLayout) layoutInflater.inflate(R.layout.view_product_checkout, null);

                    Glide.with(TLUserLiveStreaming.this)
                            .load(cart.getProduct().getProduct_image())
                            .thumbnail(0.1f)
                            .centerCrop()
                            .placeholder(TLUserLiveStreaming.this.getResources().getDrawable(R.drawable.placeholder))
                            .into((ImageView) productView.findViewById(R.id.img_product));

                    Variation variation = cart.getProduct().getVariations().get(cart.getSelectedVariantIndex());
                    ((TextView) productView.findViewById(R.id.txt_product_title)).setText(cart.getProduct().getProduct_name());
                    ((TextView) productView.findViewById(R.id.txt_product_description)).setText(cart.getProduct().getProduct_description());
                    ((TextView) productView.findViewById(R.id.txt_product_variant)).setText(variation.getProduct_variation_size());
                    ((TextView) productView.findViewById(R.id.txt_product_quantity)).setText(String.format("x%d", cart.getQuantity()));
                    ((TextView) productView.findViewById(R.id.txt_product_price)).setText(String.format("RM%.2f", (cart.getPrice() * cart.getQuantity())));

                    Button btnRemove = productView.findViewById(R.id.btn_remove);
                    btnRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cartProductContainer.removeView(productView);
                            cartArrayList.remove(cart);
                            Log.d("numberofchildren", String.valueOf(cartProductContainer.getChildCount()));
                            if (cartArrayList.size() == 0) {
                                finalPriceWithDelivery = 0.00;
                                finalPrice = 0.00;
                                txtFinalPrice.setText(String.format("RM%.2f", finalPriceWithDelivery));
                                GlobalVariables.getHandlerUI().post(new Runnable() {
                                    @Override
                                    public void run() {
                                        checkoutSlideUp.hide();
                                        addToCart(cartArrayList.size());
                                    }
                                });
                            } else {
                                finalPriceWithDelivery = finalPriceWithDelivery - (cart.getPrice() * cart.getQuantity());
                                addToCart(cartArrayList.size());
                                txtFinalPrice.setText(String.format("RM%.2f", finalPriceWithDelivery));
                            }


                        }
                    });

                    finalPrice += cart.getPrice() * cart.getQuantity();
                    cartProductContainer.addView(productView);
                }

                finalPriceWithDelivery = finalPrice + deliveryFees;
                txtDeliveryFee.setText(String.format("RM%.2f", deliveryFees));
                txtFinalPrice.setText(String.format("RM%.2f", finalPriceWithDelivery));
                txtAddress.setText(String.format("%s | %s\n%s\n%s\n%s %s %s", TLStorage.getInstance().getCurrentUserName(),
                        TLStorage.getInstance().getUserPhone(), address.getAddress_line_1(), address.getAddress_line_2(),
                        address.getPostcode(), address.getCity(), address.getState()));
            }
        });

        Button btnPlaceOrder = checkoutSliderView.findViewById(R.id.btnCheckout);
        ImageView btnClose = checkoutSliderView.findViewById(R.id.img_close);
        TextView txtEditAddress = checkoutSliderView.findViewById(R.id.txt_edit_address);

        txtEditAddress.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                checkoutSlideUp.hide();
                startActivity(new Intent(TLUserLiveStreaming.this, TLAddressSetting.class));
            }
        });

        btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                checkoutSlideUp.hide();
            }
        });

        btnPlaceOrder.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent paymentIntent = new Intent(TLUserLiveStreaming.this, PaymentBankIn.class);
                paymentIntent.putExtra("name",mLiveStream.getMerchant_bank_holder_name());
                paymentIntent.putExtra("bank", mLiveStream.getMerchant_bank_name());
                paymentIntent.putExtra("acc", mLiveStream.getMerchant_bank_number());
                paymentIntent.putExtra("amount", String.format("%.2f", finalPriceWithDelivery));
                paymentIntent.putExtra("paymentType", 3);
                startActivityForResult(paymentIntent, 1234);
            }
        });
        checkoutSlideUp.show();
    }

    @Override
    public void onUpdateFavourite(String message) {
        if (message.contains("Removed"))
            imgFavourite.setChecked(false);
        else if (message.contains("Inserted"))
            imgFavourite.setChecked(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234 && resultCode == Activity.RESULT_OK) {
            String bankSlip = data.getExtras().getString("bankSlip");
            presenter.performTransaction(liveStreamID,
                    String.valueOf(merchantID),
                    defaultAddress != null ? String.valueOf(defaultAddress.getAddress_id()) : null,
                    TLStorage.getInstance().getCurrentUserId(), "Bank In", cartArrayList,
                    String.valueOf(finalPrice), String.valueOf(deliveryFees),
                    String.valueOf(finalPriceWithDelivery), bankSlip);
        }else if(resultCode == Activity.RESULT_CANCELED){
            presenter.performTransactionWithoutImage(liveStreamID, String.valueOf(merchantID),
                    defaultAddress != null ? String.valueOf(defaultAddress.getAddress_id()) : null,
                    TLStorage.getInstance().getCurrentUserId(), String.valueOf(finalPrice), String.valueOf(deliveryFees),
                    String.valueOf(finalPriceWithDelivery), cartArrayList);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVideoSurface = findViewById(R.id.VideoSurfaceView);
    }

    void getLatestResourceUri(String bambuserID) {
        showLoadingSpinner();
        Request request = new Request.Builder()
                .url(String.format("https://api.bambuser.com/broadcasts/%s", bambuserID))
                .addHeader("Accept", "application/vnd.bambuser.v1+json")
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer " + BuildConfig.BAM_API_KEY)
                .get()
                .build();
        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final Call call, final IOException e) {
                hideLoadingSpinner();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("Broadcast Log", "Http exception: " + e);
                        finish();
                    }
                });
            }

            @Override
            public void onResponse(final Call call, final Response response) throws IOException {
                String body = response.body().string();
                String resourceUri = null;
                String type = null;
                try {
                    JSONObject json = new JSONObject(body);
                    resourceUri = json.optString("resourceUri");
                    type = json.optString("type");
                } catch (Exception ignored) {
                    System.out.println(ignored);
                }
                final String uri = resourceUri;
                final String finalType = type;
                GlobalVariables.getHandlerUI().post(new Runnable() {
                    @Override
                    public void run() {
                        if (finalType == null) {
                            hideLoadingSpinner();
                            initPlayer(null);
                            return;
                        }

                        if (finalType.equalsIgnoreCase("live")) {
                            hideLoadingSpinner();
                            initPlayer(uri);
                        } else if (finalType.equalsIgnoreCase("archived")) {
                            hideLoadingSpinner();
                            initPlayer(null);
                        }
                    }
                });
            }
        });
    }

    void initPlayer(String resourceUri) {
        if (resourceUri == null) {
            Log.d("Broadcast Log", "Broadcast not found !");

            new AndExAlertDialog.Builder(this)
                    .setMessage("Broadcast Not Found !")
                    .setPositiveBtnText("OK")
                    .setCancelableOnTouchOutside(false)
                    .OnPositiveClicked(new AndExAlertDialogListener() {
                        @Override
                        public void OnClick(String input) {
                            finish();
                        }
                    })
                    .setMessageTextColor(R.color.black)
                    .setButtonTextColor(R.color.black)
                    .build();
            //finish();
            return;
        }
        if (mVideoSurface == null) {
            // UI no longer active
            return;
        }

        if (mBroadcastPlayer != null)
            mBroadcastPlayer.close();
        mBroadcastPlayer = new BroadcastPlayer(this, resourceUri, BuildConfig.BAM_APPLICATION_ID, mBroadcastPlayerObserver);
        mBroadcastPlayer.setSurfaceView(mVideoSurface);
        mBroadcastPlayer.load();
        mBroadcastPlayer.setViewerCountObserver(new BroadcastPlayer.ViewerCountObserver() {
            @Override
            public void onCurrentViewersUpdated(long l) {
                txtLiveCount.setText(String.valueOf(l));
            }

            @Override
            public void onTotalViewersUpdated(long l) {
                //txtLiveCount.setText(String.valueOf(l));
            }
        });
    }

    BroadcastPlayer.Observer mBroadcastPlayerObserver = new BroadcastPlayer.Observer() {

        @Override
        public void onStateChange(PlayerState playerState) {
            Log.d("Broadcast Log", "Status: " + playerState);

            if (playerState.equals(PlayerState.COMPLETED)) {
                Toast.makeText(TLUserLiveStreaming.this, "The merchant has ended the live stream", Toast.LENGTH_LONG).show();
                finish();
            }
        }

        @Override
        public void onBroadcastLoaded(boolean live, int width, int height) {
            Point size = getScreenSize();
            float screenAR = size.x / (float) size.y;
            float videoAR = width / (float) height;
            float arDiff = screenAR - videoAR;
            mVideoSurface.setCropToParent(Math.abs(arDiff) < 0.2);
        }
    };

    private void sendMessage() {
        String content = et_message_box.getText().toString();
        SendMessage sendData = new SendMessage(mUsername, content, mRoomName);
        String jsonData = gson.toJson(sendData);
        mSocket.emit("newMessage", jsonData);
        et_message_box.setText("");
//
//        Message message = new Message(mUsername, content, mRoomName, MessageType.CHAT_MINE);
//        addItemToRecyclerView(message);
    }

    private void addItemToRecyclerView(Message message) {
        GlobalVariables.getHandlerUI().post(new Runnable() {
            @Override
            public void run() {
                messageArrayList.add(message);
                liveChatAdapter.notifyItemInserted(messageArrayList.size());
                liveChatRV.scrollToPosition(messageArrayList.size() - 1);
            }
        });
    }

    private void addHeart() {
        GlobalVariables.getHandlerUI().post(new Runnable() {
            @Override
            public void run() {
                heartLayout.addHeart(R.color.colorHeart);
            }
        });
    }

    private Point getScreenSize() {
        if (mDefaultDisplay == null)
            mDefaultDisplay = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try {
            // this is officially supported since SDK 17 and said to work down to SDK 14 through reflection,
            // so it might be everything we need.
            mDefaultDisplay.getClass().getMethod("getRealSize", Point.class).invoke(mDefaultDisplay, size);
        } catch (Exception e) {
            // fallback to approximate size.
            mDefaultDisplay.getSize(size);
        }
        return size;
    }

    public void slideRight(View view) {
        if (isPanelShown()) {
            view.animate()
                    .translationXBy(content_container.getWidth())
                    .setDuration(500)
                    .setInterpolator(new AccelerateDecelerateInterpolator())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            view.setVisibility(View.GONE);
                        }
                    });
        }
    }

    public void slideLeft(View view) {
        if (!isPanelShown()) {
            view.animate()
                    .translationXBy(-content_container.getWidth())
                    .setDuration(500)
                    .setInterpolator(new AccelerateDecelerateInterpolator())
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            view.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }

    private boolean isPanelShown() {
        return content_container.getVisibility() == View.VISIBLE;
    }

    private void addToCart(int count) {
        badgeView.unbind();
        badgeView.setBadgeCount(count)
                .setTextColor(Color.WHITE)
                .setWidthAndHeight(15, 15)
                .setBadgeBackground(getResources().getColor(R.color.colorO))
                .setTextSize(6)
                .setBadgeGravity(Gravity.RIGHT | Gravity.TOP)
                .setShape(BadgeView.SHAPE_CIRCLE)
                .bind(btnCart);
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onFinish(String message) {
        checkoutSlideUp.hide();
        cartArrayList.clear();
        addToCart(cartArrayList.size());
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPartialFinish(String message) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOkHttpClient.dispatcher().cancelAll();
        mVideoSurface = null;

        if (mBroadcastPlayer != null)
            mBroadcastPlayer.close();
        mBroadcastPlayer = null;

        InitialLiveChat data = new InitialLiveChat(mUsername, mRoomName);
        String jsonData = gson.toJson(data);
        mSocket.emit("unsubscribe", jsonData);
        mSocket.disconnect();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void updateProduct() {
        presenter.fetchLiveStreamDetail(liveStreamID, false);
    }

    Emitter.Listener onConnect = args -> {
        InitialLiveChat data = new InitialLiveChat(mUsername, mRoomName);
        String jsonData = gson.toJson(data);
        mSocket.emit("subscribe", jsonData);
    };

    Emitter.Listener onNewUser = args -> {
        String enteredUserName = args[0].toString();
        Message chat = new Message(enteredUserName, "", mRoomName, MessageType.USER_JOIN);
        addItemToRecyclerView(chat);
        Log.d(TAG, "on New User triggered.");
    };

    Emitter.Listener onUpdateLiveChat = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String data = (String) args[0];
            try {
                JSONObject jsonObject = new JSONObject(data);
                Message chat = new Message(jsonObject.getString("userName"), jsonObject.getString("messageContent"), jsonObject.getString("roomName"), MessageType.CHAT_MINE);
                addItemToRecyclerView(chat);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener onUserLeft = args -> {
        String leftUserName = args[0].toString();
        Message chat = new Message(leftUserName, "", "", MessageType.USER_LEAVE);
        addItemToRecyclerView(chat);
    };

    Emitter.Listener onLikeClicked = args -> {
        addHeart();
    };

    Emitter.Listener onProductUpdate = args -> {
        updateProduct();
    };


}
