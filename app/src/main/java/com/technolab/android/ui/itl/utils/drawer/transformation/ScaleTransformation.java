package com.technolab.android.ui.itl.utils.drawer.transformation;

import android.view.View;

import com.technolab.android.ui.itl.utils.drawer.utils.TLDrawerUtil;

public class ScaleTransformation implements RootTransformation {
    private static final float START_SCALE = 1f;

    private final float endScale;

    public ScaleTransformation(float endScale) {
        this.endScale = endScale;
    }

    @Override
    public void transform(float dragProgress, View rootView) {
        float scale = TLDrawerUtil.evaluate(dragProgress, START_SCALE, endScale);
        rootView.setScaleX(scale);
        rootView.setScaleY(scale);
    }
}
