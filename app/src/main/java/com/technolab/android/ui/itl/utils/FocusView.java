package com.technolab.android.ui.itl.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.technolab.android.R;

public class FocusView extends View {

    public static final String INIT_TYPE_PARCELABLE = "IS_SQUARE_PARCELABLE_FocusView";

    private Paint mTransparentPaint;
    private Paint mSemiBlackPaint;
    private Path mPath = new Path();

    int mFocusType = 2; // by default
    boolean mCutImage = false;

    public static final int TYPE_ROUND = 2;
    public static final int TYPE_SQUARE = 1;
    public static final int TYPE_RECTANGLE = 3;

    public FocusView(Context context) {
        super(context);
        initPaints();
    }

    public FocusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaints();

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FocusView);
        updateAttrs(typedArray);
    }

    public FocusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaints();

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FocusView);
        updateAttrs(typedArray);
    }

    private void updateAttrs(TypedArray typedArray) {
        mFocusType = typedArray.getInt(R.styleable.FocusView_view_type, 2);
        mCutImage = typedArray.getBoolean(R.styleable.FocusView_cut_image, false);
    }

    private void initPaints() {
        mTransparentPaint = new Paint();
        mTransparentPaint.setColor(Color.TRANSPARENT);
        mTransparentPaint.setStrokeWidth(10);

        mSemiBlackPaint = new Paint();
        mSemiBlackPaint.setColor(Color.TRANSPARENT);
        mSemiBlackPaint.setStrokeWidth(10);
    }

    public void setFocusType(int focusType) {
        this.mFocusType = focusType;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPath.reset();
        int radius = canvas.getWidth() / 2;

        if (mFocusType == 2) { // circle
            mPath.addCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, radius, Path.Direction.CW);
            mPath.setFillType(Path.FillType.INVERSE_EVEN_ODD);
            canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, radius, mTransparentPaint);
        } else if (mFocusType == 1) {

            //radius=canvas.getWidth()/2;
            radius = 20;

            int canvasW = getWidth();
            int canvasH = getHeight();
            Point centerOfCanvas = new Point(canvasW / 2, canvasH / 2);
            int rectW = getWidth();
            int rectH = 800;
            int left = centerOfCanvas.x - (rectW / 2);
            int top = centerOfCanvas.y - (rectH / 2);
            int right = centerOfCanvas.x + (rectW / 2);
            int bottom = centerOfCanvas.y + (rectH / 2);

            RectF rectF = new RectF();
            rectF.set(left+30,
                    top,
                    right-30,
                    bottom);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPath.addRoundRect(rectF,radius,radius, Path.Direction.CW);
            }
            mPath.setFillType(Path.FillType.INVERSE_EVEN_ODD);

            canvas.drawRoundRect(rectF,radius,radius,mTransparentPaint);

            mTransparentPaint.setStyle(Paint.Style.STROKE);
            mTransparentPaint.setColor(Color.TRANSPARENT);
            canvas.drawRoundRect(rectF,radius,radius,mTransparentPaint);
        }else if (mFocusType == 3){
            radius = 20;

            int canvasW = getWidth();
            int canvasH = getHeight();
            Point centerOfCanvas = new Point(canvasW / 2, canvasH / 2);
            int rectW = getWidth();
            int rectH = 1200;
            int left = centerOfCanvas.x - (rectW / 2);
            int top = centerOfCanvas.y - (rectH / 2);
            int right = centerOfCanvas.x + (rectW / 2);
            int bottom = centerOfCanvas.y + (rectH / 2);

            RectF rectF = new RectF();
            rectF.set(left+50,
                    top,
                    right-50,
                    bottom);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mPath.addRoundRect(rectF,radius,radius, Path.Direction.CW);
            }
            mPath.setFillType(Path.FillType.INVERSE_EVEN_ODD);

            canvas.drawRoundRect(rectF,radius,radius,mTransparentPaint);

            mTransparentPaint.setStyle(Paint.Style.STROKE);
            mTransparentPaint.setColor(Color.TRANSPARENT);
            canvas.drawRoundRect(rectF,radius,radius,mTransparentPaint);
        }

        canvas.drawPath(mPath, mSemiBlackPaint);
        canvas.clipPath(mPath);
        canvas.drawColor(Color.parseColor("#A6000000"));
    }
}
