package com.technolab.android.ui.entity;

public class ReferralCount {
    public int total_share;
    public int total_sign_up;
    public int today_sign_up;
    public boolean is_deb_redeemable;
    public int total_energy;
    public boolean is_ts_redeemable;
    public boolean is_osu_redeemable;
    public boolean is_tsu_redeemable;
    public int ts_redeemable_energy;
    public int osu_redeemable_energy;
    public int tsu_redeemable_energy;
    public int ts_redeemable_count;
    public int osu_redeemable_count;
    public int tsu_redeemable_count;

    public int getTs_redeemable_count() {
        return ts_redeemable_count;
    }

    public void setTs_redeemable_count(int ts_redeemable_count) {
        this.ts_redeemable_count = ts_redeemable_count;
    }

    public int getOsu_redeemable_count() {
        return osu_redeemable_count;
    }

    public void setOsu_redeemable_count(int osu_redeemable_count) {
        this.osu_redeemable_count = osu_redeemable_count;
    }

    public int getTsu_redeemable_count() {
        return tsu_redeemable_count;
    }

    public void setTsu_redeemable_count(int tsu_redeemable_count) {
        this.tsu_redeemable_count = tsu_redeemable_count;
    }

    public int getTotal_share() {
        return total_share;
    }

    public void setTotal_share(int total_share) {
        this.total_share = total_share;
    }

    public int getTotal_sign_up() {
        return total_sign_up;
    }

    public void setTotal_sign_up(int total_sign_up) {
        this.total_sign_up = total_sign_up;
    }

    public int getToday_sign_up() {
        return today_sign_up;
    }

    public void setToday_sign_up(int today_sign_up) {
        this.today_sign_up = today_sign_up;
    }

    public boolean isIs_deb_redeemable() {
        return is_deb_redeemable;
    }

    public void setIs_deb_redeemable(boolean is_deb_redeemable) {
        this.is_deb_redeemable = is_deb_redeemable;
    }

    public int getTotal_energy() {
        return total_energy;
    }

    public void setTotal_energy(int total_energy) {
        this.total_energy = total_energy;
    }

    public boolean isIs_ts_redeemable() {
        return is_ts_redeemable;
    }

    public void setIs_ts_redeemable(boolean is_ts_redeemable) {
        this.is_ts_redeemable = is_ts_redeemable;
    }

    public boolean isIs_osu_redeemable() {
        return is_osu_redeemable;
    }

    public void setIs_osu_redeemable(boolean is_osu_redeemable) {
        this.is_osu_redeemable = is_osu_redeemable;
    }

    public boolean isIs_tsu_redeemable() {
        return is_tsu_redeemable;
    }

    public void setIs_tsu_redeemable(boolean is_tsu_redeemable) {
        this.is_tsu_redeemable = is_tsu_redeemable;
    }

    public int getTs_redeemable_energy() {
        return ts_redeemable_energy;
    }

    public void setTs_redeemable_energy(int ts_redeemable_energy) {
        this.ts_redeemable_energy = ts_redeemable_energy;
    }

    public int getOsu_redeemable_energy() {
        return osu_redeemable_energy;
    }

    public void setOsu_redeemable_energy(int osu_redeemable_energy) {
        this.osu_redeemable_energy = osu_redeemable_energy;
    }

    public int getTsu_redeemable_energy() {
        return tsu_redeemable_energy;
    }

    public void setTsu_redeemable_energy(int tsu_redeemable_energy) {
        this.tsu_redeemable_energy = tsu_redeemable_energy;
    }
}
