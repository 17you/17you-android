package com.technolab.android.ui.itl.utils.drawer.utils;

import android.content.Context;

import androidx.drawerlayout.widget.DrawerLayout;

import com.technolab.android.ui.itl.utils.drawer.TLDrawerLayout;

public class ActionBarToggleAdapter extends DrawerLayout {

    private TLDrawerLayout drawerLayout;

    public ActionBarToggleAdapter(Context context) {
        super(context);
    }

    @Override
    public void openDrawer(int gravity) {
        drawerLayout.openMenu();
    }

    @Override
    public void closeDrawer(int gravity) {
        drawerLayout.closeMenu();
    }

    @Override
    public boolean isDrawerVisible(int drawerGravity) {
        return !drawerLayout.isMenuClosed();
    }

    @Override
    public int getDrawerLockMode(int edgeGravity) {
        if (drawerLayout.isMenuLocked() && drawerLayout.isMenuClosed()) {
            return DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        } else if (drawerLayout.isMenuLocked() && !drawerLayout.isMenuClosed()) {
            return DrawerLayout.LOCK_MODE_LOCKED_OPEN;
        } else {
            return DrawerLayout.LOCK_MODE_UNLOCKED;
        }
    }

    public void setAdapter(TLDrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
    }
}
