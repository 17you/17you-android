package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.LiveMarket;
import com.technolab.android.ui.entity.OverallReferralResults;
import com.technolab.android.ui.entity.ReferralEmailResult;
import com.technolab.android.ui.entity.State;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLMyReferral extends AbstractActivity implements TLMainMvpView.Referral{

    private StateAdapter stateAdapter;
    private RecyclerView recyclerView;
    private TextView txtTotalShare;
    private TextView txtTotalSignUp;
    private TextView txtTotalPending;

    private TextView txtTotalShareHeader;
    private TextView txtTotalSignUpHeader;
    private TextView txtTotalPendingHeader;
    private View signedUpView;

    public TLMyReferral(){
        TAG = "TLMyReferral";
        layoutID = R.layout.activity_my_referral;
        statusBarColorID = R.color.colorD;
    }

    @Inject
    TLMainPresenter.TLUserReferralPresenter userReferralPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        setupRefresh(this);
        recyclerView = findViewById(R.id.recycler_view);
        signedUpView = findViewById(R.id.view2);

        txtTotalShare = findViewById(R.id.tv_total_share_value);
        txtTotalSignUp = findViewById(R.id.tv_total_sign_up_value);
        txtTotalPending = findViewById(R.id.tv_pending_value);

        txtTotalShareHeader = findViewById(R.id.tv_total_share_title);
        txtTotalSignUpHeader = findViewById(R.id.tv_total_sign_up_title);
        txtTotalPendingHeader = findViewById(R.id.tv_pending_title);

        findViewById(R.id.toggle_total).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                txtTotalShareHeader.setText("Total Share");
                txtTotalSignUpHeader.setText("Total Sign Up");
                txtTotalPendingHeader.setText("Pending");
                userReferralPresenter.retrieveTotalReferral();
            }
        });

        findViewById(R.id.toggle_daily).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                txtTotalShareHeader.setText("Daily Share");
                txtTotalSignUpHeader.setText("Daily Sign Up");
                txtTotalPendingHeader.setText("Daily Pending");
                userReferralPresenter.retrieveTodayReferral();
            }
        });

        findViewById(R.id.toggle_week).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                txtTotalShareHeader.setText("Weekly Share");
                txtTotalSignUpHeader.setText("Weekly Sign Up");
                txtTotalPendingHeader.setText("Weekly Pending");
                userReferralPresenter.retrieveWeekReferral();
            }
        });

        findViewById(R.id.toggle_month).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                txtTotalShareHeader.setText("Monthly Share");
                txtTotalSignUpHeader.setText("Monthly Sign Up");
                txtTotalPendingHeader.setText("Monthly Pending");
                userReferralPresenter.retrieveMonthReferral();
            }
        });

        signedUpView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMyReferral.this, TLMyReferralDetail.class);
                if(((ToggleButton)findViewById(R.id.toggle_total)).isChecked())
                    intent.putExtra("value", "total");
                else if(((ToggleButton)findViewById(R.id.toggle_daily)).isChecked())
                    intent.putExtra("value","daily");
                else if(((ToggleButton)findViewById(R.id.toggle_week)).isChecked())
                    intent.putExtra("value","weekly");
                else if(((ToggleButton)findViewById(R.id.toggle_month)).isChecked())
                    intent.putExtra("value", "monthly");
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLMyReferral.this);
        userReferralPresenter.onAttach(this);
        userReferralPresenter.retrieveTotalReferral();
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("My Referral");
        txtSubHeader.setVisibility(View.GONE);

        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> userReferralPresenter.retrieveTotalReferral());
    }

    @Override
    public void onResponseReferral(OverallReferralResults overallReferralResults) {
        refreshLayout.setRefreshing(false);
        txtTotalShare.setText(String.valueOf(overallReferralResults.getTotal_share()));
        txtTotalSignUp.setText(String.valueOf(overallReferralResults.getTotal_sign_up()));
        txtTotalPending.setText(String.valueOf(overallReferralResults.getTotal_share() - overallReferralResults.getTotal_sign_up()));
        stateAdapter = new StateAdapter(this,overallReferralResults.getTotal_sign_up_states());
        recyclerView.setAdapter(stateAdapter);
    }

    @Override
    public void onResponseEmail(ArrayList<ReferralEmailResult> referralEmailResults) {

    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
    }
}
