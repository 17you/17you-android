package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Variation implements Serializable {
    @SerializedName("product_variation_id")
    private int product_variation_id;
    @SerializedName("product_variation_color")
    private String product_variation_color;
    @SerializedName("product_variation_quantity")
    private int product_variation_quantity;
    @SerializedName("product_variation_size")
    private String product_variation_size;
    @SerializedName("product_variation_price")
    private Double product_variation_price;
    @SerializedName("product_variation_status")
    private boolean product_variation_status;

    public int getProduct_variation_id() {
        return product_variation_id;
    }

    public void setProduct_variation_id(int product_variation_id) {
        this.product_variation_id = product_variation_id;
    }

    public String getProduct_variation_color() {
        return product_variation_color;
    }

    public void setProduct_variation_color(String product_variation_color) {
        this.product_variation_color = product_variation_color;
    }

    public int getProduct_variation_quantity() {
        return product_variation_quantity;
    }

    public void setProduct_variation_quantity(int product_variation_quantity) {
        this.product_variation_quantity = product_variation_quantity;
    }

    public String getProduct_variation_size() {
        return product_variation_size;
    }

    public void setProduct_variation_size(String product_variation_size) {
        this.product_variation_size = product_variation_size;
    }

    public Double getProduct_variation_price() {
        return product_variation_price;
    }

    public void setProduct_variation_price(Double product_variation_price) {
        this.product_variation_price = product_variation_price;
    }

    public boolean getProduct_variation_status() {
        return product_variation_status;
    }

    public void setProduct_variation_status(boolean product_variation_status) {
        this.product_variation_status = product_variation_status;
    }
}
