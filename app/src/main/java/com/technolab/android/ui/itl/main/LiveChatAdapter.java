package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Message;
import com.technolab.android.utility.MessageType;

import java.util.ArrayList;

public class LiveChatAdapter extends RecyclerView.Adapter<LiveChatAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Message> liveMessages;

    public LiveChatAdapter(Context context, ArrayList<Message>liveMessages){
        this.context = context;
        this.liveMessages = liveMessages;
    }

    @NonNull
    @Override
    public LiveChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == MessageType.CHAT_MINE.getValue()) {
           view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_live_chat, parent, false);
        } else if (viewType == MessageType.USER_JOIN.getValue()) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_live_chat_entered, parent, false);
        } else if (viewType == MessageType.USER_LEAVE.getValue()) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_live_chat_entered, parent, false);
        }
        return new LiveChatAdapter.ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        return liveMessages.get(position).viewType.getValue();
    }

    @Override
    public void onBindViewHolder(@NonNull LiveChatAdapter.ViewHolder holder, int position) {
        Message liveMessage = liveMessages.get(position);
        String username = liveMessage.getUserName();
        String content = liveMessage.getMessageContent();
        MessageType viewType = liveMessage.getViewType();
        switch (viewType) {
            case CHAT_MINE:
                holder.userName.setText(username + " : ");
                holder.message.setText(content);
                break;
            case USER_JOIN:
                holder.text.setText(username + " joined the live room");
                break;
            case USER_LEAVE:
                holder.text.setText(username + " left the live room");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return liveMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView userName;
        TextView message;
        TextView text;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.username);
            message  = itemView.findViewById(R.id.message);
            text = itemView.findViewById(R.id.text);

        }
    }
}
