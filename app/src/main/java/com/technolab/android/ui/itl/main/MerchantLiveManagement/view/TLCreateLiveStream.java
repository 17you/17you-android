package com.technolab.android.ui.itl.main.MerchantLiveManagement.view;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter.TLMerchantLiveManagementMvpView;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter.TLMerchantLiveManagementPresenter;
import com.technolab.android.ui.itl.main.ViewProfileActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;
import com.technolab.android.utility.EnumLiveStreamStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

public class TLCreateLiveStream extends AbstractActivity implements TLMerchantLiveManagementMvpView.LiveStreamMVP {

    private LinearLayout ivAddImage;
    private ImageView imageView;

    private EditText tvSelectDate;
    private EditText etTitle;
    private EditText etDesc;
    private EditText etSelect;
    private EditText etShippingFee;

    private TextView tvStartTime;
    private TextView tvEndTime;

    private Button btnPublish;
    private Button btnSaved;
    private Button btnSave;
    private Group bottomButtonContainer;

    private final Calendar myCalendar = Calendar.getInstance();
    private final Calendar startCalendar = Calendar.getInstance();
    private final Calendar endCalendar = Calendar.getInstance();
    private ArrayList<Product> selectedProductList;
    private String imagePath;
    private boolean isEditMode;

    @Inject
    TLMerchantLiveManagementPresenter.TLMerchantLiveStreamPresenter merchantLiveStreamPresenter;

    public TLCreateLiveStream(){
        TAG = "TLCreateLiveStream";
        layoutID = R.layout.activity_merchant_create_live;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvSelectDate = findViewById(R.id.tv_select_the_date);
        ivAddImage = findViewById(R.id.iv_live_stream);
        imageView = findViewById(R.id.iv_imageView);
        tvStartTime = findViewById(R.id.et_start_time);
        tvEndTime = findViewById(R.id.et_end_time);
        etSelect = findViewById(R.id.et_you_can_select);
        etTitle = findViewById(R.id.et_title);
        etDesc = findViewById(R.id.et_desc);
        etShippingFee = findViewById(R.id.et_shipping_fee);
        btnPublish = findViewById(R.id.btn_publish);
        btnSaved = findViewById(R.id.btn_saved);
        btnSave = findViewById(R.id.btn_save);
        bottomButtonContainer = findViewById(R.id.bottom_button_container);
        setupToolbar(this);

        getAppComponent().inject(TLCreateLiveStream.this);
        merchantLiveStreamPresenter.onAttach(this);

        System.out.println("Show me all total : "+ TLStorage.getInstance().getTotalLiveTime());
        ivAddImage.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLCreateLiveStream.this, ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, true);
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, false);
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);
                intent.putExtra(ImageSelectActivity.FLAG_CROP, true);
                startActivityForResult(intent, 1213);
            }
        });

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        tvSelectDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(TLCreateLiveStream.this, R.style.my_dialog_theme, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                dialog.show();
            }
        });

        tvStartTime.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {

                if(tvSelectDate.getText().toString().isEmpty())
                {
                    Toast.makeText(TLCreateLiveStream.this, "Please select a date", Toast.LENGTH_SHORT).show();;
                    return;
                }

                TimePickerDialog dialog = new TimePickerDialog(TLCreateLiveStream.this, R.style.my_dialog_theme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        startCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        startCalendar.set(Calendar.MINUTE, minute);
                        startCalendar.set(Calendar.SECOND, 0);

                        if(TLStorage.getInstance().getTotalLiveTime() < (endCalendar.getTimeInMillis() - startCalendar.getTimeInMillis())){
                            Toast.makeText(TLCreateLiveStream.this, "You have insufficient live streaming time left within your account. " +
                                    "Please update your live stream duration to the range of your remaining amount of time.", Toast.LENGTH_LONG).show();
                            return;
                        }

                        String myFormat = "hh:mm:ss aa";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat,Locale.US);
                        tvStartTime.setText(sdf.format(startCalendar.getTime()));
                    }
                }, startCalendar.get(Calendar.HOUR_OF_DAY), startCalendar.get(Calendar.MINUTE), false);
                dialog.show();
            }
        });

        tvEndTime.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(tvSelectDate.getText().toString().isEmpty())
                {
                    Toast.makeText(TLCreateLiveStream.this, "Please select a date", Toast.LENGTH_SHORT).show();;
                    return;
                }else if(tvStartTime.getText().toString().isEmpty()){
                    Toast.makeText(TLCreateLiveStream.this, "Please select start time", Toast.LENGTH_SHORT).show();;
                    return;
                }

                TimePickerDialog dialog = new TimePickerDialog(TLCreateLiveStream.this, R.style.my_dialog_theme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        endCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        endCalendar.set(Calendar.MINUTE, minute);
                        endCalendar.set(Calendar.SECOND, 0);

                        if(startCalendar.getTimeInMillis() > endCalendar.getTimeInMillis()){
                            Toast.makeText(TLCreateLiveStream.this, "Time out of range. Please select a valid time", Toast.LENGTH_LONG).show();
                            tvStartTime.setText("");
                            tvEndTime.setText("");
                            return;
                        }else if(TLStorage.getInstance().getTotalLiveTime() < (endCalendar.getTimeInMillis() - startCalendar.getTimeInMillis())){
                            Toast.makeText(TLCreateLiveStream.this, "You have insufficient live streaming time left within your account. " +
                                    "Please update your live stream duration to the range of your remaining amount of time.", Toast.LENGTH_LONG).show();
                            return;
                        }

                        view.setIs24HourView(true);
                        String myFormat = "hh:mm:ss aa";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat,Locale.US);
                        tvEndTime.setText(sdf.format(endCalendar.getTime()));
                    }
                }, endCalendar.get(Calendar.HOUR_OF_DAY), endCalendar.get(Calendar.MINUTE), false);
                dialog.show();
            }
        });

        etSelect.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivityForResult(new Intent(TLCreateLiveStream.this, TLLiveProductSetupActivity.class), 1001);
            }
        });

        isEditMode = getIntent().getBooleanExtra("isEditMode", false);
        editMode(isEditMode);

    }

    private void editMode(boolean isEditMode){
        if (isEditMode) {
            bottomButtonContainer.setVisibility(View.GONE);
            btnSave.setVisibility(View.VISIBLE);
            txtHeader.setText("Edit Live Stream");
            merchantLiveStreamPresenter.fetchSingleLiveStream(getIntent().getStringExtra("liveStreamID"));
        }
        else {
            bottomButtonContainer.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.GONE);
            txtHeader.setText("Live Stream Setup");
            initSetup();
        }

    }

    private void setupData(LiveStream liveStream){
        imageView.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(liveStream.getLive_stream_image())
                .thumbnail(0.1f)
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into(imageView);
        etTitle.setText(liveStream.getLive_stream_title());
        etDesc.setText(liveStream.getLive_stream_description());
        ArrayList<String> selectedList = new ArrayList<>();

        for(Product product : liveStream.getProducts()){
            selectedList.add(product.getProduct_name());
        }
        String selectedProduct = TextUtils.join(",", selectedList);
        etSelect.setText(selectedProduct);

        try {
            String date = liveStream.getLive_stream_date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("MM/dd/yyyy");
            tvSelectDate.setText(format.format(newDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try{
        String startTime = liveStream.getLive_stream_start_time();
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
        Date newDate = format.parse(startTime);
        format = new SimpleDateFormat("hh:mm:ss aa");
        tvStartTime.setText(format.format(newDate));
        }catch (ParseException e){
            e.printStackTrace();
        }

        try{
            String endTime = liveStream.getLive_stream_end_time();
            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
            Date newDate = format.parse(endTime);
            format = new SimpleDateFormat("hh:mm:ss aa");
            tvEndTime.setText(format.format(newDate));
        }catch (ParseException e){
            e.printStackTrace();
        }

        etShippingFee.setText(liveStream.getShipping_fee());
        btnSave.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String liveStreamID = String.valueOf(liveStream.getLive_stream_id());
                String title = !etTitle.getText().toString().equals(liveStream.getLive_stream_title()) ? etTitle.getText().toString() : liveStream.getLive_stream_title();
                String desc = !etDesc.getText().toString().equals(liveStream.getLive_stream_description()) ? etDesc.getText().toString() : liveStream.getLive_stream_description();
                String date = !tvSelectDate.getText().toString().equals(liveStream.getLive_stream_date()) ? tvSelectDate.getText().toString() : liveStream.getLive_stream_date();
                String startTime = !tvStartTime.getText().toString().equals(liveStream.getLive_stream_start_time()) ? tvStartTime.getText().toString() : liveStream.getLive_stream_start_time();
                String endTime = !tvEndTime.getText().toString().equals(liveStream.getLive_stream_end_time()) ? tvEndTime.getText().toString() : liveStream.getLive_stream_end_time();
                ArrayList<Product> selectedProduct = selectedProductList != null ? selectedProductList : liveStream.getProducts();
                String shippingFee = !etShippingFee.getText().toString().equals(liveStream.getShipping_fee()) ? etShippingFee.getText().toString() : liveStream.getShipping_fee();

                String imageFile = imagePath;
                merchantLiveStreamPresenter.updateLiveStream(liveStreamID,title,desc,date,startTime,endTime,
                        selectedProduct, liveStream.getLive_stream_status(), shippingFee, imageFile);
            }
        });
    }

    private void initSetup(){
        btnPublish.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(etTitle.getText().toString().isEmpty() || etDesc.getText().toString().isEmpty() || tvSelectDate.getText().toString().isEmpty() ||
                        tvStartTime.getText().toString().isEmpty() || tvEndTime.getText().toString().isEmpty() || selectedProductList == null ||
                        etShippingFee.getText().toString().isEmpty() || imagePath == null){
                    Toast.makeText(TLCreateLiveStream.this, "Please make sure that all fields are complete.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(startCalendar.getTimeInMillis() > endCalendar.getTimeInMillis()){
                    Toast.makeText(TLCreateLiveStream.this, "Time out of range. Please select a valid time", Toast.LENGTH_LONG).show();
                    return;
                }else if(TLStorage.getInstance().getTotalLiveTime() < (endCalendar.getTimeInMillis() - startCalendar.getTimeInMillis())){
                    Toast.makeText(TLCreateLiveStream.this, "You have insufficient live streaming time left within your account. " +
                            "Please update your live stream duration to the range of your remaining amount of time.", Toast.LENGTH_LONG).show();
                    return;
                }

                merchantLiveStreamPresenter.uploadLiveStream(etTitle.getText().toString(),etDesc.getText().toString(),
                        tvSelectDate.getText().toString(),tvStartTime.getText().toString(),
                        tvEndTime.getText().toString(), selectedProductList, EnumLiveStreamStatus.PUBLISHED.getValue(), etShippingFee.getText().toString(), imagePath);
            }
        });

        btnSaved.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(etTitle.getText().toString().isEmpty() || etDesc.getText().toString().isEmpty() || tvSelectDate.getText().toString().isEmpty() ||
                        tvStartTime.getText().toString().isEmpty() || tvEndTime.getText().toString().isEmpty() || selectedProductList == null ||
                        etShippingFee.getText().toString().isEmpty() || imagePath == null){
                    Toast.makeText(TLCreateLiveStream.this, "Please make sure that all fields are complete.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(startCalendar.getTimeInMillis() > endCalendar.getTimeInMillis()){
                    Toast.makeText(TLCreateLiveStream.this, "Time out of range. Please select a valid time", Toast.LENGTH_LONG).show();
                    return;
                }else if(TLStorage.getInstance().getTotalLiveTime() < (endCalendar.getTimeInMillis() - startCalendar.getTimeInMillis())){
                    Toast.makeText(TLCreateLiveStream.this, "You have insufficient live streaming time left within your account. " +
                            "Please update your live stream duration to the range of your remaining amount of time.", Toast.LENGTH_LONG).show();
                    return;
                }

                merchantLiveStreamPresenter.uploadLiveStream(etTitle.getText().toString(),etDesc.getText().toString(),
                        tvSelectDate.getText().toString(),tvStartTime.getText().toString(),
                        tvEndTime.getText().toString(), selectedProductList, EnumLiveStreamStatus.SAVED.getValue(), etShippingFee.getText().toString(), imagePath);
            }
        });
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Live Stream Setup");
        txtSubHeader.setVisibility(View.GONE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setVisibility(View.INVISIBLE);

    }

    private void updateLabel() {
        String myFormat = "MM/dd/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tvSelectDate.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            imagePath = filePath;
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            imageView.setImageBitmap(selectedImage);
            imageView.setVisibility(View.VISIBLE);
        }else if(requestCode == 1001 && resultCode == Activity.RESULT_OK){
            selectedProductList = (ArrayList<Product>) data.getSerializableExtra("productList");
            ArrayList<String> selectedList = new ArrayList<>();

            for(Product product : selectedProductList){
                selectedList.add(product.getProduct_name());
            }
            String selectedProduct = TextUtils.join(",", selectedList);
            etSelect.setText(selectedProduct);
        }
    }

    @Override
    public void onRetrieveLiveStreams(ArrayList<LiveStream> liveStreamArrayList) {
        //Do nothing
    }

    @Override
    public void onRetrieveProductList(ArrayList<Product> productArrayList) {

    }

    @Override
    public void onFetchData(LiveStream liveStream) {
        setupData(liveStream);
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(String message) {
        finish();
    }

    @Override
    public void onDelete(String message) {
    }

    @Override
    public void onPublish(String message) {

    }
}
