package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.nex3z.togglebuttongroup.SingleSelectToggleGroup;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.LiveStreamCategory;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLLiveManagement;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLMerchantLiveStreaming;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.tablayout.TLTabLayout;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class TLLiveMarketCategory extends AbstractActivity implements TLMainMvpView.LiveMarketCategory {

    private ViewPager viewPager;
    private TLTabLayout tabLayout;
    private String marketID;
    private ArrayList<Industry> industryArrayList;
    private SingleSelectToggleGroup toggleContainer;
    private HorizontalScrollView horizontalScrollView;
    private int selectedTab;

    @Inject
    TLMainPresenter.TLLiveMarketCategoryPresenter presenter;

    private static final String SHOP_CATEGORY[] = {"Live Now", "Today", "Upcoming", "Favourite"};

    public TLLiveMarketCategory(){
        TAG = "17Live&Shop";
        layoutID = R.layout.activity_live_shop_category;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewPager = findViewById(R.id.view_pager);
        toggleContainer = findViewById(R.id.toggle_container);
        horizontalScrollView = findViewById(R.id.horizontal_view);
        marketID = getIntent().getStringExtra("marketID");
        industryArrayList = (ArrayList<Industry>) getIntent().getSerializableExtra("industry");
        getAppComponent().inject(TLLiveMarketCategory.this);
        presenter.onAttach(TLLiveMarketCategory.this);

        for(int i = 0; i < industryArrayList.size(); i++){
            ToggleButton toggleButton = new ToggleButton(this);
            toggleButton.setText(industryArrayList.get(i).getIndustry_category_name());
            toggleButton.setTextOff(industryArrayList.get(i).getIndustry_category_name());
            toggleButton.setTextOn(industryArrayList.get(i).getIndustry_category_name());
            toggleButton.setId(industryArrayList.get(i).getIndustry_category_id());
            toggleButton.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
            toggleButton.setAllCaps(false);
            toggleButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.toggle_state_selector));
            toggleButton.setPadding(25,0,25,0);
            toggleButton.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, getResources().getDimensionPixelSize(R.dimen._25sdp)));
            if(marketID.equals(String.valueOf(industryArrayList.get(i).getIndustry_category_id()))) {
                toggleButton.setChecked(true);
                toggleButton.setTextColor(getResources().getColor(R.color.white));
                marketID = String.valueOf(toggleButton.getId());
                presenter.fetchAllLiveStream();
            }
            toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked) {
                        marketID = buttonView.getTag().toString();
                        presenter.fetchAllLiveStream();
                        buttonView.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            });
            toggleContainer.addView(toggleButton);
        }

        toggleContainer.setOnCheckedChangeListener(new SingleSelectToggleGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SingleSelectToggleGroup group, int checkedId) {
                ToggleButton prevButton = group.findViewById(Integer.parseInt(marketID));
                prevButton.setTextColor(getResources().getColor(R.color.black));

                ToggleButton selectedButton = group.findViewById(checkedId);
                selectedButton.setTextColor(getResources().getColor(R.color.white));

                marketID = String.valueOf(checkedId);
                presenter.fetchAllLiveStream();
            }
        });

        setupToolbar(this);
        setupRefresh(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Live Stream");
        txtSubHeader.setVisibility(View.GONE);

        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> {
            selectedTab = tabLayout.getSelectedTabPosition();
            presenter.fetchAllLiveStream();
        });
    }

    @Override
    public void onFetchLiveMarket(ArrayList<LiveStreamCategory> liveStreamCategoryArrayList) {
        refreshLayout.setRefreshing(false);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(), liveStreamCategoryArrayList));

        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.selectTab(tabLayout.getTabAt(selectedTab));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                txtHeader.setText(tab.getText());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {

        private ArrayList<LiveStreamCategory> liveStreamCategories;

        public PagerAdapter(FragmentManager fm, ArrayList<LiveStreamCategory> liveStreamCategories) {
            super(fm);
            this.liveStreamCategories = liveStreamCategories;
        }


        @Override
        public Fragment getItem(int i) {
            for(int j = 0; j < liveStreamCategories.size(); j++){
                if(String.valueOf(liveStreamCategories.get(j).industry_category_id).equalsIgnoreCase(marketID)){
                    LiveStreamCategory liveStreamCategory = liveStreamCategories.get(j);
                    switch (i) {
                        case 0:
                            return new PageFragment(liveStreamCategory.getLive_now(), true);
                        case 1:
                            return new PageFragment(liveStreamCategory.getToday(), false);
                        case 2:
                            return new PageFragment(liveStreamCategory.getUpcoming(), false);
                        case 3:
                            return new PageFragment(liveStreamCategory.getFavourite(), false);
                    }
                }
            }
            return new PageFragment(null, false);
        }

        @Override
        public int getCount() {
            return SHOP_CATEGORY.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return SHOP_CATEGORY[position];
        }
    }

    public static class PageFragment extends Fragment implements LiveStreamAdapter.ItemClickListener{

        private RecyclerView recyclerView;
        private LiveStreamAdapter liveStreamAdapter;
        private List<LiveStream> liveStreamList;
        private boolean isClickEnabled;
        private Group noDataGroup;

        public PageFragment(List<LiveStream> liveStreamList, boolean isClickEnabled) {
            this.liveStreamList = liveStreamList;
            this.isClickEnabled = isClickEnabled;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_page, container, false);
            recyclerView = view.findViewById(R.id.recycler_view);
            noDataGroup = view.findViewById(R.id.no_product_container);
            if(liveStreamList.size() > 0){
                noDataGroup.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }else{
                noDataGroup.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            liveStreamAdapter = new LiveStreamAdapter(getActivity(), liveStreamList, isClickEnabled);
            recyclerView.setAdapter(liveStreamAdapter);
            if(isClickEnabled)
                liveStreamAdapter.setOnItemClickListener(this);
            return view;
        }

        @Override
        public void onItemClicked(String id) {
            if(isClickEnabled) {
                Intent intent = new Intent(getActivity(), TLUserLiveStreaming.class);
                intent.putExtra("liveStreamID", id);
                startActivity(intent);
            }
        }
    }

}
