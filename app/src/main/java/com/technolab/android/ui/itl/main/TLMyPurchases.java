package com.technolab.android.ui.itl.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Transaction;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.TLMerchantPayment;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLMyPurchases extends AbstractActivity implements TLMainMvpView.UserTransaction, PurchasesAdapter.ItemClickListener{

    private int viewType;
    private RecyclerView recyclerView;
    private PurchasesAdapter purchasesAdapter;
    private LinearLayout containerNoData;
    private Transaction mTransaction;

    @Inject
    TLMainPresenter.TLUserTransaction userTransactionPresenter;

    public TLMyPurchases(){
        TAG = "TLMyPurchases";
        layoutID = R.layout.activity_my_purchases;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewType = getIntent().getIntExtra("viewType", 0);
        recyclerView = findViewById(R.id.recycler_view);
        containerNoData = findViewById(R.id.container_no_data);
        setupToolbar(this);
        setupRefresh(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLMyPurchases.this);
        userTransactionPresenter.onAttach(this);
        userTransactionPresenter.fetchTransactions(viewType);
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> userTransactionPresenter.fetchTransactions(viewType));
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);

        switch (viewType) {
            case 1:
                txtHeader.setText("Processing");
                break;
            case 2:
                txtHeader.setText("Shipped");
                break;
            case 3:
                txtHeader.setText("Delivered");
                break;
            case 4:
                txtHeader.setText("All");
                break;
        }

        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }

    @Override
    public void onReceiveTransaction(ArrayList<Transaction> transactions) {
        refreshLayout.setRefreshing(false);
        if(transactions.size() == 0){
            containerNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }

        containerNoData.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        purchasesAdapter = new PurchasesAdapter(this, transactions, viewType);
        recyclerView.setAdapter(purchasesAdapter);
        purchasesAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onPayment(String message) {
        mTransaction = null;
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        userTransactionPresenter.fetchTransactions(viewType);
    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClicked(Transaction transaction) {
        mTransaction = transaction;
        Intent paymentIntent = new Intent(TLMyPurchases.this, PaymentBankIn.class);
        paymentIntent.putExtra("name",transaction.getMerchant_bank_holder_name());
        paymentIntent.putExtra("bank", transaction.getMerchant_bank_name());
        paymentIntent.putExtra("acc", transaction.getMerchant_bank_number());
        paymentIntent.putExtra("amount", transaction.getFinal_price());
        paymentIntent.putExtra("paymentType", 3);
        startActivityForResult(paymentIntent, 1234);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1234 && resultCode == Activity.RESULT_OK) {
            String bankSlip = data.getExtras().getString("bankSlip");
            userTransactionPresenter.updateTransaction(String.valueOf(mTransaction.getTransaction_id()),
                    TLStorage.getInstance().getCurrentUserId(),mTransaction.getMerchant_id(),"Bank In", bankSlip);
        }
    }
}
