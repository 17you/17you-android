package com.technolab.android.ui.itl.main;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import javax.inject.Inject;

public class TLReward extends AbstractActivity implements TLMainMvpView.Reward {

    private TextView txtWelcome;
    private TextView txtContent;
    private Button btnClaimNow;

    @Inject
    TLMainPresenter.TLRewards rewardsPresenter;

    public TLReward(){
        TAG = "TLReward";
        layoutID = R.layout.activity_reward;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        txtWelcome = findViewById(R.id.txt_welcome);
        txtContent = findViewById(R.id.txt_content);
        btnClaimNow = findViewById(R.id.btn_claim_now);
        txtWelcome.setText(String.format("Hello,\n%s !", TLStorage.getInstance().getCurrentUserName()));
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Reward");
        txtSubHeader.setVisibility(View.GONE);

        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLReward.this);
        rewardsPresenter.onAttach(this);
        rewardsPresenter.retrieveRewards();
    }

    @Override
    public void onReceiveData(boolean isReedemable, String message, String rank, String rewardPoint) {
        if(isReedemable){
            btnClaimNow.setVisibility(View.VISIBLE);
            txtContent.setText("You've maintain your rank in Top 10 in the leadership board. Well done on your achievements. Leadership board will be refresh every 1st of the month.");

            btnClaimNow.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    if(rank != null && rewardPoint != null)
                        rewardsPresenter.claimReward(rank, rewardPoint);
                }
            });
        }else{
            btnClaimNow.setVisibility(View.GONE);
            txtContent.setText("You are not in Top 10 in the leadership board yet. You are almost there, please invite and share more to your friends and family to lead you into the leadership board. Leadership board will be refresh every 1st of the month.");
        }
    }

    @Override
    public void onClaimReward(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT);
    }

    @Override
    public void onError(String message) {

    }
}
