package com.technolab.android.ui.itl.utils;

public abstract class TLRunnable implements Runnable {
    String data;

    public void run() {
        run(data);
    }

    protected abstract void run(String data);

    public void setData(String data) {
        this.data = data;
    }
}