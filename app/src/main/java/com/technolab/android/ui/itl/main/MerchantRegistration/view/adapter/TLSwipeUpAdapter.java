package com.technolab.android.ui.itl.main.MerchantRegistration.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.TLSwipeUpModel;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TypefaceTextView;

import java.util.List;

public class TLSwipeUpAdapter extends RecyclerView.Adapter<TLSwipeUpAdapter.CardViewHolder>{

    private Context mContext;
    private List<TLSwipeUpModel> mSwipeUpLists;
    private TLSwipeUpAdapter.ItemClickListener mCallback;

    public TLSwipeUpAdapter(Context context, List<TLSwipeUpModel> swipeUpLists){
        this.mContext= context;
        this.mSwipeUpLists = swipeUpLists;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_swipe_up_option, parent, false);
        return new TLSwipeUpAdapter.CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        TLSwipeUpModel model = mSwipeUpLists.get(position);
        holder.mOption.setText(model.getName());
        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onItemClicked(model);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSwipeUpLists.size();
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        private TypefaceTextView mOption;
        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            mOption=itemView.findViewById(R.id.swipe_up_text);
        }
    }

    public void setOnItemClickListener(final ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(TLSwipeUpModel model);
    }
}
