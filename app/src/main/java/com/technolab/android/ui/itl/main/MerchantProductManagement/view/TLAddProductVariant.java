package com.technolab.android.ui.itl.main.MerchantProductManagement.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.github.siyamed.shapeimageview.RoundedImageView;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.technolab.android.R;
import com.technolab.android.ui.entity.TLSwipeUpModel;
import com.technolab.android.ui.entity.Variation;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.adapter.TLSwipeUpAdapter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.utility.EnumStatus;

import java.util.ArrayList;

public class TLAddProductVariant extends AbstractActivity implements TLSwipeUpAdapter.ItemClickListener {

    LinearLayout llVariants;
    Button btnAddMore;
    Button btnSave;
    private View dim;
    private View mSliderView;
    private SlideUp mSlideUp;
    private TLSwipeUpAdapter mStatusAdapter;
    private RecyclerView mSliderRecyclerView;
    private EditText mSelectedEditText;
    private ArrayList<Variation> mTLProductVariantArrayList;


    public TLAddProductVariant() {
        TAG = "TLAddProductVariant";
        layoutID = R.layout.activity_merchant_create_product_variation;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViewById(R.id.toolbar).setBackgroundColor(getResources().getColor(R.color.colorBlue));
        setupToolbar(this);
        mTLProductVariantArrayList = new ArrayList<>();
        llVariants = findViewById(R.id.ll_variants);
        btnAddMore = findViewById(R.id.btn_add_more);
        mSliderView = findViewById(R.id.cl_slide_view);
        btnSave = findViewById(R.id.btn_add_variant);
        mSliderRecyclerView = findViewById(R.id.rv_slider);
        dim = findViewById(R.id.dim);

    }

    private void initSlider() {
        ArrayList<TLSwipeUpModel> statusList = new ArrayList<>();
        for (EnumStatus status : EnumStatus.values()) {
            statusList.add(new TLSwipeUpModel(status.getValue()));
        }

        mStatusAdapter = new TLSwipeUpAdapter(this, statusList);

        mSlideUp = new SlideUpBuilder(mSliderView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / 100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        dim.setVisibility(visibility == 0 ? View.VISIBLE : View.GONE);
                        dim.setClickable(visibility == 0);
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .build();

        dim.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mSlideUp.isVisible())
                    mSlideUp.hide();
            }
        });

        mSliderRecyclerView.setHasFixedSize(false);
        mSliderRecyclerView.setAdapter(mStatusAdapter);
    }

    private void setupExistingVariation() {
        mTLProductVariantArrayList = (ArrayList<Variation>) getIntent().getSerializableExtra("productVariationData");
        for (Variation mVariation : mTLProductVariantArrayList) {
            View child = getLayoutInflater().inflate(R.layout.view_product_variant, null);
            EditText etDisplayStatus = child.findViewById(R.id.et_display_status);
            etDisplayStatus.setText(mVariation.getProduct_variation_status() ? EnumStatus.status1.getValue() : EnumStatus.status2.getValue());
            etDisplayStatus.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    mSelectedEditText = etDisplayStatus;
                    mStatusAdapter.setOnItemClickListener(TLAddProductVariant.this::onItemClicked);
                    mSlideUp.show();
                }
            });
            EditText etVariationSize = child.findViewById(R.id.et_sizes);
            etVariationSize.setText(mVariation.getProduct_variation_size());

            EditText etVariationColor = child.findViewById(R.id.et_color);
            etVariationColor.setText(mVariation.getProduct_variation_color());

            EditText etVariationPrice = child.findViewById(R.id.et_price);
            etVariationPrice.setText(String.valueOf(mVariation.getProduct_variation_price()));

            EditText etVariationQuantity = child.findViewById(R.id.et_quantity);
            etVariationQuantity.setText(String.valueOf(mVariation.getProduct_variation_quantity()));

            RoundedImageView ivDelete = child.findViewById(R.id.iv_delete);
            ivDelete.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    if (llVariants.getChildCount() <= 1) {
                        Toast.makeText(TLAddProductVariant.this, "You need to have at least one variation. ", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    llVariants.removeView(child);
                }
            });

            llVariants.addView(child);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        initSlider();
        //Show Existing Product Variation during Modification
        if (getIntent() != null && getIntent().getSerializableExtra("productVariationData") != null) {
            setupExistingVariation();
        } else {
            addNewVariation();
        }

        btnSave.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTLProductVariantArrayList.clear();
                if (llVariants.getChildCount() <= 0) {
                    Toast.makeText(TLAddProductVariant.this, "Please add at least one variation. ", Toast.LENGTH_SHORT).show();
                    return;
                }

                for (int i = 0; i < llVariants.getChildCount(); i++) {
                    Variation mVariation = new Variation();

                    EditText etSizes = llVariants.getChildAt(i).findViewById(R.id.et_sizes);
                    EditText etQuantity = llVariants.getChildAt(i).findViewById(R.id.et_quantity);
                    EditText etPrice = llVariants.getChildAt(i).findViewById(R.id.et_price);
                    EditText etColor = llVariants.getChildAt(i).findViewById(R.id.et_color);
                    EditText etDisplayStatus = llVariants.getChildAt(i).findViewById(R.id.et_display_status);

                    if (etDisplayStatus.getText().toString().isEmpty()) {
                        Toast.makeText(TLAddProductVariant.this, "Some of the information is empty", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (etSizes.getText().toString().isEmpty()) {
                        Toast.makeText(TLAddProductVariant.this, "Some of the information is empty", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (etQuantity.getText().toString().isEmpty()) {
                        Toast.makeText(TLAddProductVariant.this, "Some of the information is empty", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (etPrice.getText().toString().isEmpty()) {
                        Toast.makeText(TLAddProductVariant.this, "Some of the information is empty", Toast.LENGTH_SHORT).show();
                        return;
                    }
//                        if (etColor.getText().toString().isEmpty()) {
//                            Toast.makeText(TLAddProductVariant.this, "Some of the information is empty", Toast.LENGTH_SHORT).show();
//                            return;
//                        }

                    mVariation.setProduct_variation_color("");

                    Double variationPrice = Double.valueOf(etPrice.getText().toString());
                    mVariation.setProduct_variation_price(variationPrice);

                    mVariation.setProduct_variation_quantity(Integer.parseInt(etQuantity.getText().toString()));

                    mVariation.setProduct_variation_status(etDisplayStatus.getText().toString().equalsIgnoreCase(EnumStatus.status1.getValue()));

                    mVariation.setProduct_variation_size(etSizes.getText().toString());

                    mTLProductVariantArrayList.add(mVariation);

                }
                Intent intent = new Intent();
                intent.putExtra("productVariationData", mTLProductVariantArrayList);
                setResult(RESULT_OK, intent);
                finish();

            }
        });
        btnAddMore.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                addNewVariation();
            }
        });
    }

    private void addNewVariation() {
        View child = getLayoutInflater().inflate(R.layout.view_product_variant, null);

        EditText etDisplayStatus = child.findViewById(R.id.et_display_status);
        etDisplayStatus.setText("Active");
        etDisplayStatus.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mSelectedEditText = etDisplayStatus;
                mStatusAdapter.setOnItemClickListener(TLAddProductVariant.this::onItemClicked);
                mSlideUp.show();
            }
        });

        RoundedImageView ivDelete = child.findViewById(R.id.iv_delete);

        ivDelete.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (llVariants.getChildCount() <= 1) {
                    Toast.makeText(TLAddProductVariant.this, "You need to have at least one variation. ", Toast.LENGTH_SHORT).show();
                    return;
                }
                llVariants.removeView(child);
            }
        });


        ivDelete.setVisibility(View.GONE);

        if ((llVariants.getChildCount() > 0)) {
            RoundedImageView ivDeletePreviousView = llVariants.getChildAt(llVariants.getChildCount() - 1).findViewById(R.id.iv_delete);
            ivDeletePreviousView.setVisibility(View.VISIBLE);
        }

        llVariants.addView(child);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Variation Setup");
        txtSubHeader.setVisibility(View.GONE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onItemClicked(TLSwipeUpModel model) {
        mSelectedEditText.setText(model.getName());
        mSlideUp.hide();
    }
}
