package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.db.SettingsController;
import com.technolab.android.ui.entity.Menu;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Menu> mainMenuList;
    private MenuClickListener mCallback;


    public MenuListAdapter(Context context,Boolean isMerchant) {
        mContext = context;
        mainMenuList = SettingsController.getInstance().getAllMainMenu(isMerchant);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_menu, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Menu menu = mainMenuList.get(position);

        holder.menuDesc.setText(menu.getDescription());
        Resources res = mContext.getResources();
        String mDrawableName = menu.getMenuIcon().substring(0, menu.getMenuIcon().length() - 4);
        int resID = res.getIdentifier(mDrawableName, "drawable", mContext.getPackageName());
        holder.menuIcon.setImageDrawable(res.getDrawable(resID));

        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onMenuClick(menu);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mainMenuList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView menuDesc;
        ImageView menuIcon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            menuDesc = itemView.findViewById(R.id.txt_desc);
            menuIcon = itemView.findViewById(R.id.img_menu);
        }
    }

    public void setOnMenuClickListener(final MenuClickListener listener) {
        mCallback = listener;
    }

    public interface MenuClickListener {
        void onMenuClick(Menu menu);
    }
}
