package com.technolab.android.ui.entity;

import java.util.List;

public class LiveStreamCategory {
    public int industry_category_id;
    public String industry_category_name;
    public String industry_category_image;
    public List<LiveStream> live_now;
    public List<LiveStream> today;
    public List<LiveStream> upcoming;
    public List<LiveStream> favourite;

    public int getIndustry_category_id() {
        return industry_category_id;
    }

    public void setIndustry_category_id(int industry_category_id) {
        this.industry_category_id = industry_category_id;
    }

    public String getIndustry_category_name() {
        return industry_category_name;
    }

    public void setIndustry_category_name(String industry_category_name) {
        this.industry_category_name = industry_category_name;
    }

    public String getIndustry_category_image() {
        return industry_category_image;
    }

    public void setIndustry_category_image(String industry_category_image) {
        this.industry_category_image = industry_category_image;
    }

    public List<LiveStream> getLive_now() {
        return live_now;
    }

    public void setLive_now(List<LiveStream> live_now) {
        this.live_now = live_now;
    }

    public List<LiveStream> getToday() {
        return today;
    }

    public void setToday(List<LiveStream> today) {
        this.today = today;
    }

    public List<LiveStream> getUpcoming() {
        return upcoming;
    }

    public void setUpcoming(List<LiveStream> upcoming) {
        this.upcoming = upcoming;
    }

    public List<LiveStream> getFavourite() {
        return favourite;
    }

    public void setFavourite(List<LiveStream> favourite) {
        this.favourite = favourite;
    }
}
