package com.technolab.android.ui.itl.main;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;

import com.technolab.android.BuildConfig;
import com.technolab.android.R;
import com.technolab.android.di.component.ApplicationComponent;
import com.technolab.android.di.component.DaggerApplicationComponent;
import com.technolab.android.di.module.ApplicationModule;
import com.technolab.android.user.TLStorage;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URISyntaxException;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.plugins.RxJavaPlugins;
import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainApplication extends Application {

    private static MainApplication mInstance;
    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {

        super.onCreate();
        TLStorage.getInstance().initSharedData(this);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/AvenirNext-Regular.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        initDaggerInjection();
        initRxJavaLogs();

        final Thread.UncaughtExceptionHandler previousHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                postToServer(errors.toString());
                previousHandler.uncaughtException(thread, e);
            }
        });
    }

    private void postToServer(String log){

        try {
            OkHttpClient client = new OkHttpClient();
            HttpUrl.Builder urlBuilder = HttpUrl.parse("http://202.9.99.236:5000/log/").newBuilder();
            urlBuilder.addQueryParameter("platform", "Android_Crash_Log");
            urlBuilder.addQueryParameter("crashLog", log);
            String url = urlBuilder.build().toString();
            Request request = new Request.Builder().url(url).build();
            client.newCall(request).enqueue(new Callback() {
                @Override public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    Headers responseHeaders = response.headers();
                    for (int i = 0, size = responseHeaders.size(); i < size; i++) {
                        Log.e(responseHeaders.name(i) , responseHeaders.value(i));
                    }

                    Log.e("response",response.body().string());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void initRxJavaLogs(){
        RxJavaPlugins.setErrorHandler(throwable -> {
            if(throwable instanceof UndeliverableException)
            Log.d("RxJava ", throwable.getCause().toString());
        });
    }

    public void initDaggerInjection() {

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        mApplicationComponent.inject(this);


    }

    public static synchronized MainApplication getInstance() {
        return mInstance;
    }

    public static ApplicationComponent getApplicationComponent(Context mContext) {
        MainApplication app = (MainApplication) mContext.getApplicationContext();
        return app.mApplicationComponent;
    }

    public void setApplicationComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(BuildConfig.BASE_SOCKET_URL);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}