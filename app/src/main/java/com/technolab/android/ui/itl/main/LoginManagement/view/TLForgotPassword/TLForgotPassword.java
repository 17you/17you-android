package com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.hbb20.CountryCodePicker;
import com.technolab.android.R;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInMvpView;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInPresenter;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLMobileVerification;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLSignInPhone;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.KeyboardUtils;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import javax.inject.Inject;

public class TLForgotPassword extends AbstractActivity implements TLSignInMvpView {

    private Button btnNext;
    private EditText etPhone;
    private ImageButton btnBack;
    private CountryCodePicker ccp;

    @Inject
    TLSignInPresenter mTLSignInPresenter;

    public TLForgotPassword(){
        TAG = "Forgot Password";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_mobile_verification_1;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        etPhone = findViewById(R.id.et_phone);
        btnNext = findViewById(R.id.btnNext);
        btnBack = findViewById(R.id.btn_back);
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.registerCarrierNumberEditText(etPhone);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        btnNext.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(ccp.isValidFullNumber()){
                    mTLSignInPresenter.phoneNumberCheck(ccp.getFullNumberWithPlus());
                }else{
                    Toast.makeText(TLForgotPassword.this, "Invalid phone number. Please enter a valid phone number.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLForgotPassword.this);
        mTLSignInPresenter.onAttach(this);
    }

    @Override
    public void onLoginSuccess(String message) {

    }

    @Override
    public void onLoginFailed(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheck(String message) {
        Intent intent = new Intent(TLForgotPassword.this, TLForgotPassword2.class);
        intent.putExtra("phone", ccp.getFullNumberWithPlus());
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        KeyboardUtils.hideSoftKeyboard(this);
    }
}
