package com.technolab.android.ui.itl.utils.drawer.callback;

public interface DragStateListener {
    void onDragStart();

    void onDragEnd(boolean isMenuOpened);
}
