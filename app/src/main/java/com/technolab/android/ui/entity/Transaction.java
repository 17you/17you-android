package com.technolab.android.ui.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Transaction implements Serializable {
    public String tracking_url;
    public int transaction_id;
    public String logistic_status;
    public String transaction_status;
    public String admin_remarks;
    public String delivery_fees;
    public String final_price;
    public String total_product_price;
    public String reference_number;
    public String transaction_date_and_time;
    public int address_id;
    public String city;
    public String postcode;
    public String state;
    public boolean isdefault;
    public int member_id;
    public String address_line_1;
    public String address_line_2;
    public String merchant_company_name;
    public String member_phone;
    public String merchant_email;
    public String merchant_id;
    public String member_email;
    public String member_username;

    public String merchant_bank_number;
    public String merchant_bank_holder_name;
    public String merchant_bank_name;

    public int live_stream_id;
    public String live_stream_title;
    public String live_stream_date;
    public String live_stream_start_time;
    public ArrayList<DetailedTransaction> detailed_transaction;
    public ArrayList<PurchasedProduct> purchased_products;

    public int getLive_stream_id() {
        return live_stream_id;
    }

    public void setLive_stream_id(int live_stream_id) {
        this.live_stream_id = live_stream_id;
    }

    public String getLive_stream_title() {
        return live_stream_title;
    }

    public void setLive_stream_title(String live_stream_title) {
        this.live_stream_title = live_stream_title;
    }

    public String getLive_stream_date() {
        return live_stream_date;
    }

    public void setLive_stream_date(String live_stream_date) {
        this.live_stream_date = live_stream_date;
    }

    public String getLive_stream_start_time() {
        return live_stream_start_time;
    }

    public void setLive_stream_start_time(String live_stream_start_time) {
        this.live_stream_start_time = live_stream_start_time;
    }

    public ArrayList<DetailedTransaction> getDetailed_transaction() {
        return detailed_transaction;
    }

    public void setDetailed_transaction(ArrayList<DetailedTransaction> detailed_transaction) {
        this.detailed_transaction = detailed_transaction;
    }

    public String getMerchant_bank_number() {
        return merchant_bank_number;
    }

    public void setMerchant_bank_number(String merchant_bank_number) {
        this.merchant_bank_number = merchant_bank_number;
    }

    public String getMerchant_bank_holder_name() {
        return merchant_bank_holder_name;
    }

    public void setMerchant_bank_holder_name(String merchant_bank_holder_name) {
        this.merchant_bank_holder_name = merchant_bank_holder_name;
    }

    public String getMerchant_bank_name() {
        return merchant_bank_name;
    }

    public void setMerchant_bank_name(String merchant_bank_name) {
        this.merchant_bank_name = merchant_bank_name;
    }

    public String getMerchant_company_name() {
        return merchant_company_name;
    }

    public void setMerchant_company_name(String merchant_company_name) {
        this.merchant_company_name = merchant_company_name;
    }

    public String getMerchant_email() {
        return merchant_email;
    }

    public void setMerchant_email(String merchant_email) {
        this.merchant_email = merchant_email;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getLogistic_status() {
        return logistic_status;
    }

    public void setLogistic_status(String logistic_status) {
        this.logistic_status = logistic_status;
    }

    public String getTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(String transaction_status) {
        this.transaction_status = transaction_status;
    }

    public String getAdmin_remarks() {
        return admin_remarks;
    }

    public void setAdmin_remarks(String admin_remarks) {
        this.admin_remarks = admin_remarks;
    }

    public String getTracking_url() {
        return tracking_url;
    }

    public void setTracking_url(String tracking_url) {
        this.tracking_url = tracking_url;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getDelivery_fees() {
        return delivery_fees;
    }

    public void setDelivery_fees(String delivery_fees) {
        this.delivery_fees = delivery_fees;
    }

    public String getFinal_price() {
        return final_price;
    }

    public void setFinal_price(String final_price) {
        this.final_price = final_price;
    }

    public String getTotal_product_price() {
        return total_product_price;
    }

    public void setTotal_product_price(String total_product_price) {
        this.total_product_price = total_product_price;
    }

    public String getReference_number() {
        return reference_number;
    }

    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }

    public String getTransaction_date_and_time() {
        return transaction_date_and_time;
    }

    public void setTransaction_date_and_time(String transaction_date_and_time) {
        this.transaction_date_and_time = transaction_date_and_time;
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isIsdefault() {
        return isdefault;
    }

    public void setIsdefault(boolean isdefault) {
        this.isdefault = isdefault;
    }

    public int getMember_id() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }

    public String getAddress_line_1() {
        return address_line_1;
    }

    public void setAddress_line_1(String address_line_1) {
        this.address_line_1 = address_line_1;
    }

    public String getAddress_line_2() {
        return address_line_2;
    }

    public void setAddress_line_2(String address_line_2) {
        this.address_line_2 = address_line_2;
    }

    public String getMember_username() {
        return member_username;
    }

    public void setMember_username(String member_username) {
        this.member_username = member_username;
    }

    public String getMember_phone() {
        return member_phone;
    }

    public void setMember_phone(String member_phone) {
        this.member_phone = member_phone;
    }

    public String getMember_email() {
        return member_email;
    }

    public void setMember_email(String member_email) {
        this.member_email = member_email;
    }

    public ArrayList<PurchasedProduct> getPurchased_products() {
        return purchased_products;
    }

    public void setPurchased_products(ArrayList<PurchasedProduct> purchased_products) {
        this.purchased_products = purchased_products;
    }
}
