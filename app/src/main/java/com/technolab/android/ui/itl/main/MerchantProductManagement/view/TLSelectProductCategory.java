package com.technolab.android.ui.itl.main.MerchantProductManagement.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.ProductMainCategory;
import com.technolab.android.ui.entity.ProductSubCategory;
import com.technolab.android.ui.itl.main.MerchantProductManagement.presenter.TLMerchantProductManagementMvpView;
import com.technolab.android.ui.itl.main.MerchantProductManagement.presenter.TLMerchantProductManagementPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLSelectProductCategory extends AbstractActivity implements TLMerchantProductManagementMvpView.ProductCategory, MerchantProductMainCategoryAdapter.ItemClickListener, MerchantProductSubCategoryAdapter.ItemClickListener {

    @Inject
    TLMerchantProductManagementPresenter.TLProductCategoryPresenter merchantProductCategoryPresenter;

    private RecyclerView mProductCategoryRecyclerView;
    private MerchantProductMainCategoryAdapter merchantProductMainCategoryAdapter;
    private MerchantProductSubCategoryAdapter merchantProductSubCategoryAdapter;
    private Integer mainCategoryId;
    private String mainCategoryName;
    private Integer subCategoryId;
    private String subCategoryName;


    public TLSelectProductCategory() {
        TAG = "TLSelectProductCategory";
        layoutID = R.layout.activity_merchant_choose_product_category;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        findViewById(R.id.toolbar).setBackgroundColor(getResources().getColor(R.color.colorBlue));
        getAppComponent().inject(TLSelectProductCategory.this);
        mProductCategoryRecyclerView = findViewById(R.id.rv_product_category);
        merchantProductCategoryPresenter.onAttach(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        merchantProductCategoryPresenter.fetchProductCategory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        merchantProductSubCategoryAdapter = null;
        merchantProductMainCategoryAdapter = null;
        merchantProductCategoryPresenter.onDetach();
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Choose Category");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onRetrieveProductCategory(ArrayList<ProductMainCategory> productCategories) {
        merchantProductMainCategoryAdapter = new MerchantProductMainCategoryAdapter(this, productCategories);
        mProductCategoryRecyclerView.setAdapter(merchantProductMainCategoryAdapter);
        merchantProductMainCategoryAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onResponse(String message) {

    }

    @Override
    public void onItemClicked(ProductMainCategory productMainCategory) {
        if (productMainCategory.getProductSubCategoryList().size() <= 0) {
            Toast.makeText(this, "There is no subcategory for the selected category. Please choose an different one", Toast.LENGTH_SHORT).show();
            return;
        }
        mainCategoryId = productMainCategory.getProductMainCategoryId();
        mainCategoryName = productMainCategory.getProductMainCategoryName();
        merchantProductSubCategoryAdapter = new MerchantProductSubCategoryAdapter(this, productMainCategory.getProductSubCategoryList());
        mProductCategoryRecyclerView.setAdapter(merchantProductSubCategoryAdapter);
        merchantProductSubCategoryAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onItemClicked(ProductSubCategory productSubCategory) {
        subCategoryId = productSubCategory.getProductSubCategoryId();
        subCategoryName = productSubCategory.getProductSubCategoryName();

        Intent intent = new Intent();
        intent.putExtra("mainCatId", mainCategoryId);
        intent.putExtra("mainCatName", mainCategoryName);
        intent.putExtra("subCatId", subCategoryId);
        intent.putExtra("subCatName", subCategoryName);
        setResult(RESULT_OK, intent);
        finish();
    }
}
