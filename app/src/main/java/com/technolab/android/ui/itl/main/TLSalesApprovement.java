package com.technolab.android.ui.itl.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.DetailedTransaction;
import com.technolab.android.ui.entity.PurchasedProduct;
import com.technolab.android.ui.entity.Transaction;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLMerchantLiveStreaming;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TLActionSheet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import ir.androidexception.andexalertdialog.AndExAlertDialog;

public class TLSalesApprovement extends AbstractActivity implements TLMainMvpView.MerchantTransaction{
    private int viewType;
    private TextView txtReferenceNumber;
    private TextView txtStatus;
    private TextView txtCustomerName;
    private TextView txtMobile;
    private TextView txtDeliveryAddress;
    private TextView txtPurchaseDate;
    private TextView txtDeliveryFees;
    private TextView txtGrandTotal;
    private LinearLayout productContainer;

    private int position = -1;
    private int transactionID;

    private Button btnApprove;
    private Button btnReject;
    private EditText txtReasonValue;
    private TextView txtAttachmentValue;

    @Inject
    TLMainPresenter.TLMerchantTransaction merchantTransactionPresenter;

    public TLSalesApprovement(){
        TAG = "TLSalesApprovement";
        layoutID = R.layout.activity_sales_approvement;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewType = getIntent().getIntExtra("viewType", 0);
        setupToolbar(this);

        DetailedTransaction transaction = (DetailedTransaction) getIntent().getSerializableExtra("transaction");
        transactionID = transaction.getTransaction_id();
        txtStatus = findViewById(R.id.txt_status_value);
        txtCustomerName = findViewById(R.id.txt_customer_name_value);
        txtMobile = findViewById(R.id.txt_mobile_value);
        txtDeliveryAddress = findViewById(R.id.txt_delivery_address_value);
        txtPurchaseDate = findViewById(R.id.txt_purchase_date);
        txtDeliveryFees = findViewById(R.id.txt_delivery_value);
        txtGrandTotal = findViewById(R.id.txt_grand_total_value);
        productContainer = findViewById(R.id.product_container);
        txtReferenceNumber = findViewById(R.id.txt_reference_no_value);
        btnApprove = findViewById(R.id.btnApprove);
        btnReject = findViewById(R.id.btnReject);
        txtReasonValue = findViewById(R.id.txt_reason_value);
        txtAttachmentValue = findViewById(R.id.txt_attachment_value);

        txtAttachmentValue.setText(transaction.getTransaction_receipt());

        btnApprove.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                new AndExAlertDialog.Builder(TLSalesApprovement.this)
                        .setTitle("Payment Approve")
                        .setMessage("Are you sure you want to approve this payment ?")
                        .setPositiveBtnText("Yes")
                        .setNegativeBtnText("No")
                        .setCancelableOnTouchOutside(true)
                        .OnNegativeClicked(input -> {})
                        .OnPositiveClicked(input -> {
                            merchantTransactionPresenter.approveTransaction(String.valueOf(transactionID), "Your transaction has been approved.");
                        })
                        .setMessageTextColor(R.color.black)
                        .setButtonTextColor(R.color.black)
                        .build();
            }
        });

        btnReject.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(txtReasonValue.getText().toString().isEmpty()){
                    Toast.makeText(TLSalesApprovement.this, "Please fill in the remarks", Toast.LENGTH_SHORT).show();
                    return;
                }

                new AndExAlertDialog.Builder(TLSalesApprovement.this)
                        .setTitle("Payment Reject")
                        .setMessage("Are you sure you want to reject this payment ?")
                        .setPositiveBtnText("Yes")
                        .setNegativeBtnText("No")
                        .setCancelableOnTouchOutside(true)
                        .OnNegativeClicked(input -> {})
                        .OnPositiveClicked(input -> {
                            merchantTransactionPresenter.rejectTransaction(String.valueOf(transactionID), txtReasonValue.getText().toString());
                        })
                        .setMessageTextColor(R.color.black)
                        .setButtonTextColor(R.color.black)
                        .build();
            }
        });

        txtReferenceNumber.setText(transaction.getReference_number());
        txtStatus.setText("Payment Verification");
        txtStatus.setTextColor(this.getResources().getColor(R.color.colorRed));

        txtCustomerName.setText(transaction.member_username);
        txtMobile.setText(transaction.getMember_phone());
        txtDeliveryAddress.setText(String.format("%s\n%s\n%s %s\n%s", transaction.getAddress_line_1(), transaction.getAddress_line_2(), transaction.getPostcode(), transaction.getCity(), transaction.getState()));

        String finalDate = "";
        try {
            String date = transaction.getTransaction_date_and_time();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            finalDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        txtPurchaseDate.setText(finalDate);

        productContainer.removeAllViews();
        ArrayList<PurchasedProduct> purchasedProducts = transaction.getPurchased_products();
        for (int i = 0; i < purchasedProducts.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            ConstraintLayout productView = (ConstraintLayout) layoutInflater.inflate(R.layout.layout_transaction_product, null);
            ImageView imgProduct = productView.findViewById(R.id.img_product);
            TextView txtProductTitle = productView.findViewById(R.id.txt_product_title);
            TextView txtProductVariantQuantity = productView.findViewById(R.id.txt_product_variant_quantity);
            TextView txtTotal = productView.findViewById(R.id.txt_total_value);

            Glide.with(this)
                    .load(purchasedProducts.get(i).product_image)
                    .thumbnail(0.1f)
                    .placeholder(getResources().getDrawable(R.drawable.placeholder))
                    .into(imgProduct);

            txtProductTitle.setText(purchasedProducts.get(i).getProduct_name());
            txtProductVariantQuantity.setText(String.format("%s x %d", purchasedProducts.get(i).getProduct_variation_size(), purchasedProducts.get(i).getQuantity_purchased()));
            txtTotal.setText(String.format("RM%.2f", Double.parseDouble(String.valueOf(purchasedProducts.get(i).getTotal_price()))));
            productContainer.addView(productView);
        }
        txtDeliveryFees.setText(String.format("RM%s", transaction.getDelivery_fees()));
        txtGrandTotal.setText(String.format("RM%s",transaction.getFinal_price()));

    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLSalesApprovement.this);
        merchantTransactionPresenter.onAttach(this);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);

        txtHeader.setText("Sales Approvement");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
    }


    @Override
    public void onReceiveTransaction(ArrayList<Transaction> transactions) {
    }

    @Override
    public void onResponse(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
