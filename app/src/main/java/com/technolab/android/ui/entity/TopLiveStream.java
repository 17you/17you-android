package com.technolab.android.ui.entity;

public class TopLiveStream {
    public int live_stream_id;
    public String live_stream_title;
    public String live_stream_image;
    public int live_stream_count;
    public String total_sale;
    public int total_buyers;

    public int getLive_stream_id() {
        return live_stream_id;
    }

    public void setLive_stream_id(int live_stream_id) {
        this.live_stream_id = live_stream_id;
    }

    public String getLive_stream_title() {
        return live_stream_title;
    }

    public void setLive_stream_title(String live_stream_title) {
        this.live_stream_title = live_stream_title;
    }

    public String getLive_stream_image() {
        return live_stream_image;
    }

    public void setLive_stream_image(String live_stream_image) {
        this.live_stream_image = live_stream_image;
    }

    public int getLive_stream_count() {
        return live_stream_count;
    }

    public void setLive_stream_count(int live_stream_count) {
        this.live_stream_count = live_stream_count;
    }

    public String getTotal_sale() {
        return total_sale;
    }

    public void setTotal_sale(String total_sale) {
        this.total_sale = total_sale;
    }

    public int getTotal_buyers() {
        return total_buyers;
    }

    public void setTotal_buyers(int total_buyers) {
        this.total_buyers = total_buyers;
    }
}
