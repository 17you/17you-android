package com.technolab.android.ui.itl.main.MerchantLiveManagement.view;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bambuser.broadcaster.BroadcastStatus;
import com.bambuser.broadcaster.Broadcaster;
import com.bambuser.broadcaster.CameraError;
import com.bambuser.broadcaster.ConnectionError;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.technolab.android.BuildConfig;
import com.technolab.android.R;
import com.technolab.android.security.KeyOne;
import com.technolab.android.security.SecOne;
import com.technolab.android.ui.entity.InitialLiveChat;
import com.technolab.android.ui.entity.Message;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.SendMessage;
import com.technolab.android.ui.itl.main.DeepLinkUtils;
import com.technolab.android.ui.itl.main.LiveChatAdapter;
import com.technolab.android.ui.itl.main.MainApplication;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter.TLMerchantLiveManagementMvpView;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.presenter.TLMerchantLiveManagementPresenter;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.adapter.ProductSelectAdapter;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLProductManagement;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TLActionSheet;
import com.technolab.android.ui.itl.utils.heartview.HeartLayout;
import com.technolab.android.user.TLStorage;
import com.technolab.android.utility.GlobalVariables;
import com.technolab.android.utility.MessageType;
import com.technolab.android.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import ir.androidexception.andexalertdialog.AndExAlertDialog;
import ir.androidexception.andexalertdialog.AndExAlertDialogListener;

public class TLMerchantLiveStreaming extends AbstractActivity implements TLMerchantLiveManagementMvpView.LiveStreamingMVP, ProductSelectAdapter.ItemClickListener {

    private SurfaceView mPreviewSurface;
    private Broadcaster mBroadcaster;
    private Button mBroadcastButton;
    private RelativeLayout mInitialContainer;
    private TextView txtLiveCount;
    private ImageView flipCamera;
    private ImageView stopLive;

    private String liveStreamID;
    private String liveStreamImage;
    private String liveStreamTitle;
    private String liveStreamDescription;
    private String bamBroadcastID;

    private EditText et_message_box;
    private ImageView btnSend;
    private RecyclerView liveChatRV;
    private Socket mSocket;
    private String mUsername = String.format("%s (Merchant)", TLStorage.getInstance().getMerchantCompanyName());
    private String mRoomName;
    private HeartLayout heartLayout;

    private ArrayList<Message> messageArrayList;
    private LiveChatAdapter liveChatAdapter;
    private ProductSelectAdapter productSelectAdapter;
    private Gson gson = new Gson();

    private ArrayList<Product> productArrayList;
    private AlertDialog alertDialog;
    private LinearLayout timerContainer;
    private TextView txtTimer;

    long totalViewerCount = 0;
    private CountDownTimer countDownTimer;
    private long timeStart;
    private long difference;
    private String liveStartTime;
    private String liveEndTime;
    private boolean showAlert = true;

    @Inject
    TLMerchantLiveManagementPresenter.TLLiveStreamingPresenter merchantLiveStreamPresenter;

    public TLMerchantLiveStreaming() {
        TAG = "TLProductManagement";
        layoutID = R.layout.activity_merchant_live_streaming;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mPreviewSurface = findViewById(R.id.PreviewSurfaceView);
        mInitialContainer = findViewById(R.id.initial_container);
        txtLiveCount = findViewById(R.id.txtLiveCount);
        flipCamera = findViewById(R.id.btn_flip_camera);
        stopLive = findViewById(R.id.btn_stop_live);
        et_message_box = findViewById(R.id.et_message_box);
        liveChatRV = findViewById(R.id.recycler_view);
        messageArrayList = new ArrayList<>();
        heartLayout = findViewById(R.id.heart_layout);
        btnSend = findViewById(R.id.btn_send);
        timerContainer = findViewById(R.id.timer_container);
        txtTimer = findViewById(R.id.txt_timer);

        liveStreamID = getIntent().getStringExtra("id");
        liveStreamImage = getIntent().getStringExtra("image");
        liveStreamTitle = getIntent().getStringExtra("title");
        liveStreamDescription = getIntent().getStringExtra("description");
        liveStartTime = getIntent().getStringExtra("sTime");
        liveEndTime = getIntent().getStringExtra("eTime");

        productArrayList = new ArrayList<>();

        mBroadcaster = new Broadcaster(this, BuildConfig.BAM_APPLICATION_ID, mBroadcasterObserver);
        mBroadcastButton = findViewById(R.id.btnBroadcast);
        mBroadcastButton.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                try {
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
                    Calendar calendar = Calendar.getInstance();
                    String sTime = format.format(calendar.getTime());

                    Date startTime = format.parse(sTime);
                    Date endTime = format.parse(liveEndTime);
                    difference = endTime.getTime() - startTime.getTime();

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if(TLStorage.getInstance().getTotalLiveTime() < difference){
                    Toast.makeText(TLMerchantLiveStreaming.this, "You have insufficient live streaming time left within your account. " +
                            "Please update your live stream duration to the range of your remaining amount of time.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (mBroadcaster.canStartBroadcasting()) {
                    mRoomName = liveStreamID;
                    mBroadcaster.startBroadcast();
                    timerContainer.setVisibility(View.VISIBLE);
                    timeStart = System.currentTimeMillis();

                   countDownTimer = new CountDownTimer(difference, 1000) {
                        public void onTick(long millisUntilFinished) {
                            txtTimer.setText(String.format("%02d:%02d:%02d  Remaining", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % TimeUnit.HOURS.toMinutes(1),
                                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % TimeUnit.MINUTES.toSeconds(1)));

                            if(showAlert) {
                                if (millisUntilFinished < 300000) {
                                    showAlert = false;
                                    runOnUiThread(() -> new AndExAlertDialog.Builder(TLMerchantLiveStreaming.this)
                                            .setMessage("Live stream will end in 5 minutes")
                                            .setPositiveBtnText("OK")
                                            .OnPositiveClicked(new AndExAlertDialogListener() {
                                                @Override
                                                public void OnClick(String input) {
                                                    //Do nothing
                                                }
                                            })
                                            .setCancelableOnTouchOutside(true)
                                            .setMessageTextColor(R.color.black)
                                            .setButtonTextColor(R.color.black)
                                            .build());
                                }
                            }
                        }

                        public void onFinish() {
                            if (!mBroadcaster.canStartBroadcasting()) {
                                timerContainer.setVisibility(View.GONE);
                                mBroadcaster.stopBroadcast();
                            }
                        }
                    };

                   countDownTimer.start();
                }
            }
        });

        Glide.with(this)
                .load(liveStreamImage)
                .thumbnail(0.1f)
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into((ImageView) findViewById(R.id.img_live_stream));

        flipCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBroadcaster.switchCamera();
            }
        });

        stopLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AndExAlertDialog.Builder(TLMerchantLiveStreaming.this)
                        .setTitle("End Live Stream ?")
                        .setMessage("Are you sure you would like to end your livestream ?")
                        .setPositiveBtnText("Yes")
                        .setNegativeBtnText("No")
                        .setCancelableOnTouchOutside(true)
                        .OnNegativeClicked(input -> {})
                        .OnPositiveClicked(input -> {
                            if (!mBroadcaster.canStartBroadcasting()) {
                                mBroadcaster.stopBroadcast();
                            }
                        })
                        .setMessageTextColor(R.color.black)
                        .setButtonTextColor(R.color.black)
                        .build();
            }
        });

        findViewById(R.id.img_view_setting).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                TLActionSheet actionSheet = new TLActionSheet(TLMerchantLiveStreaming.this);
                actionSheet.addAction("Product Control");
                actionSheet.addAction("Share Friend");

                actionSheet.setEventListener(new TLActionSheet.OnEventListener() {
                    @Override
                    public void onActionItemClick(TLActionSheet dialog, TLActionSheet.ActionItem item, int position) {
                        if (position == 0) {
                            merchantLiveStreamPresenter.retrieveProduct(liveStreamID);
                        } else if (position == 1) {
                            Uri longURL = DeepLinkUtils.createDeepLink(liveStreamID, "live");
                            DeepLinkUtils.shortTheLink(longURL, TLMerchantLiveStreaming.this, "live");
                            Uri shortURL = DeepLinkUtils.getShortLink();
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_TEXT, String.format("%s\n%s", String.format("Watch 17You's Live : %s - %s.", liveStreamTitle, liveStreamDescription), shortURL.toString()));
                            startActivity(Intent.createChooser(i, "Share URL"));
                        }
                    }

                    @Override
                    public void onCancelItemClick(TLActionSheet dialog) {

                    }
                });
                actionSheet.setCancelVisible(true);
                actionSheet.setCanceledOnTouchOutside(true);
                actionSheet.show();
            }
        });

        btnSend.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(!et_message_box.getText().toString().isEmpty() || !et_message_box.getText().toString().equals(""))
                    sendMessage();
            }
        });

        liveChatAdapter = new LiveChatAdapter(this, messageArrayList);
        liveChatRV.setAdapter(liveChatAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLMerchantLiveStreaming.this);
        merchantLiveStreamPresenter.onAttach(this);
    }

    private void addHeart() {
        GlobalVariables.getHandlerUI().post(new Runnable() {
            @Override
            public void run() {
                heartLayout.addHeart(R.color.colorHeart);
            }
        });
    }

    @Override
    public void onDestroy() {
        if(countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer.onFinish();
        }
        super.onDestroy();
        mBroadcaster.onActivityDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mBroadcaster.onActivityPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mBroadcaster.setCameraSurface(mPreviewSurface);
        mBroadcaster.setCameraId("1");
        mBroadcaster.setViewerCountObserver(new Broadcaster.ViewerCountObserver() {
            @Override
            public void onCurrentViewersUpdated(long l) {
                txtLiveCount.setText(String.valueOf(l));
            }

            @Override
            public void onTotalViewersUpdated(long l) {
                totalViewerCount = l;
            }
        });

        mBroadcaster.setRotation(getWindowManager().getDefaultDisplay().getRotation(), getWindowManager().getDefaultDisplay().getRotation(), 9, 16);
        mBroadcaster.onActivityResume();
    }

    private Broadcaster.Observer mBroadcasterObserver = new Broadcaster.Observer() {
        @Override
        public void onConnectionStatusChange(BroadcastStatus broadcastStatus) {
            Log.i("Mybroadcastingapp", "Received status change: " + broadcastStatus);

            if (broadcastStatus == BroadcastStatus.STARTING)
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            if (broadcastStatus == BroadcastStatus.IDLE)
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

            if (broadcastStatus == BroadcastStatus.FINISHING){
                if(countDownTimer != null) {
                    countDownTimer.cancel();
                    countDownTimer.onFinish();
                }

                long timeElapsed = System.currentTimeMillis() - timeStart;
                TLStorage.getInstance().setTotalLiveTime(TLStorage.getInstance().getTotalLiveTime() - timeElapsed);

                runOnUiThread(() -> new AndExAlertDialog.Builder(TLMerchantLiveStreaming.this)
                                    .setMessage("Live stream ended")
                                    .setPositiveBtnText("OK")
                                    .setCancelableOnTouchOutside(false)
                                    .OnPositiveClicked(new AndExAlertDialogListener() {
                                        @Override
                                        public void OnClick(String input) {
                                            merchantLiveStreamPresenter.stopLiveStream(liveStreamID, bamBroadcastID, totalViewerCount, TLStorage.getInstance().getTotalLiveTime());
                                        }
                                    })
                                    .setMessageTextColor(R.color.black)
                                    .setButtonTextColor(R.color.black)
                                    .build());
            }

            mInitialContainer.setVisibility(broadcastStatus == BroadcastStatus.IDLE ? View.VISIBLE : View.GONE);

        }

        @Override
        public void onStreamHealthUpdate(int i) {
        }

        @Override
        public void onConnectionError(ConnectionError connectionError, String s) {
            Log.w("Mybroadcastingapp", "Received connection error: " + connectionError + ", " + s);
        }

        @Override
        public void onCameraError(CameraError cameraError) {
        }

        @Override
        public void onChatMessage(String s) {
            System.out.println(TAG + ": " + s);
        }

        @Override
        public void onResolutionsScanned() {
        }

        @Override
        public void onCameraPreviewStateChanged() {
        }

        @Override
        public void onBroadcastInfoAvailable(String s, String s1) {
        }

        @Override
        public void onBroadcastIdAvailable(String broadcastID) {
            merchantLiveStreamPresenter.startLiveStream(liveStreamID, broadcastID);
            bamBroadcastID = broadcastID;
        }
    };

    private void sendMessage() {
        String content = et_message_box.getText().toString();
        SendMessage sendData = new SendMessage(mUsername, content, mRoomName);
        String jsonData = gson.toJson(sendData);
        mSocket.emit("newMessage", jsonData);
        et_message_box.setText("");

//        Message message = new Message(mUsername, content, mRoomName, MessageType.CHAT_MINE);
//        //addItemToRecyclerView(message);
    }

    private void addItemToRecyclerView(Message message) {
        GlobalVariables.getHandlerUI().post(new Runnable() {
            @Override
            public void run() {
                messageArrayList.add(message);
                liveChatAdapter.notifyItemInserted(messageArrayList.size());
                liveChatRV.scrollToPosition(messageArrayList.size() - 1);
            }
        });
    }

    Emitter.Listener onConnect = args -> {
        InitialLiveChat data = new InitialLiveChat(mUsername, mRoomName);
        String jsonData = gson.toJson(data);
        mSocket.emit("subscribe", jsonData);
    };

    Emitter.Listener onNewUser = args -> {
        String enteredUserName = args[0].toString();
        Message chat = new Message(enteredUserName, "", mRoomName, MessageType.USER_JOIN);
        addItemToRecyclerView(chat);
        Log.d(TAG, "on New User triggered.");
    };

    Emitter.Listener onUpdateLiveChat = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String data = (String) args[0];
            System.out.println(data);
            try {
                JSONObject jsonObject = new JSONObject(data);
                Message chat = new Message(jsonObject.getString("userName"), jsonObject.getString("messageContent"), jsonObject.getString("roomName"), MessageType.CHAT_MINE);
                addItemToRecyclerView(chat);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    Emitter.Listener onUserLeft = args -> {
        String leftUserName = args[0].toString();
        Message chat = new Message(leftUserName, "", "", MessageType.USER_LEAVE);
        addItemToRecyclerView(chat);
    };

    Emitter.Listener onLikeClicked = args -> {
        addHeart();
    };

    @Override
    public void onError(String message) {

    }

    @Override
    public void onResponse(String message) {
        MainApplication app = (MainApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on("newUserToLiveRoom", onNewUser);
        mSocket.on("updateLiveChat", onUpdateLiveChat);
        mSocket.on("userLeftLiveRoom", onUserLeft);
        mSocket.on("like", onLikeClicked);
        mSocket.connect();
    }

    @Override
    public void onProductResponse(ArrayList<Product> productArrayList) {
        this.productArrayList = productArrayList;
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(TLMerchantLiveStreaming.this);
        mBuilder.setTitle("Product Control");
        LayoutInflater inflater = getLayoutInflater();
        View convertView = inflater.inflate(R.layout.dialog_product_control, null);

        RecyclerView list = convertView.findViewById(R.id.rv_live_stream_product);
        productSelectAdapter = new ProductSelectAdapter(this, productArrayList);
        list.setAdapter(productSelectAdapter);
        productSelectAdapter.setOnItemClickListener(this);
        mBuilder.setView(convertView);

        alertDialog = mBuilder.create();
        alertDialog.show();

        convertView.findViewById(R.id.btn_update).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                merchantLiveStreamPresenter.updateLiveStreamProduct(liveStreamID, productArrayList);
            }
        });
    }

    @Override
    public void onProductUpdate(String message) {
        alertDialog.hide();
        InitialLiveChat data = new InitialLiveChat(mUsername, mRoomName);
        String jsonData = gson.toJson(data);
        mSocket.emit("update", jsonData);
    }

    @Override
    public void onStop(String message) {
        finish();
    }

    @Override
    public void onBackPressed() {
        if(!mBroadcaster.canStartBroadcasting()) {
            new AndExAlertDialog.Builder(TLMerchantLiveStreaming.this)
                    .setTitle("End Live Stream ?")
                    .setMessage("Are you sure you would like to end your livestream ?")
                    .setPositiveBtnText("Yes")
                    .setNegativeBtnText("No")
                    .setCancelableOnTouchOutside(true)
                    .OnNegativeClicked(input -> {
                    })
                    .OnPositiveClicked(new AndExAlertDialogListener() {
                        @Override
                        public void OnClick(String input) {
                            mBroadcaster.stopBroadcast();
                        }
                    })
                    .setMessageTextColor(R.color.black)
                    .setButtonTextColor(R.color.black)
                    .build();
        }else{
            finish();
        }
    }

    @Override
    public void onItemClicked(Product product, String action) {

        int index = 0;
        for (Product liveProducts : productArrayList) {
            if (liveProducts.getProduct_id().equals(product.getProduct_id())) {
                if (action.equalsIgnoreCase("Add"))
                    productArrayList.get(index).setStatus(true);
                else if (action.equalsIgnoreCase("Remove"))
                    productArrayList.get(index).setStatus(false);
            }
            index++;
        }
    }
}
