package com.technolab.android.ui.itl.main.presenter;

import com.technolab.android.ui.entity.Address;
import com.technolab.android.ui.entity.EnergyLevel;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.LevelResult;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.LiveStreamCategory;
import com.technolab.android.ui.entity.Merchant;
import com.technolab.android.ui.entity.MerchantApplication;
import com.technolab.android.ui.entity.MerchantEnergyInfo;
import com.technolab.android.ui.entity.OverallReferralResults;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.ReferralCount;
import com.technolab.android.ui.entity.ReferralEmailResult;
import com.technolab.android.ui.entity.TopLiveStream;
import com.technolab.android.ui.entity.Transaction;
import com.technolab.android.ui.entity.TransactionHistory;
import com.technolab.android.ui.entity.Cart;
import com.technolab.android.ui.itl.spine.AbstractMvpView;

import java.util.ArrayList;

public interface TLMainMvpView {
    interface SwitchMerchantMVP extends AbstractMvpView {
        void onFetchMerchantDetails(Boolean isMerchant, String msg, Merchant merchantDetail);
        void onFetchLiveMarket(ArrayList<Industry> industryArrayList);
        void onShareSuccessful(String message);
        void onFetchReferralEnergyCount(ReferralCount referralCount, ArrayList<EnergyLevel> energyLevels);
        void onError(String message);
    }

    interface LiveMarket extends AbstractMvpView{
        void onFetchLiveMarket(ArrayList<Industry> industryArrayList);
        void onCheckMerchantRegistrationStatus(boolean status, MerchantApplication application);
        void onError(String message);
    }

    interface LiveMarketCategory extends AbstractMvpView{
        void onFetchLiveMarket(ArrayList<LiveStreamCategory> liveStreamCategoryArrayList);
        void onError(String message);
    }

    interface MerchantMVP extends AbstractMvpView{
        void onFetchUpcomingLive(Boolean status, LiveStream liveStream);
        void onFetchEnergy(MerchantEnergyInfo merchantEnergyInfo);
        void onFetchTopLiveStream(Boolean status, ArrayList<TopLiveStream> liveStreams);
        void onRetrieveProductDetails(ArrayList<Product> products);
        void onFetchTransactionHistory(Boolean status, ArrayList<TransactionHistory> transactionHistories);
        void onError(String message);
    }

    interface LiveStreamingCategory extends AbstractMvpView{
        void onFetchLiveStreamDetails(LiveStream liveStream, boolean isInitialFetch);
        void onFetchAddress(Boolean status, Address address, ArrayList<Cart> cartArrayList);
        void onUpdateFavourite(String message);
        void onError(String message);
        void onFinish(String message);
        void onPartialFinish(String message);
    }

    interface UserProfile extends AbstractMvpView{
        void onReceiveAddress(ArrayList<Address> addressList);
        void onFinish(String message);
        void onError(String message);
    }

    interface Referral extends AbstractMvpView{
        void onResponseReferral(OverallReferralResults overallReferralResults);
        void onResponseEmail(ArrayList<ReferralEmailResult> referralEmailResults);
        void onError(String message);
    }

    interface MerchantProfile extends AbstractMvpView{
        void onReceiveProfile(Boolean isMerchant, String msg, Merchant merchantDetail);
        void onFinish(String message);
        void onError(String message);
    }

    interface UserTransaction extends AbstractMvpView{
        void onReceiveTransaction(ArrayList<Transaction> transactions);
        void onPayment(String message);
        void onError(String message);
    }

    interface MerchantTransaction extends AbstractMvpView{
        void onReceiveTransaction(ArrayList<Transaction> transactions);
        void onResponse(String message);
        void onError(String message);
    }

    interface RedeemEnergy extends AbstractMvpView{
        void onReceiveEnergyData(ReferralCount referralCount);
        void onReceiveEnergyLevel(ArrayList<LevelResult> levelResults);
        void onRedeemEnergy(Boolean status, String message, int energy);
        void onError(String message);
    }

    interface Ranking extends AbstractMvpView{
        void onReceiveData(com.technolab.android.ui.entity.Ranking ranking, int type);
        void onError(String message);
    }

    interface Reward extends AbstractMvpView{
        void onReceiveData(boolean isReedemable, String message, String rank, String rewardPoint);
        void onClaimReward(String message);
        void onError(String message);
    }
}
