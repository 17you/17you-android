package com.technolab.android.ui.itl.main;

import android.graphics.Bitmap;
import android.net.UrlQuerySanitizer;
import android.os.Bundle;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.technolab.android.BuildConfig;
import com.technolab.android.R;
import com.technolab.android.ui.itl.spine.AbstractActivity;

public class PaymentWebView extends AbstractActivity {

    WebView webView;

    public PaymentWebView(){
        TAG = "PaymentWebView";
        layoutID = R.layout.activity_payment_webview;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(url.contains(BuildConfig.PAYMENT_REDIRECT)) {
                    System.out.println("Redirecting here second : " + url);
                    UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(url);
                    String value = sanitizer.getValue("status_id");
                    String msg = sanitizer.getValue("msg");
                    if (value.equalsIgnoreCase("1")) {
                        Toast.makeText(PaymentWebView.this, msg, Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(PaymentWebView.this, msg, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        webView.loadUrl("https://sandbox.senangpay.my/payment/672162091848660?detail=Frutie&amount=100.00&order_id=1&name=Vishanth&email=vishanth@gmail.com&phone=12345675&hash=ce27a2157c2f0d98b921087a87c43883e92efe322a1c0dfeaee9a8dd190a65b9");
    }
}
