package com.technolab.android.ui.itl.utils.drawer;

public interface TLDrawer {
    boolean isMenuClosed();

    boolean isMenuOpened();

    boolean isMenuLocked();

    void closeMenu();

    void closeMenu(boolean animated);

    void openMenu();

    void openMenu(boolean animated);

    void setMenuLocked(boolean locked);

    TLDrawerLayout getLayout();
}
