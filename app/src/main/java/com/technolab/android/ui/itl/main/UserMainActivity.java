package com.technolab.android.ui.itl.main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.mackhartley.roundedprogressbar.RoundedProgressBar;
import com.technolab.android.R;
import com.technolab.android.db.SettingsController;
import com.technolab.android.ui.entity.EnergyLevel;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.Menu;
import com.technolab.android.ui.entity.Merchant;
import com.technolab.android.ui.entity.ReferralCount;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.drawer.TLDrawerLayout;
import com.technolab.android.ui.itl.utils.drawer.TLDrawerLayoutBuilder;
import com.technolab.android.user.TLStorage;
import com.technolab.android.utility.GlobalVariables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class UserMainActivity extends AbstractActivity implements MenuListAdapter.MenuClickListener, TLMainMvpView.SwitchMerchantMVP, MiniLiveMarketAdapter.ItemClickListener {

    private RecyclerView rvLiveStream;
    private RelativeLayout rvContainer;
    private LottieAnimationView pbLoading;
    private TextView profileName;
    private ImageView imgProfile;
    private RoundedProgressBar roundedProgressBar;
    private Button btnShare;
    private LinearLayout redeemEnergyContainer;
    MiniLiveMarketAdapter liveMarketAdapter;
    private ArrayList<Industry> industryArrayList;

    @Inject
    TLMainPresenter.TLSwitchMerchantPresenter switchMerchantPresenter;

    public UserMainActivity(){
        TAG = "User Main Activity";
        statusBarColorID = R.color.colorD;
        layoutID = R.layout.activity_main;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        setupRefresh(this);
        cacheActivityMap = new HashMap<String, ArrayList<AbstractActivity>>();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        TLDrawerLayout drawerLayout = new TLDrawerLayoutBuilder(this)
                .withViewMenuToggle(imgLeft1)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.fragment_user_drawer)
                .inject();

        RecyclerView menuRecyclerView = drawerLayout.findViewById(R.id.menu_recycler_view);
        profileName = drawerLayout.findViewById(R.id.txt_profile_name);
        profileName.setText(TLStorage.getInstance().getCurrentUserName());
        imgProfile = drawerLayout.findViewById(R.id.img_profile);
        roundedProgressBar = findViewById(R.id.rounded_progress_bar);
        btnShare = findViewById(R.id.btn_share);
        redeemEnergyContainer = findViewById(R.id.redeem_energy_container);

        btnShare.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                switchMerchantPresenter.addShare();
            }
        });

        redeemEnergyContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onClickMenu(new Menu("4"));
            }
        });

        drawerLayout.findViewById(R.id.container_view_profile).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(UserMainActivity.this, ViewProfileActivity.class));
            }
        });

        findViewById(R.id.tv_view_more).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onClickMenu(new Menu("1"));
            }
        });

        findViewById(R.id.container_my_referral).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onClickMenu(new Menu("1"));
            }
        });

        findViewById(R.id.btn_more).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                onClickMenu(new Menu("3"));
            }
        });

        MenuListAdapter menuListAdapter = new MenuListAdapter(this, false);
        menuListAdapter.setOnMenuClickListener(this);
        menuRecyclerView.setAdapter(menuListAdapter);

        LinearLayout menuContainer = drawerLayout.findViewById(R.id.menu_container);

        ImageView infoButton = findViewById(R.id.img_info);
        infoButton.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(UserMainActivity.this, TLInviteFriendDetail.class));
            }
        });

        rvLiveStream = findViewById(R.id.rv_live_stream);
        rvContainer = findViewById(R.id.rv_container);
        pbLoading = findViewById(R.id.pb_loading);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Welcome");
        txtSubHeader.setText(TLStorage.getInstance().getCurrentUserName());
        txtHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP,14);
        txtSubHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);

        imgRight1.setImageDrawable(getResources().getDrawable(R.drawable.ic_synchronize));
        imgRight1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                processSwitchMerchant();
            }
        });
        imgRight2.setImageDrawable(getResources().getDrawable(R.drawable.ic_notification));
        imgRight2.setColorFilter(ContextCompat.getColor(this, R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);
        imgRight2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(UserMainActivity.this, TLUserNotification.class));
            }
        });
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> switchMerchantPresenter.fetchReferralEnergyCount());
    }

    @Override
    protected void onStart() {
        super.onStart();
        rvContainer.setVisibility(View.GONE);
        pbLoading.setVisibility(View.VISIBLE);

        getAppComponent().inject(UserMainActivity.this);
        switchMerchantPresenter.onAttach(this);
        switchMerchantPresenter.fetchActiveLiveStream();
        switchMerchantPresenter.fetchReferralEnergyCount();
    }

    private void processSwitchMerchant(){
        switchMerchantPresenter.fetchMerchantDetail();
    }

    @Override
    public void onMenuClick(Menu menu) {
        onClickMenu(menu);
    }

    private void onClickMenu(final Menu menu) {

        if(menu.getDescription() != null) {
            if (menu.getDescription().equalsIgnoreCase("Switch to Merchant")) {
                processSwitchMerchant();
                return;
            }
        }

        cacheActivityMap.clear();
        ArrayList<AbstractActivity> newList = getActivityForMenu(menu.getMenuID());
        activeActivityArrayList = newList;

        GlobalVariables.getHandlerUI().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (AbstractActivity activity : activeActivityArrayList) {

                    startActivity(new Intent(UserMainActivity.this, activity.getClass()));
                }
            }
        }, 100);

//        GlobalVariables.getHandlerWorker().post(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        });
    }

    private ArrayList<AbstractActivity> getActivityForMenu(String mainViewID) {
        ArrayList<AbstractActivity> list = new ArrayList<AbstractActivity>();
        ArrayList<AbstractActivity> temp = cacheActivityMap.get(mainViewID);
        if (temp == null && !cacheActivityMap.containsKey(mainViewID)) {
            temp = SettingsController.getInstance().getActivityCtrlListForViewID(mainViewID);
            cacheActivityMap.put(mainViewID, temp);
        }
        if (temp != null)
            list.addAll(temp);

        return list;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!hasPermission(Manifest.permission.CAMERA)
                && !hasPermission(Manifest.permission.RECORD_AUDIO))
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO}, 1);
        else if (!hasPermission(Manifest.permission.RECORD_AUDIO))
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.RECORD_AUDIO}, 1);
        else if (!hasPermission(Manifest.permission.CAMERA))
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, 1);

        profileName.setText(TLStorage.getInstance().getCurrentUserName());
        Glide.with(this)
                .load(TLStorage.getInstance().getPrefUserImage())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into(imgProfile);
    }

    private boolean hasPermission(String permission) {
        return ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onFetchMerchantDetails(Boolean isMerchant, String msg, Merchant merchantDetail) {
        if(!isMerchant) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

            return;
        }

        TLStorage.getInstance().setMerchantId(String.valueOf(merchantDetail.getMerchantId()));
        TLStorage.getInstance().setMerchantCompanyName(merchantDetail.getMerchantCompanyName());
        TLStorage.getInstance().setMerchantImage(merchantDetail.getMerchantImage());
        TLStorage.getInstance().setMerchantFresh(merchantDetail.getFreshMerchant());
        TLStorage.getInstance().setTotalLiveTime(Long.parseLong(merchantDetail.getRemainingLiveTime()));

        finish();
        startActivity(new Intent(UserMainActivity.this, MerchantMainActivity.class));
    }

    @Override
    public void onFetchLiveMarket(ArrayList<Industry> industryArrayList) {
        this.industryArrayList = industryArrayList;
        pbLoading.setVisibility(View.GONE);
        rvContainer.setVisibility(View.VISIBLE);

        liveMarketAdapter = new MiniLiveMarketAdapter(this,industryArrayList);
        rvLiveStream.setAdapter(liveMarketAdapter);
        liveMarketAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onShareSuccessful(String message) {
        Uri longURL = DeepLinkUtils.createDeepLink(String.valueOf(TLStorage.getInstance().getReferralCode()), "referral");
        DeepLinkUtils.shortTheLink(longURL, UserMainActivity.this, "referral");
        Uri shortURL = DeepLinkUtils.getShortLink();
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");

        i.putExtra(Intent.EXTRA_TEXT, String.format("%s\n%s", "Join 17You and Start Selling via Live Streaming.\n\nPlease make sure to click on the given link after installing 17You app in your device to earn EXTRA ENERGY !\n", shortURL.toString()));

        startActivity(Intent.createChooser(i, "Share URL"));
    }

    @Override
    public void onFetchReferralEnergyCount(ReferralCount referralCount, ArrayList<EnergyLevel> energyLevels) {
        refreshLayout.setRefreshing(false);
        String[] energyType = {"1", "0", "1000", "2", "1001", "2000", "3", "2001", "3000", "4", "3001",
                "4000", "5", "4001", "5000", "6", "5001", "6000", "7", "6001", "7000", "8", "7001", "8000"};
        String energyLevel = "";
        int energyLimit = 0;
        int energy = referralCount.getTotal_energy();
        for (int j = 0; j < energyType.length; j++) {
            energyLevel = energyType[j];
            j = j + 1;
            int min = Integer.parseInt(energyType[j]);
            j = j + 1;
            int max = Integer.parseInt(energyType[j]);
            if (energy >= min && energy <= max) {
                energyLimit = max;
                break;
            }
        }
        ((TextView) findViewById(R.id.txt_level_progress)).setText(String.format("Level %s progress", energyLevel));
        ((TextView)findViewById(R.id.txt_level_value)).setText(String.valueOf(referralCount.getTotal_energy()));
        ((TextView)findViewById(R.id.txt_total_share_friends)).setText(String.format("Total Share %d Friends", referralCount.getTotal_share()));
        ((TextView)findViewById(R.id.txt_energy_remaining)).setText(String.format("%d/%d", referralCount.getTotal_energy(), energyLimit));
        ((TextView)findViewById(R.id.txt_redeem_energy_count)).setText(String.format("%skwp", referralCount.getTotal_energy()));

        ((TextView) findViewById(R.id.tv_total_share_value)).setText(String.valueOf(referralCount.getTotal_share()));
        ((TextView) findViewById(R.id.tv_total_sign_up_value)).setText(String.valueOf(referralCount.getTotal_sign_up()));
        ((TextView) findViewById(R.id.tv_pending_value)).setText(String.valueOf(referralCount.getTotal_share() - referralCount.getTotal_sign_up()));

        float totalEnergy = (float) referralCount.getTotal_energy();
        float energyPercentage = (float) (totalEnergy/energyLimit) * 100;
        roundedProgressBar.setAnimationLength(1500);
        roundedProgressBar.setProgressPercentage(energyPercentage,true);
    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onItemClicked(int id) {
        Intent intent = new Intent(UserMainActivity.this,TLLiveMarketCategory.class);
        intent.putExtra("marketID", String.valueOf(id));
        intent.putExtra("industry", this.industryArrayList);
        startActivity(intent);
    }
}
