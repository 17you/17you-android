package com.technolab.android.ui.itl.main.MerchantRegistration.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationMvpView;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationPresenter;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.adapter.MerchantIndustryAdapter;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLMerchantChooseIndustry extends AbstractActivity implements TLMerchantRegistrationMvpView.IndustryMVP, MerchantIndustryAdapter.ItemClickListener{

    @Inject
    TLMerchantRegistrationPresenter.TLMerchantIndustryPresenter merchantRegistrationPresenter;

    private MerchantIndustryAdapter merchantIndustryAdapter;
    private RecyclerView recyclerView;

    public TLMerchantChooseIndustry(){
        TAG = "Merchant Choose Industry";
        statusBarColorID = R.color.colorD;
        layoutID = R.layout.activity_merchant_choose_industry;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        recyclerView = findViewById(R.id.recycler_view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLMerchantChooseIndustry.this);
        merchantRegistrationPresenter.onAttach(this);
        merchantRegistrationPresenter.fetchIndustries();
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Business Application");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setImageDrawable(getResources().getDrawable(R.drawable.ic_round_close));
        imgRight2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMerchantChooseIndustry.this, UserMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRetrieveIndustries(ArrayList<Industry> industryArrayList) {
        merchantIndustryAdapter = new MerchantIndustryAdapter(this,industryArrayList);
        recyclerView.setAdapter(merchantIndustryAdapter);
        merchantIndustryAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onItemClicked(Industry industry) {
        Intent intent = new Intent(TLMerchantChooseIndustry.this, TLMerchantInfo.class);
        intent.putExtra("industry_id",industry.getIndustry_category_id());
        startActivity(intent);
    }
}
