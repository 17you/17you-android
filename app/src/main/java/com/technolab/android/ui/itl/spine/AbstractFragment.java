package com.technolab.android.ui.itl.spine;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.utility.GlobalVariables;
import com.technolab.android.utility.TLConstant;

public class AbstractFragment extends Fragment {
    protected String TAG;
    protected String viewID;
    protected String menuIdx;

    /**
     * Loading spinner for the fragment
     */
    private ProgressBar progressBar;

    /**
     * IMPORTANT: This is the layout ID which is used to load fragment's layout. All subclasses must declare this value before calling super.onCreateView
     */
    protected Integer layoutID;

    /**
     * The container that contain the loading spinner (progress bar). If not set then it will be the main layout
     */
    protected Integer progressBarContainer;

    protected Boolean isLandscape;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(layoutID, container, false);
        view.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                //Do nothing
            }
        });
        //Todo : Uncomment this
     //   if (TLConstant.DEBUG_OTHERS && TAG != null && progressBar == null)
     //       Log.d(TAG, "Init");

        //Add load spinner to fragment
        ViewGroup viewGroup = (ViewGroup) view;
        progressBar = new ProgressBar(GlobalVariables.getInstance().getContext(), null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(false);
        progressBar.setVisibility(View.GONE);
        //Todo : Set correct dimensions
        /*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(GlobalVariables.getInstance().getContext().getResources().getDimensionPixelSize(R.dimen.loading_spinner), GlobalVariables.getInstance().getContext().getResources().getDimensionPixelSize(R.dimen.loading_spinner));
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        progressBar.setLayoutParams(params);*/
        //Todo : Set correct progress bar
        ////progressBar.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_style));

        if (progressBarContainer != null) {
            View v = view.findViewById(progressBarContainer);
            if (v != null && v instanceof ViewGroup)
                ((ViewGroup) v).addView(progressBar);
            else
                viewGroup.addView(progressBar);
        } else {
            if (viewGroup instanceof DrawerLayout)
                viewGroup.addView(progressBar, 1);
            else if (viewGroup instanceof ScrollView)
                ((ViewGroup) viewGroup.getChildAt(0)).addView(progressBar);
            else
                viewGroup.addView(progressBar);
        }

        GlobalVariables.getInstance().getMainActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

        return view;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        isLandscape = newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    public String getViewID() {
        return viewID;
    }

    public void setViewID(String viewID) {
        this.viewID = viewID;
    }

    public String getMenuIdx() {
        return menuIdx;
    }

    public void setMenuIdx(String menuIdx) {
        this.menuIdx = menuIdx;
    }

    /**
     * This method to show the loading spinner for the fragment
     */
    protected void showLoadingSpinner() {
        GlobalVariables.getHandlerUI().post(runnableShow);
    }

    private Runnable runnableShow = new Runnable() {
        @Override
        public void run() {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.bringToFront();
        }
    };

    /**
     * This method to hide the loading spinner
     */
    protected void hideLoadingSpinner() {
        GlobalVariables.getHandlerUI().post(runnableHide);
    }

    private Runnable runnableHide = new Runnable() {
        @Override
        public void run() {
            progressBar.setVisibility(View.GONE);
        }
    };

    public void addToFragment(FragmentManager fragmentManager, Integer container) {
        if (GlobalVariables.getInstance().registerForPopup(this)) {
            fragmentManager.beginTransaction().add(container, this).commit();
        }
    }

    public void removeFromFragment() {
        getFragmentManager().beginTransaction().remove(this).commit();
        GlobalVariables.getInstance().unregisterForPopup(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public int isShowingProgressBar() {
        return progressBar.getVisibility();
    }
}
