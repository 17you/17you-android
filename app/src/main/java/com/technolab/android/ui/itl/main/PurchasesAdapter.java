package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.PurchasedProduct;
import com.technolab.android.ui.entity.Transaction;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PurchasesAdapter extends RecyclerView.Adapter<PurchasesAdapter.ViewHolder> {

    private PurchasesAdapter.ItemClickListener mCallback;
    private ArrayList<Transaction> transactions;
    private Context context;
    private int viewType;

    public PurchasesAdapter(Context context, ArrayList<Transaction> transactions, int viewType) {
        this.transactions = transactions;
        this.context = context;
        this.viewType = viewType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_my_purchases, parent, false);
        return new PurchasesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Transaction transaction = transactions.get(position);
        holder.reasonContainer.setVisibility(transaction.getLogistic_status().equals("PAYMENT REJECTED") ? View.VISIBLE : View.GONE);
        holder.txtReferenceNumber.setText(transaction.getReference_number());
        switch (transaction.getLogistic_status()) {
            case "PAYMENT PENDING":
                holder.txtStatus.setText("Payment Pending");
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.colorYellow));
                holder.itemView.setOnClickListener(new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                        if (mCallback != null) {
                            mCallback.onItemClicked(transaction);
                        }
                    }
                });
                holder.trackingShipmentContainer.setVisibility(View.GONE);
                break;
            case "PAYMENT VERIFICATION":
                holder.txtStatus.setText("Payment Verification");
                holder.trackingShipmentContainer.setVisibility(View.GONE);
                break;
            case "PAYMENT REJECTED":
                holder.txtStatus.setText("Payment Rejected");
                holder.txtStatus.setTextColor(context.getResources().getColor(R.color.colorRed));
                holder.trackingShipmentContainer.setVisibility(View.GONE);
                break;
            case "PROCESSING":
                holder.txtStatus.setText("Processing");
                holder.trackingShipmentContainer.setVisibility(View.GONE);
                break;
            case "SHIPPED":
                holder.txtStatus.setText("Shipped");
                holder.trackingShipmentContainer.setVisibility(View.VISIBLE);
                break;
            case "DELIVERED":
                holder.txtStatus.setText("Delivered");
                holder.trackingShipmentContainer.setVisibility(View.VISIBLE);
                break;
        }
        holder.txtCustomerName.setText(transaction.getMerchant_company_name());
        holder.txtMobile.setText(transaction.getMerchant_email());
        holder.txtDeliveryAddress.setText(String.format("%s\n%s\n%s %s\n%s", transaction.getAddress_line_1(), transaction.getAddress_line_2(), transaction.getPostcode(), transaction.getCity(), transaction.getState()));

        holder.txtReason.setText(transaction.getAdmin_remarks());
        holder.txtTrackingShipmentURL.setText(transaction.getTracking_url());

        String finalDate = "";
        try {
            String date = transaction.getTransaction_date_and_time();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            finalDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.txtPurchaseDate.setText(finalDate);

        holder.productContainer.removeAllViews();
        ArrayList<PurchasedProduct> purchasedProducts = transaction.getPurchased_products();
        for (int i = 0; i < purchasedProducts.size(); i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            ConstraintLayout productView = (ConstraintLayout) layoutInflater.inflate(R.layout.layout_transaction_product, null);
            ImageView imgProduct = productView.findViewById(R.id.img_product);
            TextView txtProductTitle = productView.findViewById(R.id.txt_product_title);
            TextView txtProductVariantQuantity = productView.findViewById(R.id.txt_product_variant_quantity);
            TextView txtTotal = productView.findViewById(R.id.txt_total_value);
            TextView txtUnitPrice = productView.findViewById(R.id.txt_unit_value);


            Glide.with(context)
                    .load(purchasedProducts.get(i).product_image)
                    .thumbnail(0.1f)
                    .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                    .into(imgProduct);

            txtProductTitle.setText(purchasedProducts.get(i).getProduct_name());
            txtProductVariantQuantity.setText(String.format("%s x %d", purchasedProducts.get(i).getProduct_variation_size(), purchasedProducts.get(i).getQuantity_purchased()));
            txtUnitPrice.setText(String.format("RM%.2f", Double.parseDouble(String.valueOf(purchasedProducts.get(i).getUnit_price()))));
            txtTotal.setText(String.format("RM%.2f", Double.parseDouble(String.valueOf(purchasedProducts.get(i).getTotal_price()))));
            holder.productContainer.addView(productView);
        }
        holder.txtDeliveryFees.setText(String.format("RM%s", transaction.getDelivery_fees()));
        holder.txtGrandTotal.setText(String.format("RM%s",transaction.getFinal_price()));
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtReferenceNumber;
        private TextView txtStatus;
        private TextView txtCustomerName;
        private TextView txtMobile;
        private TextView txtDeliveryAddress;
        private TextView txtTrackingShipmentURL;
        private TextView txtPurchaseDate;
        private TextView txtDeliveryFees;
        private TextView txtGrandTotal;
        private TextView txtReason;

        private LinearLayout productContainer;
        private ConstraintLayout trackingShipmentContainer;
        private ConstraintLayout reasonContainer;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            txtReferenceNumber = itemView.findViewById(R.id.txt_reference_no_value);
            txtStatus = itemView.findViewById(R.id.txt_status_value);
            txtCustomerName = itemView.findViewById(R.id.txt_customer_name_value);
            txtMobile = itemView.findViewById(R.id.txt_mobile_value);
            txtDeliveryAddress = itemView.findViewById(R.id.txt_delivery_address_value);
            txtTrackingShipmentURL = itemView.findViewById(R.id.txt_tracking_shipment_value);
            txtPurchaseDate = itemView.findViewById(R.id.txt_purchase_date);
            txtDeliveryFees = itemView.findViewById(R.id.txt_delivery_value);
            txtGrandTotal = itemView.findViewById(R.id.txt_grand_total_value);
            productContainer = itemView.findViewById(R.id.product_container);
            trackingShipmentContainer = itemView.findViewById(R.id.tracking_shipment_container);
            reasonContainer = itemView.findViewById(R.id.reason_container);
            txtReason = itemView.findViewById(R.id.txt_reason_value);
        }
    }

    public void setOnItemClickListener(final PurchasesAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(Transaction transaction);
    }
}
