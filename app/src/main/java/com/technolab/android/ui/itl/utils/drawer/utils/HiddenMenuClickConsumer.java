package com.technolab.android.ui.itl.utils.drawer.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

import com.technolab.android.ui.itl.utils.drawer.TLDrawerLayout;

public class HiddenMenuClickConsumer extends View {

    private TLDrawerLayout menuHost;

    public HiddenMenuClickConsumer(Context context) {
        super(context);
    }

    @Override
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent event) {
        return menuHost.isMenuClosed();
    }

    public void setMenuHost(TLDrawerLayout layout) {
        this.menuHost = layout;
    }
}
