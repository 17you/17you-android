package com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.ui.itl.main.LandingPageActivity;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLMobileVerification;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

public class TLForgotPassword4 extends AbstractActivity {

    Button btnSignIn;

    public TLForgotPassword4(){
        TAG = "Forgot Password";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_reset_successful;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        btnSignIn = findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(TLStorage.getInstance().getIsLoggedIn())
                    TLStorage.resetInstance();

                Intent intent = new Intent(TLForgotPassword4.this, LandingPageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }
}
