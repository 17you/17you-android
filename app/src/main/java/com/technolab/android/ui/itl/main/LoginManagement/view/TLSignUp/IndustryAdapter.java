package com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.itl.main.LiveMarketAdapter;

import java.util.ArrayList;


public class IndustryAdapter extends RecyclerView.Adapter<IndustryAdapter.ViewHolder>{

    ArrayList<Industry> industryArrayList;
    Context context;
    private IndustryAdapter.ItemClickListener mCallback;

    public IndustryAdapter(Context context, ArrayList<Industry> industryArrayList){
        this.context = context;
        this.industryArrayList = industryArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_tell_us_industry, parent, false);
        return new IndustryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Industry industry = industryArrayList.get(position);
        holder.toggleButton.setText(industry.getIndustry_category_name());
        holder.toggleButton.setTextOff(industry.getIndustry_category_name());
        holder.toggleButton.setTextOn(industry.getIndustry_category_name());
        holder.toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mCallback != null) {
                    mCallback.onItemClicked(industry.getIndustry_category_id(), isChecked ? "Add" : "Remove");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return industryArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ToggleButton toggleButton;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            toggleButton = itemView.findViewById(R.id.btn_industry);
        }
    }

    public void setOnItemClickListener(final IndustryAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(Integer id, String action);
    }
}
