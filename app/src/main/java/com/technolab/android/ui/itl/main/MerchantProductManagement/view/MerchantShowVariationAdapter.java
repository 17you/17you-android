package com.technolab.android.ui.itl.main.MerchantProductManagement.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.Variation;
import java.util.ArrayList;
import java.util.Locale;

public class MerchantShowVariationAdapter extends RecyclerView.Adapter<MerchantShowVariationAdapter.ViewHolder>  {

    private ArrayList<Variation> mProductVariationList;
    private Context mContext;

    public MerchantShowVariationAdapter(Context context, ArrayList<Variation> productVariationList) {
        this.mContext = context;
        this.mProductVariationList = productVariationList;
    }

    @NonNull
    @Override
    public MerchantShowVariationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_show_product_variation, parent, false);
        return new MerchantShowVariationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MerchantShowVariationAdapter.ViewHolder holder, int position) {
        Variation productVariation = mProductVariationList.get(position);
        holder.tvSize.setText(productVariation.getProduct_variation_size());
        Double variationPrice = productVariation.getProduct_variation_price();
        String formattedVariationPrice = String.format(Locale.ENGLISH,"RM %.2f",variationPrice);
        holder.tvPrice.setText(formattedVariationPrice);
        holder.tvQuantity.setText(String.valueOf(productVariation.getProduct_variation_quantity()));
    }


    @Override
    public int getItemCount() {
        return mProductVariationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvSize;
        TextView tvPrice;
        TextView tvQuantity;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSize = itemView.findViewById(R.id.tv_variation_size);
            tvPrice = itemView.findViewById(R.id.tv_variation_price);
            tvQuantity = itemView.findViewById(R.id.tv_variation_quantity);
        }
    }
}
