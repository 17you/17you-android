package com.technolab.android.ui.itl.main.MerchantRegistration.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.technolab.android.R;
import com.technolab.android.ui.entity.TLSwipeUpModel;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationMvpView;
import com.technolab.android.ui.itl.main.MerchantRegistration.presenter.TLMerchantRegistrationPresenter;
import com.technolab.android.ui.itl.main.MerchantRegistration.view.adapter.TLSwipeUpAdapter;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.spine.AbstractFragment;
import com.technolab.android.ui.itl.utils.KeyboardUtils;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TypefaceEditText;
import com.technolab.android.utility.EnumBanks;
import com.technolab.android.utility.EnumCompany;
import com.technolab.android.utility.EnumStates;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLMerchantInfoFillUp extends AbstractActivity implements TLSwipeUpAdapter.ItemClickListener, TLMerchantRegistrationMvpView.MerchantInfoDetail, TextWatcher {

    private TLSwipeUpAdapter mBankAdapter;
    private TLSwipeUpAdapter mStateAdapter;
    private TLSwipeUpAdapter mCompanyAdapter;

    private RecyclerView mSliderRecyclerView;
    private TypefaceEditText mBankNameEditText;
    private TypefaceEditText mStateEditText;
    private TypefaceEditText mCompanyTypeEditText;
    private View dim;
    private View mSliderView;
    private SlideUp mSlideUp;
    private TypefaceEditText selectedEditText;
    private Button submitApplication;

    @Inject
    TLMerchantRegistrationPresenter.TLMerchantInfoDetailPresenter merchantRegistrationPresenter;

    public TLMerchantInfoFillUp() {
        TAG = "Merchant Info Fill Up";
        layoutID = R.layout.fragment_merchant_info_fill_up;
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(this);
        merchantRegistrationPresenter.onAttach(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        mBankNameEditText = findViewById(R.id.et_bank_name);
        mStateEditText = findViewById(R.id.et_state);
        mCompanyTypeEditText = findViewById(R.id.et_company_type);
        mSliderView = findViewById(R.id.cl_slide_view);
        mSliderRecyclerView = findViewById(R.id.rv_slider);
        dim = findViewById(R.id.dim);

        EditText companyName = findViewById(R.id.et_company_name);
        EditText companyRegistrationNumber = findViewById(R.id.et_registration_number);
        EditText companyType = findViewById(R.id.et_company_type);
        EditText bankName = findViewById(R.id.et_bank_name);
        EditText accNumber = findViewById(R.id.et_bank_acc_number);
        EditText holderName = findViewById(R.id.et_bank_holder_name);
        EditText email = findViewById(R.id.et_email);
        EditText businessAddress = findViewById(R.id.et_business_address);
        EditText city = findViewById(R.id.et_city);
        EditText state = findViewById(R.id.et_state);
        EditText postcode = findViewById(R.id.et_postcode);
        submitApplication = findViewById(R.id.btn_sign_up_as_merchant);

        submitApplication.setEnabled(false);
        submitApplication.setAlpha(0.3f);

        companyName.addTextChangedListener(this);
        companyRegistrationNumber.addTextChangedListener(this);
        companyType.addTextChangedListener(this);
        bankName.addTextChangedListener(this);
        accNumber.addTextChangedListener(this);
        holderName.addTextChangedListener(this);
        email.addTextChangedListener(this);
        businessAddress.addTextChangedListener(this);
        city.addTextChangedListener(this);
        state.addTextChangedListener(this);
        postcode.addTextChangedListener(this);

        submitApplication.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {

                merchantRegistrationPresenter.uploadInfoDetail(companyName.getText().toString(),
                        companyRegistrationNumber.getText().toString(),
                        companyType.getText().toString(),
                        bankName.getText().toString(),
                        accNumber.getText().toString(),
                        holderName.getText().toString(),
                        email.getText().toString(),
                        businessAddress.getText().toString(),
                        city.getText().toString(),
                        state.getText().toString(),
                        postcode.getText().toString());
            }
        });

        initSlider();
    }

    private void initSlider() {
        ArrayList<TLSwipeUpModel> bankNameList = new ArrayList<>();
        for (EnumBanks banks : EnumBanks.values()) {
            bankNameList.add(new TLSwipeUpModel(banks.getValue()));
        }
        ArrayList<TLSwipeUpModel> stateList = new ArrayList<>();
        for (EnumStates states : EnumStates.values()) {
            stateList.add(new TLSwipeUpModel(states.getValue()));
        }
        ArrayList<TLSwipeUpModel> companyList = new ArrayList<>();
        for (EnumCompany company : EnumCompany.values()) {
            companyList.add(new TLSwipeUpModel(company.getValue()));
        }

        mSliderRecyclerView.setHasFixedSize(false);
        mBankAdapter = new TLSwipeUpAdapter(this, bankNameList);
        mStateAdapter = new TLSwipeUpAdapter(this, stateList);
        mCompanyAdapter = new TLSwipeUpAdapter(this, companyList);

        mSlideUp = new SlideUpBuilder(mSliderView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                        dim.setAlpha(1 - (percent / 100));
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        dim.setVisibility(visibility == 0 ? View.VISIBLE : View.GONE);
                        dim.setClickable(visibility == 0);
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .build();

        dim.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(mSlideUp.isVisible())
                    mSlideUp.hide();
            }
        });

        mBankNameEditText.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mSliderRecyclerView.setAdapter(mBankAdapter);
                mBankAdapter.setOnItemClickListener(TLMerchantInfoFillUp.this);
                KeyboardUtils.hideSoftKeyboard(TLMerchantInfoFillUp.this);
                mSlideUp.show();
                selectedEditText = mBankNameEditText;
            }
        });

        mStateEditText.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mSliderRecyclerView.setAdapter(mStateAdapter);
                mStateAdapter.setOnItemClickListener(TLMerchantInfoFillUp.this);
                KeyboardUtils.hideSoftKeyboard(TLMerchantInfoFillUp.this);
                mSlideUp.show();
                selectedEditText = mStateEditText;
            }
        });

        mCompanyTypeEditText.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mSliderRecyclerView.setAdapter(mCompanyAdapter);
                mCompanyAdapter.setOnItemClickListener(TLMerchantInfoFillUp.this);
                KeyboardUtils.hideSoftKeyboard(TLMerchantInfoFillUp.this);
                mSlideUp.show();
                selectedEditText = mCompanyTypeEditText;
            }
        });
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Business Application");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setImageDrawable(getResources().getDrawable(R.drawable.ic_round_close));
        imgRight2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLMerchantInfoFillUp.this, UserMainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemClicked(TLSwipeUpModel model) {
        if(selectedEditText == mBankNameEditText)
            mBankNameEditText.setText(model.getName());
        else if(selectedEditText == mStateEditText)
            mStateEditText.setText(model.getName());
        else if(selectedEditText == mCompanyTypeEditText)
            mCompanyTypeEditText.setText(model.getName());

        mSlideUp.hide();
    }


    @Override
    public void onDetailsUpload(String message) {
        finish();
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        submitApplication.setEnabled(!TextUtils.isEmpty(s.toString().trim()));
        submitApplication.setAlpha(!TextUtils.isEmpty(s.toString().trim()) ? 1.0f : 0.3f);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
