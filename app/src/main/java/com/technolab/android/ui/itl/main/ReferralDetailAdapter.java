package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.technolab.android.R;
import com.technolab.android.ui.entity.ReferralEmailResult;
import com.technolab.android.ui.entity.TotalSignUpState;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class ReferralDetailAdapter extends RecyclerView.Adapter<ReferralDetailAdapter.ViewHolder>{

    private Context context;
    private ArrayList<ReferralEmailResult> referralEmailResult;

    public ReferralDetailAdapter(Context context, ArrayList<ReferralEmailResult> referralEmailResult){
        this.context = context;
        this.referralEmailResult = referralEmailResult;
    }

    @Override
    public ReferralDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_referral_details, parent, false);
        return new ReferralDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReferralDetailAdapter.ViewHolder holder, int position) {
        ReferralEmailResult result = referralEmailResult.get(position);
        holder.maskedEmail.setText(result.getMask_email());
        String finalDate = null;
        try {
            String date = result.getMember_signed_up_date_and_time();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            finalDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.txtDate.setText(finalDate);
        holder.txtReferred.setText(result.getTotal_referred() > 0 ? String.format("Referred %d friends.", result.getTotal_referred()) : "");
        holder.txtTotalBooster.setText(String.format("%d Booster",result.getTotal_booster()));
    }

    @Override
    public int getItemCount() {
        return referralEmailResult.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView maskedEmail;
        TextView txtDate;
        TextView txtReferred;
        TextView txtTotalBooster;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            maskedEmail = itemView.findViewById(R.id.txt_masked_email);
            txtDate = itemView.findViewById(R.id.txt_date);
            txtReferred = itemView.findViewById(R.id.txt_referred);
            txtTotalBooster = itemView.findViewById(R.id.txt_energy_booster);

        }
    }
}
