package com.technolab.android.ui.itl.utils.drawer.utils;

import android.view.View;

import androidx.drawerlayout.widget.DrawerLayout;

import com.technolab.android.ui.itl.utils.drawer.callback.DragListener;
import com.technolab.android.ui.itl.utils.drawer.callback.DragStateListener;

public class DrawerListenerAdapter implements DragListener, DragStateListener {

    private DrawerLayout.DrawerListener drawerListener;
    private View drawer;

    public DrawerListenerAdapter(DrawerLayout.DrawerListener drawerListener, View drawer) {
        this.drawerListener = drawerListener;
        this.drawer = drawer;
    }

    @Override
    public void onDrag(float progress) {
        drawerListener.onDrawerSlide(drawer, progress);
    }

    @Override
    public void onDragStart() {
        drawerListener.onDrawerStateChanged(DrawerLayout.STATE_DRAGGING);
    }

    @Override
    public void onDragEnd(boolean isMenuOpened) {
        if (isMenuOpened) {
            drawerListener.onDrawerOpened(drawer);
        } else {
            drawerListener.onDrawerClosed(drawer);
        }
        drawerListener.onDrawerStateChanged(DrawerLayout.STATE_IDLE);
    }
}