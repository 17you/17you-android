package com.technolab.android.ui.itl.main.MerchantProductManagement.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.ProductMainCategory;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

public class MerchantProductMainCategoryAdapter extends RecyclerView.Adapter<MerchantProductMainCategoryAdapter.ViewHolder> {

   private ArrayList<ProductMainCategory> mProductMainCategoryArrayList;
   private Context mContext;
   private MerchantProductMainCategoryAdapter.ItemClickListener mCallback;

    public MerchantProductMainCategoryAdapter(Context context, ArrayList<ProductMainCategory> productMainCategoryList) {
        this.mContext = context;
        this.mProductMainCategoryArrayList = productMainCategoryList;
    }

    @NonNull
    @Override
    public MerchantProductMainCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_product_category, parent, false);
        return new MerchantProductMainCategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MerchantProductMainCategoryAdapter.ViewHolder holder, int position) {
        ProductMainCategory productMainCategory = mProductMainCategoryArrayList.get(position);
        holder.tvCategoryName.setText(productMainCategory.getProductMainCategoryName());
        holder.clProductCategory.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
               if (mCallback!=null)
                   mCallback.onItemClicked(productMainCategory);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProductMainCategoryArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategoryName;
        ConstraintLayout clProductCategory;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryName = itemView.findViewById(R.id.tv_category);
            clProductCategory = itemView.findViewById(R.id.cl_product_category);
        }
    }

    public void setOnItemClickListener(final MerchantProductMainCategoryAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(ProductMainCategory productMainCategory);
    }

}
