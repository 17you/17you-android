package com.technolab.android.ui.itl.main;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Address;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLCreateLiveStream;
import com.technolab.android.ui.itl.main.MerchantProductManagement.view.TLCreateProduct;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.KeyboardUtils;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import java.util.ArrayList;

import javax.inject.Inject;

import in.mayanknagwanshi.imagepicker.ImageSelectActivity;

public class ViewProfileActivity extends AbstractActivity implements TLMainMvpView.UserProfile {

    private ImageView imgProfile;
    private TextView txtName, txtEmail, txtPhone, txtExpertise;
    private EditText etName, etEmail, etPhone, etExpertise;
    private String imagePath;

    public ViewProfileActivity(){
        TAG = "View Profile Activity";
        statusBarColorID = R.color.colorD;
        layoutID = R.layout.activity_view_profile;
    }

    @Inject
    TLMainPresenter.TLUserProfilePresenter userProfilePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        imgProfile = findViewById(R.id.img_profile);
        txtName = findViewById(R.id.txt_profile_name);
        txtEmail = findViewById(R.id.txt_email);
        txtPhone = findViewById(R.id.txt_phone);
        txtExpertise = findViewById(R.id.txt_expertise);

        etName = findViewById(R.id.et_profile_name);
        etEmail = findViewById(R.id.et_email);
        etPhone = findViewById(R.id.et_phone);
        etExpertise = findViewById(R.id.et_expertise);

        etName.setText(TLStorage.getInstance().getCurrentUserName());
        etEmail.setText(TLStorage.getInstance().getUserEmail());
        etPhone.setText(TLStorage.getInstance().getUserPhone());

        editMode(false);
        setupData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(ViewProfileActivity.this);
        userProfilePresenter.onAttach(this);
    }

    private void setupData(){
        txtName.setText(TLStorage.getInstance().getCurrentUserName());
        txtEmail.setText(TLStorage.getInstance().getUserEmail());
        txtPhone.setText(TLStorage.getInstance().getUserPhone());
        txtExpertise.setText(TLStorage.getInstance().getUserExpertise());

        etName.setText(TLStorage.getInstance().getCurrentUserName());
        etEmail.setText(TLStorage.getInstance().getUserEmail());
        etPhone.setText(TLStorage.getInstance().getUserPhone());
        etExpertise.setText(TLStorage.getInstance().getUserExpertise());

        Glide.with(this)
                .load(TLStorage.getInstance().getPrefUserImage())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into(imgProfile);
    }

    private void editMode(boolean isEditMode){
        txtName.setVisibility(isEditMode ? View.GONE : View.VISIBLE);
        txtEmail.setVisibility(View.VISIBLE);
        txtPhone.setVisibility(View.VISIBLE);
        txtExpertise.setVisibility(isEditMode ? View.GONE : View.VISIBLE);

        etName.setVisibility(isEditMode ? View.VISIBLE : View.GONE);
        etEmail.setVisibility(View.GONE);
        etPhone.setVisibility(View.GONE);
        etExpertise.setVisibility(isEditMode ? View.VISIBLE : View.GONE);

        imgProfile.setEnabled(isEditMode);
        imgProfile.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(ViewProfileActivity.this, ImageSelectActivity.class);
                intent.putExtra(ImageSelectActivity.FLAG_COMPRESS, true);
                intent.putExtra(ImageSelectActivity.FLAG_CAMERA, false);
                intent.putExtra(ImageSelectActivity.FLAG_GALLERY, true);
                intent.putExtra(ImageSelectActivity.FLAG_CROP, true);
                startActivityForResult(intent, 1213);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1213 && resultCode == Activity.RESULT_OK) {
            String filePath = data.getStringExtra(ImageSelectActivity.RESULT_FILE_PATH);
            imagePath = filePath;
            Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
            Glide.with(this)
                    .load(selectedImage)
                    .thumbnail(0.1f)
                    .centerCrop()
                    .placeholder(getResources().getDrawable(R.drawable.placeholder))
                    .into(imgProfile);
        }
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Profile");
        txtTemp.setVisibility(View.VISIBLE);
        txtTemp.setText("Edit");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        txtTemp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                KeyboardUtils.hideSoftKeyboard(ViewProfileActivity.this);
                if(txtTemp.getText().toString().equalsIgnoreCase("Edit")){
                    editMode(true);
                    txtTemp.setText("Save");
                }else if(txtTemp.getText().toString().equalsIgnoreCase("Save")){
                    String name = !etName.getText().toString().equalsIgnoreCase(txtName.getText().toString()) ? etName.getText().toString() : null;
                    String expertise = !etExpertise.getText().toString().equalsIgnoreCase(txtExpertise.getText().toString()) ? etExpertise.getText().toString() : null;

                    if(name == null && expertise == null && imagePath == null) {
                        Toast.makeText(ViewProfileActivity.this,"No changes made",Toast.LENGTH_SHORT).show();
                        setupData();
                        editMode(false);
                        txtTemp.setText("Edit");
                    }
                    else
                        userProfilePresenter.uploadUserDetail(name, expertise, imagePath);
                }
            }
        });
    }

    @Override
    public void onReceiveAddress(ArrayList<Address> addressList) {

    }

    @Override
    public void onFinish(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        setupData();
        editMode(false);
        txtTemp.setText("Edit");
    }

    @Override
    public void onError(String message) {

    }
}
