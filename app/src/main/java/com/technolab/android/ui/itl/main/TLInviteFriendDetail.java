package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.google.zxing.WriterException;
import com.technolab.android.R;
import com.technolab.android.ui.entity.EnergyLevel;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.Merchant;
import com.technolab.android.ui.entity.ReferralCount;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.ImagePopup;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.user.TLStorage;

import java.util.ArrayList;

import javax.inject.Inject;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class TLInviteFriendDetail extends AbstractActivity implements TLMainMvpView.SwitchMerchantMVP{

    private LinearLayout containerSocialMedia;

    @Inject
    TLMainPresenter.TLSwitchMerchantPresenter switchMerchantPresenter;

    public TLInviteFriendDetail(){
        layoutID  = R.layout.activity_invite_friends_detail;
        statusBarColorID = R.color.colorD;
        TAG = "TLInviteFriendDetail";
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        containerSocialMedia = findViewById(R.id.container_social_media);
        getAppComponent().inject(TLInviteFriendDetail.this);
        switchMerchantPresenter.onAttach(this);

        containerSocialMedia.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                switchMerchantPresenter.addShare();
            }
        });
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        txtHeader.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.VISIBLE);
        imgRight2.setImageDrawable(getResources().getDrawable(R.drawable.ic_qr_code));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        imgRight2.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Uri longURL = DeepLinkUtils.createDeepLink(String.valueOf(TLStorage.getInstance().getReferralCode()), "referral");
                DeepLinkUtils.shortTheLink(longURL, TLInviteFriendDetail.this, "referral");
                Uri shortURL = DeepLinkUtils.getShortLink();

                WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
                Display display = manager.getDefaultDisplay();
                Point point = new Point();
                display.getSize(point);
                int width = point.x;
                int height = point.y;
                int smallerDimension = Math.min(width, height);
                smallerDimension = smallerDimension * 3 / 4;

                QRGEncoder qrgEncoder = new QRGEncoder(shortURL.toString(), null, QRGContents.Type.TEXT, smallerDimension);
                qrgEncoder.setColorBlack(getResources().getColor(R.color.black));
                qrgEncoder.setColorWhite(getResources().getColor(R.color.white));

                final ImagePopup imagePopup = new ImagePopup(TLInviteFriendDetail.this);
                imagePopup.setBackgroundColor(Color.TRANSPARENT);
                imagePopup.setFullScreen(false);
                imagePopup.setHideCloseIcon(true);
                imagePopup.setImageOnClickClose(true);
                imagePopup.initiatePopup(qrgEncoder.getBitmap());
                imagePopup.viewPopup();
            }
        });
    }

    @Override
    public void onFetchMerchantDetails(Boolean isMerchant, String msg, Merchant merchantDetail) {
    }

    @Override
    public void onFetchLiveMarket(ArrayList<Industry> industryArrayList) {
    }

    @Override
    public void onShareSuccessful(String message) {
        Uri longURL = DeepLinkUtils.createDeepLink(String.valueOf(TLStorage.getInstance().getReferralCode()), "referral");
        DeepLinkUtils.shortTheLink(longURL, TLInviteFriendDetail.this, "referral");
        Uri shortURL = DeepLinkUtils.getShortLink();
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT, String.format("%s\n%s", "Join 17You and Start Selling via Live Streaming.\n\nPlease make sure to click on the given link after installing 17You app in your device to earn EXTRA ENERGY !\n", shortURL.toString()));
        startActivity(Intent.createChooser(i, "Share URL"));
    }

    @Override
    public void onFetchReferralEnergyCount(ReferralCount referralCount, ArrayList<EnergyLevel> energyLevels) {
    }

    @Override
    public void onError(String message) {
    }
}
