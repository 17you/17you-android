package com.technolab.android.ui.itl.main.MerchantProductManagement.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.entity.Variation;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.adapter.MerchantLiveStreamAdapter;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.utility.EnumStatus;

import java.util.ArrayList;

public class MerchantProductAdapter extends RecyclerView.Adapter<MerchantProductAdapter.ViewHolder>{

    private Context context;
    private ArrayList<Product> productArrayList;
    private MerchantProductAdapter.ItemClickListener mCallback;

    public MerchantProductAdapter(Context context, ArrayList<Product> productArrayList){
        this.context = context;
        this.productArrayList = productArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_merchant_product, parent, false);
        return new MerchantProductAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = productArrayList.get(position);

        Glide.with(context)
                .load(product.getProduct_image())
                .thumbnail(0.1f)
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.iv_product);

        holder.tv_product_title.setText(product.getProduct_name());
        holder.tv_product_desc.setText(product.getProduct_description());

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(Variation variation : product.getVariationArrayList()){
            View child = inflater.inflate(R.layout.view_text_variation,  null);
            TextView txtVariation = (TextView) child.findViewById(R.id.tv_variation);
            txtVariation.setText(String.format("%s   RM%.2f", variation.getProduct_variation_size(), variation.getProduct_variation_price()));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0,20, 0, 0);
            child.setLayoutParams(params);
            holder.variationContainer.addView(child);
        }

        holder.btn_edit.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(mCallback != null)
                    mCallback.onItemClicked(1, product);
            }
        });

        holder.btn_delete.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(mCallback != null)
                    mCallback.onItemClicked(2, product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_product;
        TextView tv_product_title;
        TextView tv_product_desc;
        TextView tv_quantity;
        ImageView btn_edit;
        ImageView btn_delete;
        LinearLayout variationContainer;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_product = itemView.findViewById(R.id.iv_product);
            tv_product_title = itemView.findViewById(R.id.tv_product_title);
            tv_product_desc = itemView.findViewById(R.id.tv_product_desc);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            btn_edit = itemView.findViewById(R.id.btn_edit);
            btn_delete = itemView.findViewById(R.id.btn_delete);
            variationContainer = itemView.findViewById(R.id.container_variation);
        }
    }

    public void setOnItemClickListener(final MerchantProductAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(int type, Product product);
    }
}
