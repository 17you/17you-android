package com.technolab.android.ui.itl.spine;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.technolab.android.db.SettingsController;
import com.technolab.android.ui.entity.Menu;
import com.technolab.android.utility.GlobalVariables;
import com.technolab.android.utility.TLConstant;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

public class AbstractFragmentContainer extends AbstractFragment {

    protected ArrayList<Menu> menuArrayList;

    protected ArrayList<AbstractFragment> menuFragmentArrayList;

    protected Integer currentMenuIndex;

    protected AbstractFragment currentMenuFragment;

    protected int fragmentContainerID;

    private Boolean menuReload;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        progressBarContainer = fragmentContainerID;
        View view = super.onCreateView(inflater, container, savedInstanceState);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}