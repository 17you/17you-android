package com.technolab.android.ui.itl.main.MerchantRegistration.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.technolab.android.R;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TypefaceButton;
import com.technolab.android.user.TLStorage;

public class TLMerchantOnboardingActivity extends AbstractActivity {

    private TypefaceButton btnSignUpAsMerchant;

    public TLMerchantOnboardingActivity() {
        TAG = "Merchant Onboarding";
        statusBarColorID = R.color.colorD;
        layoutID = R.layout.activity_merchant_onboarding;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        btnSignUpAsMerchant = findViewById(R.id.btn_sign_up_as_merchant);
        btnSignUpAsMerchant.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(TLStorage.getInstance().isFirstStepComplete())
                    startActivity(new Intent(TLMerchantOnboardingActivity.this, TLMerchantInfo.class));
                else
                    startActivity(new Intent(TLMerchantOnboardingActivity.this, TLMerchantChooseIndustry.class));
            }
        });
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Become Merchant");
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.INVISIBLE);
        imgRight2.setVisibility(View.INVISIBLE);
    }


}

