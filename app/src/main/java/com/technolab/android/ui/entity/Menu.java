package com.technolab.android.ui.entity;

public class Menu {
    private String menuID;
    private String description;
    private String menuIcon;
    private String abstractViewID;
    private int isMerchant;
    private String abstractViewName;
    private int menuOrder;
    private String serverMenuID;

    public Menu(String menuID) {
        this.menuID = menuID;
    }

    public String getMenuID() {
        return menuID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getAbstractViewID() {
        return abstractViewID;
    }

    public void setAbstractViewID(String abstractViewID) {
        this.abstractViewID = abstractViewID;
    }

    public String getAbstractViewName() {
        return abstractViewName;
    }

    public void setAbstractViewName(String abstractViewName) {
        this.abstractViewName = abstractViewName;
    }

    public int getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(int menuOrder) {
        this.menuOrder = menuOrder;
    }

    public String getServerMenuID() {
        return serverMenuID;
    }

    public void setServerMenuID(String serverMenuID) {
        this.serverMenuID = serverMenuID;
    }

    public int getMerchant() {
        return isMerchant;
    }

    public void setMerchant(int merchant) {
        isMerchant = merchant;
    }
}
