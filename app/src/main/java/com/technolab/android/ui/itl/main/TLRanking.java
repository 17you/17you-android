package com.technolab.android.ui.itl.main;

import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Ranking;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.RoundedCornerLayout;
import com.technolab.android.ui.itl.utils.badge.BadgeFactory;
import com.technolab.android.ui.itl.utils.badge.BadgeView;
import com.technolab.android.user.TLStorage;

import javax.inject.Inject;

public class TLRanking extends AbstractActivity implements TLMainMvpView.Ranking {

    private ImageView ranking1;
    private ImageView ranking2;
    private ImageView ranking3;

    private TextView rankingName1;
    private TextView rankingName2;
    private TextView rankingName3;

    private TextView rankingValue1;
    private TextView rankingValue2;
    private TextView rankingValue3;

    private ToggleButton toggleDaily;
    private ToggleButton toggleWeekly;
    private ToggleButton toggleMontly;

    private RecyclerView recyclerView;
    private RankingAdapter rankingAdapter;
    private int selectedType = 0;

    public TLRanking(){
        TAG = "TLRanking";
        layoutID = R.layout.activity_ranking;
        statusBarColorID = R.color.colorD;
    }

    @Inject
    TLMainPresenter.TLRanking rankingPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        setupRefresh(this);
        ranking1 = findViewById(R.id.ranking1);
        ranking2 = findViewById(R.id.ranking2);
        ranking3 = findViewById(R.id.ranking3);

        rankingName1 = findViewById(R.id.ranking_name_1);
        rankingName2 = findViewById(R.id.ranking_name_2);
        rankingName3 = findViewById(R.id.ranking_name_3);

        rankingValue1 = findViewById(R.id.ranking_value_1);
        rankingValue2 = findViewById(R.id.ranking_value_2);
        rankingValue3 = findViewById(R.id.ranking_value_3);

        toggleDaily = findViewById(R.id.toggle_daily);
        toggleWeekly = findViewById(R.id.toggle_week);
        toggleMontly = findViewById(R.id.toggle_month);

        recyclerView = findViewById(R.id.recycler_view);

        BadgeFactory.createHexagon(this)
                .setTextColor(Color.WHITE)
                .setWidthAndHeight(50,50)
                .setBadgeBackground(getResources().getColor(R.color.green))
                .setTextSize(15)
                .setMargin(0,0,0,30)
                .setBadgeGravity(Gravity.TOP | Gravity.CENTER)
                .setBadgeCount(1)
                .setShape(BadgeView.SHAPE_HEXAGON)
                .bind(findViewById(R.id.ranking1));

        BadgeFactory.createHexagon(this)
                .setTextColor(Color.WHITE)
                .setWidthAndHeight(50,50)
                .setBadgeBackground(getResources().getColor(R.color.green))
                .setTextSize(15)
                .setMargin(0,0,0,30)
                .setBadgeGravity(Gravity.TOP | Gravity.CENTER)
                .setBadgeCount(3)
                .setShape(BadgeView.SHAPE_HEXAGON)
                .bind(findViewById(R.id.ranking3));

        BadgeFactory.createHexagon(this)
                .setTextColor(Color.WHITE)
                .setWidthAndHeight(50,50)
                .setBadgeBackground(getResources().getColor(R.color.green))
                .setTextSize(15)
                .setMargin(0,0,0,30)
                .setBadgeGravity(Gravity.TOP | Gravity.CENTER)
                .setBadgeCount(2)
                .setShape(BadgeView.SHAPE_HEXAGON)
                .bind(findViewById(R.id.ranking2));

        toggleDaily.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                selectedType = 0;
                rankingPresenter.retrieveRanking(selectedType);
            }
        });

        toggleWeekly.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                selectedType = 1;
                rankingPresenter.retrieveRanking(selectedType);
            }
        });

        toggleMontly.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                selectedType = 2;
                rankingPresenter.retrieveRanking(selectedType);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLRanking.this);
        rankingPresenter.onAttach(this);
        rankingPresenter.retrieveRanking(selectedType);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Ranking");
        txtSubHeader.setVisibility(View.VISIBLE);
        txtSubHeader.setText("Leadership board for referral");
        txtSubHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);

        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void setupRefresh(AbstractActivity activity) {
        super.setupRefresh(activity);
        refreshLayout.setOnRefreshListener(() -> rankingPresenter.retrieveRanking(selectedType));
    }

    @Override
    public void onReceiveData(Ranking ranking, int type) {
        refreshLayout.setRefreshing(false);
        switch (type) {
            case 0:
                rankingAdapter = new RankingAdapter(this, ranking.getDaily().getOthers());
                recyclerView.setAdapter(rankingAdapter);
                rankingAdapter.notifyDataSetChanged();
                Glide.with(this)
                        .load(ranking.getDaily().getTop().get(0).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking1);

                rankingName1.setText(ranking.getDaily().getTop().get(0).getMember_username());
                rankingValue1.setText(String.valueOf(ranking.getDaily().getTop().get(0).getTotal_sign_up()));

                Glide.with(this)
                        .load(ranking.getDaily().getTop().get(1).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking2);

                rankingName2.setText(ranking.getDaily().getTop().get(1).getMember_username());
                rankingValue2.setText(String.valueOf(ranking.getDaily().getTop().get(1).getTotal_sign_up()));

                Glide.with(this)
                        .load(ranking.getDaily().getTop().get(2).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking3);

                rankingName3.setText(ranking.getDaily().getTop().get(2).getMember_username());
                rankingValue3.setText(String.valueOf(ranking.getDaily().getTop().get(2).getTotal_sign_up()));
                break;
            case 1:
                rankingAdapter = new RankingAdapter(this, ranking.getWeekly().getOthers());
                recyclerView.setAdapter(rankingAdapter);
                rankingAdapter.notifyDataSetChanged();
                Glide.with(this)
                        .load(ranking.getWeekly().getTop().get(0).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking1);

                rankingName1.setText(ranking.getWeekly().getTop().get(0).getMember_username());
                rankingValue1.setText(String.valueOf(ranking.getWeekly().getTop().get(0).getTotal_sign_up()));

                Glide.with(this)
                        .load(ranking.getWeekly().getTop().get(1).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking2);

                rankingName2.setText(ranking.getWeekly().getTop().get(1).getMember_username());
                rankingValue2.setText(String.valueOf(ranking.getWeekly().getTop().get(1).getTotal_sign_up()));

                Glide.with(this)
                        .load(ranking.getWeekly().getTop().get(2).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking3);

                rankingName3.setText(ranking.getWeekly().getTop().get(2).getMember_username());
                rankingValue3.setText(String.valueOf(ranking.getWeekly().getTop().get(2).getTotal_sign_up()));
                break;
            case 2:
                rankingAdapter = new RankingAdapter(this, ranking.getMonthly().getOthers());
                recyclerView.setAdapter(rankingAdapter);
                rankingAdapter.notifyDataSetChanged();
                Glide.with(this)
                        .load(ranking.getMonthly().getTop().get(0).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking1);

                rankingName1.setText(ranking.getMonthly().getTop().get(0).getMember_username());
                rankingValue1.setText(String.valueOf(ranking.getMonthly().getTop().get(0).getTotal_sign_up()));

                Glide.with(this)
                        .load(ranking.getMonthly().getTop().get(1).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking2);

                rankingName2.setText(ranking.getMonthly().getTop().get(1).getMember_username());
                rankingValue2.setText(String.valueOf(ranking.getMonthly().getTop().get(1).getTotal_sign_up()));

                Glide.with(this)
                        .load(ranking.getMonthly().getTop().get(2).getDefault_ranking_image())
                        .thumbnail(0.1f)
                        .centerCrop()
                        .placeholder(getResources().getDrawable(R.drawable.placeholder))
                        .into(ranking3);

                rankingName3.setText(ranking.getMonthly().getTop().get(2).getMember_username());
                rankingValue3.setText(String.valueOf(ranking.getMonthly().getTop().get(2).getTotal_sign_up()));
                break;
        }
    }

    @Override
    public void onError(String message) {
        refreshLayout.setRefreshing(false);
    }
}
