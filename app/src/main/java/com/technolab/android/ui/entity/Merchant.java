package com.technolab.android.ui.entity;

import com.google.gson.annotations.SerializedName;

public class Merchant {
    @SerializedName("merchant_id")
    private int merchantId;
    @SerializedName("merchant_ic_front_image")
    private String merchantIcFrontImage;
    @SerializedName("merchant_ic_back_image")
    private String merchantIcBackImage;
    @SerializedName("merchant_selfie_image")
    private String merchantSelfieImage;
    @SerializedName("merchant_company_name")
    private String merchantCompanyName;
    @SerializedName("merchant_company_reg_number")
    private String merchantCompanyRegNumber;
    @SerializedName("merchant_bank_number")
    private long merchantBankNumber;
    @SerializedName("merchant_email")
    private String merchantEmail;
    @SerializedName("merchant_street_address")
    private String merchantStreetAddress;
    @SerializedName("merchant_city")
    private String merchantCity;
    @SerializedName("merchant_state")
    private String merchantState;
    @SerializedName("merchant_postcode")
    private String merchantPostcode;
    @SerializedName("member_id")
    private int memberId;
    @SerializedName("industry_category_id")
    private int industryCategoryId;
    @SerializedName("merchant_account_status")
    private Boolean merchantAccountStatus;
    @SerializedName("merchant_bank_name")
    private String merchantBankName;
    @SerializedName("merchant_bank_holder_name")
    private String merchantBankAccHolder;
    @SerializedName("merchant_image")
    private String merchantImage;
    @SerializedName("is_fresh_merchant")
    private Boolean isFreshMerchant;
    @SerializedName("remaining_live_time")
    private String remainingLiveTime;

    public String getRemainingLiveTime() {
        return remainingLiveTime;
    }

    public void setRemainingLiveTime(String remainingLiveTime) {
        this.remainingLiveTime = remainingLiveTime;
    }

    public Boolean getFreshMerchant() {
        return isFreshMerchant;
    }

    public void setFreshMerchant(Boolean freshMerchant) {
        isFreshMerchant = freshMerchant;
    }

    public String getMerchantImage() {
        return merchantImage;
    }

    public void setMerchantImage(String merchantImage) {
        this.merchantImage = merchantImage;
    }

    public String getMerchantBankAccHolder() {
        return merchantBankAccHolder;
    }

    public void setMerchantBankAccHolder(String merchantBankAccHolder) {
        this.merchantBankAccHolder = merchantBankAccHolder;
    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantIcFrontImage() {
        return merchantIcFrontImage;
    }

    public void setMerchantIcFrontImage(String merchantIcFrontImage) {
        this.merchantIcFrontImage = merchantIcFrontImage;
    }

    public String getMerchantIcBackImage() {
        return merchantIcBackImage;
    }

    public void setMerchantIcBackImage(String merchantIcBackImage) {
        this.merchantIcBackImage = merchantIcBackImage;
    }

    public String getMerchantSelfieImage() {
        return merchantSelfieImage;
    }

    public void setMerchantSelfieImage(String merchantSelfieImage) {
        this.merchantSelfieImage = merchantSelfieImage;
    }

    public String getMerchantCompanyName() {
        return merchantCompanyName;
    }

    public void setMerchantCompanyName(String merchantCompanyName) {
        this.merchantCompanyName = merchantCompanyName;
    }

    public String getMerchantCompanyRegNumber() {
        return merchantCompanyRegNumber;
    }

    public void setMerchantCompanyRegNumber(String merchantCompanyRegNumber) {
        this.merchantCompanyRegNumber = merchantCompanyRegNumber;
    }

    public long getMerchantBankNumber() {
        return merchantBankNumber;
    }

    public void setMerchantBankNumber(long merchantBankNumber) {
        this.merchantBankNumber = merchantBankNumber;
    }

    public String getMerchantEmail() {
        return merchantEmail;
    }

    public void setMerchantEmail(String merchantEmail) {
        this.merchantEmail = merchantEmail;
    }

    public String getMerchantStreetAddress() {
        return merchantStreetAddress;
    }

    public void setMerchantStreetAddress(String merchantStreetAddress) {
        this.merchantStreetAddress = merchantStreetAddress;
    }

    public String getMerchantCity() {
        return merchantCity;
    }

    public void setMerchantCity(String merchantCity) {
        this.merchantCity = merchantCity;
    }

    public String getMerchantState() {
        return merchantState;
    }

    public void setMerchantState(String merchantState) {
        this.merchantState = merchantState;
    }

    public String getMerchantPostcode() {
        return merchantPostcode;
    }

    public void setMerchantPostcode(String merchantPostcode) {
        this.merchantPostcode = merchantPostcode;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getIndustryCategoryId() {
        return industryCategoryId;
    }

    public void setIndustryCategoryId(int industryCategoryId) {
        this.industryCategoryId = industryCategoryId;
    }

    public Boolean getMerchantAccountStatus() {
        return merchantAccountStatus;
    }

    public void setMerchantAccountStatus(Boolean merchantAccountStatus) {
        this.merchantAccountStatus = merchantAccountStatus;
    }

    public String getMerchantBankName() {
        return merchantBankName;
    }

    public void setMerchantBankName(String merchantBankName) {
        this.merchantBankName = merchantBankName;
    }
}
