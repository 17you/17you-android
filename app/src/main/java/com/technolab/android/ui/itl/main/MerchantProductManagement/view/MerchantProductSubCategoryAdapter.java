package com.technolab.android.ui.itl.main.MerchantProductManagement.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.ProductMainCategory;
import com.technolab.android.ui.entity.ProductSubCategory;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

public class MerchantProductSubCategoryAdapter extends RecyclerView.Adapter<MerchantProductSubCategoryAdapter.ViewHolder> {

    private ArrayList<ProductSubCategory> mProductSubCategoryArrayList;
    private Context mContext;
    private MerchantProductSubCategoryAdapter.ItemClickListener mCallback;

    public MerchantProductSubCategoryAdapter(Context context, ArrayList<ProductSubCategory> productSubCategoryList) {
        this.mContext = context;
        this.mProductSubCategoryArrayList = productSubCategoryList;
    }

    @NonNull
    @Override
    public MerchantProductSubCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_product_category, parent, false);
        return new MerchantProductSubCategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MerchantProductSubCategoryAdapter.ViewHolder holder, int position) {
        ProductSubCategory productMainCategory = mProductSubCategoryArrayList.get(position);
        holder.tvCategoryName.setText(productMainCategory.getProductSubCategoryName());
        holder.ivRightArrow.setVisibility(View.INVISIBLE);
        holder.clProductCategory.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null)
                    mCallback.onItemClicked(productMainCategory);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mProductSubCategoryArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCategoryName;
        ConstraintLayout clProductCategory;
        ImageView ivRightArrow;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryName = itemView.findViewById(R.id.tv_category);
            clProductCategory = itemView.findViewById(R.id.cl_product_category);
            ivRightArrow = itemView.findViewById(R.id.iv_right_arrow);
        }
    }

    public void setOnItemClickListener(final MerchantProductSubCategoryAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(ProductSubCategory productSubCategory);
    }

}
