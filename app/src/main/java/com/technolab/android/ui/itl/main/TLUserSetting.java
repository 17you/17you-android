package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.technolab.android.BuildConfig;
import com.technolab.android.R;
import com.technolab.android.ui.entity.EnergyLevel;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.Merchant;
import com.technolab.android.ui.entity.ReferralCount;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword.TLForgotPassword2;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword.TLForgotPassword3;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLSignInEmail;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.badge.BadgeFactory;
import com.technolab.android.ui.itl.utils.badge.BadgeView;
import com.technolab.android.user.TLStorage;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLUserSetting extends AbstractActivity implements TLMainMvpView.SwitchMerchantMVP{

    private LinearLayout processingContainer;
    private LinearLayout shippedContainer;
    private LinearLayout deliveredContainer;
    private LinearLayout allContainer;

    @Inject
    TLMainPresenter.TLSwitchMerchantPresenter switchMerchantPresenter;

    public TLUserSetting(){
        TAG = "TLUserSetting";
        layoutID = R.layout.activity_user_setting;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);

        processingContainer = findViewById(R.id.container_processing);
        shippedContainer = findViewById(R.id.container_shipped);
        deliveredContainer = findViewById(R.id.container_delivered);
        allContainer = findViewById(R.id.container_all);

        processingContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLUserSetting.this, TLMyPurchases.class);
                intent.putExtra("viewType", 1);
                startActivity(intent);
            }
        });

        shippedContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLUserSetting.this, TLMyPurchases.class);
                intent.putExtra("viewType", 2);
                startActivity(intent);
            }
        });

        deliveredContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLUserSetting.this, TLMyPurchases.class);
                intent.putExtra("viewType", 3);
                startActivity(intent);
            }
        });

        allContainer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLUserSetting.this, TLMyPurchases.class);
                intent.putExtra("viewType", 4);
                startActivity(intent);
            }
        });

        findViewById(R.id.container_view_profile).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLUserSetting.this, ViewProfileActivity.class));
            }
        });
        TextView profileName = findViewById(R.id.txt_profile_name);
        profileName.setText(TLStorage.getInstance().getCurrentUserName());

        findViewById(R.id.container_logout).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLUserSetting.this, TLSignInEmail.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                TLStorage.resetInstance();
            }
        });

        TextView txtVersion = findViewById(R.id.txt_version);
        txtVersion.setText(String.format("Version %s.%d", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));

        findViewById(R.id.merchant_dashboard_container).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                processSwitchMerchant();
            }
        });

        findViewById(R.id.container_delivery_address).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLUserSetting.this, TLAddressSetting.class));
            }
        });

        findViewById(R.id.container_notification).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLUserSetting.this, TLUserNotification.class));
            }
        });

        findViewById(R.id.container_change_password).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(TLUserSetting.this, TLForgotPassword3.class);
                intent.putExtra("phone", TLStorage.getInstance().getUserPhone());
                startActivity(intent);
            }
        });

        findViewById(R.id.container_energy_history).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                startActivity(new Intent(TLUserSetting.this, TLEnergyHistory.class));
            }
        });

        Glide.with(this)
                .load(TLStorage.getInstance().getPrefUserImage())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(getResources().getDrawable(R.drawable.placeholder))
                .into((ImageView)findViewById(R.id.img_profile));
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Setting");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLUserSetting.this);
        switchMerchantPresenter.onAttach(this);
    }

    private void processSwitchMerchant(){
        switchMerchantPresenter.fetchMerchantDetail();
    }

    @Override
    public void onFetchMerchantDetails(Boolean isMerchant, String msg, Merchant merchantDetail) {
        if(!isMerchant) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

            return;
        }

        TLStorage.getInstance().setMerchantId(String.valueOf(merchantDetail.getMerchantId()));
        TLStorage.getInstance().setMerchantCompanyName(merchantDetail.getMerchantCompanyName());
        TLStorage.getInstance().setMerchantImage(merchantDetail.getMerchantImage());
        TLStorage.getInstance().setMerchantFresh(merchantDetail.getFreshMerchant());
        TLStorage.getInstance().setTotalLiveTime(Long.parseLong(merchantDetail.getRemainingLiveTime()));

        finish();
        startActivity(new Intent(TLUserSetting.this, MerchantMainActivity.class));
    }

    @Override
    public void onFetchLiveMarket(ArrayList<Industry> industryArrayList) {

    }

    @Override
    public void onShareSuccessful(String message) {

    }

    @Override
    public void onFetchReferralEnergyCount(ReferralCount referralCount, ArrayList<EnergyLevel> energyLevels) {

    }

    @Override
    public void onError(String message) {

    }
}
