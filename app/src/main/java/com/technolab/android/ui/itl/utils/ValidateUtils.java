package com.technolab.android.ui.itl.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.technolab.android.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtils {
    private Activity mActivity;
    private List<View> xFields = new ArrayList<>();
    private List<HashMap<String, Object>> maps = new ArrayList<>();

    private boolean isAnimationEnabled = true, isErrorEnabled = true;


    public ValidateUtils(Activity activity) {
        mActivity = activity;
    }



    public ValidateUtils addField(View view) {
        xFields.add(view);
        HashMap<String, Object> map = new HashMap<>();
        map.put("defaultHintText", ((EditText) view).getHint().toString());
        map.put("defaultHintColor", ((EditText) view).getCurrentHintTextColor());
        map.put("defaultTextColor", ((EditText) view).getCurrentTextColor());
        maps.add(map);
        return this;
    }

    public boolean isValid() {
        for (View view : xFields) {
            if (isEmpty(view, xFields.indexOf(view))) {
                view.setFocusableInTouchMode(true);
                view.requestFocus();
                return false;
            }
        }
        return true;
    }


    private boolean isEmpty(View view, int index) {
        if (view instanceof EditText) {
            if (TextUtils.isEmpty(((EditText) view).getText().toString().trim())) {
                ((EditText) view).setText("");
                ((EditText) view).setHint(mActivity.getString(R.string.error_empty_field));
                ((EditText) view).setHintTextColor(Color.RED);
                return true;
            }


            if (((EditText) view).getInputType() == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS + 1) {
                if (!isEmailValid(((EditText) view).getText().toString().trim())) {
                    ((EditText) view).setText("");
                    ((EditText) view).setHintTextColor(Color.RED);
                    ((EditText) view).setHint(mActivity.getString(R.string.error_invalid_email));
                    return true;
                }
            }


            if (((EditText) view).getInputType() == InputType.TYPE_TEXT_VARIATION_PASSWORD + 1) {
                if (!isPasswordValid(((EditText) view).getText().toString().trim())) {
                    ((EditText) view).setText("");
                    ((EditText) view).setHintTextColor(Color.RED);
                    ((EditText) view).setHint(mActivity.getString(R.string.error_invalid_password));
                    return true;
                }
            }

            ((EditText) view).setHint((CharSequence) maps.get(index).get("defaultHintText"));
            ((EditText) view).setHintTextColor((Integer) maps.get(index).get("defaultHintColor"));
            ((EditText) view).setTextColor((Integer) maps.get(index).get("defaultTextColor"));
        }

        return false;
    }

    private boolean isPasswordValid (String password){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).{8,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    private boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @SuppressLint("ClickableViewAccessibility")
    public static void showHidePassword(EditText etPassword) {
        etPassword.setOnTouchListener((v, event) -> {
            final int DRAWABLE_RIGHT = 2;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (etPassword.getRight() - etPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                    if (etPassword.getTransformationMethod() == HideReturnsTransformationMethod.getInstance()) {
                        etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        etPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_lock, 0, R.drawable.ico_see_password, 0);
                    }
                    else
                    {
                        etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        etPassword.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ico_lock, 0, R.drawable.ico_unsee_password, 0);
                    }

                    return true;
                }
            }
            return false;
        });
    }


    public ValidateUtils remove(View view) {
        for (View v : xFields) {
            if (v.equals(view)) {
                xFields.remove(v);
                break;
            }
        }
        return this;
    }

    public void empty() {
        xFields.clear();
    }


    public void onDestroy() {
        xFields = null;
        maps = null;
        mActivity = null;
    }
}
