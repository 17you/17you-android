package com.technolab.android.ui.itl.utils.circularprogressbar;

public interface OnCircularSeekBarChangeListener {
    void onProgressChanged(CircularProgressBar circularBar, int progress, boolean fromUser);
    void onClick(CircularProgressBar circularBar);
    void onLongPress(CircularProgressBar circularBar);
}
