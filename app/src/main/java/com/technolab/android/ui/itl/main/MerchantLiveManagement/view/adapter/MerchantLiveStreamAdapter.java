package com.technolab.android.ui.itl.main.MerchantLiveManagement.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.LiveStream;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.utility.EnumLiveStreamStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class MerchantLiveStreamAdapter extends RecyclerView.Adapter<MerchantLiveStreamAdapter.ViewHolder>{

    private Context context;
    private ArrayList<LiveStream> liveStreamArrayList;
    private MerchantLiveStreamAdapter.ItemClickListener mCallback;

    public MerchantLiveStreamAdapter(Context context, ArrayList<LiveStream> liveStreamArrayList){
        this.context = context;
        this.liveStreamArrayList = liveStreamArrayList;
    }

    @NonNull
    @Override
    public MerchantLiveStreamAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_merchant_live_stream, parent, false);
        return new MerchantLiveStreamAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MerchantLiveStreamAdapter.ViewHolder holder, int position) {
        LiveStream liveStream = liveStreamArrayList.get(position);

        Glide.with(context)
                .load(liveStream.getLive_stream_image())
                .thumbnail(0.1f)
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.iv_live_stream);

        holder.tv_live_stream_title.setText(liveStream.getLive_stream_title());
        holder.tv_live_stream_desc.setText(liveStream.getLive_stream_description());

        String finalDate = "";
        String finalTime = "";
        try {
            String date = liveStream.getLive_stream_date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("dd/MM/yyyy");
            finalDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            String startTime = liveStream.getLive_stream_start_time();
            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);
            Date newDate = format.parse(startTime);
            format = new SimpleDateFormat("HH:mm aa");
            finalTime = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tv_live_stream_date.setText(String.format("Live on %s , %s", finalDate, finalTime));
    
        if(liveStream.getLive_stream_status().equalsIgnoreCase(EnumLiveStreamStatus.PUBLISHED.getValue())){
            holder.tv_live_stream_status.setText("\u2022 Published");
            holder.tv_live_stream_status.setTextColor(context.getResources().getColor(R.color.green));
        }else if(liveStream.getLive_stream_status().equalsIgnoreCase(EnumLiveStreamStatus.LIVE_NOW.getValue())){
            holder.tv_live_stream_status.setText("\u2022 Live Now");
            holder.tv_live_stream_status.setTextColor(context.getResources().getColor(R.color.colorO));
        }else if(liveStream.getLive_stream_status().equalsIgnoreCase(EnumLiveStreamStatus.SAVED.getValue())){
            holder.tv_live_stream_status.setText("\u2022 Pending Publish");
            holder.tv_live_stream_status.setTextColor(context.getResources().getColor(R.color.colorRed));
        }else if(liveStream.getLive_stream_status().equalsIgnoreCase(EnumLiveStreamStatus.FINISHED.getValue())){
            holder.tv_live_stream_status.setText("\u2022 Completed");
            holder.tv_live_stream_status.setTextColor(context.getResources().getColor(R.color.colorBlue));
        }else if(liveStream.getLive_stream_status().equalsIgnoreCase(EnumLiveStreamStatus.EXPIRED.getValue())){
            holder.tv_live_stream_status.setText("\u2022 Expired");
            holder.tv_live_stream_status.setTextColor(context.getResources().getColor(R.color.colorGray));
        }

        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onItemClicked(liveStream);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return liveStreamArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_live_stream;
        TextView tv_live_stream_title,tv_live_stream_desc, tv_live_stream_status,tv_live_stream_date;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_live_stream = itemView.findViewById(R.id.iv_live_stream);
            tv_live_stream_title = itemView.findViewById(R.id.tv_live_stream_title);
            tv_live_stream_desc = itemView.findViewById(R.id.tv_live_stream_desc);
            tv_live_stream_status = itemView.findViewById(R.id.tv_live_stream_status);
            tv_live_stream_date = itemView.findViewById(R.id.tv_live_stream_date);
        }
    }

    public void setOnItemClickListener(final MerchantLiveStreamAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(LiveStream liveStream);
    }
}
