package com.technolab.android.ui.itl.spine;

public interface AbstractMvpView {
void showLoadingSpinner();
void hideLoadingSpinner();
}
