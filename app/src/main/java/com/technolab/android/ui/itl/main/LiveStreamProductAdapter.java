package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Product;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class LiveStreamProductAdapter extends RecyclerView.Adapter<LiveStreamProductAdapter.ViewHolder>{

    private LiveStreamProductAdapter.ItemClickListener mCallback;
    ArrayList<Product> productArrayList;
    Context mContext;

    public LiveStreamProductAdapter(Context mContext, ArrayList<Product> productArrayList){
        this.productArrayList = productArrayList;
        this.mContext = mContext;
    }

    @Override
    public LiveStreamProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_product_live_stream, parent, false);
        return new LiveStreamProductAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LiveStreamProductAdapter.ViewHolder holder, int position) {
        Product product = productArrayList.get(position);

        Glide.with(mContext)
                .load(product.getProduct_image())
                .thumbnail(0.1f)
                .placeholder(mContext.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.imgProduct);

        holder.productTitle.setText(product.getProduct_name());
        holder.productDesc.setText(product.getProduct_description());

        holder.btnBuy.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onItemClicked(product);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return productArrayList != null ? productArrayList.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProduct;
        TextView productTitle;
        TextView productDesc;
        Button btnBuy;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.img_product);
            productTitle = itemView.findViewById(R.id.tv_product_title);
            productDesc = itemView.findViewById(R.id.tv_product_desc);
            btnBuy = itemView.findViewById(R.id.btn_buy);
        }
    }

    public void setOnItemClickListener(final LiveStreamProductAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(Product product);
    }
}
