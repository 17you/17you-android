package com.technolab.android.ui.itl.main.LoginManagement.presenter;

import com.technolab.android.error.TLError;
import com.technolab.android.ui.itl.spine.AbstractPresenter;
import com.technolab.android.user.TLStorage;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class TLSignInPresenter extends AbstractPresenter<TLSignInMvpView> {

    @Inject
    public TLSignInPresenter() {

    }

    public void signInViaEmail(String email, String password) {
        getMvpView().showLoadingSpinner();
        JSONObject object = new JSONObject();
        try {
            object.put("email", email);
            object.put("password", password);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
        mCompositeDisposable.add(
                mDataRepositoryManager
                        .login(requestBody)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(baseResponse -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            getMvpView().hideLoadingSpinner();
                            if (baseResponse != null && baseResponse.getStatus()) {
                                getMvpView().onLoginSuccess(baseResponse.getMessage());
                                TLStorage.getInstance().setCurrentUserId(baseResponse.getLoginResponse().getMemberId().toString());
                                TLStorage.getInstance().setCurrentUserName(baseResponse.getLoginResponse().getMemberUsername());
                                TLStorage.getInstance().setUserEmail(baseResponse.getLoginResponse().getMemberEmail());
                                TLStorage.getInstance().setUserPhone(baseResponse.getLoginResponse().getMemberPhone());
                                TLStorage.getInstance().setUserImage(baseResponse.getLoginResponse().getMemberImage());
                                TLStorage.getInstance().setUserExpertise(baseResponse.getLoginResponse().getMemberExpertise());
                                TLStorage.getInstance().setReferralCode(baseResponse.getLoginResponse().getMemberReferralCode());
                            }
                        }, throwable -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            TLError error = TLError.checkForError(throwable);
                            if (error != null)
                            {
                                System.out.println("Testing :"+error.getErrorCode());
                                System.out.println("Testing :"+error.getErrorMessage());
                            }

                            getMvpView().hideLoadingSpinner();
                            getMvpView().onLoginFailed(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                        }));
    }

    public void signInViaPhone(String phone){
        getMvpView().showLoadingSpinner();
        JSONObject object = new JSONObject();
        try {
            object.put("phone", phone);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
        mCompositeDisposable.add(
                mDataRepositoryManager
                        .loginWithPhone(requestBody)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(baseResponse -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            getMvpView().hideLoadingSpinner();
                            if (baseResponse != null && baseResponse.getStatus()) {
                                getMvpView().onLoginSuccess(baseResponse.getMessage());
                                TLStorage.getInstance().setCurrentUserId(baseResponse.getLoginResponse().getMemberId().toString());
                                TLStorage.getInstance().setCurrentUserName(baseResponse.getLoginResponse().getMemberUsername());
                                TLStorage.getInstance().setUserEmail(baseResponse.getLoginResponse().getMemberEmail());
                                TLStorage.getInstance().setUserPhone(baseResponse.getLoginResponse().getMemberPhone());
                                TLStorage.getInstance().setUserImage(baseResponse.getLoginResponse().getMemberImage());
                                TLStorage.getInstance().setUserExpertise(baseResponse.getLoginResponse().getMemberExpertise());
                                TLStorage.getInstance().setReferralCode(baseResponse.getLoginResponse().getMemberReferralCode());
                            }
                        }, throwable -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            TLError error = TLError.checkForError(throwable);
                            if (error != null)
                            {
                                System.out.println("Testing :"+error.getErrorCode());
                                System.out.println("Testing :"+error.getErrorMessage());
                            }

                            getMvpView().hideLoadingSpinner();
                            getMvpView().onLoginFailed(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());

                        }));
    }

    public void phoneNumberCheck(String phone){
        getMvpView().showLoadingSpinner();
        JSONObject object = new JSONObject();
        try {
            object.put("phone", phone);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
        mCompositeDisposable.add(
                mDataRepositoryManager
                        .checkPhoneNumber(requestBody)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(baseResponse -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            getMvpView().hideLoadingSpinner();
                            if (baseResponse != null && baseResponse.getStatus()) {
                                getMvpView().onCheck(baseResponse.getMessage());
                            }
                        }, throwable -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            TLError error = TLError.checkForError(throwable);
                            if (error != null)
                            {
                                System.out.println("Testing :"+error.getErrorCode());
                                System.out.println("Testing :"+error.getErrorMessage());
                            }

                            getMvpView().hideLoadingSpinner();
                            getMvpView().onLoginFailed(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                        }));
    }

    public void updatePassword(String phone, String pass){
        getMvpView().showLoadingSpinner();
        JSONObject object = new JSONObject();
        try {
            object.put("phone", phone);
            object.put("password", pass);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(object.toString(), MediaType.parse("raw"));
        mCompositeDisposable.add(
                mDataRepositoryManager
                        .updatePassword(requestBody)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(baseResponse -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            getMvpView().hideLoadingSpinner();
                            if (baseResponse != null && baseResponse.getStatus()) {
                                getMvpView().onLoginSuccess(baseResponse.getMessage());
                            }
                        }, throwable -> {
                            if (!isViewAttached()) {
                                return;
                            }
                            TLError error = TLError.checkForError(throwable);
                            if (error != null)
                            {
                                System.out.println("Testing :"+error.getErrorCode());
                                System.out.println("Testing :"+error.getErrorMessage());
                            }

                            getMvpView().hideLoadingSpinner();
                            getMvpView().onLoginFailed(!error.getErrorMessage().equals("") ? error.getErrorMessage() : throwable.getMessage());
                        }));
    }
}
