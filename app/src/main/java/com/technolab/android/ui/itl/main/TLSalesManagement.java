package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.DetailedTransaction;
import com.technolab.android.ui.entity.Transaction;
import com.technolab.android.ui.entity.TransactionHistory;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TLActionSheet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

public class TLSalesManagement extends AbstractActivity implements TLMainMvpView.MerchantTransaction, SalesManagementAdapter.ItemClickListener{

    private int viewType = 1;
    private RecyclerView recyclerView;
    private SalesManagementAdapter salesManagementAdapter;
    private LinearLayout containerNoData;

    private LinearLayout containerProcessing;
    private LinearLayout containerShipped;
    private LinearLayout containerDelivered;
    private LinearLayout containerAll;
    private TextView txtFilter;

    @Inject
    TLMainPresenter.TLMerchantTransaction merchantTransactionPresenter;

    public TLSalesManagement(){
        TAG = "TLSalesManagement";
        layoutID = R.layout.activity_sales_management;
        statusBarColorID = R.color.colorBlue;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewType = 1;
        recyclerView = findViewById(R.id.recycler_view);
        containerNoData = findViewById(R.id.container_no_data);
        setupToolbar(this);

        containerProcessing = findViewById(R.id.container_processing);
        containerShipped = findViewById(R.id.container_shipped);
        containerDelivered = findViewById(R.id.container_delivered);
        containerAll = findViewById(R.id.container_all);
        txtFilter = findViewById(R.id.txt_filter);

        containerProcessing.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                containerNoData.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                viewType = 1;
                merchantTransactionPresenter.fetchTransactions(viewType);
            }
        });

        containerShipped.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                containerNoData.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                viewType = 2;
                merchantTransactionPresenter.fetchTransactions(viewType);
            }
        });

        containerDelivered.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                containerNoData.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                viewType = 3;
                merchantTransactionPresenter.fetchTransactions(viewType);
            }
        });

        containerAll.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                containerNoData.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                viewType = 4;
                merchantTransactionPresenter.fetchTransactions(viewType);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLSalesManagement.this);
        merchantTransactionPresenter.onAttach(this);
        merchantTransactionPresenter.fetchTransactions(viewType);
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);

        txtHeader.setText("Order Sales");
        txtTemp.setVisibility(View.GONE);
        txtSubHeader.setVisibility(View.GONE);

        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);

    }

    @Override
    public void onReceiveTransaction(ArrayList<Transaction> transactions) {
        if(transactions.size() == 0){
            containerNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }

        String finalDate = "";
        try {
            String date = transactions.get(0).getLive_stream_date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            Date newDate = format.parse(date);
            format = new SimpleDateFormat("dd.MM.yyyy");
            finalDate = format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        containerNoData.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        txtFilter.setText(transactions.get(0).getLive_stream_title() + "(" + finalDate + ", " + transactions.get(0).getLive_stream_start_time() + ")");
        parseTransactionData(transactions.get(0).getDetailed_transaction());

        TLActionSheet actionSheet = new TLActionSheet(TLSalesManagement.this);
        for (Transaction transaction : transactions) {
            actionSheet.addAction(transaction.getLive_stream_title() + "(" + transaction.getLive_stream_date() + ", " + transaction.getLive_stream_start_time() + ")");
        }

        actionSheet.setEventListener(new TLActionSheet.OnEventListener() {
            @Override
            public void onActionItemClick(TLActionSheet dialog, TLActionSheet.ActionItem item, int position) {
                txtFilter.setText(item.title);
                parseTransactionData(transactions.get(position).getDetailed_transaction());
            }

            @Override
            public void onCancelItemClick(TLActionSheet dialog) {

            }
        });

        txtFilter.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                actionSheet.setCancelVisible(false);
                actionSheet.setCanceledOnTouchOutside(true);
                actionSheet.show();
            }
        });
    }

    private void parseTransactionData(ArrayList<DetailedTransaction> detailedTransaction) {
        salesManagementAdapter = new SalesManagementAdapter(this, detailedTransaction, viewType);
        recyclerView.setAdapter(salesManagementAdapter);
        salesManagementAdapter.notifyDataSetChanged();
        salesManagementAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onResponse(String message) {
        
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onItemClicked(DetailedTransaction transaction) {
        if(transaction.getLogistic_status().equals("PAYMENT VERIFICATION")) {
            Intent intent = new Intent(TLSalesManagement.this, TLSalesApprovement.class);
            intent.putExtra("viewType", viewType);
            intent.putExtra("transaction", transaction);
            startActivity(intent);
        }else if(!transaction.getLogistic_status().equals("DELIVERED") && !transaction.getLogistic_status().equals("PAYMENT REJECTED")){
            Intent intent = new Intent(TLSalesManagement.this, TLSalesManagementDetail.class);
            intent.putExtra("viewType", viewType);
            intent.putExtra("transaction", transaction);
            startActivity(intent);
        }
    }
}
