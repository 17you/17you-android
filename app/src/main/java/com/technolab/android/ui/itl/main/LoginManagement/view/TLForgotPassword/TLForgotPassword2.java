package com.technolab.android.ui.itl.main.LoginManagement.view.TLForgotPassword;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.technolab.android.R;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInMvpView;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignInPresenter;
import com.technolab.android.ui.itl.main.LoginManagement.view.TLSignIn.TLMobileVerification;
import com.technolab.android.ui.itl.main.UserMainActivity;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.KeyboardUtils;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.OtpEditText;
import com.technolab.android.user.TLStorage;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class TLForgotPassword2 extends AbstractActivity{

    private Button btnVerify;
    private String txtPhoneNumber;
    private FirebaseAuth mAuth;
    private String verificationId;
    private OtpEditText otpEditText;
    private TextView txtResend;
    private ImageView btnBack;

    public TLForgotPassword2(){
        TAG = "Forgot Password";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_mobile_verification_2;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        txtPhoneNumber = getIntent().getStringExtra("phone");
        otpEditText = findViewById(R.id.et_otp);
        txtResend = findViewById(R.id.txt_resend_code);
        txtResend.setClickable(false);
        sendVerificationCode(txtPhoneNumber);

        ((TextView) findViewById(R.id.txt_phone_header)).setText(String.format("We sent it to number %s", txtPhoneNumber));
        findViewById(R.id.btn_back).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        btnVerify = findViewById(R.id.btnVerify);
        btnVerify.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String code = otpEditText.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    return;
                }
                verifyCode(code);
            }
        });

        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
    }

    private void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential) {
        showLoadingSpinner();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            hideLoadingSpinner();
                            finish();
                            Intent intent = new Intent(TLForgotPassword2.this, TLForgotPassword3.class);
                            intent.putExtra("phone", txtPhoneNumber);
                            startActivity(intent);
                        } else {
                            Toast.makeText(TLForgotPassword2.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            hideLoadingSpinner();
                        }
                    }
                });
    }

    private void sendVerificationCode(String number) {
        showLoadingSpinner();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                this,
                mCallBack
        );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            hideLoadingSpinner();
            verificationId = s;

            new CountDownTimer(60000, 1000) {

                public void onTick(long millisUntilFinished) {
                    long second = (millisUntilFinished / 1000) % 60;
                    long minutes = (millisUntilFinished/(1000*60)) % 60;
                    txtResend.setText("Resend code in " + minutes + ":" + second);
                }

                public void onFinish() {
                    txtResend.setText("Click here to resend code");
                    txtResend.setClickable(true);
                    txtResend.setOnClickListener(new OnSingleClickListener() {
                        @Override
                        public void onSingleClick(View v) {
                            sendVerificationCode(String.format("+6%s", txtPhoneNumber));
                        }
                    });
                }

            }.start();
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                otpEditText.setText(code);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(TLForgotPassword2.this, e.getMessage(), Toast.LENGTH_LONG).show();
            hideLoadingSpinner();
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        KeyboardUtils.hideSoftKeyboard(this);
    }

}
