package com.technolab.android.ui.entity;

import com.technolab.android.ui.entity.Product;

public class Cart {
    public Product product;
    public int quantity;
    public int selectedVariantIndex;
    public double price;

    public Cart(Product product, int quantity, int selectedVariantIndex, double price) {
        this.product = product;
        this.quantity = quantity;
        this.selectedVariantIndex = selectedVariantIndex;
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSelectedVariantIndex() {
        return selectedVariantIndex;
    }

    public void setSelectedVariantIndex(int selectedVariantIndex) {
        this.selectedVariantIndex = selectedVariantIndex;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
