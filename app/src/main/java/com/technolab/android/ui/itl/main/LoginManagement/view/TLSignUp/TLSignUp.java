package com.technolab.android.ui.itl.main.LoginManagement.view.TLSignUp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.hbb20.CountryCodePicker;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.entity.State;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignUpMvpView;
import com.technolab.android.ui.itl.main.LoginManagement.presenter.TLSignUpPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.KeyboardUtils;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.ValidateUtils;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLSignUp extends AbstractActivity implements TLSignUpMvpView {

    private EditText etUsername;
    private EditText etPassword;
    private EditText etEmail;
    private EditText etPhone;
    private Button btnSignUp;
    private ValidateUtils mValidator;
    private ImageButton btnBack;
    private CountryCodePicker ccp;

    @Inject
    TLSignUpPresenter mTLSignUpPresenter;

    public TLSignUp() {
        TAG = "Sign Up";
        statusBarColorID = R.color.colorO;
        layoutID = R.layout.activity_sign_up;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getAppComponent().inject(TLSignUp.this);
        mTLSignUpPresenter.onAttach(this);
        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        etEmail = findViewById(R.id.et_email);
        etPhone = findViewById(R.id.et_phone);
        btnSignUp = findViewById(R.id.btn_sign_up);
        ValidateUtils.showHidePassword(etPassword);
        btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
        ccp = (CountryCodePicker) findViewById(R.id.ccp);
        ccp.registerCarrierNumberEditText(etPhone);

        mValidator = new ValidateUtils(this);
        mValidator.addField(etUsername)
                .addField(etPassword)
                .addField(etEmail)
                .addField(etPhone);
        btnSignUp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mValidator.isValid()) {
                    mTLSignUpPresenter.checkMobileAndEmail(ccp.getFullNumberWithPlus(), etEmail.getText().toString());
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        KeyboardUtils.hideSoftKeyboard(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mValidator.onDestroy();
        mTLSignUpPresenter.onDetach();
    }

    @Override
    public void onRegistrationSuccess(String message) {
        Intent intent = new Intent(TLSignUp.this,TLState.class);
        intent.putExtra("username", etUsername.getText().toString());
        intent.putExtra("password", etPassword.getText().toString());
        intent.putExtra("phone", ccp.getFullNumberWithPlus());
        intent.putExtra("email", etEmail.getText().toString());
        startActivity(intent);
    }

    @Override
    public void onRegistrationFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReceiveIndustryInfo(ArrayList<Industry> industry) {

    }

    @Override
    public void onReceiveStates(ArrayList<State> stateArrayList) {

    }

    @Override
    public void onError(String message) {

    }
}
