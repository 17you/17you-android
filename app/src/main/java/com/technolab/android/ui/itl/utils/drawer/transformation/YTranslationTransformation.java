package com.technolab.android.ui.itl.utils.drawer.transformation;

import android.view.View;

import com.technolab.android.ui.itl.utils.drawer.utils.TLDrawerUtil;

public class YTranslationTransformation implements RootTransformation {

    private static final float START_TRANSLATION = 0f;

    private final float endTranslation;

    public YTranslationTransformation(float endTranslation) {
        this.endTranslation = endTranslation;
    }

    @Override
    public void transform(float dragProgress, View rootView) {
        float translation = TLDrawerUtil.evaluate(dragProgress, START_TRANSLATION, endTranslation);
        rootView.setTranslationY(translation);
    }
}