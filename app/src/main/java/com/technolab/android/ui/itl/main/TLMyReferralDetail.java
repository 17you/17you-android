package com.technolab.android.ui.itl.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.technolab.android.R;
import com.technolab.android.ui.entity.OverallReferralResults;
import com.technolab.android.ui.entity.ReferralEmailResult;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLLiveManagement;
import com.technolab.android.ui.itl.main.MerchantLiveManagement.view.TLMerchantLiveStreaming;
import com.technolab.android.ui.itl.main.presenter.TLMainMvpView;
import com.technolab.android.ui.itl.main.presenter.TLMainPresenter;
import com.technolab.android.ui.itl.spine.AbstractActivity;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;
import com.technolab.android.ui.itl.utils.TLActionSheet;

import java.util.ArrayList;

import javax.inject.Inject;

public class TLMyReferralDetail extends AbstractActivity implements TLMainMvpView.Referral{

    private RecyclerView recyclerView;
    private ReferralDetailAdapter referralDetailAdapter;
    private TextView txtSelector;

    @Inject
    TLMainPresenter.TLUserReferralPresenter userReferralPresenter;

    public TLMyReferralDetail(){
        TAG = "TLMyReferralDetail";
        layoutID = R.layout.activity_my_referral_details;
        statusBarColorID = R.color.colorD;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar(this);
        recyclerView = findViewById(R.id.recycler_view);
        txtSelector = findViewById(R.id.txt_selector);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAppComponent().inject(TLMyReferralDetail.this);
        userReferralPresenter.onAttach(this);

        if(getIntent().getStringExtra("value").equalsIgnoreCase("total")) {
            txtSelector.setText("Total");
            userReferralPresenter.retrieveEmailTotalReferral();
        }
        else if(getIntent().getStringExtra("value").equalsIgnoreCase("daily")) {
            txtSelector.setText("Daily");
            userReferralPresenter.retrieveEmailTodayReferral();
        }
        else if(getIntent().getStringExtra("value").equalsIgnoreCase("weekly")) {
            txtSelector.setText("Weekly");
            userReferralPresenter.retrieveEmailWeekReferral();
        }
        else if(getIntent().getStringExtra("value").equalsIgnoreCase("monthly")) {
            txtSelector.setText("Monthly");
            userReferralPresenter.retrieveEmailMonthReferral();
        }

        txtSelector.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                TLActionSheet actionSheet = new TLActionSheet(TLMyReferralDetail.this);
                actionSheet.addAction("All");
                actionSheet.addAction("Daily");
                actionSheet.addAction("Weekly");
                actionSheet.addAction("Monthly");

                actionSheet.setEventListener(new TLActionSheet.OnEventListener() {
                    @Override
                    public void onActionItemClick(TLActionSheet dialog, TLActionSheet.ActionItem item, int position) {
                        switch (position) {
                            case 0:
                                txtSelector.setText(item.title);
                                userReferralPresenter.retrieveEmailTotalReferral();
                                break;
                            case 1:
                                txtSelector.setText(item.title);
                                userReferralPresenter.retrieveEmailTodayReferral();
                                break;
                            case 2:
                                txtSelector.setText(item.title);
                                userReferralPresenter.retrieveEmailWeekReferral();
                                break;
                            case 3:
                                txtSelector.setText(item.title);
                                userReferralPresenter.retrieveEmailMonthReferral();
                                break;
                        }
                    }

                    @Override
                    public void onCancelItemClick(TLActionSheet dialog) {

                    }
                });
                actionSheet.show();
            }
        });
    }

    @Override
    protected void setupToolbar(AbstractActivity activity) {
        super.setupToolbar(activity);
        txtHeader.setText("Total Sign Up");
        txtSubHeader.setVisibility(View.GONE);
        imgRight1.setVisibility(View.GONE);
        imgRight2.setVisibility(View.GONE);
        imgLeft1.setVisibility(View.VISIBLE);
        imgLeft1.setImageDrawable(getResources().getDrawable(R.drawable.ic_back_plain));
        imgLeft1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onResponseReferral(OverallReferralResults overallReferralResults) {
        //Do nothing
    }

    @Override
    public void onResponseEmail(ArrayList<ReferralEmailResult> referralEmailResults) {
        if(referralEmailResults != null) {
            referralDetailAdapter = new ReferralDetailAdapter(this, referralEmailResults);
            recyclerView.setAdapter(referralDetailAdapter);
        }
    }

    @Override
    public void onError(String message) {

    }
}
