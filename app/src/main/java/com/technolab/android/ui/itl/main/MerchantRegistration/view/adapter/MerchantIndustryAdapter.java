package com.technolab.android.ui.itl.main.MerchantRegistration.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.Industry;
import com.technolab.android.ui.itl.utils.OnSingleClickListener;

import java.util.ArrayList;

public class MerchantIndustryAdapter extends RecyclerView.Adapter<MerchantIndustryAdapter.ViewHolder>{

    ArrayList<Industry> industryArrayList;
    Context context;
    private MerchantIndustryAdapter.ItemClickListener mCallback;

    public MerchantIndustryAdapter(Context context, ArrayList<Industry> industryArrayList){
        this.context = context;
        this.industryArrayList = industryArrayList;
    }

    @NonNull
    @Override
    public MerchantIndustryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_live_market, parent, false);
        return new MerchantIndustryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MerchantIndustryAdapter.ViewHolder holder, int position) {
        Industry industry = industryArrayList.get(position);
        holder.txtTitle.setText(industry.getIndustry_category_name());
        holder.contentView.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mCallback != null) {
                    mCallback.onItemClicked(industry);
                }
            }
        });

        Glide.with(context)
                .load(industry.getIndustry_category_image())
                .thumbnail(0.1f)
                .centerCrop()
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.imgCategory);
    }

    @Override
    public int getItemCount() {
        return industryArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle;
        LinearLayout contentView;
        ImageView imgCategory;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txt_title);
            contentView = itemView.findViewById(R.id.live_count_view);
            imgCategory = itemView.findViewById(R.id.img_category);
            imgCategory.setColorFilter(ContextCompat.getColor(context, R.color.tint), android.graphics.PorterDuff.Mode.MULTIPLY);
            imgCategory.setImageResource(R.drawable.placeholder);
        }
    }

    public void setOnItemClickListener(final MerchantIndustryAdapter.ItemClickListener listener) {
        mCallback = listener;
    }

    public interface ItemClickListener {
        void onItemClicked(Industry industry);
    }
}
