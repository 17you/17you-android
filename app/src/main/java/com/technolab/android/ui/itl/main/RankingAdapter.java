package com.technolab.android.ui.itl.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.technolab.android.R;
import com.technolab.android.ui.entity.RankingOthers;
import com.technolab.android.ui.entity.TopLiveStream;

import java.util.ArrayList;

public class RankingAdapter extends RecyclerView.Adapter<RankingAdapter.ViewHolder> {

    private ArrayList<RankingOthers> rankingOthers;
    private Context context;

    public RankingAdapter(Context context, ArrayList<RankingOthers> rankingOthers) {
        this.rankingOthers = rankingOthers;
        this.context = context;
    }

    @Override
    public RankingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_ranking, parent, false);
        return new RankingAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RankingAdapter.ViewHolder holder, int position) {
        RankingOthers ranking = rankingOthers.get(position);

        Glide.with(context)
                .load(ranking.default_ranking_image)
                .thumbnail(0.1f)
                .placeholder(context.getResources().getDrawable(R.drawable.placeholder))
                .into(holder.imgRanking);

        holder.txtUsername.setText(ranking.member_username);
        holder.txtRanking.setText(String.valueOf(position + 4));
        holder.txtTotalSignUp.setText(String.valueOf(ranking.getTotal_sign_up()));
    }

    @Override
    public int getItemCount() {
        return rankingOthers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtRanking;
        private TextView txtUsername;
        private TextView txtTotalSignUp;
        private ImageView imgRanking;

        public ViewHolder(View itemView) {
            super(itemView);
            imgRanking = itemView.findViewById(R.id.img_ranking);
            txtRanking = itemView.findViewById(R.id.txt_ranking);
            txtUsername = itemView.findViewById(R.id.txt_username);
            txtTotalSignUp = itemView.findViewById(R.id.txt_total_sign_up);
        }
    }
}